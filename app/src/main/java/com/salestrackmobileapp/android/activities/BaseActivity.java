package com.salestrackmobileapp.android.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.PendingOrderItem;
import com.salestrackmobileapp.android.gson.SaveOrder;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.gson.VisitedGoals;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kanchan on 3/15/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public LayoutInflater inflater;
    public PrefsHelper sharedPreference;
    private ProgressDialog mProgressDialog;

    String TAG = "BaseActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sharedPreference = new PrefsHelper(this);

    }

    public void showDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            if (!((Activity) this).isFinishing())
                mProgressDialog.show();
//                    Log.e("showDialog","true");

        } else {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.show();
        }
    }


    public void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
//            Log.e("hideDialog","true");
        }
    }


    public void sessionWorkingWithServer() {
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.checkSession(CommonUtils.getDeviceID(getApplicationContext()), sharedPreference.getStringValue(PrefsHelper.REGID), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                try {
                    JSONObject jsonObject = new JSONObject(arr);
                    if (jsonObject.getBoolean("Status")) {
                        Log.d("continue ", "session connected");
                    } else {

                        SharedPreferences preferences = getSharedPreferences("SalesTrack", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                   /* SugarContext.terminate();
                    SchemaGenerator schemaGenerator = new SchemaGenerator(getApplicationContext());
                    schemaGenerator.deleteTables(new SugarDb(getApplicationContext()).getDB());
                    SugarContext.init(getApplicationContext());
                    schemaGenerator.createDatabase(new SugarDb(getApplicationContext()).getDB());
                    finish();*/

                        if (Select.from(SaveOrder.class).first() != null) {
                            SaveOrder.deleteAll(SaveOrder.class);
                            if (Select.from(PendingOrderItem.class).first() != null) {
                                PendingOrderItem.deleteAll(PendingOrderItem.class);
                            }

                        }

                        if (Select.from(VisitedGoals.class).first() != null) {
                            VisitedGoals.deleteAll(VisitedGoals.class);
                        }
                        sharedPreference.saveStringData(PrefsHelper.ACCESS_TOKEN, "");

                        Toast.makeText(getApplicationContext(), "Your session has been expired.please login again...", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        finish();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS_ATTENDANCE = 1800 * 1000;//30 mins

    //private static final long UPDATE_INTERVAL_IN_MILLISECONDS_ATTENDANCE = 60 * 1000;//1 mins

    public final long SESSION_CHECK_TIMER = 30000;
    Handler handlerSession = new Handler();
    Runnable sessionCheckThread = new Runnable() {
        @Override
        public void run() {
            if (!sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN).equals("")) {
                Log.e("sessionCheckThread", "Running");
                if (!sharedPreference.getBooleanValue("UserLogin")) {
                    SecurityLogsApiStatus();

                    long oldTime = sharedPreference.getLongData("AttendanceTime");
                    Log.e(TAG, " 1 oldTime " + oldTime);

                    if (oldTime == 0)
                        sharedPreference.saveLongData("AttendanceTime", System.currentTimeMillis());

                    long currentTime = System.currentTimeMillis();

                    long diff = currentTime - oldTime;

                    Log.e(TAG, " oldTime " + oldTime);
                    Log.e(TAG, " currentTime " + currentTime);
                    Log.e(TAG, " diff " + diff);

                    if (diff >= UPDATE_INTERVAL_IN_MILLISECONDS_ATTENDANCE) {
                        Log.e(TAG, " start ");
                        if (!sharedPreference.getBooleanValue(PrefsHelper.IS_START_DAY))
                        {
                            SecurityLogsApi(); //dayStartApiCall();
                            if (Connectivity.isNetworkAvailable(BaseActivity.this))
                                sharedPreference.saveLongData("AttendanceTime", System.currentTimeMillis());
                        }
                    }

                    //dayStartApiCall();

                }
                handlerSession.postDelayed(sessionCheckThread, SESSION_CHECK_TIMER);
            } else {
                Log.e("sessionCheckThread", "Stopped");
            }
        }
    };


    public void dayStartApiCall() {
        //showDialog();
        Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.startAttendence(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String serverResponse = CommonUtils.getServerResponse(response);
                try {
                    JSONObject jsonObject = new JSONObject(serverResponse);
                    String message = jsonObject.getString("Message");
                    if (message.equals("Success")) {
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                //hideDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

                try {
                    String responseError = CommonUtils.getServerResponse(error.getResponse());
                    JSONObject jsonErrorObj = new JSONObject(responseError);
                    error.printStackTrace();
                    //  Toast.makeText(getApplicationContext(), jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //hideDialog();

            }
        });
//startAttendence
    }


    private void SecurityLogsApi() {
        if (Connectivity.isNetworkAvailable(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Attendance/SaveAttendanceStart");
                object.put("JsonObject", "null");
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(this));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);



                Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                        .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    dayStartApiCall();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void SecurityLogsApiStatus() {
        if (Connectivity.isNetworkAvailable(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/UserDevice/VerifyFCMRegID");
                object.put("JsonObject", "null");
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(this));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);
                Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                        .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    //dayStartApiCall();
                                    sessionWorkingWithServer();

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
