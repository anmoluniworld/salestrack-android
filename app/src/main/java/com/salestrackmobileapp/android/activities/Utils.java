/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.salestrackmobileapp.android.activities;


import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.SaveException;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.gson.UserInfoProfile;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Utils {

    static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     *
     * @param requestingLocationUpdates The location updates state.
     */
    static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

    public static void SaveExceptionApi(Context context, String message, String logtime, PrefsHelper sharedPreference) {
        try {

            String userProfileID = "", SalespersonId = "",companyCode="";
            List<UserInfoProfile> userInfoProfileList = UserInfoProfile.listAll(UserInfoProfile.class);
            if (userInfoProfileList != null) {
                SalespersonId = "" + userInfoProfileList.get(0).getSalespersonID();
                userProfileID = "" + userInfoProfileList.get(0).getUserProfileID();
                companyCode = "" + userInfoProfileList.get(0).getCompanyCode();
            }

            JSONObject object = new JSONObject();
            object.put("ExceptionMessage", message);
            object.put("ControllerName", "Dashboard");
            //object.put("ExceptionStarckTrace", "");
            object.put("logtime", logtime);
            object.put("SalespersonId", SalespersonId);
            //object.put("IsAdmin", "false");
            object.put("UserProfileId", userProfileID);
            object.put("CompanyCode", companyCode);

            //object.put("ModuleName", "Dashboard");


            Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

            SaveException securityLog = builder.fromJson(object.toString(), SaveException.class);
            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.SaveException(securityLog, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String arr = CommonUtils.getServerResponse(response);
                    Log.e("SaveExceptionApi", " success SecurityLogsApi arr " + arr);

                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Log.e("SaveExceptionApi", " failure retrofitError arr " + retrofitError.getMessage());

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
