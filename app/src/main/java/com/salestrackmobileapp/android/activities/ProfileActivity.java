package com.salestrackmobileapp.android.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.custome_views.CircleTransform;
import com.salestrackmobileapp.android.custome_views.Custome_EditText;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.UserInfoChange;
import com.salestrackmobileapp.android.gson.UserInfoProfile;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProfileActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback {

    @BindView(R.id.action_bar_title)
    TextView actionBarTitle;
    @BindView(R.id.firstname_tv)
    Custome_TextView userTitleTV;
    @BindView(R.id.email_tv)
    Custome_TextView emailTV;
    @BindView(R.id.phone_tv)
    Custome_TextView phoneTV;
    @BindView(R.id.address_tv)
    Custome_TextView addressTV;
    @BindView(R.id.address_2_tv)
    Custome_TextView address2TV;
    @BindView(R.id.city_tv)
    Custome_TextView cityTV;
    @BindView(R.id.state_tv)
    Custome_TextView stateTV;
    @BindView(R.id.save_change_btn)
    Button saveChangeBtn;
    @BindView(R.id.pick_image_img)
    ImageView pickImage;

    private static int RESULT_LOAD_IMAGE = 1;

    @BindView(R.id.firstname_et)
    Custome_EditText firstNameET;
    @BindView(R.id.lastname_et)
    Custome_EditText lastNameET;
    @BindView(R.id.email_et)
    Custome_EditText emailET;
    @BindView(R.id.phone_et)
    Custome_EditText phoneET;
    @BindView(R.id.address_et)
    Custome_EditText addressET;
    @BindView(R.id.address_2_et)
    Custome_EditText address2ET;
    @BindView(R.id.city_et)
    Custome_EditText cityET;
    @BindView(R.id.state_et)
    Custome_EditText stateET;
    @BindView(R.id.setting_dots_img)
    ImageView threeDotsImg;


    UserInfoChange userChangeFeilds;
    ImageView imageView;

    Bitmap selectedImage;
    //String encodedImage = "";

    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    IntentFilter filter;
    boolean isProfile = false;

    static int clickSaveBtn = 0;
    private ProgressDialog mProgressDialog;

    private static final int RC_GALLERY_PERM = 102;
    private static final int RC_CAMERA_PERM = 101;

    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath = "";

    Bitmap myBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        if (!sharedPreference.getBooleanValue("UserLogin"))
            handlerSession.postDelayed(sessionCheckThread, SESSION_CHECK_TIMER);
        imageView = (ImageView) findViewById(R.id.imgProfilePicture);
        ButterKnife.bind(this);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "Avenir-Next-LT-Pro_5196.ttf");
        actionBarTitle.setText("PROFILE");
        actionBarTitle.setTypeface(custom_font);

        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
        if (Connectivity.isNetworkAvailable(ProfileActivity.this)) {
            if (!isUserProfile) {
                getUserInfo();
            }
        } else {

            UserInfoProfile userInfoProfile = Select.from(UserInfoProfile.class).first();
            if (userInfoProfile.getFirstName() != null) {
                firstNameET.setText(userInfoProfile.getFirstName());
            } else {
                firstNameET.setText(" ");
            }

            if (userInfoProfile.getLastName() != null) {
                lastNameET.setText(userInfoProfile.getLastName());
            } else {
                lastNameET.setText(" ");
            }

            if (userInfoProfile.getEmailID() != null) {
                emailET.setText(userInfoProfile.getEmailID() + " ");
                emailET.setFocusable(false);
            }
            if (userInfoProfile.getMobilePhoneNumber() != null) {
                phoneET.setText(userInfoProfile.getMobilePhoneNumber() + " ");
            }
            if (userInfoProfile.getAddress1() != null) {
                addressET.setText(userInfoProfile.getAddress1() + " ");
            } else {
                addressET.setText(userInfoProfile.getAddress1() + " ");
            }

            if (userInfoProfile.getAddress2() != null) {
                address2ET.setText(userInfoProfile.getAddress2() + " ");
            }

            if (userInfoProfile.getCity() != null) {
                cityET.setText(userInfoProfile.getCity() + " ");
            }

            if (userInfoProfile.getState() != null) {
                stateET.setText(userInfoProfile.getState() + " ");
            }


            if (userInfoProfile.getImageUrl().equals("")) {
                imageView.setImageResource(R.drawable.user_pic);
            } else {
                Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).transform(new CircleTransform()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);
            }
        }


    }

    @OnClick(R.id.clicked)
    public void clicked()
    {
        if(getCurrentFocus()!=null && getCurrentFocus().getWindowToken()!=null)
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @OnClick(R.id.pick_image_img)
    public void pickImageFromMedia() {
        startDialog();
    }

    private void startDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_image);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        TextView camera = dialog.findViewById(R.id.camera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });
        TextView gallery = dialog.findViewById(R.id.gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageSingle();
                dialog.dismiss();
            }
        });

        TextView cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permission),
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
            myBitmap = BitmapFactory.decodeFile(pickerPath);
            imageView.setImageBitmap(myBitmap);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permission),
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else pickerPath = image.getOriginalPath();

            myBitmap = BitmapFactory.decodeFile(pickerPath);


            Uri uri = Uri.fromFile(new File(pickerPath));

            Picasso.with(getApplicationContext()).load(uri).transform(new CircleTransform()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);

        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
                //myBitmap = BitmapFactory.decodeFile(pickerPath);
                //imageView.setImageBitmap(selectedImage);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
                //pickerPath = cameraPicker.pickImage();
                //myBitmap = BitmapFactory.decodeFile(pickerPath);
                //imageView.setImageBitmap(selectedImage);
            }
        }
    }


    @OnClick(R.id.setting_dots_img)
    public void showMenu() {
        final PopupMenu menu = new PopupMenu(getApplicationContext(), threeDotsImg);
        menu.inflate(R.menu.profile_menu);

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                hideDialog();
                if (menu.getMenu().getItem(1).getItemId() == R.id.logout_menu) {
                    Toast.makeText(getApplicationContext(), "logout is clicked", Toast.LENGTH_SHORT).show();
                    PrefsHelper.clearPreference(getApplicationContext());
                    Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                    startActivity(intent);
                    finish();
                } else if (menu.getMenu().getItem(0).getItemId() == R.id.edit_profile) {
                    Toast.makeText(getApplicationContext(), "change password is clicked", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        menu.show();
    }

    @OnClick(R.id.save_change_btn)
    public void saveChanges() {

        if (Connectivity.isConnected(this)) {
            if (validation())
                saveDetailApiCall();

        } else {
            /* clickSaveBtn = 1;*/
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            registerReceiver(receiver, filter);
        }
    }

    private boolean validation() {
        if (firstNameET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide firstname", Toast.LENGTH_SHORT).show();
            return false;
        } else if (lastNameET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide lastname", Toast.LENGTH_SHORT).show();
            return false;
        } else if (emailET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (phoneET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (phoneET.getText().toString().trim().length() < 10) {
            Toast.makeText(this, "Phone number should not be less then 10", Toast.LENGTH_SHORT).show();
            return false;
        } else if (addressET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (address2ET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide address 2", Toast.LENGTH_SHORT).show();
            return false;
        } else if (cityET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide city", Toast.LENGTH_SHORT).show();
            return false;
        } else if (stateET.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please provide state", Toast.LENGTH_SHORT).show();
            return false;
        } else
            return true;
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                selectedImage = BitmapFactory.decodeStream(imageStream);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);
                imageView.setImageBitmap(selectedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }

        } else if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                selectedImage = BitmapFactory.decodeStream(imageStream);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);
                imageView.setImageBitmap(selectedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }

        }
        else {
            Toast.makeText(getApplicationContext(), "You haven't picked Image", Toast.LENGTH_SHORT).show();
        }
    }*/

    @OnClick(R.id.home_icon_img)
    public void homeIconClick() {
        hideDialog();
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        finish();
    }

    boolean isUserProfile = false;

    private void getUserInfo() {
        isUserProfile = true;
        mProgressDialog = new ProgressDialog(ProfileActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.show();
        UserInfoProfile.deleteAll(UserInfoProfile.class);
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getUserInfo(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);

                try {

                    UserInfoProfile userInfoProfile = builder.fromJson(arr, UserInfoProfile.class);
                    userInfoProfile.save();


                    if (userInfoProfile.getFirstName() != null) {
                        firstNameET.setText(userInfoProfile.getFirstName());
                    } else {
                        firstNameET.setText(" ");
                    }

                    if (userInfoProfile.getLastName() != null) {
                        lastNameET.setText(userInfoProfile.getLastName());
                    } else {
                        lastNameET.setText(" ");
                    }

                    if (userInfoProfile.getEmailID() != null) {
                        emailET.setText(userInfoProfile.getEmailID() + " ");
                        emailET.setFocusable(false);

                    }
                    if (userInfoProfile.getMobilePhoneNumber() != null) {
                        phoneET.setText(userInfoProfile.getMobilePhoneNumber() + " ");
                    }
                    if (userInfoProfile.getAddress2() != null) {
                        addressET.setText(userInfoProfile.getAddress1() + " ");
                    } else {
                        addressET.setText(userInfoProfile.getAddress1() + " ");
                    }

                    if (userInfoProfile.getAddress2() != null) {
                        address2ET.setText(userInfoProfile.getAddress2() + " ");
                    }

                    if (userInfoProfile.getCity() != null) {
                        cityET.setText(userInfoProfile.getCity() + " ");
                    }

                    if (userInfoProfile.getState() != null) {
                        stateET.setText(userInfoProfile.getState() + " ");
                    }


                    if (userInfoProfile.getImageUrl().equals("")) {
                        imageView.setImageResource(R.drawable.user_pic);
                    } else {
                        Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).transform(new CircleTransform()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);
                    }
                    hideDialog();

                } catch (Exception ex) {
                    hideDialog();
                    ex.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                hideDialog();
            }
        });
    }
//changeInfo

    private String convertBase64(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private void saveDetailApiCall() {
        showDialog();
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        try {

            final UserInfoProfile userInfoPro = Select.from(UserInfoProfile.class).first();
            if (userInfoPro != null) {
                final String zip = userInfoPro.getZip();
                final String country = userInfoPro.getCountry();
                final Integer userId = userInfoPro.getUserProfileID();
                String base64 = "";
                JSONObject object = new JSONObject();
                object.put("UserProfileID", userInfoPro.getUserProfileID());
                object.put("FirstName", firstNameET.getText().toString().trim());
                object.put("LastName", lastNameET.getText().toString().trim());
                object.put("EmailID", emailET.getText().toString().trim());
                object.put("Address1", addressET.getText().toString().trim() + "");
                object.put("Address2", address2ET.getText().toString().trim() + "");
                object.put("City", cityET.getText().toString().trim());
                object.put("State", stateET.getText().toString().trim());
                object.put("Zip", userInfoPro.getZip());
                object.put("Country", userInfoPro.getCountry());
                object.put("MobileNo", phoneET.getText().toString().trim());

                if (pickerPath.isEmpty())
                    object.put("Image", pickerPath);
                else object.put("Image", convertBase64(myBitmap));

                /*if (!encodedImage.isEmpty())
                    object.put("Image", encodedImage);
                else {
                    InputStream in = null;
                    int responseCode = -1;
                    try {

                        URL url = new URL(userInfoPro.getImageUrl());//"http://192.xx.xx.xx/mypath/img1.jpg
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        con.setDoInput(true);
                        con.connect();
                        responseCode = con.getResponseCode();
                        if (responseCode == HttpURLConnection.HTTP_OK) {
                            //download
                            in = con.getInputStream();
                            selectedImage = BitmapFactory.decodeStream(in);
                            in.close();
                        }

                    } catch (Exception ex) {
                        Log.e("Exception", ex.toString());
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);
                    object.put("Image", encodedImage);
                }*/
                @SuppressLint("RestrictedApi") final AlertDialog.Builder builderdialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
                Log.e("JSON", "::::::" + object.toString());

                userChangeFeilds = builder.fromJson(object.toString(), UserInfoChange.class);

                final String finalBase6 = base64;
                serviceHandler.changeInfo(userChangeFeilds, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String serverResponse = CommonUtils.getServerResponse(response);
                        try {
                           /* UserInfoProfile userInfoProfile = new UserInfoProfile();
                            userInfoProfile.setAddress1(addressET.getText().toString().trim() + "");
                            userInfoProfile.setAddress2(address2ET.getText().toString().trim() + "");
                            userInfoProfile.setCity(cityET.getText().toString().trim());
                            userInfoProfile.setFirstName(firstNameET.getText().toString().trim());
                            userInfoProfile.setLastName(lastNameET.getText().toString().trim());
                            userInfoProfile.setEmailID(emailET.getText().toString());
                            userInfoProfile.setState(stateET.getText().toString().trim());
                            userInfoProfile.setZip(zip);
                            userInfoProfile.setCountry(country);
                            userInfoProfile.setMobilePhoneNumber(phoneET.getText().toString().trim());
                            userInfoProfile.setUserProfileID(userId);
                            if (!encodedImage.isEmpty()) {
                                userInfoProfile.setImageUrl(encodedImage);
                            } else {
                                userInfoProfile.setImageUrl(finalBase6);
                            }*/


                            JSONObject jsonObject = new JSONObject(serverResponse);
                            Log.e("message", "::::" + jsonObject.getString("Message"));
                            if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                hideDialog();

                                builderdialog.setMessage("Profile is updated  successfully!. ")
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                /*Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                                                intent.addCategory(Intent.CATEGORY_HOME);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
                                                finish();*/
                                            }
                                        });
                                AlertDialog alert = builderdialog.create();
                                alert.show();

                            } else {
                                Toast.makeText(getBaseContext(), "Unsuccessful! Please check your feilds", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Toast.makeText(getBaseContext(), "Unsuccessful! Please check your feilds", Toast.LENGTH_SHORT).show();
                        }
                        hideDialog();
                    }

                    @Override
                    public void failure(RetrofitError error) {
//                        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
//                        startActivity(intent);
//                        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
//                        // error.printStackTrace();
                        String responseError = CommonUtils.getServerResponse(error.getResponse());
                        try {
                            JSONObject jsonErrorObj = new JSONObject(responseError);
                            error.printStackTrace();
                            hideDialog();
                            Toast.makeText(getApplicationContext(), jsonErrorObj.getString("Message") + "", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideDialog();
                        }
                        hideDialog();

                    }
                });

            } else {
                hideDialog();

                Toast.makeText(this, "User detail not found", Toast.LENGTH_SHORT).show();

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            hideDialog();
            Toast.makeText(getApplicationContext(), "please check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.v("Goals getting", "Receieved notification about network status");
            if (Connectivity.isNetworkAvailable(context)) {
                if (!isConnected) {
                    Log.v("is network Available", "Now you are connected to Internet!");
                    isConnected = true;
                    sharedPreference.saveBooleanValue("IsProfile", true);
                    //Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                    if (!isUserProfile) {
                        getUserInfo();
                    }
                             /*   if(clickSaveBtn==1) {
                                    saveDetailApiCall();
                                }*/
                    unregisterReceiver(receiver);
                } else {

                    Log.d("Internet not Connected", "??????");
                }
            } else {
                Log.v("not connected", "You are not connected to Internet!");
                UserInfoProfile userInfoProfile = Select.from(UserInfoProfile.class).first();
                if (userInfoProfile.getFirstName() != null) {
                    firstNameET.setText(userInfoProfile.getFirstName());
                } else {
                    firstNameET.setText(" ");
                }

                if (userInfoProfile.getLastName() != null) {
                    lastNameET.setText(userInfoProfile.getLastName());
                } else {
                    lastNameET.setText(" ");
                }

                if (userInfoProfile.getEmailID() != null) {
                    emailET.setText(userInfoProfile.getEmailID() + " ");
                }
                if (userInfoProfile.getMobilePhoneNumber() != null) {
                    phoneET.setText(userInfoProfile.getMobilePhoneNumber() + " ");
                }
                if (userInfoProfile.getAddress2() != null) {
                    addressET.setText(userInfoProfile.getAddress1() + " ");
                } else {
                    addressET.setText(userInfoProfile.getAddress1() + " ");
                }

                if (userInfoProfile.getImageUrl().equals("")) {
                    imageView.setImageResource(R.drawable.user_pic);
                } else {
                    Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).transform(new CircleTransform()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);
                }

                isConnected = false;
            }

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v("is network Available", "Now you are connected to Internet!");
                                isConnected = true;
                                sharedPreference.saveBooleanValue("IsProfile", true);
                                //Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                                if (!isUserProfile) {
                                    getUserInfo();
                                }
                             /*   if(clickSaveBtn==1) {
                                    saveDetailApiCall();
                                }*/
                                unregisterReceiver(receiver);
                            } else {

                                Log.d("Internet not Connected", "??????");
                            }
                            return true;
                        }
                    }
                }
            } else {

            }


            Log.v("not connected", "You are not connected to Internet!");
            UserInfoProfile userInfoProfile = Select.from(UserInfoProfile.class).first();
            if (userInfoProfile.getFirstName() != null) {
                firstNameET.setText(userInfoProfile.getFirstName());
            } else {
                firstNameET.setText(" ");
            }

            if (userInfoProfile.getLastName() != null) {
                lastNameET.setText(userInfoProfile.getLastName());
            } else {
                lastNameET.setText(" ");
            }

            if (userInfoProfile.getEmailID() != null) {
                emailET.setText(userInfoProfile.getEmailID() + " ");
            }
            if (userInfoProfile.getMobilePhoneNumber() != null) {
                phoneET.setText(userInfoProfile.getMobilePhoneNumber() + " ");
            }
            if (userInfoProfile.getAddress2() != null) {
                addressET.setText(userInfoProfile.getAddress1() + " ");
            } else {
                addressET.setText(userInfoProfile.getAddress1() + " ");
            }

            if (userInfoProfile.getImageUrl().equals("")) {
                imageView.setImageResource(R.drawable.user_pic);
            } else {

                Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).transform(new CircleTransform()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);
            }
            //  Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
            isConnected = false;
            return false;
        }
    }

    public void showDialog() {

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();


        }
    }

    public void hideDialog() {


        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        finish();
    }
}
