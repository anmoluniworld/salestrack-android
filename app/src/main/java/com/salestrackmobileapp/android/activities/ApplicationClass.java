package com.salestrackmobileapp.android.activities;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;

import androidx.multidex.MultiDex;

import com.orm.SugarApp;
import com.orm.SugarContext;
import com.orm.query.Select;
import com.salestrackmobileapp.android.gson.UserLoginTime;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.PrefsHelper;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.fabric.sdk.android.Fabric.TAG;

/**
 * Created by kanchan on 3/21/2017.
 */

public class ApplicationClass extends SugarApp {
    private static CountDownTimer countDownTimer;
    public static Button timerButton;
    public static long millisup;
    public PrefsHelper sharedPreference;
    private static Context context;

    String businessName;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;

    public String getGoalDate() {
        return goalDate;
    }

    public void setGoalDate(String goalDate) {
        this.goalDate = goalDate;
    }

    String goalDate;

    public String getGoalId() {
        return goalId;
    }

    public void setGoalId(String goalId) {
        this.goalId = goalId;
    }

    String goalId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    String businessId;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    String areaId;


    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    String dealId;


    public boolean isFromDeal() {
        return fromDeal;
    }

    public void setFromDeal(boolean fromDeal) {
        this.fromDeal = fromDeal;
    }

    boolean fromDeal = false;


    private static ApplicationClass ourInstance;

    public static ApplicationClass getInstance() {

        if (ourInstance == null)
            ourInstance = new ApplicationClass();

        return ourInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        sharedPreference = new PrefsHelper(this);
    }

    /*  private static ApplicationClass enableMultiDex;
        public static Context context;

        public ApplicationClass(){
            enableMultiDex=this;
        }

        public static ApplicationClass getEnableMultiDexApp() {
            return enableMultiDex;
        }

        @Override
        public void onCreate() {
            super.onCreate();
            context = getApplicationContext();

        }*/
    public static void setTimer(long milli, String stringValue) {

        millisup = milli;

        if (countDownTimer == null) {
            Calendar cal = Calendar.getInstance();
//            TimeZone tz = TimeZone.getTimeZone("GMT");
//            cal.setTimeZone(tz);
            // int minute = cal.get(Calendar.MINUTE);
            int Totalminute = 24 * 60;
            Log.e("TOTAl_MINUTE", "::::" + Totalminute);
            int currentminute = cal.get(Calendar.MINUTE);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            Log.e("hour", ":::::" + hour);

            Log.e("currentminute", ":::::" + currentminute);
            int currentMinuteOfDay = ((hour * 60) + currentminute);
            Log.e("currentMinuteOfDay", ":::::" + currentMinuteOfDay);

            int minute = Totalminute - currentMinuteOfDay;
            Log.e("minute", ":::::" + minute);

            // String getMinutes = minute.getText().toString();//Get minutes from edittexf
            //Check validation over edittext
            if (minute > 0) {
                int noOfMinutes = minute * 60 * 1000;//Convert minutes into milliseconds
                Log.e(TAG, "values : " + String.valueOf(noOfMinutes));
                startTimer(noOfMinutes, stringValue);//start countdown
                //  startTimer.setText(getString(R.string.stop_timer));//Change Text
//                Log.e("TImER_text",":::::::"+)

            } else {
                //Else stop timer and change text
                stopCountdown();
                // startTimer.setText(getString(R.string.start_timer));
            }
        }

    }

    public static void stopCountdown() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    //Start Countodwn method
    private static void startTimer(final int noOfMinutes, final String stringValue) {

        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                //long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                //Log.d("seconds elapsed: ", "" + (noOfMinutes - millis));

//                millisup=(noOfMinutes - millis);
                millisup = millisup + 1000;

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisup), TimeUnit.MILLISECONDS.toMinutes(millisup) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisup)), TimeUnit.MILLISECONDS.toSeconds(millisup) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisup)));
                // countdownTimerText.setText(hms);//set text
//                Log.e("(((","::START_TIMER:::"+hms);
                if (timerButton != null) {
//                    Log.e("(((","timerButton:not null");

                    timerButton.setText("       " + hms);
                }
                CommonUtils.lastMilliofActivity = millisup;
//                Log.e("lastMilliofActivity",":::::"+CommonUtils.lastMilliofActivity);
                List<UserLoginTime> userLogged = Select.from(UserLoginTime.class).list();
                if (userLogged.size() > 0) {
                    for (int i = 0; i < userLogged.size(); i++) {
                        if (userLogged.get(i).getUserEmail().equals(stringValue)) {
                            UserLoginTime update = UserLoginTime.findById(UserLoginTime.class, i + 1);
                            update.setTime(ApplicationClass.millisup);
                            update.save();
                            break;
                        }
                    }
                }

            }

            public void onFinish() {

                countDownTimer = null;//set CountDownTimer to null
                Log.e("ON_FINISH", ":::::TIME'S UP!!!");

            }
        }.start();


    }


    public void onCreate() {
        super.onCreate();
        ourInstance = this;
        context = getApplicationContext();
        SugarContext.init(this);

    }

    public static Context getAppContext() {
        return context;
    }


}
