package com.salestrackmobileapp.android.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.custome_views.Custome_EditText;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AddBusiness;
import com.salestrackmobileapp.android.gson.AllBusiness;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.gson.StateCitiesModal;
import com.salestrackmobileapp.android.gson.UserInfoProfile;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddBusinessActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback, OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    String TAG = "AddBusinessActivity";


    @BindView(R.id.businessName)
    Custome_EditText businessName;
    /*  @BindView(R.id.businessType)
      Spinner businessType;*/
    @BindView(R.id.address1)
    Custome_EditText address1;
    @BindView(R.id.address2)
    Custome_EditText address2;
    @BindView(R.id.city)
    Custome_EditText city;
    @BindView(R.id.gstn)
    Custome_EditText gstn;
    @BindView(R.id.state)
    Custome_EditText state;
    @BindView(R.id.zipcode)
    Custome_EditText zipcode;
    @BindView(R.id.website)
    Custome_EditText website;
    @BindView(R.id.contact_person_name)
    Custome_EditText contact_person_name;
    @BindView(R.id.contact_person_email)
    Custome_EditText contact_person_email;
    @BindView(R.id.contact_person_phone)
    Custome_EditText contact_person_phone;
    @BindView(R.id.iv_add_business)
    ImageView iv_add_business;
    @BindView(R.id.relativeMap)
    RelativeLayout relativeMap;

    @BindView(R.id.linearDetectLocation)
    LinearLayout linearDetectLocation;
    @BindView(R.id.clickDetectLocation)
    LinearLayout clickDetectLocation;
    /*@BindView(R.id.relativeMap)
     relativeMap;*/

    @BindView(R.id.linearEdit)
    LinearLayout linearEdit;
    @BindView(R.id.add_business_btn)
    Button add_business_btn;

    @BindView(R.id.action_bar_title)
    Custome_TextView action_bar_title;

    @BindView(R.id.editBtn)
    ImageView editBtn;

    @BindView(R.id.hintBusinessType)
    Custome_TextView hintBusinessType;

    @BindView(R.id.spinnerState)
    AppCompatSpinner spinnerState;

    @BindView(R.id.spinnerCity)
    AppCompatSpinner spinnerCity;

    String selectedState = "", selectedCity = "";

    String addressSelectedState = "", addressSelectedCity = "";

    //AllBusiness allBusiness;

    boolean check = false;

    private Context context;
    private ProgressDialog mProgressDialog;
    //AllBusiness addBusiness;
    ServiceHandler serviceHandler;
    private static final int RC_GALLERY_PERM = 102;
    private static final int RC_CAMERA_PERM = 101;

    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String pickerPath = "", businessSelected = "Business Type";

    Bitmap myBitmap;

    private MapView mapView;
    private GoogleMap mMap;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    ArrayList<String> mState;
    ArrayList<String> mCitie;
    ArrayList<StateCitiesModal> stateCitiesModals;
    ArrayAdapter adapterState, adapterCity;
    int businessId, userProfileID;
    Gson builder;

    @OnClick({R.id.add_business_btn, R.id.clickDetectLocation, R.id.backPressed, R.id.save_btn, R.id.cancel_btn})
    public void addBusinessClicked(View view) {
        switch (view.getId()) {
            case R.id.add_business_btn:
                if (validation()) {
                    addBusinessApi();
                }
                break;
            case R.id.clickDetectLocation:
                relativeMap.setVisibility(View.VISIBLE);
                getAddressFromLatLng();
                break;
            case R.id.backPressed:
                onBackPressed();
                break;
            case R.id.save_btn:
                addBusinessApi();
                break;
            case R.id.cancel_btn:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void getAddressFromLatLng() {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Singleton.getInstance().SLAT, Singleton.getInstance().SLNG, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() > 0) {
                Log.e(TAG, " getAddressFromLatLng addresses " +addresses);

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String cty = addresses.get(0).getLocality();
                String subLocality = addresses.get(0).getSubLocality();
                String ste = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

               /* String address_1 = "";
                if (addresses.get(0).getThoroughfare() != null)
                    address_1 = addresses.get(0).getThoroughfare() + " ";
                if (addresses.get(0).getSubThoroughfare() != null)
                    address_1 = address_1 + addresses.get(0).getSubThoroughfare();
*/
                address1.setText(address);
                address2.setText(subLocality);
                city.setText(cty);
                state.setText(ste);
                zipcode.setText(postalCode);

                addressSelectedState = ste;
                addressSelectedCity = cty;
                SetStateAndCityFromLatLng();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SecurityLogsApi(JSONObject jsonObj) {
        if (Connectivity.isNetworkAvailable(this)) {
            AddBusiness addBusiness = builder.fromJson(jsonObj.toString(), AddBusiness.class);

            if (Connectivity.isNetworkAvailable(this)) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Loading...");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);

                serviceHandler.addBusiness(addBusiness, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String serverResponse = CommonUtils.getServerResponse(response);
                        Log.e("serverResponse", ":::" + serverResponse);
                        try {
                            JSONObject jsonObject = new JSONObject(serverResponse);
                            if (jsonObject.has("Message")) {
                                String Message = jsonObject.getString("Message");
                                if (getIntent().getStringExtra("function").equals("addBusiness"))
                                    Toast.makeText(AddBusinessActivity.this, "This Business has been added successfully.", Toast.LENGTH_LONG).show();
                                else if (getIntent().getStringExtra("function").equals("edit"))
                                    Toast.makeText(AddBusinessActivity.this, "This Business has been edited successfully.", Toast.LENGTH_LONG).show();
                                sharedPreference.saveBooleanValue("refresh", true);
                                onBackPressed();
                            }
                            hideDialog();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            hideDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(serverResponse);
                                Toast.makeText(AddBusinessActivity.this, jsonObject.getString("Message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideDialog();
                        try {
                            String responseError = CommonUtils.getServerResponse(error.getResponse());
                            JSONObject jsonErrorObj = new JSONObject(responseError);
                            Toast.makeText(AddBusinessActivity.this, jsonErrorObj.getString("Message"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                addBusiness.save();
                sharedPreference.saveBooleanValue("refresh", true);

                Toast.makeText(AddBusinessActivity.this, "Your offline Business has been saved successfully", Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }
    }

    private void addBusinessApi() {

        try {
            JSONObject object = new JSONObject();
            if (getIntent().getStringExtra("function").equals("addBusiness")) {
                object.put("BusinessID", /*"kunal@sonyindia.com"*/0);
                object.put("CreatedBy", userProfileID);

            } else if (getIntent().getStringExtra("function").equals("edit"))
                object.put("BusinessID", /*"kunal@sonyindia.com"*/getIntent().getIntExtra("BusinessID", 0));
            object.put("BusnessName", /*"Pass@123"*/businessName.getText().toString().trim());
          /*  if(regId.equals(null) || regId.isEmpty())
            {
                object.put("DeviceId", deviceId*//*regId*//*);
            }else
            {
                object.put("DeviceId", regId*//*regId*//*);
            }*/
            object.put("Address1", address1.getText().toString().trim());
            object.put("Address2", address2.getText().toString().trim());
            object.put("City", selectedCity);
            object.put("State", selectedState);
            object.put("ZipCode", zipcode.getText().toString().trim());
            object.put("Country", "India");
            object.put("WebsiteName", website.getText().toString().trim());
            object.put("ContactPersonName", contact_person_name.getText().toString().trim());
            object.put("ContactPersonPhone", contact_person_phone.getText().toString().trim());
            object.put("ContactPersonEmail", contact_person_email.getText().toString().trim());
            object.put("BusinessType", businessSelected);
            object.put("GSTN_Number", gstn.getText().toString().trim());


            if (getIntent().getStringExtra("function").equals("addBusiness")) {
                object.put("CheckedIn", 1);
                object.put("CityID", 0);
                object.put("StateId", 0);
                object.put("Latitude", String.valueOf(Singleton.getInstance().SLAT));
                object.put("Longitude", String.valueOf(Singleton.getInstance().SLNG));
            }

            if (pickerPath.isEmpty())
                object.put("ImageName", pickerPath);
            else object.put("ImageName", convertBase64(myBitmap));


            try {

                JSONObject object1 = new JSONObject();
                object1.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object1.put("CreatedDate", CommonUtils.CurrentDateTime());
                object1.put("ApiName", "/Business/SaveBusiness");
                object1.put("JsonObject", object.toString());
                object1.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object1.put("IpAddress", CommonUtils.getDeviceID(this));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                AddBusiness addBusiness = builder.fromJson(object.toString(), AddBusiness.class);
                if (Connectivity.isNetworkAvailable(this)) {
                    serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);

                    SecurityLog securityLog = builder.fromJson(object1.toString(), SecurityLog.class);
                    ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                    serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {
                            String arr = CommonUtils.getServerResponse(response);
                            Log.e(TAG, " success SecurityLogsApi arr " + arr);
                            try {
                                JSONObject jsonObject = new JSONObject(arr);
                                if (jsonObject != null && jsonObject.has("Message")) {
                                    if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                        SecurityLogsApi(object);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                        }
                    });
                } else {
                    addBusiness.save();
                    sharedPreference.saveBooleanValue("refresh", true);

                    Toast.makeText(AddBusinessActivity.this, "Your offline Business has been saved successfully", Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, "", Toast.LENGTH_LONG).show();
            hideDialog();
        }

    }


    private String convertBase64(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private boolean validation() {
        if (businessName.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter business name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (address1.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter address1", Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (address2.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter address2", Toast.LENGTH_SHORT).show();
            return false;
        }*/ else if (selectedState.isEmpty() && selectedState.equalsIgnoreCase("Select state")) {
            Toast.makeText(this, "Please enter state", Toast.LENGTH_SHORT).show();
            return false;
        } else if (selectedCity.isEmpty() && selectedCity.equalsIgnoreCase("Select city")) {
            Toast.makeText(this, "Please enter city", Toast.LENGTH_SHORT).show();
            return false;
        } else if (zipcode.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter zip code", Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (website.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter website", Toast.LENGTH_SHORT).show();
            return false;
        } */else if (contact_person_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter contact person name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (contact_person_phone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter contact person phone", Toast.LENGTH_SHORT).show();
            return false;
        } else if (contact_person_email.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter contact person email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (businessSelected.equals("Business Type")) {
            Toast.makeText(this, "Please select business type", Toast.LENGTH_SHORT).show();
            return false;
        } else
            return true;
        /*else if (gstn.getText().toString().trim().length() > 0)
        {
            if (gstn.getText().toString().trim().length() != 15) {
                Toast.makeText(this, "Invalid gstn number", Toast.LENGTH_SHORT).show();
                return false;
            } else return true;

        }*/
    }

    @OnClick(R.id.linearSpinner)
    void clicked() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_business_type);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);


        Custome_TextView tvDealer = dialog.findViewById(R.id.tvDealer);
        tvDealer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSelected = "Dealer";
                hintBusinessType.setText("Dealer");
                dialog.dismiss();
            }
        });
        Custome_TextView tvRetailer = dialog.findViewById(R.id.tvRetailer);
        tvRetailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSelected = "Retailer";
                hintBusinessType.setText("Retailer");
                dialog.dismiss();
            }
        });
        Custome_TextView tvWholesaler = dialog.findViewById(R.id.tvWholesaler);
        tvWholesaler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSelected = "Wholesaler";
                hintBusinessType.setText("Wholesaler");
                dialog.dismiss();
            }
        });
        Custome_TextView tvDistributor = dialog.findViewById(R.id.tvDistributor);
        tvDistributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSelected = "Distributor";
                hintBusinessType.setText("Distributor");
                dialog.dismiss();
            }
        });
        Custome_TextView tvPartner = dialog.findViewById(R.id.tvPartner);
        tvPartner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSelected = "Partner";
                hintBusinessType.setText("Partner");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_business);
        if (sharedPreference.getBooleanValue("UserLogin"))
            handlerSession.postDelayed(sessionCheckThread, 10000);
        ButterKnife.bind(this);
        sharedPreference.saveBooleanValue("refresh", false);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

        List<UserInfoProfile> userInfoProfileList = UserInfoProfile.listAll(UserInfoProfile.class);
        if (userInfoProfileList != null)
            userProfileID=userInfoProfileList.get(0).getUserProfileID();

                    mState = new ArrayList<>();
        mCitie = new ArrayList<>();
        mCitie.add("Select City");
        stateCitiesModals = new ArrayList<>();
        contact_person_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!s.toString().isEmpty() && s.toString().length() == 10) {
                    if (contact_person_email.getText().toString().trim().isEmpty()) {
                        contact_person_email.setText(s.toString() + "@salestrackmobile.com");
                    }
                }
            }
        });
        try {
            BufferedReader br = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                br = new BufferedReader(new InputStreamReader(getAssets().open("cities_state.csv"), StandardCharsets.UTF_8));
            }
            String line;

            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(",");
                String s0 = tokens[0];
                String s1 = tokens[1];

                StateCitiesModal stateCitiesModal = new StateCitiesModal();
                stateCitiesModal.setCity(s0);
                stateCitiesModal.setState(s1);
                stateCitiesModals.add(stateCitiesModal);

                mState.add(s1);
                // mCitie.add(s0);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(mState);
        mState.clear();
        mState.addAll(hashSet);
        Collections.sort(mState, String.CASE_INSENSITIVE_ORDER);

        ArrayList<String> list = new ArrayList<>();
        list.addAll(mState);
        mState.clear();
        mState.add("Select state");
        mState.addAll(list);


        adapterState = new ArrayAdapter(this, R.layout.spinner_item, mState);
        adapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerState.setAdapter(adapterState);

        adapterCity = new ArrayAdapter(this, R.layout.spinner_item, mCitie);
        adapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerCity.setAdapter(adapterCity);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    mCitie.clear();
                    mCitie.add("Select city");
                    selectedState = parent.getItemAtPosition(position).toString();
                    for (StateCitiesModal stateCitiesModal : stateCitiesModals) {
                        if (stateCitiesModal.getState().equals(selectedState)) {
                            mCitie.add(stateCitiesModal.getCity());
                        }
                    }
                    Collections.sort(mCitie, String.CASE_INSENSITIVE_ORDER);
                    //spinnerState.setAdapter(adapterState);
                    ArrayList<String> array = new ArrayList<>();
                    array.addAll(mCitie);
                    mCitie.clear();
                    mCitie.add("Select city");
                    mCitie.addAll(array);
                    adapterCity.notifyDataSetChanged();
                    spinnerCity.setSelection(0);
                    selectedCity = "";
                } else {
                    mCitie.clear();
                    mCitie.add("Select city");
                    adapterCity.notifyDataSetChanged();
                    spinnerCity.setSelection(0);
                    selectedState = "";
                }
                Log.e(TAG, "spinnerState position " + position);
                Log.e(TAG, "spinnerState selectedState " + selectedState);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedCity = parent.getItemAtPosition(position).toString();
                } else {
                    selectedCity = "";
                }

                Log.e(TAG, "spinnerCity position " + position);
                Log.e(TAG, "spinnerCity selectedCity " + selectedCity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("function").equals("addBusiness")) {
                linearDetectLocation.setVisibility(View.VISIBLE);
                linearEdit.setVisibility(View.GONE);
                add_business_btn.setVisibility(View.VISIBLE);
                action_bar_title.setText("Add Business");
                editBtn.setVisibility(View.GONE);
            } else if (getIntent().getStringExtra("function").equals("edit")) {
                action_bar_title.setText("Edit Business");
                linearDetectLocation.setVisibility(View.GONE);
                linearEdit.setVisibility(View.VISIBLE);
                add_business_btn.setVisibility(View.GONE);
                editBtn.setVisibility(View.VISIBLE);
                setBusinessDetails();
            }
        }


    }


    private void setBusinessDetails() {
        businessId = getIntent().getIntExtra("BusinessID", 0);
        AllBusiness allBusiness = Select.from(AllBusiness.class).where(Condition.prop("business_id").eq(businessId)).first();

        //AllBusiness usertable= AllBusiness.findById(AllBusiness.class,businessId);


        if (!allBusiness.getBusnessName().isEmpty())
            businessName.setText(allBusiness.getBusnessName());
        if (!allBusiness.getAddress1().isEmpty())
            address1.setText(allBusiness.getAddress1());
        if (!allBusiness.getAddress2().isEmpty())
            address2.setText(allBusiness.getAddress2());
        if (!allBusiness.getCity().isEmpty())
            city.setText(allBusiness.getCity());
        if (!allBusiness.getState().isEmpty())
            state.setText(allBusiness.getState());
        if (!allBusiness.getZipCode().isEmpty())
            zipcode.setText(allBusiness.getZipCode());
        if (!allBusiness.getWebsiteName().isEmpty())
            website.setText(allBusiness.getWebsiteName());
        if (!allBusiness.getContactPersonName().isEmpty())
            contact_person_name.setText(allBusiness.getContactPersonName());
        if (!allBusiness.getContactPersonPhone().isEmpty())
            contact_person_phone.setText(allBusiness.getContactPersonPhone());
        if (!allBusiness.getContactPersonEmail().isEmpty())
            contact_person_email.setText(allBusiness.getContactPersonEmail());
        /*if (allBusiness.getBusinesstype().equalsIgnoreCase("Dealer"))
            //businessType.setSelection(0);
            hintBusinessType.setText(allBusiness.getBusinesstype());
        else if (allBusiness.getBusinesstype().equalsIgnoreCase("Retailor"))
            businessType.setSelection(1);
        else if (allBusiness.getBusinesstype().equalsIgnoreCase("Wholesaler"))
            businessType.setSelection(2);
        else if (allBusiness.getBusinesstype().equalsIgnoreCase("Distributor"))
            businessType.setSelection(3);
        else if (allBusiness.getBusinesstype().equalsIgnoreCase("Partner"))
            businessType.setSelection(4);*/
        hintBusinessType.setText(allBusiness.getBusinesstype());
        businessSelected = allBusiness.getBusinesstype();

        Picasso.with(this).load(allBusiness.getImageName()).error(R.drawable.add_image).into(iv_add_business);

        if (!allBusiness.getState().isEmpty()) {
            spinnerState.setSelection(adapterState.getPosition(allBusiness.getState()));

            for (StateCitiesModal stateCitiesModal : stateCitiesModals) {
                if (stateCitiesModal.getState().equals(allBusiness.getState())) {
                    mCitie.add(stateCitiesModal.getCity());
                }
            }
            Collections.sort(mCitie, String.CASE_INSENSITIVE_ORDER);
            //spinnerState.setAdapter(adapterState);
            ArrayList<String> array = new ArrayList<>();
            array.addAll(mCitie);
            mCitie.clear();
            mCitie.add("Select city");
            mCitie.addAll(array);
            adapterCity.notifyDataSetChanged();
        }
        if (!allBusiness.getCity().isEmpty()) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Do something after 100ms
                    if (mCitie != null) {
                        for (int i = 0; i < mCitie.size(); i++) {
                            if (mCitie.get(i).trim().equalsIgnoreCase(allBusiness.getCity().trim())) {
                                spinnerCity.setSelection(i);
                            }
                        }
                    }
                }
            }, 1000);
        }
    }

    public void onBackPressed() {
        Intent intent = new Intent(this, GoalsActivities.class);
        intent.putExtra("nameActivity", "AllBusinessDashBoard");

        if (getIntent().getStringExtra("function").equals("addBusiness"))
            intent.putExtra("addBusiness", true);
        else
            intent.putExtra("addBusiness", false);

        startActivity(intent);
    }

    public void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();

        }
    }

    @OnClick({R.id.iv_add_business, R.id.editBtn})
    public void addBusinessImage() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_image);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        TextView camera = dialog.findViewById(R.id.camera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });
        TextView gallery = dialog.findViewById(R.id.gallery);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageSingle();
                dialog.dismiss();
            }
        });

        TextView cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permission),
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
            myBitmap = BitmapFactory.decodeFile(pickerPath);
            iv_add_business.setImageBitmap(myBitmap);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permission),
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else pickerPath = image.getOriginalPath();

            myBitmap = BitmapFactory.decodeFile(pickerPath);
            iv_add_business.setImageBitmap(myBitmap);
        }
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(Singleton.getInstance().SLAT, Singleton.getInstance().SLNG);
        mMap.addMarker(new MarkerOptions().position(sydney).title(""));
        float zoomLevel = 16.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
    }

    void SetStateAndCityFromLatLng() {
        if (addressSelectedState != null && !addressSelectedState.isEmpty()) {
            if (mState != null)
                for (int i = 0; i < mState.size(); i++) {
                    if (mState.get(i).equalsIgnoreCase(addressSelectedState)) {
                        spinnerState.setSelection(i);
                    }
                }
        }


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

                if (addressSelectedCity != null && !addressSelectedCity.isEmpty()) {
                    if (mCitie != null) {
                        for (int i = 0; i < mCitie.size(); i++) {
                            if (mCitie.get(i).equalsIgnoreCase(addressSelectedCity)) {
                                spinnerCity.setSelection(i);
                            }
                        }
                    }
                }
            }
        }, 2000);

    }
}
