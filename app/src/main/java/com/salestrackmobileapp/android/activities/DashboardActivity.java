package com.salestrackmobileapp.android.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.adapter.MyGoalsAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.DashboardArray;
import com.salestrackmobileapp.android.gson.GoalBusiness;
import com.salestrackmobileapp.android.gson.GoalsAccDate;
import com.salestrackmobileapp.android.gson.LatLng;
import com.salestrackmobileapp.android.gson.PendingOrderItem;
import com.salestrackmobileapp.android.gson.SaveOrder;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.gson.UserInfoProfile;
import com.salestrackmobileapp.android.gson.UserLoginTime;
import com.salestrackmobileapp.android.gson.VisitedGoals;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.AlertDialogUtils;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Config;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.CountUpTimer;
import com.salestrackmobileapp.android.utils.LocationUtils;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.droidsonroids.gif.GifImageView;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.salestrackmobileapp.android.activities.ForgetPasswordActivity.mypreference;

public class DashboardActivity extends BaseActivity implements RecyclerClick, NavigationView.OnNavigationItemSelectedListener {

    public static int LOCATION_UPDATE_REQUEST = 11;

    String TAG = "DashboardActivity";

    @BindView(R.id.setting_dots_img)
    ImageView threeDotsImg;

    @BindView(R.id.mygoals_ll)
    LinearLayout myGoalsLL;
    @BindView(R.id.myorder_ll)
    LinearLayout myOrdersLL;
    @BindView(R.id.product_ll)
    LinearLayout productLL;
    @BindView(R.id.business_ll)
    LinearLayout businessLL;
    @BindView(R.id.chat_ll)
    LinearLayout chatLL;
    @BindView(R.id.deals_ll)
    LinearLayout dealsLL;


    @BindView(R.id.my_goals_tv)
    TextView myGoalsTV;
    @BindView(R.id.my_order_tv)
    TextView myOrdersTV;
    @BindView(R.id.my_product_tv)
    TextView productTV;
    @BindView(R.id.my_business_tv)
    TextView businessTV;
    @BindView(R.id.my_chat_tv)
    TextView chatTV;
    @BindView(R.id.my_deals_tv)
    TextView dealsTV;

    @BindView(R.id.my_goals_img)
    ImageView myGoalsImg;
    @BindView(R.id.my_order_img)
    ImageView myOrdersImg;
    @BindView(R.id.product_img)
    ImageView productImg;
    @BindView(R.id.buisness_img)
    ImageView businessImg;
    @BindView(R.id.chat_img)
    ImageView chatImg;
    @BindView(R.id.deals_img)
    ImageView dealsImg;
    @BindView(R.id.username_name_tv)
    Custome_BoldTextView userNameTv;
    @BindView(R.id.daily_goal_rv)
    RecyclerView dailyGoalsRv;
    @BindView(R.id.todays_goal_tv)
    Custome_TextView todaysGoalTitle;
    @BindView(R.id.acheived_target_txt)
    Custome_TextView acheivedTargetTxt;
    @BindView(R.id.monthly_target_txt)
    Custome_TextView monthlyTargetTxt;
    @BindView(R.id.ic_internet)
    public ImageView ivInternet;

    @BindView(R.id.ic_no_internet)
    GifImageView icNoInternet;

    @BindView(R.id.linearBusiness)
    LinearLayout linearBusiness;
    @BindView(R.id.linearChat)
    LinearLayout linearChat;
    @BindView(R.id.linearMyGoal)
    LinearLayout linearMyGoal;
    @BindView(R.id.linearProduct)
    LinearLayout linearProduct;
    @BindView(R.id.chat_linear)
    LinearLayout chat_linear;
    @BindView(R.id.business_linear)
    LinearLayout business_linear;


    //@BindView(R.id.profile_img)
    //ImageView profileImg;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;
    @BindView(R.id.start_day)
    Button startDayBtn;
    @BindView(R.id.stop_day)
    Button stopDayBtn;
    @BindView(R.id.target)
    LinearLayout target;

    @BindView(R.id.textViewBuildVersion)
    TextView textViewBuildVersion;

    public static boolean backToCheckIn = false;

    static String userName = "";
    String dateSt;
    Gson builder;
    List<GoalsAccDate> listAllGoals;
    List<GoalBusiness> goalBusinessList;
    ServiceHandler serviceHandler;
    private MyGoalsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    //static int loadDeal = 0;
    private static Dialog mProgressDialog;
    static CircleImageView imageView;
    public TextView notificationNumberTV;
    public RelativeLayout relative_notification;
    boolean isDashboard;


    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    IntentFilter filter;
    static int i = 0;
    Custome_BoldTextView nav_user;
    CircleImageView prImg;
    long stopTime = 0;
    boolean isChronometerRunning = false;
    private static CountDownTimer countDownTimer;

    private CountUpTimer timer;
    String date = "";
    int minute = 0;
    long lastLogout = 0;
    SharedPreferences preferences, userPref;
    boolean isLogoutCalled = false;
    NavigationView navigationView;
    Handler handler;
    long aTime = 0;
    String beforeGoingToBackground = "", afterComingFromBackground = "", extraTime = "";
    int delay = 1000; //milliseconds

    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            //Do Something
            setTimerService();
            handler.postDelayed(myRunnable, delay);
        }
    };
    private static final String[] LOCATION_AND_CONTACTS =
            {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigational_drawer_dashboard);

        sharedPreference.saveIntData(PrefsHelper.APP_VERSION, BuildConfig.VERSION_CODE);

        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String date1 = df1.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

        Utils.SaveExceptionApi(DashboardActivity.this, "Application Active", date1, sharedPreference);


        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        EasyPermissions.requestPermissions(
                this,
                "This app needs access to your Location access for tracking",
                1,
                LOCATION_AND_CONTACTS);

        if (!sharedPreference.getBooleanValue("UserLogin"))
            handlerSession.postDelayed(sessionCheckThread, SESSION_CHECK_TIMER);

        ButterKnife.bind(this);
        preferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        userPref = getSharedPreferences(PrefsHelper.USER_LOGIN_TIME, Context.MODE_PRIVATE);
        Log.e("@@@", "::::onCreate");

        //AllDeals.deleteAll(AllDeals.class);
        //SugarRecord.delete(AllDeals.class);
        if (getIntent() != null)
            System.out.println("fcm notification" + getIntent().getStringExtra("fcm_notification"));

        if (getIntent().hasExtra("fcm_notification")) {
            if (getIntent().getStringExtra("fcm_notification").equalsIgnoreCase("Y")) {
                if (getIntent().hasExtra("message")) {
                    String message = getIntent().getStringExtra("message");
                    Log.e(TAG, " message " + message);

                    if (message != null && !message.isEmpty() && message.equalsIgnoreCase("New Daily goal has been created for you")) {
                        Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
                        intent.putExtra("nameActivity", "MyGoals");
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    } else {
                        Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
                    startActivity(intent);
                }

                System.out.println("inside if");
            } else System.out.println("else part");
        } else {
            System.out.println("getextra");
        }

        String userID = sharedPreference.getStringValue(PrefsHelper.USER_EMAIL);
        String lastUserId = preferences.getString("LastEmailID", "");
        Log.e("###", "::userID" + userID);
        Log.e("###", "::::" + lastUserId);


        if (userID.equals(lastUserId)) {
            Log.e("if case", "::::true");

        } else {
            Log.e("else case", "::::true");
            SharedPreferences preferencesClear = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
            editorTwo = preferencesClear.edit();
            editorTwo.clear();
            editorTwo.commit();
            try {
                List<ProductInCart> productInCart = ProductInCart.listAll(ProductInCart.class);
                if (productInCart != null) {
                    ProductInCart.deleteAll(ProductInCart.class);
                }

                List<SaveOrder> saveOrderList = SaveOrder.listAll(SaveOrder.class);
                Log.e(TAG, " saveOrderList " + saveOrderList);
                if (saveOrderList != null)
                    Log.e(TAG, " saveOrderList size " + saveOrderList.size());


            } catch (Exception e) {
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

       /* if (BuildConfig.DEBUG) {

        } else {
            textViewBuildVersion.setVisibility(View.GONE);
        }*/

        textViewBuildVersion.setText("Build Version : " + BuildConfig.VERSION_NAME);
        textViewBuildVersion.setVisibility(View.VISIBLE);

        if (Connectivity.isNetworkAvailable(getApplicationContext())) {
            //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_offline));
            icNoInternet.setVisibility(View.GONE);
            ivInternet.setVisibility(View.VISIBLE);
        } else {
            //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_online));
            icNoInternet.setVisibility(View.VISIBLE);
            ivInternet.setVisibility(View.GONE);
        }


        View hView = navigationView.getHeaderView(0);
        nav_user = (Custome_BoldTextView) hView.findViewById(R.id.username_name);
        prImg = (CircleImageView) hView.findViewById(R.id.imgProfilePicture);


        notificationNumberTV = (TextView) findViewById(R.id.no_notification);
        relative_notification = (RelativeLayout) findViewById(R.id.relative_notification);
        imageView = (CircleImageView) findViewById(R.id.imgProfilePicture);

        /*need to uncomment again*/
        //sessionWorkingWithServer();

        filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();


        if (sharedPreference.getStringValue(PrefsHelper.USERNAME) != null) {
            nav_user.setText("Welcome " + sharedPreference.getStringValue(PrefsHelper.USERNAME));
        }

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "Avenir-Next-LT-Pro_5196.ttf");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        dateSt = df.format(Calendar.getInstance().getTime());
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        dailyGoalsRv.setLayoutManager(mLayoutManager);
        dailyGoalsRv.setNestedScrollingEnabled(false);
        myGoalsTV.setTypeface(custom_font);
        myOrdersTV.setTypeface(custom_font);
        productTV.setTypeface(custom_font);
        businessTV.setTypeface(custom_font);
        chatTV.setTypeface(custom_font);
        dealsTV.setTypeface(custom_font);


        if (!sharedPreference.getBooleanValue("UserLogin")) {
            startDayBtn.setVisibility(View.GONE);
            stopDayBtn.setVisibility(View.VISIBLE);
        } else {
            startDayBtn.setVisibility(View.VISIBLE);
            stopDayBtn.setVisibility(View.GONE);
        }

        if (!userName.equals("")) {
            userNameTv.setText("Welcome " + userName);
        }
        serviceHandlerApiInit();
        /* getDailyGoals();*/
        if (sharedPreference.getBooleanValue("UserLogin")) {
            linearBusiness.setVisibility(View.VISIBLE);
            linearChat.setVisibility(View.GONE);
            linearMyGoal.setVisibility(View.GONE);
            linearProduct.setVisibility(View.GONE);
            target.setVisibility(View.GONE);
            stopDayBtn.setVisibility(View.GONE);
            startDayBtn.setVisibility(View.GONE);
            hideItem();
        } else {
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.reports).setVisible(false);
            linearBusiness.setVisibility(View.GONE);
            linearChat.setVisibility(View.VISIBLE);
            linearMyGoal.setVisibility(View.VISIBLE);
            linearProduct.setVisibility(View.VISIBLE);
        }

        getUserInfo();

        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = getString(R.string.msg_subscribed);
                        if (!task.isSuccessful()) {
                            msg = getString(R.string.msg_subscribe_failed);
                        }
                        // Utility.debug(TAG, msg);
                        //if(BuildConfig.DEBUG)
                        //  Toast.makeText(DashboardActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        if (getIntent() != null && getIntent().hasExtra("fromLogin") && getIntent().getBooleanExtra("fromLogin", false))
            getOrderList();
    }


    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.reports).setVisible(false);
        nav_Menu.findItem(R.id.mycart).setVisible(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.reports) {
            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "orderhistory");
            intent.putExtra("isfrom", "dashboard");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
            startActivity(intent);

        } else if (id == R.id.profile) {
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.mycart) {
            if (ProductInCart.listAll(ProductInCart.class).size() != 0) {
                Intent intent = new Intent(DashboardActivity.this, GoalsActivities.class);
                intent.putExtra("Slider_cart", true);
                intent.putExtra("nameActivity", "");

                startActivity(intent);
            } else {
                Toast.makeText(DashboardActivity.this, "No Item found in Cart", Toast.LENGTH_SHORT).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void serviceHandlerApiInit() {
        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
    }


    @OnClick(R.id.start_day)
    public void startDay() {
        if (!sharedPreference.getBooleanValue("UserLogin")) {
            List<UserLoginTime> userLoginTime = Select.from(UserLoginTime.class).list();
            if (userLoginTime != null) {
                for (int i = 0; i < userLoginTime.size(); i++) {
                    UserLoginTime update = userLoginTime.get(i);
                    update.setTime(System.currentTimeMillis());
                    update.save();
                }
            }
            handler.postDelayed(myRunnable, delay);
            dayStartApiCall();
        }
    }


    @OnClick(R.id.stop_day)
    public void stopDay() {
        Log.e("stop.millisup", "::::" + ApplicationClass.millisup);
        editor = preferences.edit();
        editor.putLong("LastLogOut", ApplicationClass.millisup);
        editor.commit();
        if (!sharedPreference.getBooleanValue("UserLogin")) {
            handler.removeCallbacks(myRunnable);
            ApplicationClass.stopCountdown();
            List<UserLoginTime> userLoginTime = Select.from(UserLoginTime.class).list();
            if (userLoginTime != null) {
                for (int i = 0; i < userLoginTime.size(); i++) {
                    UserLoginTime update = userLoginTime.get(i);
                    Log.e(TAG, " stopDay update " + update);
                    long diff = System.currentTimeMillis() - update.getTime();
                    Log.e(TAG, " stopDay diff " + diff);
                    Log.e(TAG, " stopDay TIMER_TIME " + sharedPreference.getLongData(PrefsHelper.TIMER_TIME));
                    long temp = diff + sharedPreference.getLongData(PrefsHelper.TIMER_TIME);
                    Log.e(TAG, " stopDay temp " + temp);
                    sharedPreference.saveLongData(PrefsHelper.TIMER_TIME, temp);
                    update.setTime(System.currentTimeMillis());
                    update.save();
                }
            }
            dayStopApiCall();
        }


    }

    public void dayStartApiCall() {
        //showDialog();
        stopDayBtn.setVisibility(View.VISIBLE);
        startDayBtn.setVisibility(View.GONE);
        Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.startAttendence(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String serverResponse = CommonUtils.getServerResponse(response);
                try {
                    Log.e(TAG, "dayStartApiCall serverResponse :: " + serverResponse);

                    serverResponse = serverResponse.trim();
                    JSONObject jsonObject = new JSONObject(serverResponse);
                    String message = jsonObject.getString("Message");
                    if (message.equalsIgnoreCase("Success")) {
                        sharedPreference.saveBooleanData(PrefsHelper.START_DAY, true);
                        LocationUpdatesService.STARTDAY = true;
                        stopDayBtn.setVisibility(View.VISIBLE);
                        startDayBtn.setVisibility(View.GONE);
                        sharedPreference.saveBooleanData(PrefsHelper.IS_START_DAY, false);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

                try {
                    String responseError = CommonUtils.getServerResponse(error.getResponse());
                    JSONObject jsonErrorObj = new JSONObject(responseError);
                    error.printStackTrace();
                    //  Toast.makeText(getApplicationContext(), jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //hideDialog();

            }
        });
//startAttendence
    }


    @OnClick(R.id.notification_img)
    public void callNotificationPage() {
        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
        startActivity(intent);
    }

    public void dayStopApiCall() {
        startDayBtn.setVisibility(View.VISIBLE);
        startDayBtn.setText(stopDayBtn.getText().toString());
        stopDayBtn.setVisibility(View.GONE);
        //showDialog();
        Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.stopAttendence(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String serverResponse = CommonUtils.getServerResponse(response);
                try {
                    Log.e(TAG, "dayStopApiCall serverResponse :: " + serverResponse);

                    serverResponse = serverResponse.trim();

                    JSONObject jsonObject = new JSONObject(serverResponse);
                    Log.e("OBJECT", ":::" + jsonObject.toString());
                    String message = jsonObject.getString("Message");
                    Log.e(TAG, "dayStopApiCall Message:: " + message);
                    if (message.equals("Success")) {
                        sharedPreference.saveBooleanData(PrefsHelper.START_DAY, false);
                        sharedPreference.saveBooleanData(PrefsHelper.IS_START_DAY, true);

                        LocationUpdatesService.STARTDAY = false;
                        if (!sharedPreference.getBooleanValue("UserLogin")) {
                            startDayBtn.setVisibility(View.VISIBLE);
                            startDayBtn.setText(stopDayBtn.getText().toString());
                        }
                        stopDayBtn.setVisibility(View.GONE);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                //hideDialog();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

                try {
                    String responseError = CommonUtils.getServerResponse(error.getResponse());
                    JSONObject jsonErrorObj = new JSONObject(responseError);
                    error.printStackTrace();
//                    Toast.makeText(getApplicationContext(), jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //hideDialog();
            }
        });
//startAttendence
    }


    public void fetchLocationAndResult() {

        if (!LocationUtils.checkLocationEnable(this)) {

            AlertDialogUtils.getInstance().singleButtonDialog(this, getString(R.string.discount_voucher_enable_location_enter_manually), "enable location", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(intent, LOCATION_UPDATE_REQUEST);

                }
            });

        } else {

            LocationUtils locationUtils = new LocationUtils();

            locationUtils.getCurrentLocation(this, new LocationUtils.LocationListener() {
                @Override
                public void onLocationProvided(Location arg0) {
                    onLocationUpdate(arg0);

                }
            });
        }
    }

    public void onLocationUpdate(Location location) {

        if (location == null) {
            //  Toast.makeText(DashboardActivity.this, R.string.landing_unable_fetch_location, Toast.LENGTH_SHORT).show();
        } else if (!Connectivity.isConnected(this)) {
            // Toast.makeText(DashboardActivity.this, R.string.landing_unable_fetch_location, Toast.LENGTH_SHORT).show();
        } else {
            Singleton.getInstance().SLAT = location.getLatitude();
            Singleton.getInstance().SLNG = location.getLongitude();
            //  Toast.makeText(DashboardActivity.this, "" + Singleton.getInstance().SLAT + "" + Singleton.getInstance().SLNG, Toast.LENGTH_SHORT).show();
            //  AlertDialogUtils.closeAlertDialog(this);
        }
    }


    public void getTimerForCallService() {
        CommonUtils.showProgress(this);
        Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

            final JSONObject jsonObj = new JSONObject();
            jsonObj.put("Latitude", String.valueOf(Singleton.getInstance().SLAT));
            jsonObj.put("Longitude", String.valueOf(Singleton.getInstance().SLNG));
            jsonObj.put("TrackDate", date + "Z");
            jsonObj.put("SalesPersonName", userName);
            jsonObj.put("Acuracy", "0");
            jsonObj.put("Altitude", "0");
            jsonObj.put("Velocity", "0");
            jsonObj.put("Bearing", "0");
            LatLng latLongObject = builder.fromJson(jsonObj.toString(), LatLng.class);
            Log.e(TAG, " Location_params " + jsonObj.toString());
            //Longitude

            final ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.trackLatLong(latLongObject, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String serverResponse = CommonUtils.getServerResponse(response);
                    Log.e(TAG, " Location serverResponse " + serverResponse);

                    try {
                        JSONObject jsonObject = new JSONObject(serverResponse);
                        if (jsonObject.has("Message")) {
                            Log.e("Message", ":::::" + jsonObject.getString("Message"));
                        }

                        Intent intentSer = new Intent(getApplicationContext(), LocationUpdatesService.class);
                        intentSer.putExtra("ACCESS_TOKEN", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                        sharedPreference.saveStringData(PrefsHelper.SALESPERSONNAME, userName);
                        intentSer.putExtra("USERNAME", userName);
                        startService(intentSer);


                    } catch (Exception ex) {
                        ex.printStackTrace();
                        try {
                            JSONObject jsonObject = new JSONObject(serverResponse);
                            //Toast.makeText(DashboardActivity.this, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println(error);
                    try {
                        String responseError = CommonUtils.getServerResponse(error.getResponse());
                        JSONObject jsonErrorObj = new JSONObject(responseError);
                        //Toast.makeText(DashboardActivity.this, jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            CommonUtils.dismissProgress();
        } catch (Exception ex) {
            ex.printStackTrace();
            CommonUtils.dismissProgress();
        }
    }


    private void getUserInfo() {
        if (Connectivity.isNetworkAvailable(DashboardActivity.this)) {

            List<UserInfoProfile> userInfoProfileList = UserInfoProfile.listAll(UserInfoProfile.class);
            if (userInfoProfileList != null) {
                UserInfoProfile.deleteAll(UserInfoProfile.class);
            }
            final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.getUserInfo(new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String arr = CommonUtils.getServerResponse(response);
                    Log.e("USER:::::", "::::::" + arr);
                    try {
                        UserInfoProfile userInfoProfile = builder.fromJson(arr, UserInfoProfile.class);
                        userName = userInfoProfile.getFirstName();
                        nav_user.setText("Welcome " + userName);
                        sharedPreference.saveStringData(PrefsHelper.USER_ID, String.valueOf(userInfoProfile.getUserProfileID()));
                        sharedPreference.saveStringData(PrefsHelper.SALES_PERSON_ID, String.valueOf(userInfoProfile.getSalespersonID()));
                        sharedPreference.saveStringData(PrefsHelper.COMPANY_ID, String.valueOf(userInfoProfile.getCompanyID()));
                        sharedPreference.saveStringData(PrefsHelper.USERNAME, userName);
                        userInfoProfile.save();
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        if (userInfoProfile.getImageUrl().equals("")) {
                            prImg.setImageResource(R.drawable.user_pic);
                        } else {
                            new DownloadImage().execute(userInfoProfile.getImageUrl());
                            //Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(prImg);
                        }
                        SecurityLogsApiLocation();
                        if (sharedPreference.getBooleanValue(PrefsHelper.CALL_APP_INSTALL_API))
                        {
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                            String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z
                            Utils.SaveExceptionApi(DashboardActivity.this, "App Install Version: " + BuildConfig.VERSION_NAME, date, sharedPreference);
                            sharedPreference.saveBooleanData(PrefsHelper.CALL_APP_INSTALL_API,false);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    //  CommonUtils.showErrorMessage("Please check your internet connectivity", coordinateLayout);
                    //  Toast.makeText(getApplicationContext(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
                    //  finish();
                }
            });
        }
    }

    private void SecurityLogsApiLocation() {
        if (Connectivity.isNetworkAvailable(this)) {
            try {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

                final JSONObject jsonObj = new JSONObject();
                jsonObj.put("Latitude", String.valueOf(Singleton.getInstance().SLAT));
                jsonObj.put("Longitude", String.valueOf(Singleton.getInstance().SLNG));
                jsonObj.put("TrackDate", date + "Z");
                jsonObj.put("SalesPersonName", userName);

                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Track/SaveTrack");
                object.put("JsonObject", jsonObj.toString());
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(this));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);
                Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                        .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApiLocation arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    getTimerForCallService();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onResume() {
        handler = new Handler();


        super.onResume();
        // timerUp();
        Log.e("@@@", "::::On resume true");
        /*if (sharedPreference.getBooleanValue(PrefsHelper.START_DAY))
        {
            startDayBtn.setVisibility(View.GONE);
            stopDayBtn.setVisibility(View.VISIBLE);
            handler.postDelayed(myRunnable, delay);
        } else {
            if (!sharedPreference.getBooleanValue("UserLogin"))
            {
                handler.removeCallbacks(myRunnable);
                startDayBtn.setVisibility(View.VISIBLE);
            }
            stopDayBtn.setVisibility(View.GONE);
        }*/

        Log.e(TAG, "::::On resume IS_START_DAY " + sharedPreference.getBooleanValue(PrefsHelper.IS_START_DAY));
        Log.e(TAG, "::::On resume UserLogin " + sharedPreference.getBooleanValue("UserLogin"));

        if (!sharedPreference.getBooleanValue("UserLogin")) {
            if (sharedPreference.getBooleanValue(PrefsHelper.IS_START_DAY)) {
                handler.removeCallbacks(myRunnable);
                startDayBtn.setVisibility(View.VISIBLE);
                stopDayBtn.setVisibility(View.GONE);
            } else {
                startDayBtn.setVisibility(View.GONE);
                stopDayBtn.setVisibility(View.VISIBLE);
                handler.postDelayed(myRunnable, delay);
            }
        } else {
            startDayBtn.setVisibility(View.VISIBLE);
            stopDayBtn.setVisibility(View.GONE);
        }


        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals));
        myGoalsTV.setTextColor(getResources().getColor(R.color.grey));
        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat));
        chatTV.setTextColor(getResources().getColor(R.color.grey));
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals));
        dealsTV.setTextColor(getResources().getColor(R.color.grey));
        try {
            registerReceiver(receiver, filter);
        } catch (Exception e) {
            Log.e("@Network_error", "::::" + e.toString());
        }
        fetchLocationAndResult();
        dashboardDetails();

        String count = sharedPreference.getStringValue(PrefsHelper.NOTIFICATION_COUNT);
        if (count.isEmpty())
            count = "0";

        Log.e(TAG, " count " + count);


        if (!count.equalsIgnoreCase("0")) {
            relative_notification.setVisibility(View.VISIBLE);
            notificationNumberTV.setVisibility(View.VISIBLE);
            notificationNumberTV.setText(count);
        } else {
            relative_notification.setVisibility(View.GONE);
            notificationNumberTV.setVisibility(View.GONE);
        }
        ApplicationClass.getInstance().setFromDeal(false);
        ApplicationClass.getInstance().setGoalId("");
        ApplicationClass.getInstance().setBusinessId("");
        ApplicationClass.getInstance().setDealId("");
    }

    @Override
    protected void onNewIntent(Intent intent1) {
        super.onNewIntent(intent1);

        Log.e(TAG, " onNewIntent " + intent1);

        if (intent1.hasExtra("fcm_notification")) {
            if (intent1.getStringExtra("fcm_notification").equalsIgnoreCase("Y")) {
                if (intent1.hasExtra("message")) {
                    String message = intent1.getStringExtra("message");
                    Log.e(TAG, " message " + message);
                    if (message != null && !message.isEmpty() && message.equalsIgnoreCase("New Daily goal has been created for you")) {
                        Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
                        intent.putExtra("nameActivity", "MyGoals");
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    } else {
                        Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(DashboardActivity.this, NotificationActivity.class);
                    startActivity(intent);
                }
                System.out.println("inside if");
            } else System.out.println("else part");
        } else {
            System.out.println("getextra");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void setTimerService() {

        List<UserLoginTime> userLoginTime = UserLoginTime.listAll(UserLoginTime.class);
        if (userLoginTime != null) {
            Log.e(TAG, " setTimerService userLoginTime size " + userLoginTime.size());
            if (userLoginTime.size() > 0) {
                UserLoginTime userLoginTime1 = userLoginTime.get(0);
                if (!userLoginTime1.getUserEmail().equalsIgnoreCase(sharedPreference.getStringValue(PrefsHelper.USER_EMAIL))) {
                    UserLoginTime.deleteAll(UserLoginTime.class);
                }
            }
        }

        if (userLoginTime != null && userLoginTime.size() == 0) {
            UserLoginTime userLoginTime1 = new UserLoginTime();
            userLoginTime1.setDate(getCurrentDate());
            userLoginTime1.setUserEmail(sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
            userLoginTime1.setTime(System.currentTimeMillis());
            userLoginTime1.save();
            userLoginTime.add(userLoginTime1);
        }

        //Date d1 = null;
        //Date d2 = null;
        //SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");

        for (int i = 0; i < userLoginTime.size(); i++) {
            Log.e(TAG, " setTimerService userLoginTime " + userLoginTime.get(i));
            UserLoginTime update = userLoginTime.get(i);

            if (update.getUserEmail().equals(sharedPreference.getStringValue(PrefsHelper.USER_EMAIL))) {

                if (update.getDate().equals(getCurrentDate())) {
                    Log.e(TAG, " setTimerService Date Not Change " + update);
                    Log.e(TAG, " setTimerService Date Not Change getTime " + update.getTime());

                    if (update.getTime() == 0 || update.getTime() < 1000000)
                        update.setTime(System.currentTimeMillis());

                    aTime = System.currentTimeMillis() - update.getTime();
                } else {
                    Log.e(TAG, " setTimerService Date Change " + update);
                    update.setDate(getCurrentDate()); // modify the values
                    update.setTime(System.currentTimeMillis());
                    sharedPreference.saveLongData(PrefsHelper.TIMER_TIME, 0);
                    aTime = 1;
                }
                update.save();
            }
        }

        Log.e(TAG, " setTimerService  aTime " + aTime);

        Log.e(TAG, " setTimerService  TIMER_TIME " + sharedPreference.getLongData(PrefsHelper.TIMER_TIME));

        aTime = aTime + sharedPreference.getLongData(PrefsHelper.TIMER_TIME);

        //long seconds = TimeUnit.MILLISECONDS.toSeconds(aTime);

        //long minutes = TimeUnit.MILLISECONDS.toMinutes(aTime);

        //long hours = TimeUnit.MILLISECONDS.toHours(aTime);


        long seconds = aTime / 1000 % 60;
        long minutes = aTime / (60 * 1000) % 60;
        long hours = aTime / (60 * 60 * 1000) % 24;

        String srtHour = String.format("%02d", hours);
        String strSeconds = String.format("%02d", minutes);
        String strMilliseconds = String.format("%02d", seconds);

        //if (aTime > 0)
        stopDayBtn.setText("       " + srtHour + ":" + strSeconds + ":" + strMilliseconds);
        // else stopDayBtn.setText("       " + 00 + ":" + 00 + ":" + 00);
    }


    public void onPause() {
        handler.removeCallbacks(myRunnable);
        super.onPause();
        Log.e("OnPAuse", ":::::::");
        unregisterReceiver(receiver);
        // ApplicationClass.stopCountdown();
        beforeGoingToBackground = getCurrentTime();
        sharedPreference.saveStringData("saveTime", getCurrentTime());
    }


    public String getCurrentTime() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss aa");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    @OnClick(R.id.mygoals_ll)
    public void myGoalsNavigate() {

        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals_active));
        myGoalsTV.setTextColor(getResources().getColor(R.color.colorAccent));

        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat));
        chatTV.setTextColor(getResources().getColor(R.color.grey));
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals));
        dealsTV.setTextColor(getResources().getColor(R.color.grey));

        ApplicationClass.getInstance().setGoalDate("");
        if (Connectivity.isConnected(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "MyGoals");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        } else {
            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "MyGoals");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        }

    }

    @OnClick(R.id.product_ll)
    public void allProductNavigate() {
        sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "product");
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products_active));
        productTV.setTextColor(getResources().getColor(R.color.colorAccent));

        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals));
        myGoalsTV.setTextColor(getResources().getColor(R.color.grey));
        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat));
        chatTV.setTextColor(getResources().getColor(R.color.grey));
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals));
        dealsTV.setTextColor(getResources().getColor(R.color.grey));

        GoalsActivities.backToCheckIn = false;//for checkin fragment stopping distraction

        if (Connectivity.isConnected(getApplicationContext())) {
            Intent intent = new Intent(DashboardActivity.this, GoalsActivities.class);
            sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, null);
            intent.putExtra("nameActivity", "AllProductHome");
            intent.putExtra("backToCheckIn", "false");


            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            Log.e("@@@", "::::Goalsdashboard");
        } else {
            // CommonUtils.showErrorMessage("Please check your internet connectivity", coordinateLayout);

            //  if (AllProduct.listAll(AllProduct.class).size() != 0) {
            Intent intent = new Intent(DashboardActivity.this, GoalsActivities.class);
            //  sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, null);
            intent.putExtra("nameActivity", "AllProductHome");
            intent.putExtra("backToCheckIn", "false");

            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            Log.e("@@@", "::::Goalsdashboard");

            //  }
            // Toast.makeText(getApplicationContext(), "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
            /*   finish();*/
        }

    }

    @OnClick({R.id.business_ll, R.id.business_linear})
    public void allBuisnessNavigate() {

        sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "business");
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business_active));
        businessTV.setTextColor(getResources().getColor(R.color.colorAccent));

        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals));
        myGoalsTV.setTextColor(getResources().getColor(R.color.grey));
        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat));
        chatTV.setTextColor(getResources().getColor(R.color.grey));
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals));
        dealsTV.setTextColor(getResources().getColor(R.color.grey));

        if (Connectivity.isConnected(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusinessDashBoard");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        } else {
            // CommonUtils.showErrorMessage("Please check your internet connectivity", coordinateLayout);

            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusinessDashBoard");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        }

    }


    @OnClick(R.id.deals_ll)
    public void allDeals() {


        String arr = sharedPreference.getStringValue("dealList");

        Log.e(TAG, " dealList " + arr);

        //sharedPreference.saveStringData("dealList", arr);
/*
        new AlertDialog.Builder(this)
                //.setTitle("Delete entry")
                .setMessage("Coming Soon!")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                        dialog.dismiss();
                    }
                })
                .show();*/
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals_active));
        dealsTV.setTextColor(getResources().getColor(R.color.colorAccent));

        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals));
        myGoalsTV.setTextColor(getResources().getColor(R.color.grey));
        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat));
        chatTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));

        Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
        intent.putExtra("nameActivity", "AllDeals");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    @OnClick(R.id.myorder_ll)
    public void myorder() {
        Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
        intent.putExtra("nameActivity", "orderhistory");
        intent.putExtra("isfrom", "dashboard");
        startActivity(intent);
        Log.e("@@@", ":::::ORDER_dashbioard");
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

    }

    boolean isOnBAck = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DashboardActivity.this);
        // Setting Dialog Title
        alertDialog.setTitle("Confirm close app...");
        // Setting Dialog Message
        alertDialog.setMessage("Do you want to close this app");
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (!isLogoutCalled) {
                    if (!isOnBAck) {
                        editor = preferences.edit();
                        editor.putLong("LastLogOut", ApplicationClass.millisup);
                        CommonUtils.lastMilliofActivity = 0;

                        Log.e("###", ":::ID3" + sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                        editor.putString("LastEmailID", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                        editor.commit();

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                        String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z
                        //PrefsHelper sharedPreference = new PrefsHelper(this);
                        Utils.SaveExceptionApi(DashboardActivity.this, "Application Supspend or inactive", date, sharedPreference);


                        ApplicationClass.stopCountdown();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
                        finish();
                        isOnBAck = true;
                    }
                }


                // Write your code here to invoke YES event
                //  Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                // Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void productClick(View v, int position, Boolean inStock) {

    }

    @Override
    public void notAvailableDialog() {

    }


    public class NetworkChangeReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.v("@@@Goals", "Receieved notification about network status");


            try {
                //if (Connectivity.isNetworkAvailable(context)) {

                if (Connectivity.isNetworkAvailable(context)) {
                    //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_offline));
                    icNoInternet.setVisibility(View.GONE);
                    ivInternet.setVisibility(View.VISIBLE);

                    if (!isConnected) {
                        Log.v("is network Available", "Now you are connected to Internet!");
                        isConnected = true;
                        sharedPreference.saveBooleanValue("IsDashBoard", true);
                        //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_offline));
                        //icNoInternet.setVisibility(View.GONE);
                        //ivInternet.setVisibility(View.VISIBLE);
                        // Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                        //getLatLongWithTiming();
                        dashboardDetails();

                        try {
                            //getUserInfo();
                        } catch (Exception e) {
                            Log.e("Exception", ":::" + e.toString());
                        }


                    }


                } else {
                    //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_online));
                    icNoInternet.setVisibility(View.VISIBLE);
                    ivInternet.setVisibility(View.GONE);

                    DashboardArray dashboardArray = Select.from(DashboardArray.class).first();
                    monthlyTargetTxt.setText(dashboardArray.getMonthlyGoals().getMonthlyTarget() + "");
                    acheivedTargetTxt.setText(dashboardArray.getMonthlyGoals().getAchievedTarget() + "");

                    UserInfoProfile userInfoProfile = Select.from(UserInfoProfile.class).first();
                    userNameTv.setText("Welcome " + userInfoProfile.getFirstName());
                    Picasso.with(getApplicationContext()).load(userInfoProfile.getImageUrl()).placeholder(getApplicationContext().getResources().getDrawable(R.drawable.user_pic)).error(R.drawable.user_pic).into(imageView);
                    //  Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
                    isConnected = false;
                }
            } catch (Exception e) {
                icNoInternet.setVisibility(View.VISIBLE);
                ivInternet.setVisibility(View.GONE);
            }

        }
    }

    SharedPreferences.Editor editor, editorTwo, userEditor;

    @OnClick(R.id.setting_dots_img)
    public void showMenu() {
        final PopupMenu menu = new PopupMenu(getApplicationContext(), threeDotsImg);
        menu.inflate(R.menu.profile_menu);

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.logout_menu) {
                    callLogoutApi();
                } else if (item.getItemId() == R.id.edit_profile) {
                    // Toast.makeText(getApplicationContext(), "Edit Profile is clicked", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(intent);
                    //overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    //finish();
                }
                return false;
            }
        });

        menu.show();
    }

    private void callLogoutApi() {
        stopLocationUpdateService();
        //mProgressDialog = new ProgressDialog(this);
        //mProgressDialog.setMess
        //mProgressDialog.setCanceledOnTouchOutside(false);
        //mProgressDialog.show();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.logout(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                if (response.getStatus() == 200) {
                    //  if (mProgressDialog != null && mProgressDialog.isShowing())
                    //    mProgressDialog.dismiss();

                } else {
                    // if (mProgressDialog != null && mProgressDialog.isShowing())
                    //   mProgressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                // if (mProgressDialog != null && mProgressDialog.isShowing())
                //   mProgressDialog.dismiss();
            }
        });
        dayStopApiCall();

        String username = sharedPreference.getStringValue(PrefsHelper.LOIGN_USER_NAME);
        String password = sharedPreference.getStringValue(PrefsHelper.LOIGN_PASSWORD);
        boolean temp = sharedPreference.getBooleanValue(PrefsHelper.REMEMBER_ME);


        Log.e(TAG, " callLogoutApi temp " + temp);
        Log.e(TAG, " callLogoutApi username " + username);
        Log.e(TAG, " callLogoutApi password " + password);


        Log.e("save_token", "" + sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
        Log.e("###", ":::uswe_email:::" + sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
        CommonUtils.lastMilliofActivity = 0;

        editor = preferences.edit();
        editor.putString("lastToken", "" + sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
        editor.putLong("LastLogOut", ApplicationClass.millisup);
        editor.putString("LastEmailID", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
        editor.commit();
        Log.e("###", "::::ID::::" + preferences.getString("LastEmailID", ""));

        SharedPreferences preferencesTwo = getSharedPreferences("SalesTrack", Context.MODE_PRIVATE);
        editorTwo = preferencesTwo.edit();
        editorTwo.clear();
        editorTwo.commit();

        sharedPreference = new PrefsHelper(this);

        sharedPreference.saveBooleanData(PrefsHelper.REMEMBER_ME, temp);
        sharedPreference.saveStringData(PrefsHelper.LOIGN_USER_NAME, username);
        sharedPreference.saveStringData(PrefsHelper.LOIGN_PASSWORD, password);

        sharedPreference.saveIntData(PrefsHelper.APP_VERSION, BuildConfig.VERSION_CODE);

        SharedPreferences preferencesThree = getSharedPreferences("mypref", Context.MODE_PRIVATE);
        editorTwo = preferencesThree.edit();
        editorTwo.clear();
        editorTwo.commit();
        Log.e("###", "::::ID2::::" + preferences.getString("LastEmailID", ""));
        isLogoutCalled = true;


        ApplicationClass.stopCountdown();



                    /*SugarContext.terminate();
                    SchemaGenerator schemaGenerator = new SchemaGenerator(getApplicationContext());
                    schemaGenerator.deleteTables(new SugarDb(getApplicationContext()).getDB());
                    SugarContext.init(getApplicationContext());
                    schemaGenerator.createDatabase(new SugarDb(getApplicationContext()).getDB());
                    finish();*/

        if (Select.from(SaveOrder.class).first() != null) {
            SaveOrder.deleteAll(SaveOrder.class);
            if (Select.from(PendingOrderItem.class).first() != null) {
                PendingOrderItem.deleteAll(PendingOrderItem.class);
            }

        }

        if (Select.from(VisitedGoals.class).first() != null) {
            VisitedGoals.deleteAll(VisitedGoals.class);
        }


        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    public void pauseTimer() {

    }


    public void onDestroy() {


        super.onDestroy();
        if (!isLogoutCalled) {
            if (!isOnBAck) {

                editor = preferences.edit();
                editor.putLong("LastLogOut", ApplicationClass.millisup);
                Log.e("###", ":::ID3" + sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                editor.putString("LastEmailID", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                editor.commit();
                ApplicationClass.stopCountdown();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
                finish();
            }

        }


    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    private String getMinute(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("mm", cal).toString();
        Log.e("Minute", ":::" + date);
        return date;

    }


    @OnClick({R.id.chat_ll, R.id.chat_linear})
    public void allChat() {
        chatImg.setImageDrawable(getResources().getDrawable(R.drawable.chat_active));
        chatTV.setTextColor(getResources().getColor(R.color.colorAccent));


        myGoalsImg.setImageDrawable(getResources().getDrawable(R.drawable.my_goals));
        myGoalsTV.setTextColor(getResources().getColor(R.color.grey));
        myOrdersImg.setImageDrawable(getResources().getDrawable(R.drawable.orders));
        myOrdersTV.setTextColor(getResources().getColor(R.color.grey));
        productImg.setImageDrawable(getResources().getDrawable(R.drawable.products));
        productTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));
        dealsImg.setImageDrawable(getResources().getDrawable(R.drawable.deals));
        dealsTV.setTextColor(getResources().getColor(R.color.grey));
        businessImg.setImageDrawable(getResources().getDrawable(R.drawable.business));
        businessTV.setTextColor(getResources().getColor(R.color.grey));


        if (Connectivity.isConnected(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "AllChatDashBoard");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        } else {
            // CommonUtils.showErrorMessage("Please check your internet connectivity", coordinateLayout);

            Intent intent = new Intent(getApplicationContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "AllChatDashBoard");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        }
    }


    public String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            prImg.setImageBitmap(result);
        }
    }

    private void startLocationUpdateService() {
        startService(new Intent(this, LocationUpdatesService.class));
    }

    private void stopLocationUpdateService() {
        stopService(new Intent(this, LocationUpdatesService.class));
    }


    public void dashboardDetails() {

        //showDialog();
//        Log.e("showDialog()","3");

        try {
            List<DashboardArray> productInCart = DashboardArray.listAll(DashboardArray.class);
            if (productInCart != null && productInCart.size() > 0) {
                DashboardArray.deleteAll(DashboardArray.class);
            }

        } catch (Exception e) {
        }


        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getDashboardDetail(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                Log.e(TAG, "dashboardDetails arr " + arr);

                try {
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        DashboardArray dashboardArray = builder.fromJson(jsonArr.get(i).toString(), DashboardArray.class);
                        monthlyTargetTxt.setText(dashboardArray.getMonthlyGoals().getMonthlyTarget() + "");
                        acheivedTargetTxt.setText(dashboardArray.getMonthlyGoals().getAchievedTarget() + "");
                        dashboardArray.save();
                    }

                    DashboardArray dashboardArray = Select.from(DashboardArray.class).first();
                    System.out.println(dashboardArray);

                    // hideDialog();

                } catch (Exception ex) {
                    ex.printStackTrace();

                    //hideDialog();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    String ACCESS_TOKEN = "Access_Token";


    private void getOrderList() {
        //showDialog();
//        Intent intentSer = new Intent(getApplicationContext(), GetProductBusinessService.class);
//        intentSer.putExtra("ACCESS_TOKEN", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
//        startService(intentSer);

        ACCESS_TOKEN = sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN);
        new Thread(new Runnable() {
            @Override
            public void run() {

                sharedPreference.saveStringData(PrefsHelper.NOTIFICATION_COUNT, "0");

                if (!sharedPreference.getBooleanValue("UserLogin")) {
                    getAllProduct();
                    getAllOrders();
                    //getAllDeals();
                    //getAllChats();
                    getDailyPlanner();
                }
                getAllBusiness();
                //getProfile();
                //getNotification();

                //hideDialog();
            }
        }).start();
    }

    private void getDailyPlanner() {
        final CountDownLatch latch = new CountDownLatch(1);
        int year;
        int month;
        int day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String dateString = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);
        serviceHandler.getGoalsAccDate(dateString, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("dailyPlannerList", arr);
                latch.countDown();
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "failure: " + error);
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getAllProduct() {
        final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);
        serviceHandler.getAllProduct(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("productList", arr);
                latch.countDown();
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "failure: " + error);
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAllBusiness() {
        final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllBusinessArray(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("businessList", arr);
                latch.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAllOrders() {
        final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getOrderHistory("NEW", new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("orderList", arr);
                latch.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAllDeals() {
        final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllDealsArray(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                Log.e(TAG, " getAllDeals arr " + arr);

                sharedPreference.saveStringData("dealList", arr);
                latch.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getAllChats() {
        final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllChats(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("chatList", arr);
                latch.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getProfile() {
        final CountDownLatch latch = new CountDownLatch(1);
        UserInfoProfile.deleteAll(UserInfoProfile.class);
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getUserInfo(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                try {

                    UserInfoProfile userInfoProfile = builder.fromJson(arr, UserInfoProfile.class);
                    userInfoProfile.save();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                latch.countDown();
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getNotification() {
        //final CountDownLatch latch = new CountDownLatch(1);
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getAllNotification(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("notificationList", arr);
                //allDone = true;
                //latch.countDown();

            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                //allDone = true;
                //latch.countDown();
            }
        });
    }

}