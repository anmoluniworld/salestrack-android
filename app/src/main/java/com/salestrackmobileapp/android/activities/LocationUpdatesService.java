/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.salestrackmobileapp.android.activities;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.LatLng;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.services.GpsHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A bound and started service that is promoted to a foreground service when location updates have
 * been requested and all clients unbind.
 * <p>
 * For apps running in the background on "O" devices, location is computed only once every 10
 * minutes and delivered batched every 30 minutes. This restriction applies even to apps
 * targeting "N" or lower which are run on "O" devices.
 * <p>
 * This sample show how to use a long-running service for location updates. When an activity is
 * bound to this service, frequent location updates are permitted. When the activity is removed
 * from the foreground, the service promotes itself to a foreground service, and location updates
 * continue. When the activity comes back to the foreground, the foreground service stops, and the
 * notification assocaited with that service is removed.
 */
public class LocationUpdatesService extends Service {

    private static final String PACKAGE_NAME =
            "com.salestrackmobileapp.android";

    private static final String TAG = LocationUpdatesService.class.getSimpleName();

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "channel_01";

    public static final String ACTION_BROADCAST = PACKAGE_NAME + ".broadcast";

    public static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";
    private static final String EXTRA_STARTED_FROM_NOTIFICATION = PACKAGE_NAME +
            ".started_from_notification";

//    private final IBinder mBinder = new LocalBinder();

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 30 * 1000;//30 second


    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    /**
     * The identifier for the notification displayed for the foreground service.
     */
    private static final int NOTIFICATION_ID = 12345678;

    /**
     * Used to check whether the bound activity has really gone away and not unbound as part of an
     * orientation change. We create a foreground service notification only if the former takes
     * place.
     */
    private boolean mChangingConfiguration = false;

    private NotificationManager mNotificationManager;

    /**
     * Contains parameters used by {@link com.google.android.gms.location.FusedLocationProviderApi}.
     */
    private LocationRequest mLocationRequest;

    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Callback for changes in location.
     */
    private LocationCallback mLocationCallback;

    private Handler mServiceHandler;

    /**
     * The current location.
     */
    private Location mLocation;

    public static String ACCESS_TOKEN = "Access_Token";
    public static String EXPIRTIME = "expire_time";
    public static Boolean STARTDAY = false;
    private String username;
    public PrefsHelper sharedPreference;

    public LocationUpdatesService() {
    }

    Location loc;
    double latitude;
    double longitude;


    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) return;

                /*for (Location location : locationResult.getLocations())
                {
                    if (location != null)
                    {
                        Log.e(TAG, " onLocationResult wayLatitude "+location.getLatitude());
                        Log.e(TAG, " onLocationResult wayLongitude "+location.getLongitude());
                    }
                }*/
                //onNewLocation(locationResult.getLastLocation());

                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (locationManager != null) {
                    loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (loc != null) {
                        latitude = loc.getLatitude();
                        longitude = loc.getLongitude();

                        onNewLocation(loc);
                    } else
                        onNewLocation(locationResult.getLastLocation());
                } else
                    onNewLocation(locationResult.getLastLocation());


                //Intent intentSer = new Intent(getApplicationContext(), LocationTrack.class);
                //startService(intentSer);

                //Intent intent=new Intent(getApplicationContext(), GpsHandler.class);
                //sendBroadcast(intent);

            }
        };

        createLocationRequest();
        getLastLocation();
        sharedPreference = new PrefsHelper(this);

        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        mServiceHandler = new Handler(handlerThread.getLooper());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Android O requires a Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Create the channel for the notification
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription("no sound");
            mChannel.setSound(null, null);
            mChannel.enableLights(false);
            mChannel.setLightColor(Color.BLUE);
            mChannel.enableVibration(false);

            // Set the Notification Channel for the Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "Service started");
//        boolean startedFromNotification = intent.getBooleanExtra(EXTRA_STARTED_FROM_NOTIFICATION,
//                false);

        // We got here because the user decided to remove location updates from the notification.
//        if (startedFromNotification) {
//            removeLocationUpdates();
//            stopSelf();
//        }
        // Tells the system to not try to recreate the service after it has been killed.

        initService(intent);
        requestLocationUpdates();
        startForeground(NOTIFICATION_ID, getNotification());
        return START_NOT_STICKY;
    }

    private void initService(Intent intent) {
        try {
            if (intent != null) {
//                if (intent.hasExtra("TIME_IN_MINUTESC")) {
//                    MIN_TIME_BW_UPDATES = intent.getLongExtra("TIME_IN_MINUTES", 1000);
//                }
//                if (intent.hasExtra("EXPIRTIME")) {
//                    EXPIRTIME = intent.getStringExtra("EXPIRTIME");
//                }
                if (intent.hasExtra("ACCESS_TOKEN")) {
                    ACCESS_TOKEN = intent.getStringExtra("ACCESS_TOKEN");
                }
                if (intent.hasExtra("USERNAME")) {
                    username = intent.getStringExtra("USERNAME");
                }
//                if (intent.hasExtra("start_day")) {
//                    STARTDAY = intent.getBooleanExtra("start_day", false);
//                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        mChangingConfiguration = true;
//    }
//
    @Override
    public IBinder onBind(Intent intent) {
        // Called when a client (MainActivity in case of this sample) comes to the foreground
        // and binds with this service. The service should cease to be a foreground service
        // when that happens.
//        Log.e(TAG, "in onBind()");
//        stopForeground(true);
//        mChangingConfiguration = false;
        return null;
    }
//
//    @Override
//    public void onRebind(Intent intent) {
//        // Called when a client (MainActivity in case of this sample) returns to the foreground
//        // and binds once again with this service. The service should cease to be a foreground
//        // service when that happens.
//        Log.e(TAG, "in onRebind()");
//        stopForeground(true);
//        mChangingConfiguration = false;
//        super.onRebind(intent);
//    }
//
//    @Override
//    public boolean onUnbind(Intent intent) {
//        Log.e(TAG, "Last client unbound from service");
//
//        // Called when the last client (MainActivity in case of this sample) unbinds from this
//        // service. If this method is called due to a configuration change in MainActivity, we
//        // do nothing. Otherwise, we make this service a foreground service.
//        if (!mChangingConfiguration && Utils.requestingLocationUpdates(this)) {
//            Log.e(TAG, "Starting foreground service");
//
//            startForeground(NOTIFICATION_ID, getNotification());
//        }
//        return true; // Ensures onRebind() is called when a client re-binds.
//    }

    @Override
    public void onDestroy() {
        mServiceHandler.removeCallbacksAndMessages(null);
        removeLocationUpdates();
        Log.e("LocationUpdatesService", "onDestroy");
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void requestLocationUpdates() {
        Log.e(TAG, "Requesting location updates");
        Utils.setRequestingLocationUpdates(this, true);
//        startService(new Intent(getApplicationContext(), LocationUpdatesService.class));
        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, false);
            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * {@link SecurityException}.
     */
    public void removeLocationUpdates() {
        Log.e(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            Utils.setRequestingLocationUpdates(this, false);
            stopSelf();
        } catch (SecurityException unlikely) {
            Utils.setRequestingLocationUpdates(this, true);
            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }
    }

    /**
     * Returns the {@link NotificationCompat} used as part of the foreground service.
     */
    private Notification getNotification() {
        Intent intent = new Intent(this, LocationUpdatesService.class);

        CharSequence text = Utils.getLocationText(mLocation);

        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(EXTRA_STARTED_FROM_NOTIFICATION, true);

        // The PendingIntent that leads to a call to onStartCommand() in this service.
        PendingIntent servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // The PendingIntent to launch activity.
        PendingIntent activityPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, DashboardActivity.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                .addAction(R.mipmap.ic_launcher, getString(R.string.launch_activity),
//                        activityPendingIntent)
//                .addAction(R.mipmap.ic_launcher, getString(R.string.remove_location_updates),
//                        servicePendingIntent)
//                .setContentText(text)
                .setContentTitle(Utils.getLocationTitle(this))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.logo2)
                .setTicker(text)

                .setWhen(System.currentTimeMillis());

        // Set the Channel ID for Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        return builder.build();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLocation = task.getResult();
                            } else {
                                Log.w(TAG, "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private Location lastLocation = null;

    private void onNewLocation(Location location) {
        Log.e(TAG, "New location: " + location);

        mLocation = location;


        Log.e(TAG, " location latitude " + location.getLatitude());
        Log.e(TAG, " location longitude " + location.getLongitude());
        Log.e(TAG, " location getAccuracy " + location.getAccuracy());

        if (lastLocation != null) {
            Log.e(TAG, " lastLocation latitude " + lastLocation.getLatitude());
            Log.e(TAG, " lastLocation longitude " + lastLocation.getLongitude());
        }

        mNotificationManager.notify(NOTIFICATION_ID, getNotification());

        if (location != null) {
            if (lastLocation == null) {
                //getTimerForCallService(location);
                Singleton.getInstance().SLAT = location.getLatitude();
                Singleton.getInstance().SLNG = location.getLongitude();
                SecurityLogsApi(location);
                lastLocation = location;

            } else if (location.getLatitude() != lastLocation.getLatitude() && location.getLongitude() != lastLocation.getLongitude()) {
                //getTimerForCallService(location);
                Singleton.getInstance().SLAT = location.getLatitude();
                Singleton.getInstance().SLNG = location.getLongitude();
                SecurityLogsApi(location);
                lastLocation = location;

            }
        }

        //if (location != null)
        //  SecurityLogsApi(location);

    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(50);
    }

    public void getTimerForCallService(Location location) {
        // CommonUtils.showProgress(getApplicationContext());
        Log.e(TAG, "clicked");
        Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

            final JSONObject jsonObj = new JSONObject();
            jsonObj.put("Latitude", "" + location.getLatitude());
            jsonObj.put("Longitude", "" + location.getLongitude());
            jsonObj.put("TrackDate", date + "Z");
            jsonObj.put("SalesPersonName", username);
            jsonObj.put("Acuracy", location.getAccuracy());
            jsonObj.put("Altitude", location.getAltitude());
            jsonObj.put("Velocity", location.getSpeed());
            jsonObj.put("Bearing", location.getBearing());

            LatLng latLongObject = builder.fromJson(jsonObj.toString(), LatLng.class);
            Log.e(TAG, " Location_params " + jsonObj.toString());
            //Longitude

            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);
            serviceHandler.trackLatLong(latLongObject, new Callback<Response>() {
                @SuppressLint("LongLogTag")
                @Override
                public void success(Response response, Response response2) {
                    String serverResponse = CommonUtils.getServerResponse(response);
                    try {
                        JSONObject jsonObject = new JSONObject(serverResponse);
                        Log.e(TAG, " getTimerForCallService Response " + jsonObject.toString());

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println(error);
                }
            });
            CommonUtils.dismissProgress();
        } catch (Exception ex) {
            ex.printStackTrace();
            CommonUtils.dismissProgress();
        }
    }

    private void SecurityLogsApi(Location location) {
        if (Connectivity.isNetworkAvailable(this)) {
            try {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

                final JSONObject jsonObj = new JSONObject();
                jsonObj.put("Latitude", "" + location.getLatitude());
                jsonObj.put("Longitude", "" + location.getLongitude());
                jsonObj.put("TrackDate", date + "Z");
                jsonObj.put("SalesPersonName", username);

                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Track/SaveTrack");
                object.put("JsonObject", jsonObj.toString());
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(this));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                        .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    getTimerForCallService(location);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
