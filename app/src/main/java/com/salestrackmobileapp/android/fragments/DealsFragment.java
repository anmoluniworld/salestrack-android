package com.salestrackmobileapp.android.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.DealsAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DealsFragment extends BaseFragment implements RecyclerClick {

    String TAG = "DealsFragment";

    @BindView(R.id.deals_rv)
    RecyclerView dealsRv;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.tv_no_deal_available)
    Custome_TextView tv_no_deal_available;

    private LinearLayoutManager mLayoutManager;


    private ProgressDialog mProgressDialog;
    private DealsAdapter mAdapter;
    List<AllDeals> listAllDeals;

    Context mContext;

    Gson builder;

    boolean flag = true;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deals, container, false);
        ButterKnife.bind(this, view);
        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();


        mLayoutManager = new LinearLayoutManager(baseActivity);
        dealsRv.setLayoutManager(mLayoutManager);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);

                if (Connectivity.isNetworkAvailable(baseActivity)) {
                    getAllDeals();
                } else
                    CommonUtils.ShowDialog(mContext, "Check Internet Connection");
            }
        });

        showDialog(mContext);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                getAllDealsArray();

            }
        }, 1000);


        return view;
    }

    private void getAllDealsArray() {
        String arr = sharedPreference.getStringValue("dealList");
        //Log.e(TAG, " getAllDealsArray arr " + arr);
        if (arr != null && !arr.isEmpty()) {
            try {
                try {
                    if (Select.from(AllDeals.class).first() != null) {
                        AllDeals.deleteAll(AllDeals.class);
                    }
                } catch (Exception e) {
                }
                JSONArray jsonArr = new JSONArray(arr);
                if (jsonArr != null) {
                    Log.e(TAG, " getAllDealsArray jsonArr " + jsonArr.length());
                    for (int i = 0; i < jsonArr.length(); i++) {
                        //Log.e(TAG, " Deal Is " + jsonArr.get(i).toString());
                        AllDeals allDeals = builder.fromJson(jsonArr.get(i).toString(), AllDeals.class);
                        allDeals.save();
                    }
                    if (listAllDeals != null)
                        listAllDeals.clear();

                    listAllDeals = AllDeals.listAll(AllDeals.class);
                    hideDialog();
                    if (listAllDeals.size() != 0) {
                        mAdapter = new DealsAdapter(baseActivity, this);
                        mAdapter.setListDeal(listAllDeals);
                        dealsRv.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        dealsRv.setVisibility(View.VISIBLE);
                        tv_no_deal_available.setVisibility(View.GONE);
                    } else {

                        if (flag) {
                            flag = false;
                            getAllDeals();
                        } else {
                            tv_no_deal_available.setVisibility(View.VISIBLE);
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                            builder1.setMessage("No Deals are available");
                            builder1.setCancelable(false);
                            builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Toast.makeText(getContext(), "Back is pressed", Toast.LENGTH_LONG).show();
                                    Intent intent2 = new Intent(baseActivity, DashboardActivity.class);
                                    startActivity(intent2);
                                }
                            });
                            builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getAllDeals();
                                }
                            });
                            builder1.show();
                            dealsRv.setVisibility(View.GONE);
                            tv_no_deal_available.setVisibility(View.VISIBLE);
                        }
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            hideDialog();
        } else {
            hideDialog();
            if (flag) {
                getAllDeals();
                flag = false;
            } else {
                tv_no_deal_available.setVisibility(View.VISIBLE);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                builder1.setMessage("No Deals are available");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getContext(), "Back is pressed", Toast.LENGTH_LONG).show();
                        Intent intent2 = new Intent(baseActivity, DashboardActivity.class);
                        startActivity(intent2);
                    }
                });
                builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAllDeals();
                    }
                });
                builder1.show();
            }
        }
    }


    @Override
    public void productClick(View v, int position, Boolean inStock) {

        Log.e(TAG, " productClick position " + position);

        if (position != -1 && listAllDeals != null) {
            Log.e(TAG, " productClick getDealProductID " + listAllDeals.get(position).getProductID());

            Log.e(TAG, " productClick getDealType " + listAllDeals.get(position).getDealType());
            Log.e(TAG, " productClick getBussinessID " + listAllDeals.get(position).getBussinessID());
            Log.e(TAG, " productClick sharedPreference BUSINESS_ID " + sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID));


            if (listAllDeals.get(position).getDealType() != null && !listAllDeals.get(position).getDealType().isEmpty()) {
                sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, "");
                ProductInCart.deleteAll(ProductInCart.class);

                ApplicationClass.getInstance().setFromDeal(true);
                ApplicationClass.getInstance().setGoalId(listAllDeals.get(position).getProductID());
                ApplicationClass.getInstance().setBusinessId(listAllDeals.get(position).getBussinessID());
                ApplicationClass.getInstance().setDealId(String.valueOf(listAllDeals.get(position).getDealID()));
                ApplicationClass.getInstance().setAreaId(String.valueOf(listAllDeals.get(position).getAreaID()));

                if (listAllDeals.get(position).getDealType().equalsIgnoreCase("Product")) {
                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                    intent.putExtra("nameActivity", "AllProduct");
                    intent.putExtra("backoncheckin", "true");
                    intent.putExtra("businessId", "0");
                    Log.d("successfully checkin", "checkined");
                    startActivity(intent);
                } else {
                    ///ApplicationClass.getInstance().setFromDeal(true);
                    //ApplicationClass.getInstance().setGoalId(listAllDeals.get(position).getProductID());
                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                    intent.putExtra("nameActivity", "AllProduct");
                    //intent.putExtra("backoncheckin", "true");
                    //intent.putExtra("businessId", "0");
                    //Log.d("successfully checkin", "checkined");
                    startActivity(intent);
                }
            }

        }
    }

    @Override
    public void notAvailableDialog() {

    }


    public void onBackPressed() {
        Intent intent = new Intent(baseActivity, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
    }

    public void showDialog(Context context) {
        Log.e(TAG, " showDialog mProgressDialog " + mProgressDialog);
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.show();
    }


    public void hideDialog() {
        Log.e(TAG, "hideDialog mProgressDialog " + mProgressDialog);
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void getAllDeals() {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            showDialog(mContext);
            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(mContext, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.getAllDealsArray(new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    // hideDialog();
                    String arr = CommonUtils.getServerResponse(response);
                    Log.e(TAG, " getAllDeals arr " + arr);
                    sharedPreference.saveStringData("dealList", arr);
                    //showDialog(mContext);
                    getAllDealsArray();
                }

                @Override
                public void failure(RetrofitError error) {
                    hideDialog();
                    error.printStackTrace();
                    Log.e(TAG, " failure error " + error.getMessage());

                }
            });
        } else
            CommonUtils.ShowDialog(mContext, "Check Internet Connection");

    }
}
