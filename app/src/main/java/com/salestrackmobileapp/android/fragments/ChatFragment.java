package com.salestrackmobileapp.android.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.adapter.ChatAdapter;
import com.salestrackmobileapp.android.gson.AddChats;
import com.salestrackmobileapp.android.gson.AllChats;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.salestrackmobileapp.android.activities.ForgetPasswordActivity.mypreference;


public class ChatFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String TAG="ChatFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String param1, String param2) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private Context context;
    private ChatAdapter chatAdapter;
    private LinearLayoutManager linearLayoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_chat)
    EditText tv_chat;
    @BindView(R.id.tv_welcome_to_chat)
    TextView tv_welcome_to_chat;
    private String companyId, senderId;
    SharedPreferences preferences;
    public List<AllChats> allChatsList = new ArrayList<>();
    private ProgressDialog mProgressDialog;
    private AddChats addChats;
    View view;
    private boolean value = true;
    private Handler handler = new Handler();
    private Runnable runnable;

    Gson builder;
    ServiceHandler serviceHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        preferences = context.getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        companyId = sharedPreference.getStringValue(PrefsHelper.COMPANY_ID);
        senderId = sharedPreference.getStringValue(PrefsHelper.SALES_PERSON_ID);
        linearLayoutManager = new LinearLayoutManager(baseActivity);
        recyclerView.setLayoutManager(linearLayoutManager);
        chatAdapter = new ChatAdapter(baseActivity, senderId);
        recyclerView.setAdapter(chatAdapter);
        return view;
    }


    private void getAllChats(final boolean check) {
        if (check && getActivity() != null && isAdded()) {
            mProgressDialog = new ProgressDialog(baseActivity);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.show();
        }
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();


        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(baseActivity, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getAllChats(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                AllChats.deleteAll(AllChats.class);
                String arr = CommonUtils.getServerResponse(response);
                try {
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        AllChats allChats = builder.fromJson(jsonArr.get(i).toString(), AllChats.class);
                        String time = allChats.getMsgTime().substring(0, allChats.getMsgTime().length() - 3);
                        allChats.setMsgTime(time);
                        allChats.setDateLayout(0);

                        if(allChats.getCreatedDate()!=null && !allChats.getCreatedDate().isEmpty())
                        {
                            String date[] = allChats.getCreatedDate().split("T");
                            allChats.setCreatedDate(date[0]);
                        }
                        allChats.save();
                    }
                    allChatsList = AllChats.listAll(AllChats.class);

                    if (allChatsList.size() != 0)
                    {
                        ChatLayoutUpdate();
                        chatAdapter.setAllChatList(allChatsList);
                        tv_welcome_to_chat.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.smoothScrollToPosition(allChatsList.size() - 1);
                    } else {
                        tv_welcome_to_chat.setVisibility(View.VISIBLE);
                        if (!sharedPreference.getBooleanValue(PrefsHelper.WELCOME_CHAT)) {
                            tv_welcome_to_chat.setText("Welcome to chat");
                            sharedPreference.saveBooleanValue(PrefsHelper.WELCOME_CHAT, true);
                        } else
                            tv_welcome_to_chat.setText("No message");
                        recyclerView.setVisibility(View.GONE);
                        //Toast.makeText(context, "Welcome to chat.", Toast.LENGTH_SHORT).show();
                    }
                    if (check) {
                        hideDialog();
                        value = false;
                    }

                } catch (Exception e) {
                    if (check)
                        hideDialog();
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (check)
                    hideDialog();
            }
        });
    }

    @OnClick(R.id.iv_send)
    void Clicked() {
        hideKeyboardFrom(baseActivity, view);
        if (!tv_chat.getText().toString().trim().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String currentDateandTime = sdf.format(new Date());

            String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            AllChats allChats = new AllChats();
            allChats.setSenderId(Integer.parseInt(senderId));
            allChats.setMessage(tv_chat.getText().toString().trim());
            allChats.setMsgTime(currentDateandTime);
            allChats.setDateLayout(0);

            /*if(allChats.getCreatedDate()!=null && !allChats.getCreatedDate().isEmpty())
            {
                String date[] = format.split("T");
                allChats.setCreatedDate(date[0]);
            }*/
            allChats.setCreatedDate(format);

            allChats.save();

            allChatsList = AllChats.listAll(AllChats.class);

            if (allChatsList.size() != 0)
            {
                ChatLayoutUpdate();
                chatAdapter.setAllChatList(allChatsList);
            }
            updateChats();
            recyclerView.smoothScrollToPosition(allChatsList.size() - 1);
        } else Toast.makeText(context, "Please type message", Toast.LENGTH_SHORT).show();
    }
    private void SecurityLogsApi(JSONObject location)
    {
        if (Connectivity.isNetworkAvailable(getActivity()))
        {
            try {

                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Message/SaveUpdate");
                object.put("JsonObject", location.toString());
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getActivity(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    AddChat(location);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateChats() {

        try
        {
            JSONObject object = new JSONObject();
            object.put("MessageId", 0);
            object.put("SenderId", senderId);

            object.put("ReceiverId", companyId);
            object.put("Message", tv_chat.getText().toString().trim());
            object.put("IsRead", 1);
            object.put("IsAttachment", false);

            SecurityLogsApi(object);

        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, "", Toast.LENGTH_LONG).show();
            //hideDialog();
        }

    }

    private void AddChat(JSONObject object)
    {

        try
        {
            addChats = builder.fromJson(object.toString(), AddChats.class);
            serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.addChats(addChats, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String serverResponse = CommonUtils.getServerResponse(response);
                    Log.e("serverResponse", ":::" + serverResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(serverResponse);
                        if (jsonObject.has("Message")) {
                            String Message = jsonObject.getString("Message");
                            if (sharedPreference.getBooleanValue(PrefsHelper.WAIT_FOR_REPLY)) {
                                Toast.makeText(context, "Please wait for Reply.", Toast.LENGTH_SHORT).show();
                                sharedPreference.saveBooleanValue(PrefsHelper.WAIT_FOR_REPLY, false);
                            }

                            tv_chat.setText("");
                        }
                        //hideDialog();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        //hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(serverResponse);
                            Toast.makeText(context, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    //hideDialog();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, "", Toast.LENGTH_LONG).show();
            //hideDialog();
        }

    }


    @Override
    public void onResume() {
        displayChat();
        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                //do something
                System.out.println("clicked>>");
                getAllChats(value);
                handler.postDelayed(runnable, 10000);
            }
        }, 1000);
        super.onResume();
    }

    private void displayChat() {
        AllChats.deleteAll(AllChats.class);

        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

        String arr = sharedPreference.getStringValue("chatList");
        if (arr != null || !arr.isEmpty()) {
            try {
                JSONArray jsonArr = new JSONArray(arr);
                for (int i = 0; i < jsonArr.length(); i++) {
                    AllChats allChats = builder.fromJson(jsonArr.get(i).toString(), AllChats.class);
                    String time = allChats.getMsgTime().substring(0, allChats.getMsgTime().length() - 3);
                    allChats.setMsgTime(time);

                    allChats.save();
                }
//                    Log.e("getAllBusiness", "getAllBusinessArray");
                allChatsList = AllChats.listAll(AllChats.class);

//                    Log.e("getAllBusiness", "list true");

                if (allChatsList.size() != 0)
                {
                    ChatLayoutUpdate();
                    chatAdapter.setAllChatList(allChatsList);
                    tv_welcome_to_chat.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.smoothScrollToPosition(allChatsList.size() - 1);
                } else {
                    tv_welcome_to_chat.setVisibility(View.VISIBLE);
                    if (!sharedPreference.getBooleanValue(PrefsHelper.WELCOME_CHAT)) {
                        tv_welcome_to_chat.setText("Welcome to chat");
                        sharedPreference.saveBooleanValue(PrefsHelper.WELCOME_CHAT, true);
                    } else
                        tv_welcome_to_chat.setText("No message");
                    recyclerView.setVisibility(View.GONE);
                    //Toast.makeText(context, "Welcome to chat.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            tv_welcome_to_chat.setVisibility(View.VISIBLE);
            if (!sharedPreference.getBooleanValue(PrefsHelper.WELCOME_CHAT)) {
                tv_welcome_to_chat.setText("Welcome to chat");
                sharedPreference.saveBooleanValue(PrefsHelper.WELCOME_CHAT, true);
            } else
                tv_welcome_to_chat.setText("No message");
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void hideDialog()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class getAllChatListTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            Log.e("clicked", ": clicked");
            getAllChats(value);
            return null;
        }

        protected void onPostExecute(Void result) {

        }
    }


    public void onBackPressed() {
        handler.removeCallbacksAndMessages(runnable);
        Intent intent = new Intent(baseActivity, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
    }

    public void chatDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_welcome_chat);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    void ChatLayoutUpdate()
    {
        List<AllChats> tempChatsList = new ArrayList<>();
        String date="";
        if(allChatsList!=null)
        {
            Log.e(TAG," ChatLayoutUpdate allChatsList "+allChatsList.size());

            for(AllChats allChats:allChatsList)
            {
                Log.e(TAG," ChatLayoutUpdate date "+date);
                Log.e(TAG," ChatLayoutUpdate getCreatedDate "+allChats.getCreatedDate());

                if(date.isEmpty())
                {
                    date=allChats.getCreatedDate();
                    AllChats allChats1=new AllChats();
                    allChats1.setDateLayout(1);
                    allChats1.setCreatedDate(allChats.getCreatedDate());
                    tempChatsList.add(allChats1);
                }
                else if(!date.equalsIgnoreCase(allChats.getCreatedDate()))
                {
                    Log.e(TAG," ChatLayoutUpdate date change "+date);
                    date=allChats.getCreatedDate();
                    AllChats allChats1=new AllChats();
                    allChats1.setDateLayout(1);
                    allChats1.setCreatedDate(allChats.getCreatedDate());
                    tempChatsList.add(allChats1);
                }

                allChats.setDateLayout(0);
                tempChatsList.add(allChats);
            }
        }
        allChatsList.clear();
        allChatsList.addAll(tempChatsList);
        Log.e(TAG," ChatLayoutUpdate tempChatsList "+tempChatsList.size());
        Log.e(TAG," ChatLayoutUpdate allChatsList "+allChatsList.size());

        //chatAdapter.setAllChatList(allChatsList);
    }
}
