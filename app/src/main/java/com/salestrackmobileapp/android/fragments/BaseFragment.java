package com.salestrackmobileapp.android.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.salestrackmobileapp.android.activities.BaseActivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;

/**
 * Created by kanchan on 3/23/2017.
 */

public class BaseFragment extends Fragment {
    public BaseActivity baseActivity;
    public PrefsHelper sharedPreference;

    private ProgressDialog mProgressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
        sharedPreference = new PrefsHelper(baseActivity);
    }


    public void showDialog(Context context)
    {
        Log.e("BaseFragment"," showDialog mProgressDialog "+mProgressDialog);
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
            mProgressDialog=null;
        }

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        if(getActivity()!=null && isAdded() && !(getActivity()).isFinishing())
        mProgressDialog.show();
    }


    public void hideDialog() {
        Log.e("BaseFragment","hideDialog mProgressDialog "+mProgressDialog);

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
