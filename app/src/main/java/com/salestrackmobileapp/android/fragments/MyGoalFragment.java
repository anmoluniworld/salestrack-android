package com.salestrackmobileapp.android.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.angads25.toggle.LabeledSwitch;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.GoalBusinessNameAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.CheckedDataHit;
import com.salestrackmobileapp.android.gson.DashboardArray;
import com.salestrackmobileapp.android.gson.GoalBusiness;
import com.salestrackmobileapp.android.gson.GoalBusinessBackup;
import com.salestrackmobileapp.android.gson.GoalBusinessName;
import com.salestrackmobileapp.android.gson.GoalsAccDate;
import com.salestrackmobileapp.android.gson.Product;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SelectDateFragment;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.widget.LinearLayout.VERTICAL;
import static com.salestrackmobileapp.android.activities.GoalsActivities.actionBarTitle;
import static com.salestrackmobileapp.android.activities.GoalsActivities.homeIconImg;


public class MyGoalFragment extends BaseFragment implements RecyclerClick {

    @BindView(R.id.list_goals_rv)
    RecyclerView listGoalRv;

    @BindView(R.id.date_tv)
    TextView dateTV;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.textViewDayPlan)
    TextView textViewDayPlan;

    String TAG = "MyGoalFragment";


    public static int yy, mm, dd;

    public static String dateForApiST;
    public static String dateForApi;
    public static String dateForOffline;
    List<GoalsAccDate> listAllGoals;
    List<Integer> goalIDlist;
    List<GoalBusiness> goalBusinessList;
    List<GoalBusinessBackup> goalBusinessBackupsList;
    private GoalBusinessNameAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int year;
    private int month;
    private int day;
    int itemPosition;

    Gson builder;
    ServiceHandler serviceHandler;
    CheckedDataHit checkedDataHit;
    GoalBusiness goalBusiness;
    String goalBusinessName;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    private ProgressDialog mProgressDialog;

    @BindView(R.id.null_value_layout)
    LinearLayout nullValueLayout;

    @BindView(R.id.inc_img)
    ImageView incImg;
    @BindView(R.id.dec_img)
    ImageView decImg;
    String currentDate;

    public static int flagTocallAllGoalsApi = 0;
    DialogFragment newFragment;
    List<GoalBusinessName> goalBusinessNames;
    HashSet<GoalBusinessName> hashSet;
    List<GoalBusinessName> noRepeat;
    ArrayList<String> businessName, goalName;
    ArrayList<String> afterFilterBusinessName, afterFilterGoalName;


    @BindView(R.id.dailyPlannerGoal)
    TextView dailyPlannerGoal;
    @BindView(R.id.dailyPlannerBusiness)
    TextView dailyPlannerBusiness;
    private String value = "business";

    @BindView(R.id.pb)
    ProgressBar pb;

    @BindView(R.id.percentageGoals)
    Custome_TextView percentageGoals;

    @BindView(R.id.txt1)
    Custome_TextView txt1;


/*    @BindView(R.id.switch_button)
    SwitchButton switch_button;*/

    // @BindView(R.id.switchSelected)
    //TextView switchSelected;

    @BindView(R.id.linearSwitch)
    LabeledSwitch linearSwitch;

    //@BindView(R.id.view1)
    //View view1;
    //@BindView(R.id.view2)
    //View view2;
    //private boolean checkSwitch = true;

    @OnClick({R.id.dailyPlannerBusiness, R.id.dailyPlannerGoal})
    void Clicked(View view) {
        switch (view.getId()) {
            case R.id.dailyPlannerBusiness:
                dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.white));
                //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                dailyPlannerBusiness.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_business));
                dailyPlannerGoal.setTextColor(getResources().getColor(R.color.colorDark));
                //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                dailyPlannerGoal.setBackgroundColor(Color.TRANSPARENT);

                afterFilterGoalName = new ArrayList<String>();
                HashSet<String> hashSets = new HashSet<String>();
                hashSets.addAll(goalName);
                afterFilterGoalName.clear();
                afterFilterGoalName.addAll(hashSets);
                mAdapter.setListGoals(afterFilterGoalName);
                listGoalRv.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                value = "business";
                break;
            case R.id.dailyPlannerGoal:
                dailyPlannerGoal.setTextColor(getResources().getColor(R.color.white));
                //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                dailyPlannerGoal.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_goal));
                dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.colorDark));
                //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                dailyPlannerBusiness.setBackgroundColor(Color.TRANSPARENT);

                afterFilterBusinessName = new ArrayList<String>();
                HashSet<String> hashSet = new HashSet<String>();
                hashSet.addAll(businessName);
                afterFilterBusinessName.clear();
                afterFilterBusinessName.addAll(hashSet);
                mAdapter.setListGoals(afterFilterBusinessName);
                listGoalRv.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                value = "goal";
                break;

            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_goal, container, false);
        ButterKnife.bind(this, view);

        mLayoutManager = new LinearLayoutManager(getActivity());
        listGoalRv.setLayoutManager(mLayoutManager);
        listGoalRv.setNestedScrollingEnabled(false);


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        Calendar cal = Calendar.getInstance();
        Log.e(TAG, "onCreateView getGoalDate " + ApplicationClass.getInstance().getGoalDate());

        if (ApplicationClass.getInstance().getGoalDate() != null && !ApplicationClass.getInstance().getGoalDate().isEmpty()) {
            String dateSplit[] = ApplicationClass.getInstance().getGoalDate().split("-");
            mm = Integer.parseInt(dateSplit[0]);
            dd = Integer.parseInt(dateSplit[1]);
            yy = Integer.parseInt(dateSplit[2]);

            cal.set(Calendar.YEAR, yy);
            cal.set(Calendar.DAY_OF_MONTH, dd);
            cal.set(Calendar.MONTH, mm - 1);
        } else {
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.DAY_OF_MONTH, day);
            cal.set(Calendar.MONTH, month);

            yy = year;
            mm = month;
            dd = day;
        }

        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

        String format = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());
        dateForApiST = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());
        dateForApi = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
        dateForOffline = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

        dateTV.setText(format + "");

        dateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                newFragment = new SelectDateFragment(dateTV, MyGoalFragment.this);
                newFragment.show(getFragmentManager(), "DatePicker");

                //getAllGoalsAccDate();
            }
        });
        currentDate = new SimpleDateFormat("MMM d, yyyy").format(cal.getTime());

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllGoalsAccDate();
            }
        });
        if (!isAllGoals) {
            getAllGoalsAccDate();
        }
        incImg.setEnabled(true);
        value = "goal";


        getMonthlyAchieviedTarget();

        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), VERTICAL);
        listGoalRv.addItemDecoration(decoration);

        linearSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(LabeledSwitch labeledSwitch, boolean isOn) {
                // Implement your switching logic here

                if (isOn) {

                    dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.white));
                    //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                    dailyPlannerBusiness.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_business));
                    dailyPlannerGoal.setTextColor(getResources().getColor(R.color.colorDark));
                    //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                    dailyPlannerGoal.setBackgroundColor(Color.TRANSPARENT);

                    try {
                        afterFilterGoalName = new ArrayList<String>();
                        HashSet<String> hashSetsGoal = new HashSet<String>();
                        hashSetsGoal.addAll(goalName);
                        afterFilterGoalName.clear();
                        afterFilterGoalName.addAll(hashSetsGoal);
                        mAdapter.setListGoals(afterFilterGoalName);
                        listGoalRv.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        value = "goal";

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    dailyPlannerGoal.setTextColor(getResources().getColor(R.color.white));
                    //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                    dailyPlannerGoal.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_goal));
                    dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.colorDark));
                    //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
                    dailyPlannerBusiness.setBackgroundColor(Color.TRANSPARENT);

                    try {
                        afterFilterBusinessName = new ArrayList<String>();
                        HashSet<String> hashSetBusiness = new HashSet<String>();
                        hashSetBusiness.addAll(businessName);
                        afterFilterBusinessName.clear();
                        afterFilterBusinessName.addAll(hashSetBusiness);
                        mAdapter.setListGoals(afterFilterBusinessName);
                        listGoalRv.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        value = "business";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (isOn) {

                } else {

                }
            }
        });

        return view;
    }

    public void getMonthlyAchieviedTarget() {

        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getDashboardDetail(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                try {
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        DashboardArray dashboardArray = builder.fromJson(jsonArr.get(i).toString(), DashboardArray.class);
                        txt1.setText("₹" + dashboardArray.getMonthlyGoals().getAchievedTarget() + "/" + "₹" + dashboardArray.getMonthlyGoals().getMonthlyTarget());
                        if (dashboardArray.getMonthlyGoals().getAchievedTarget() > dashboardArray.getMonthlyGoals().getMonthlyTarget()) {
                            pb.setProgress(100);
                            percentageGoals.setText("100%");
                        } else {
                            float value = (dashboardArray.getMonthlyGoals().getAchievedTarget() / dashboardArray.getMonthlyGoals().getMonthlyTarget()) * 100;
                            pb.setProgress((int) value);
                            percentageGoals.setText((int) value + "%");
                        }

                        //pb.setProgress((int) 56);

                    }
                    // hideDialog();

                } catch (Exception ex) {
                    ex.printStackTrace();

                    //hideDialog();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });

    }


    @Override
    public void productClick(View v, int position, Boolean inStock) {

        Log.e(TAG, "productClick " + position);


        if (value.equalsIgnoreCase("business")) {
            goalBusinessName = afterFilterBusinessName.get(listGoalRv.getChildPosition(v));

            for (int i = 0; i < goalBusinessList.size(); i++) {
                if (goalBusinessName.equalsIgnoreCase(goalBusinessList.get(i).getBusnessName())) {
                    ApplicationClass.getInstance().setGoalId(String.valueOf(goalBusinessList.get(i).getBusinessID()));
                    break;
                }
            }
            Log.e(TAG, " getDailyGoalID " + ApplicationClass.getInstance().getGoalId());
        } else {
            goalBusinessName = afterFilterGoalName.get(listGoalRv.getChildPosition(v));
            for (int i = 0; i < listAllGoals.size(); i++) {
                if (goalBusinessName.equalsIgnoreCase(listAllGoals.get(i).getGoalTitle())) {
                    ApplicationClass.getInstance().setGoalId(String.valueOf(listAllGoals.get(i).getDailyGoalID()));
                    break;
                }
            }

            Log.e(TAG, " getDailyGoalID " + ApplicationClass.getInstance().getGoalId());

        }

        itemPosition = listGoalRv.getChildPosition(v);

        ApplicationClass.getInstance().setGoalDate(dateForApi);


        actionBarTitle.setText("DAILY PLANNER");
        homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));
        DailyPlannerFragment fragment = new DailyPlannerFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("business_Name", goalBusinessName);
        bundle.putSerializable("type", value);
        //bundle.putSerializable("lastSelectedDate",for);

        fragment.setArguments(bundle);
        FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
        // ft.addToBackStack("dailypalnnerfragment");
        ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @Override
    public void notAvailableDialog() {

    }

    boolean isAllGoals = false;

    private void SecurityLogsApi() {

        textViewDayPlan.setText("DAY PLAN");

        isAllGoals = true;
        mProgressDialog = new ProgressDialog(baseActivity);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        //mProgressDialog.show();
//        Log.e("###NETWORK","::::::::::"+ Connectivity.isNetworkAvailable(baseActivity));
        mAdapter = new GoalBusinessNameAdapter(baseActivity, this);

        if (Connectivity.isNetworkAvailable(baseActivity)) {
            showDialog();


            serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            String dateString = dateForApi;

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            Date currentLocalTime = cal.getTime();
            DateFormat date = new SimpleDateFormat("HH:mm a");

            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));

            String localTime = date.format(currentLocalTime);

            Log.e(TAG, "getAllGoalsAccDate dateForApiST " + dateForApiST);
            Log.e(TAG, "getAllGoalsAccDate dateForApi " + dateForApi);

            GoalsAccDate.deleteAll(GoalsAccDate.class);
            GoalBusiness.deleteAll(GoalBusiness.class);
            serviceHandler.getGoalsAccDate(dateString, new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String arr = CommonUtils.getServerResponse(response);
                    GoalsAccDate.deleteAll(GoalsAccDate.class);
                    GoalBusiness.deleteAll(GoalBusiness.class);
                    Product.deleteAll(Product.class);


                    try {
                        String jsonString = arr.replace("null", "\"\"");
                        JSONArray jsonArr = new JSONArray(jsonString);
//                        Log.e("JSON",":::::"+jsonArr.toString());
                        if (jsonArr.length() == 0) {
                            if (listAllGoals != null) {
                                if (listAllGoals.size() != 0) {
                                    listAllGoals.clear();
                                }
                            }
                        }
//            Log.e("ARRAy_LENGTH","::::"+jsonArr.length());
                        for (int i = 0; i < jsonArr.length(); i++) {
                            GoalsAccDate allGoalsDate = builder.fromJson(jsonArr.get(i).toString(), GoalsAccDate.class);
                            JSONObject jsonObject = (JSONObject) jsonArr.get(i);
                            JSONArray jsonArray = jsonObject.getJSONArray("BusinessMapping");
                            JSONArray jsonArray2 = jsonObject.getJSONArray("ProductMapping");

                            if (jsonArray2.length() != 0) {
                                for (int j = 0; j < jsonArray2.length(); j++) {
                                    JSONObject json = jsonArray2.getJSONObject(j);
                                    Product product = builder.fromJson(json.getJSONObject("product").toString(), Product.class);
                                    // goalbusiness.setGoalBusinessDate(dateForApi);
                                    product.setDailyGoalID(String.valueOf(allGoalsDate.getDailyGoalID()));
                                    product.save();
                                }
                            }


//                            Log.e("BusinessMapping", "::::" + allGoalsDate.getBusinessMapping().get(0).getBusiness().getBusnessName());

                            if (jsonArray.length() != 0) {
                                sharedPreference.saveBooleanValue("IsMyGoalFrag", true);
                                listGoalRv.setVisibility(View.VISIBLE);
                                nullValueLayout.setVisibility(View.GONE);
                                for (int j = 0; j < jsonArray.length(); j++) {
                                    JSONObject json = jsonArray.getJSONObject(j);

                                    GoalBusiness goalbusiness = builder.fromJson(json.getJSONObject("Business").toString(), GoalBusiness.class);
                                    goalbusiness.setGoalBusinessDate(dateForApi);

                                    goalbusiness.save();


                                    GoalBusinessBackup goalbusinessbackup = builder.fromJson(json.getJSONObject("Business").toString(), GoalBusinessBackup.class);
                                    goalbusinessbackup.setGoalBusinessDate(dateForApi);
//                                    Log.e("GOAL_ID_ONLINE","::::::"+allGoalsDate.getDailyGoalID());
                                    goalbusinessbackup.setDailyGoalID(allGoalsDate.getDailyGoalID());
                                    goalbusinessbackup.save();
                                }


                                allGoalsDate.save();
                                Log.e("", "set " + noRepeat);
//                                }

                                businessName = new ArrayList<>();
                                List<GoalBusiness> goalBusiness = GoalBusiness.listAll(GoalBusiness.class);
                                for (GoalBusiness goalBusiness1 : goalBusiness) {
                                    businessName.add(goalBusiness1.getBusnessName());
                                }

                                goalName = new ArrayList<>();
                                List<GoalsAccDate> goalsAccDates = GoalsAccDate.listAll(GoalsAccDate.class);
                                for (GoalsAccDate goalsAccDate : goalsAccDates) {
                                    goalName.add(goalsAccDate.getGoalTitle());
                                }

                                if (goalName.size() != 0)
                                    textViewDayPlan.setText("DAY PLAN (" + goalName.size() + ")");
                                else
                                    textViewDayPlan.setText("DAY PLAN");

                            } else {
                                nullValueLayout.setVisibility(View.VISIBLE);
                                listGoalRv.setVisibility(View.GONE);

                            }
                            listAllGoals = GoalsAccDate.listAll(GoalsAccDate.class);
                            goalBusinessList = GoalBusiness.listAll(GoalBusiness.class);


                        }


                        listAllGoals = GoalsAccDate.listAll(GoalsAccDate.class);
                        goalBusinessList = GoalBusiness.listAll(GoalBusiness.class);
                        // Log.e("LIST_goals",":::Business::::"+listAllGoals.get(0).toString());

                        for (int p = 0; p < goalBusinessList.size(); p++) {
//                            if (goalBusinessList.get(p).getGoalBusinessDate() != null)
//                                Log.e("getGoalBusinessDate",":::getGoalBusinessDate::::"+goalBusinessList.get(p).getGoalBusinessDate());
                        }

                        Log.e("", "" + noRepeat);
                        if (listAllGoals != null) {
                            if (listAllGoals.size() != 0) {
                                afterFilterGoalName = new ArrayList<String>();
                                HashSet<String> hashSets = new HashSet<String>();
                                hashSets.addAll(goalName);
                                afterFilterGoalName.clear();
                                afterFilterGoalName.addAll(hashSets);


                                mAdapter.setListGoals(afterFilterGoalName);
                                listGoalRv.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                if (mProgressDialog != null && mProgressDialog.isShowing())
                                    mProgressDialog.dismiss();
                            } else {
                                listGoalRv.setVisibility(View.GONE);
                                nullValueLayout.setVisibility(View.VISIBLE);
                                hideDialog();
                            }
                        } else {

                            listGoalRv.setVisibility(View.GONE);
                            nullValueLayout.setVisibility(View.VISIBLE);
                            hideDialog();
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        hideDialog();
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    try {
                        if (error.getMessage().equals("timeout")) {
                            getAllGoalsAccDate();
                        } else {
//                            Toast.makeText(getActivity(), "Goals not found", Toast.LENGTH_SHORT).show();

                            GoalBusiness.deleteAll(GoalBusiness.class);
//            if (!sharedPreference.getBooleanValue("IsMyGoalFrag")) {
//                Toast.makeText(getActivity(), "You must visit every screen once after first time login with internet connected.", Toast.LENGTH_SHORT).show();
//
//            }
                            if (listAllGoals != null) {
                                if (listAllGoals.size() != 0) {
                                    listAllGoals.clear();
                                }
                            }
                            listGoalRv.setVisibility(View.VISIBLE);
                            nullValueLayout.setVisibility(View.GONE);
//                            Log.e("date","::::::"+dateForOffline);
                            try {

                                listAllGoals = Select.from(GoalsAccDate.class).where(Condition.prop("Date").eq(dateForOffline + "T00:00:00")).list();
                            } catch (Exception e) {
//                                Log.e("Exception","::::"+e.toString());
                            }

//                            Log.e("listAllGoals","::::::"+listAllGoals.size());
                            if (GoalBusinessBackup.listAll(GoalBusinessBackup.class).size() > 0) {
                                try {
                                    goalBusinessBackupsList = Select.from(GoalBusinessBackup.class).where(Condition.prop("Date").eq(dateForOffline)).list();
                                    //goalBusinessBackupsList =GoalBusinessBackup.listAll(GoalBusinessBackup.class);
                                    goalIDlist = new ArrayList<>();
                                    for (int j = 0; j < goalBusinessBackupsList.size(); j++) {


                                        GoalBusinessBackup goalbusiness = goalBusinessBackupsList.get(j);

                                        if (goalIDlist.contains(goalbusiness.getDailyGoalID())) {
//                                        Log.e("GOAL_ID_IF", "::::::" + goalbusiness.getDailyGoalID());
                                        } else {
                                            goalIDlist.add(goalbusiness.getDailyGoalID());
//                                        Log.e("GOAL_ID_ELSE", "::::::" + goalbusiness.getDailyGoalID());
//                                        Log.e("getBusnessNameDATE", "::::::" + goalbusiness.getGoalBusinessDate());
                                            GoalBusiness goalBusiness = new GoalBusiness();
                                            goalBusiness.setGoalBusinessDate(goalbusiness.getGoalBusinessDate());
                                            goalBusiness.setAddress1(goalbusiness.getAddress1());
                                            goalBusiness.setAddress2(goalbusiness.getAddress2());
                                            goalBusiness.setBusnessName(goalbusiness.getBusnessName());
                                            goalBusiness.setCheckedIN(goalbusiness.getCheckedIN());
                                            goalBusiness.setCity(goalbusiness.getCity());
                                            goalBusiness.setContactPersonEmail(goalbusiness.getContactPersonEmail());
                                            goalBusiness.setContactPersonName(goalbusiness.getContactPersonName());
                                            goalBusiness.setContactPersonPhone(goalbusiness.getContactPersonPhone());
                                            goalBusiness.setCountry(goalbusiness.getCountry());
                                            goalBusiness.setBusinessID(goalbusiness.getBusinessID());
                                            goalBusiness.setDefaultGoalbusinessID(goalbusiness.getDefaultGoalbusinessID());
                                            goalBusiness.setImageName(goalbusiness.getImageName());
                                            goalBusiness.setState(goalbusiness.getState());
                                            goalBusiness.setZipCode(goalbusiness.getZipCode());
                                            goalBusiness.setId(goalbusiness.getId());
//                                        goalBusiness.setBusinesstype(goalbusiness.getBu);

                                            goalBusiness.save();
                                        }

                                    }
                                } catch (Exception e) {
//                                    Log.e("Exception","::::"+e.toString());
                                }
                            }

                            if (goalBusinessBackupsList != null) {
                                if (goalBusinessBackupsList.size() != 0) {
                                    mAdapter.setListGoals(businessName);
                                    listGoalRv.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();
                                    if (mProgressDialog != null && mProgressDialog.isShowing())
                                        mProgressDialog.dismiss();
                                } else {
                                    listGoalRv.setVisibility(View.GONE);
                                    nullValueLayout.setVisibility(View.VISIBLE);

                                }

                            } else {
                                listGoalRv.setVisibility(View.GONE);
                                nullValueLayout.setVisibility(View.VISIBLE);
                            }
                            swipeRefreshLayout.setRefreshing(false);

                        }
                        hideDialog();
//                    getAllGoalsAccDate();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            swipeRefreshLayout.setRefreshing(false);

        } else {

            if (listAllGoals != null) {
                listAllGoals.clear();
            }
            listGoalRv.setVisibility(View.VISIBLE);
            nullValueLayout.setVisibility(View.GONE);

            Log.e(TAG, "getAllGoalsAccDate dateForApiST " + dateForApiST);
            Log.e(TAG, "getAllGoalsAccDate dateForApi " + dateForApi);
            Log.e(TAG, "getAllGoalsAccDate dateForOffline " + dateForOffline);

            String arr = sharedPreference.getStringValue("dailyPlannerList");
            if (arr != null || !arr.isEmpty()) {
                GoalsAccDate.deleteAll(GoalsAccDate.class);
                GoalBusiness.deleteAll(GoalBusiness.class);
                Product.deleteAll(Product.class);


                try {
                    String jsonString = arr.replace("null", "\"\"");
                    JSONArray jsonArr = new JSONArray(jsonString);
//                        Log.e("JSON",":::::"+jsonArr.toString());
                    if (jsonArr.length() == 0) {
                        if (listAllGoals != null) {
                            if (listAllGoals.size() != 0) {
                                listAllGoals.clear();
                            }
                        }
                    }
//            Log.e("ARRAy_LENGTH","::::"+jsonArr.length());
                    for (int i = 0; i < jsonArr.length(); i++) {
                        GoalsAccDate allGoalsDate = builder.fromJson(jsonArr.get(i).toString(), GoalsAccDate.class);
                        Log.e(TAG, " getDate " + allGoalsDate.getDate());

                        if (allGoalsDate.getDate() != null && !allGoalsDate.getDate().contains(dateForOffline))
                            continue;


                        JSONObject jsonObject = (JSONObject) jsonArr.get(i);
                        JSONArray jsonArray = jsonObject.getJSONArray("BusinessMapping");
                        JSONArray jsonArray2 = jsonObject.getJSONArray("ProductMapping");

                        if (jsonArray2.length() != 0) {
                            for (int j = 0; j < jsonArray2.length(); j++) {
                                JSONObject json = jsonArray2.getJSONObject(j);
                                Product product = builder.fromJson(json.getJSONObject("product").toString(), Product.class);
                                // goalbusiness.setGoalBusinessDate(dateForApi);
                                product.setDailyGoalID(String.valueOf(allGoalsDate.getDailyGoalID()));
                                product.save();
                            }
                        }


//                            Log.e("BusinessMapping", "::::" + allGoalsDate.getBusinessMapping().get(0).getBusiness().getBusnessName());

                        if (jsonArray.length() != 0) {
                            sharedPreference.saveBooleanValue("IsMyGoalFrag", true);
                            listGoalRv.setVisibility(View.VISIBLE);
                            nullValueLayout.setVisibility(View.GONE);
                            for (int j = 0; j < jsonArray.length(); j++) {
                                JSONObject json = jsonArray.getJSONObject(j);

                                GoalBusiness goalbusiness = builder.fromJson(json.getJSONObject("Business").toString(), GoalBusiness.class);
                                goalbusiness.setGoalBusinessDate(dateForApi);

                                goalbusiness.save();


                                GoalBusinessBackup goalbusinessbackup = builder.fromJson(json.getJSONObject("Business").toString(), GoalBusinessBackup.class);
                                goalbusinessbackup.setGoalBusinessDate(dateForApi);
//                                    Log.e("GOAL_ID_ONLINE","::::::"+allGoalsDate.getDailyGoalID());
                                goalbusinessbackup.setDailyGoalID(allGoalsDate.getDailyGoalID());
                                goalbusinessbackup.save();
                            }


                            allGoalsDate.save();
                            Log.e("", "set " + noRepeat);
//                                }

                            businessName = new ArrayList<>();
                            List<GoalBusiness> goalBusiness = GoalBusiness.listAll(GoalBusiness.class);
                            for (GoalBusiness goalBusiness1 : goalBusiness) {
                                businessName.add(goalBusiness1.getBusnessName());
                            }

                            goalName = new ArrayList<>();
                            List<GoalsAccDate> goalsAccDates = GoalsAccDate.listAll(GoalsAccDate.class);
                            for (GoalsAccDate goalsAccDate : goalsAccDates) {
                                goalName.add(goalsAccDate.getGoalTitle());
                            }


                        } else {
                            nullValueLayout.setVisibility(View.VISIBLE);
                            listGoalRv.setVisibility(View.GONE);

                        }
                        listAllGoals = GoalsAccDate.listAll(GoalsAccDate.class);
                        goalBusinessList = GoalBusiness.listAll(GoalBusiness.class);


                    }


                    listAllGoals = GoalsAccDate.listAll(GoalsAccDate.class);
                    goalBusinessList = GoalBusiness.listAll(GoalBusiness.class);
                    // Log.e("LIST_goals",":::Business::::"+listAllGoals.get(0).toString());

                    for (int p = 0; p < goalBusinessList.size(); p++) {
//                            if (goalBusinessList.get(p).getGoalBusinessDate() != null)
//                                Log.e("getGoalBusinessDate",":::getGoalBusinessDate::::"+goalBusinessList.get(p).getGoalBusinessDate());
                    }

                    Log.e("", "" + noRepeat);
                    if (listAllGoals != null) {
                        if (listAllGoals.size() != 0) {
                            afterFilterGoalName = new ArrayList<String>();
                            HashSet<String> hashSets = new HashSet<String>();
                            hashSets.addAll(goalName);
                            afterFilterGoalName.clear();
                            afterFilterGoalName.addAll(hashSets);
                            if (goalName.size() != 0)
                                textViewDayPlan.setText("DAY PLAN (" + afterFilterGoalName.size() + ")");
                            else
                                textViewDayPlan.setText("DAY PLAN");

                            mAdapter.setListGoals(afterFilterGoalName);
                            listGoalRv.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                        } else {
                            listGoalRv.setVisibility(View.GONE);
                            nullValueLayout.setVisibility(View.VISIBLE);
                            hideDialog();
                        }
                    } else {

                        listGoalRv.setVisibility(View.GONE);
                        nullValueLayout.setVisibility(View.VISIBLE);
                        hideDialog();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    hideDialog();
                }

            }

            swipeRefreshLayout.setRefreshing(false);

        }

    }


    public void getAllGoalsAccDate() {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/DailyGoals/GetByDate");
                object.put("JsonObject", "Date=" + dateForApi);
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);
                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    SecurityLogsApi();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            //getAllProduct();
        }


    }

    @OnClick(R.id.inc_img)
    public void incDate() {

        if (currentDate.equals(dateForApiST)) {

        } else {
            SelectDateFragment.incDateByOne();
            dateTV.setText(dateForApiST + "");
            getAllGoalsAccDate();
        }

        Log.e(TAG, "incDate dateForApiST " + dateForApiST);
        Log.e(TAG, "incDate dateForApi " + dateForApi);


        dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.white));
        //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
        dailyPlannerBusiness.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_business));
        dailyPlannerGoal.setTextColor(getResources().getColor(R.color.colorDark));
        //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
        dailyPlannerGoal.setBackgroundColor(Color.TRANSPARENT);

        try {

            afterFilterBusinessName = new ArrayList<String>();
            HashSet<String> hashSet = new HashSet<String>();
            if (value.equalsIgnoreCase("goal")) {
                value = "goal";
                hashSet.addAll(goalName);
            } else {
                value = "business";
                hashSet.addAll(businessName);
            }

            afterFilterBusinessName.clear();
            afterFilterBusinessName.addAll(hashSet);
            // if(afterFilterBusinessName.size()!=0)
            //   textViewDayPlan.setText("DAY PLAN ("+afterFilterBusinessName.size()+")");
            //else
            //  textViewDayPlan.setText("DAY PLAN");
            mAdapter.setListGoals(afterFilterBusinessName);
            listGoalRv.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.dec_img)
    public void decDate() {

        //textViewDayPlan.setText("DAY PLAN");

        SelectDateFragment.decDateByOne();
        dateTV.setText(dateForApiST + "");
        getAllGoalsAccDate();

        dailyPlannerBusiness.setTextColor(getResources().getColor(R.color.white));
        //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
        dailyPlannerBusiness.setBackgroundDrawable(ContextCompat.getDrawable(baseActivity, R.drawable.back_dailyplanner_business));
        dailyPlannerGoal.setTextColor(getResources().getColor(R.color.colorDark));
        //dailyPlannerBusiness.setBackground(getResources().getDrawable(R.drawable.back_dailyplanner_business));
        dailyPlannerGoal.setBackgroundColor(Color.TRANSPARENT);
        try {
            afterFilterBusinessName = new ArrayList<String>();
            HashSet<String> hashSet = new HashSet<String>();
            // hashSet.addAll(businessName);
            if (value.equalsIgnoreCase("goal")) {
                value = "goal";
                hashSet.addAll(goalName);
            } else {
                value = "business";
                hashSet.addAll(businessName);
            }
            afterFilterBusinessName.clear();
            afterFilterBusinessName.addAll(hashSet);

          /*  if(afterFilterBusinessName.size()!=0)
                textViewDayPlan.setText("DAY PLAN ("+afterFilterBusinessName.size()+")");
            else
                textViewDayPlan.setText("DAY PLAN");
*/
            mAdapter.setListGoals(afterFilterBusinessName);
            listGoalRv.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            //value = "business";
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.v("Goals getting", "Receieved notification about network status");
            if (Connectivity.isNetworkAvailable(context)) {
                if (!isConnected) {
                    Log.v("is network Available", "Now you are connected to Internet!");
                    isConnected = true;
                    sharedPreference.saveBooleanValue("IsMyGoalFrag", true);
                    //  Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                    if (!isAllGoals) {
                        getAllGoalsAccDate();
                    }
//                                if (getContext()!=null)
//                                    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);                                //do your processing here ---
                    //if you need to post any data to the server or get status
                    //update from the server
                }
            } else {
                if (!isAllGoals) {
                    getAllGoalsAccDate();
                }
//            if (!sharedPreference.getBooleanValue("IsMyGoalFrag")){
//                Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//            }
                Log.v("not connected", "You are not connected to Internet!");
                //  Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
                isConnected = false;
            }

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v("is network Available", "Now you are connected to Internet!");
                                isConnected = true;
                                sharedPreference.saveBooleanValue("IsMyGoalFrag", true);
                                //  Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                                if (!isAllGoals) {
                                    getAllGoalsAccDate();
                                }
//                                if (getContext()!=null)
//                                    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);                                //do your processing here ---
                                //if you need to post any data to the server or get status
                                //update from the server
                            }
                            return true;
                        }
                    }
                }
            }
            if (!isAllGoals) {
                getAllGoalsAccDate();
            }
//            if (!sharedPreference.getBooleanValue("IsMyGoalFrag")){
//                Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//            }
            Log.v("not connected", "You are not connected to Internet!");
            //  Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
            isConnected = false;
            return false;
        }

    }

    public void showDialog() {

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            if (getActivity() != null && isAdded() && !(getActivity()).isFinishing())
                mProgressDialog.show();

        }
    }

    public void hideDialog() {


        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();


        }
    }

}
