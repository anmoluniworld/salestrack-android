package com.salestrackmobileapp.android.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.AllProductAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_EditText;
import com.salestrackmobileapp.android.gson.AllProduct;
import com.salestrackmobileapp.android.gson.AllVariants;
import com.salestrackmobileapp.android.gson.Product;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.gson.UOMArray;
import com.salestrackmobileapp.android.gson.VariantProduct;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.ProductPriceList;
import com.salestrackmobileapp.android.utils.RecyclerNewClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ProductSearchFragment extends BaseFragment implements RecyclerNewClick, SearchView.OnQueryTextListener {

    String TAG = "ProductSearchFragment";

    @BindView(R.id.search_spn_ll)
    LinearLayout searchSpinnerLayout;
    @BindView(R.id.product_rv)
    RecyclerView productRv;
    @BindView(R.id.search_cat_spn)
    Spinner searchCatSpn;
    @BindView(R.id.search_subcat_spn)
    Spinner searchSubcatSpn;
    @BindView(R.id.search_et)
    Custome_EditText searchEt;
    SearchView mSearchView;
    @BindView(R.id.view_item_btn)
    Button viewItemBtn;

    Gson builder;
    ServiceHandler serviceHandler;


//    @BindView(R.id.nested_scroll_view)
//    NestedScrollView nestedScrollView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private AllProductAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    static List<String> categories = new ArrayList<String>();
    static List<String> categoriesids = new ArrayList<String>();
    static List<String> brandArrayList = new ArrayList<String>();
    static List<Integer> brandIdList = new ArrayList<Integer>();
    ArrayList<Integer> productIdList = new ArrayList<>();

    List<AllProduct> listAllProduct;
    List<ProductPriceList> listProductPriceList = new ArrayList<>();
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    Integer categoryID = 0, brandId = 0;

    String brandName = "SelectBrand";

    int catPosition = 0, subcatePosition = 0;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    String searchSt;
    @BindView(R.id.brand_spinner_layout)
    LinearLayout brandSpinnerLayout;
    CoordinatorLayout coordinateLayout;

    List<AllProduct> listProduct = new ArrayList<AllProduct>();
    //private ProgressDialog mProgressDialog;


    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    IntentFilter filter;
    EditText searchEditText;


    List<String> listUomArray = new ArrayList<String>();
    List<Integer> listUomIdArray = new ArrayList<Integer>();
    public static boolean backToCheckIn = false;

    public boolean addProductFlag = false;


    Context mContext;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragments
        View view = inflater.inflate(R.layout.fragment_product_search, container, false);
        ButterKnife.bind(this, view);
        coordinateLayout = (CoordinatorLayout) view.findViewById(R.id.coordinateLayout);
        getMeasurementValues();
        Log.e(TAG, " onCreateView ");

        listUomArray.add("Select..");
        /*mProgressDialog = new ProgressDialog(baseActivity);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.show();*/

        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        try {
            if (ApplicationClass.getAppContext() != null)
                LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).registerReceiver(receiver, filter);
        } catch (Exception e) {
//            Log.e("EXCEPTION:", ":::::::::" + e.toString());
        }

        if (getArguments() != null && getArguments().containsKey("add_product") && getArguments().getBoolean("add_product", false))
        {
            addProductFlag = getArguments().getBoolean("add_product", false);
        } else {
            addProductFlag = false;
        }


        mSearchView = (SearchView) view.findViewById(R.id.searchview);
        EditText searchEditText = (EditText) mSearchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.grey));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                searchCatSpn.setSelection(searchCatSpn.getSelectedItemPosition());
//                searchSubcatSpn.setSelection(searchCatSpn.getSelectedItemPosition());
                searchEt.setText("");
                swipeRefreshLayout.setRefreshing(false);
                viewProductItems();
                SecurityLogsApi(); //getAllProductRefresh();
//                getAllvariants();
            }
        });
        if (!brandArrayList.contains("Select Brand")) {
            brandArrayList.add("Select Brand");
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        searchSubcatSpn.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
        searchSubcatSpn.setSelection(0);

        if (!categories.contains("Select Category")) {
            categories.add(getResources().getString(R.string.select_category));
        }


        ArrayAdapter<String> dataAdapters = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
        dataAdapters.setDropDownViewResource(R.layout.spinner_dropdown_item);
        searchCatSpn.setAdapter(dataAdapters);
        dataAdapters.notifyDataSetChanged();

        searchCatSpn.setSelection(0);


        searchCatSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.e("position","::::"+position);
//                Log.e("parent.position",":::::"+parent.getSelectedItemPosition());
                catPosition = position;
//                showDialog();


                if (position >= 1) {
                    categoryID = Integer.valueOf(categoriesids.get(position - 1).toString());
                    viewProductItems();
                    brandSpinnerLayout.setVisibility(View.VISIBLE);

                    brandArrayList.clear();
                    brandIdList.clear();
                    brandName = "Select Brand";
                    brandArrayList.add("Select Brand");
                    List<AllProduct> listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();
                    for (int k = 0; k < listProduct.size(); k++) {
                        if (!brandArrayList.contains(listProduct.get(k).getBrandName())) {
                            brandArrayList.add(listProduct.get(k).getBrandName());

                            try {
                                brandIdList.add(listProduct.get(k).getBrandID());


                                //hideDialog();


                            } catch (Exception ec) {
                                ec.printStackTrace();


                                //hideDialog();

                            }
                        }


                        //hideDialog();
                    }
                    if (brandIdList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.single_text_item, brandArrayList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        searchSubcatSpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        searchSubcatSpn.setSelection(0);
                        viewProductItems();
                    }


                    //hideDialog();
                } else {
                    categoryID = 0;


                    viewProductItems();

//                    hideDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                Log.e("onNothingSelected()","1");


                //hideDialog();
            }

        });


        searchSubcatSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 1) {


//                    hideDialog();
                    subcatePosition = position;
                    // brandId = brandIdList.get(position - 1);
                    brandName = brandArrayList.get(position);
                    viewProductItems();


                } else {
                    brandName = "Select Brand";
                    viewProductItems();
//                    hideDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


                //hideDialog();

            }
        });
        mAdapter = new AllProductAdapter(baseActivity, this, productRv);
        productRv.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(baseActivity);
        productRv.setLayoutManager(mLayoutManager);


        setupSearchView();

        productRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = productRv.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {

                    loading = true;
                }
            }
        });
        backToCheckIn = false;

        if (getArguments() != null) {
            if (getArguments().getString("backoncheckin") != null) {
                if (getArguments().getString("backoncheckin").equals("true")) {
                    backToCheckIn = true;

                    Log.e(TAG, " isFromDeal " + ApplicationClass.getInstance().isFromDeal());
                    Log.e(TAG, " getGoalId " + ApplicationClass.getInstance().getGoalId());


                    if (ApplicationClass.getInstance().isFromDeal()) {
                        String productId = ApplicationClass.getInstance().getGoalId();
                        Log.e(TAG, " productId " + productId);
                        String productArray[] = productId.split(",");

                        for (int i = 0; i < productArray.length; i++)
                        {
                            Log.e(TAG, " productArray isFromDeal " + productArray[i]);
                            if(!productArray[i].isEmpty())
                            productIdList.add(Integer.parseInt(productArray[i]));
                        }

                    } else {
                        //String businessId=getArguments().getString("businessId");
                        List<Product> product = Product.listAll(Product.class);
                        System.out.println(product);
                        for (int i = 0; i < product.size(); i++) {
                            Log.e(TAG, " getDailyGoalID " + product.get(i).getDailyGoalID());

                            if (product.get(i).getDailyGoalID().equalsIgnoreCase(ApplicationClass.getInstance().getGoalId()))
                                productIdList.add(product.get(i).getProductID());
                        }
                    }


                } else {
                    backToCheckIn = false;
                }

            } else {
                backToCheckIn = false;
            }
        }

        Log.e(TAG, " backToCheckIn " + backToCheckIn);

        Log.e(TAG, " isAllProduct " + isAllProduct);
        Log.e(TAG, " addProductFlag " + addProductFlag);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (!isAllProduct) {
                    getAllProduct();
                }
            }
        }, 1000);


//        if (!isAllVariant) {
//            getAllvariants();
//
//        }

        //  getAllvariants();
     /*   searchCatSpn.setSelection(catPosition);
        searchSubcatSpn.setSelection(subcatePosition);*/


        return view;
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setFocusableInTouchMode(true);
        mSearchView.setQueryHint("Search product by name..");
        //   mSearchView.requestFocusFromTouch();
    }

    @OnClick(R.id.view_item_btn)
    public void viewProductItems() {

        Log.e(TAG, " viewProductItems ");


        searchSt = searchEt.getText().toString();
//        showDialog();


//        Log.e("viewproductItems","::0");
//        Log.e("CategoryID","::::"+categoryID);
        if (categoryID == 0 && brandName.equals("Select Brand")) {

            listAllProduct = AllProduct.listAll(AllProduct.class);
//            Log.e("viewproductItems","::1");


//            hideDialog();
        } else {
//            Log.e("viewproductItems","::2");


            listAllProduct = new ArrayList<AllProduct>();
            if (searchSt.equals("") || searchSt.isEmpty()) {
                if (brandName.equals("Select Brand")) {
//                    Log.e("viewproductItems","::3");
                    listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();
//                    hideDialog();
                } else {
                    if (categoryID != 0) {
//                        Log.e("viewproductItems","::4");
                        if (brandName.equals("Select Brand")) {
                            listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();

                        } else {

                            listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID), Condition.prop("brand_name").eq(brandName)).list();
                        }
//                        hideDialog();
                    } else {
//                        Log.e("viewproductItems","::5");
                        listProduct = Select.from(AllProduct.class).where(Condition.prop("brand_name").eq(brandName)).list();
//                        hideDialog();
                    }
                }
            } else {
                if (brandName.equals("Select Brand")) {
//                    Log.e("viewproductItems","::6");
                    listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();
//                    hideDialog();
                } else {
                    if (categoryID != 0) {
//                        Log.e("viewproductItems","::7");
                        if (brandName.equals("Select Brand")) {
                            listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();

                        } else {
                            listProduct = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID), Condition.prop("brand_name").eq(brandName), Condition.prop("product_name").eq(searchSt)).list();
                        }
//                        hideDialog();
                    } else {
//                        Log.e("viewproductItems","::8");
                        listProduct = Select.from(AllProduct.class).where(Condition.prop("brand_name").eq(brandName), Condition.prop("product_name").eq(searchSt)).list();
//                        hideDialog();

                    }
                }
            }

            if (listProduct.size() != 0) {
                if (!searchSt.equals("")) {

                    for (AllProduct allProduct : listProduct) {
                        if (allProduct.getProductName().contains(searchSt)) {
                            listAllProduct.add(allProduct);
                        } else {
                            Toast.makeText(baseActivity, "search item not found", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    listAllProduct = listProduct;

                }


                productRv.setVisibility(View.VISIBLE);
                mSearchView.setVisibility(View.VISIBLE);
//                hideDialog();

            } else {
                //Toast.makeText(baseActivity, "Item not found.", Toast.LENGTH_SHORT).show();
            }


//            hideDialog();
        }


//        hideDialog();
        if (backToCheckIn && !addProductFlag) {
            List<AllProduct> allProductList = new ArrayList<>();
            allProductList.addAll(listAllProduct);
            listAllProduct.clear();
            for (AllProduct allProduct : allProductList) {
                if (productIdList.contains(allProduct.getProductID())) {
                    listAllProduct.add(allProduct);
                }
            }
        }
        mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
        productRv.setAdapter(mAdapter);
    }

    boolean isProductData = false;

    public void getAllProductRefresh() {
        Log.e(TAG, " getAllProductRefresh ");
        if (mContext != null)
            showDialog(mContext);

        isAllProduct = true;
        brandSpinnerLayout.setVisibility(View.VISIBLE);
        brandName = "Select Brand";


        if (!categories.contains("Select Category")) {
            categories.add(getResources().getString(R.string.select_category));
        }
        if (!brandArrayList.contains("Select Brand")) {
            brandArrayList.add("Select Brand");
        }
        if (Connectivity.isNetworkAvailable(baseActivity)) {

            final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.getAllProduct(new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String arr = CommonUtils.getServerResponse(response);
                    sharedPreference.saveStringData("productList", arr);
                    AllProduct.deleteAll(AllProduct.class);
                    sharedPreference.saveBooleanValue("IsProductSearchFrag", true);
                    try {
                        hideDialog();
                        JSONArray jsonArr = new JSONArray(arr);
                        for (int i = 0; i < jsonArr.length(); i++) {
                            AllProduct alproduct = builder.fromJson(jsonArr.get(i).toString(), AllProduct.class);
                            if (alproduct.getDefaultPrice() == null) {
                                alproduct.setDefaultPrice(0.0);
                            }
                            if (alproduct.getSalePrice() == null) {
                                alproduct.setSalePrice(0.0);
                            }
                            if (alproduct.getPurchasePrice() == null) {
                                alproduct.setPurchasePrice(0.0);
                            }

                            if (alproduct.getProductCategoryID() != 0) {
                                for (int j = 0; j < categories.size(); j++) {
                                    String categoryName = categories.get(j);
                                    categories.contains(categoryName);
                                    if (!categories.contains(alproduct.getProductCategoryName())) {
                                        categoriesids.add(alproduct.getProductCategoryID() + "");
                                        categories.add(alproduct.getProductCategoryName() + "");
                                    }
                                }
                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                                searchCatSpn.setAdapter(dataAdapter);
                                dataAdapter.notifyDataSetChanged();

                            }


                            if (alproduct.getBrandID() != 0) {

                                for (int k = 0; k < brandArrayList.size(); k++) {
                                    if (!brandArrayList.contains(alproduct.getBrandName())) {
                                        brandArrayList.add(alproduct.getBrandName());

                                        try {
                                            brandIdList.add(alproduct.getBrandID());


                                        } catch (Exception ec) {
                                            ec.printStackTrace();


                                        }
                                    }


                                }

                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
                                dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                                searchSubcatSpn.setAdapter(dataAdapter);
                                dataAdapter.notifyDataSetChanged();


                            }


                            int productId = alproduct.getProductID();


                            alproduct.save();
                            //getProductPriceList(productId);

                        }
                        listAllProduct = AllProduct.listAll(AllProduct.class);
                        List<UOMArray> uomArray = Select.from(UOMArray.class).list();
                        for (UOMArray uomArray1 : uomArray) {
                            if (listUomArray.contains(uomArray1.getUOM())) {

                            } else {
                                listUomArray.add(uomArray1.getUOM() + "");
                                listUomIdArray.add(uomArray1.getUOMID());
                            }
                        }

                        if (backToCheckIn && !addProductFlag) {
                            List<AllProduct> allProductList = new ArrayList<>();
                            allProductList.addAll(listAllProduct);
                            listAllProduct.clear();
                            for (AllProduct allProduct : allProductList) {
                                if (productIdList.contains(allProduct.getProductID())) {
                                    listAllProduct.add(allProduct);
                                }
                            }
                        }

                        mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                        productRv.setAdapter(mAdapter);
                        productRv.getLayoutManager().scrollToPosition(CommonUtils.position);
                        searchCatSpn.setSelection(0);
                        searchSubcatSpn.setSelection(0);

                        // hideDialog();

                    } catch (Exception ex) {
                        ex.printStackTrace();


                        hideDialog();
                    }

                    if (!isAllVariant) {
                        getAllvariants();

                    }
                }


                @Override
                public void failure(RetrofitError error) {
                    Log.e(TAG, " getAllProductRefresh failure ");

                    if (error.getMessage().equals("timeout")) {
                        SecurityLogsApi(); //getAllProductRefresh();
                    } else {


                        listAllProduct = AllProduct.listAll(AllProduct.class);
                        if (listAllProduct != null && listAllProduct.size() > 0) {
                            for (AllProduct allproduct : listAllProduct) {
                                if (!categories.contains(allproduct.getProductCategoryName())) {
                                    categories.add(allproduct.getProductCategoryName());
                                    categoriesids.add(allproduct.getProductCategoryID() + "");
                                }
                            }


                            ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
                            dataAdapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            searchCatSpn.setAdapter(dataAdapter1);
                            dataAdapter1.notifyDataSetChanged();
                            searchCatSpn.setSelection(0);


                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
                            dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            searchSubcatSpn.setAdapter(dataAdapter);
                            dataAdapter.notifyDataSetChanged();
                            searchSubcatSpn.setSelection(0);


                            List<UOMArray> uomArray = Select.from(UOMArray.class).list();
//                            Log.e("uomArray", ":::::" + uomArray.size());
                            if (uomArray != null && uomArray.size() > 0) {
                                for (UOMArray uomArray1 : uomArray) {
//                                    Log.e("uomArray", ":::::for");

                                    if (listUomArray.contains(uomArray1.getUOM())) {
//                                        Log.e("uomArray", ":::::if");

                                    } else {

//                                        Log.e("uomArray", ":::::else");


                                        listUomArray.add(uomArray1.getUOM() + "");
                                        listUomIdArray.add(uomArray1.getUOMID());
                                    }
                                }
                            }
                            if (backToCheckIn && !addProductFlag) {
                                List<AllProduct> allProductList = new ArrayList<>();
                                allProductList.addAll(listAllProduct);
                                listAllProduct.clear();
                                for (AllProduct allProduct : allProductList) {
                                    if (productIdList.contains(allProduct.getProductID())) {
                                        listAllProduct.add(allProduct);
                                    }
                                }
                            }

                            mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                            productRv.setAdapter(mAdapter);


                        } else {
                            if (baseActivity != null) {
                                Toast.makeText(baseActivity, "No Product data available", Toast.LENGTH_SHORT).show();
                            }
                        }


                        hideDialog();
                    }


                    hideDialog();
                }

            });

        } else {
            hideDialog();

            listAllProduct = AllProduct.listAll(AllProduct.class);
            if (listAllProduct != null && listAllProduct.size() > 0) {
                for (AllProduct allproduct : listAllProduct) {
                    if (!categories.contains(allproduct.getProductCategoryName())) {
                        categories.add(allproduct.getProductCategoryName());
                        categoriesids.add(allproduct.getProductCategoryID() + "");
                    }
                }


                ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
                dataAdapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);
                searchCatSpn.setAdapter(dataAdapter1);
                dataAdapter1.notifyDataSetChanged();
                searchCatSpn.setSelection(0);


                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
                dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                searchSubcatSpn.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();
                searchSubcatSpn.setSelection(0);

                List<UOMArray> uomArray = Select.from(UOMArray.class).list();
//                Log.e("uomArray", ":::::" + uomArray.size());
                if (uomArray != null && uomArray.size() > 0) {
                    for (UOMArray uomArray1 : uomArray) {
//                    Log.e("uomArray", ":::::for");

                        if (listUomArray.contains(uomArray1.getUOM())) {
//                        Log.e("uomArray", ":::::if");

                        } else {

//                        Log.e("uomArray", ":::::else");


                            listUomArray.add(uomArray1.getUOM() + "");
                            listUomIdArray.add(uomArray1.getUOMID());
                        }
                    }
                }

                if (backToCheckIn && !addProductFlag) {
                    List<AllProduct> allProductList = new ArrayList<>();
                    allProductList.addAll(listAllProduct);
                    listAllProduct.clear();
                    for (AllProduct allProduct : allProductList) {
                        if (productIdList.contains(allProduct.getProductID())) {
                            listAllProduct.add(allProduct);
                        }
                    }
                }

                mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                productRv.setAdapter(mAdapter);


            } else {
                Toast.makeText(baseActivity, "No Product data available", Toast.LENGTH_SHORT).show();
            }

//            if (!isAllVariant) {
//                getAllvariants();
//            }

        }


    }

    public void getAllProduct() {

        if (mContext != null && getActivity() != null && isAdded())
            showDialog(mContext);

        isAllProduct = true;
        brandSpinnerLayout.setVisibility(View.VISIBLE);
        brandName = "Select Brand";


        if (!categories.contains("Select Category")) {
            categories.add(getResources().getString(R.string.select_category));
        }
        if (!brandArrayList.contains("Select Brand")) {
            brandArrayList.add("Select Brand");
        }
        // showDialog();
//            Log.e("showDialog()","2");


        //AllProduct.deleteAll(AllProduct.class);

        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        String arr = sharedPreference.getStringValue("productList");
        AllProduct.deleteAll(AllProduct.class);
        sharedPreference.saveBooleanValue("IsProductSearchFrag", true);
        try {
            //hideDialog();
            JSONArray jsonArr = new JSONArray(arr);
            for (int i = 0; i < jsonArr.length(); i++) {
                AllProduct alproduct = builder.fromJson(jsonArr.get(i).toString(), AllProduct.class);
                if (alproduct.getDefaultPrice() == null) {
                    alproduct.setDefaultPrice(0.0);
                }
                if (alproduct.getSalePrice() == null) {
                    alproduct.setSalePrice(0.0);
                }
                if (alproduct.getPurchasePrice() == null) {
                    alproduct.setPurchasePrice(0.0);
                }

                if (alproduct.getProductCategoryID() != 0) {
                    for (int j = 0; j < categories.size(); j++) {
                        String categoryName = categories.get(j);
                        categories.contains(categoryName);
                        if (!categories.contains(alproduct.getProductCategoryName())) {
                            categoriesids.add(alproduct.getProductCategoryID() + "");
                            categories.add(alproduct.getProductCategoryName() + "");
                        }
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    searchCatSpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();

                }


                if (alproduct.getBrandID() != 0) {

                    for (int k = 0; k < brandArrayList.size(); k++) {
                        if (!brandArrayList.contains(alproduct.getBrandName())) {
                            brandArrayList.add(alproduct.getBrandName());

                            try {
                                brandIdList.add(alproduct.getBrandID());


                            } catch (Exception ec) {
                                ec.printStackTrace();


                            }
                        }


                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    searchSubcatSpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();
                }

                alproduct.save();
            }
            listAllProduct = AllProduct.listAll(AllProduct.class);
            List<UOMArray> uomArray = Select.from(UOMArray.class).list();
            for (UOMArray uomArray1 : uomArray) {
                if (listUomArray.contains(uomArray1.getUOM())) {

                } else {
                    listUomArray.add(uomArray1.getUOM() + "");
                    listUomIdArray.add(uomArray1.getUOMID());
                }
            }


            Log.e(TAG, " getAllProduct backToCheckIn " + backToCheckIn);


            if (backToCheckIn && !addProductFlag) {

                /*if (getArguments() != null && getArguments().containsKey("add_product") && getArguments().getBoolean("add_product", false)) {

                    List<AllProduct> allProductList = new ArrayList<>();
                    allProductList = AllProduct.listAll(AllProduct.class);

                    mAdapter.setListProduct(allProductList, listUomArray, listUomIdArray);
                    //productRv.setAdapter(mAdapter);
                    productRv.getLayoutManager().scrollToPosition(CommonUtils.position);
                    searchCatSpn.setSelection(0);
                    searchSubcatSpn.setSelection(0);
                    mAdapter.notifyDataSetChanged();
                } else {

                }*/

                List<AllProduct> allProductList = new ArrayList<>();
                allProductList.addAll(listAllProduct);
                listAllProduct.clear();
                for (AllProduct allProduct : allProductList) {
                    if (productIdList.contains(allProduct.getProductID())) {
                        listAllProduct.add(allProduct);
                    }
                }
                mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                //productRv.setAdapter(mAdapter);
                productRv.getLayoutManager().scrollToPosition(CommonUtils.position);
                searchCatSpn.setSelection(0);
                searchSubcatSpn.setSelection(0);
                mAdapter.notifyDataSetChanged();

            } else {
                mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                //productRv.setAdapter(mAdapter);
                productRv.getLayoutManager().scrollToPosition(CommonUtils.position);
                searchCatSpn.setSelection(0);
                searchSubcatSpn.setSelection(0);
                mAdapter.notifyDataSetChanged();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        hideDialog();

        if (!isAllVariant) {
            getAllvariants();

        }

        if (listAllProduct != null && listAllProduct.size() == 0)
            SecurityLogsApi(); //getAllProductRefresh();
    }


    public void onBackPressed() {
//        Log.e("onBackPressed","::true");
        CommonUtils.position = 0;
        if (backToCheckIn) {
//            Intent intent = new Intent(baseActivity, GoalsActivities.class);
//            intent.putExtra("nameActivity", "CheckPriOrderFragment");
//            startActivity(intent);
//            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_enter);
          /*  FragmentManager fm = getActivity()
                    .getSupportFragmentManager();
            actionBarTitle.setText("BUSINESS");
            fm.popBackStack("productsearch", FragmentManager.POP_BACK_STACK_INCLUSIVE);*/

            Intent intent = new Intent(baseActivity, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);

        } else if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("business")) {
//            Log.e("onBackPressed","::1");


            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusinessDashBoard");
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_enter);
        } else if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("CheckinBusiness")) {
            Intent intent = new Intent(getContext(), GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusinessDashBoard");
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_enter);
        } else {
//            Log.e("onBackPressed","::3");
            if (CommonUtils.lastFragment.equalsIgnoreCase("goalFragment")) {
                Intent intent = new Intent(baseActivity, GoalsActivities.class);
                intent.putExtra("isfrom", "myGoal");
                intent.putExtra("nameActivity", "orderhistory");
                startActivity(intent);
            } else {
                Intent intent = new Intent(baseActivity, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
            }
        }
    }

    public void productClick(View v, AllProduct allProduct, Boolean inStock)
    {


    }

    @Override
    public void productClick(View v, int position, Boolean inStock, AllProduct allProduct)
    {
        //getMeasurementValues();
        int itemPosition = position;
        CommonUtils.position = itemPosition;
        Intent intent = new Intent(baseActivity, GoalsActivities.class);

        if (mSearchView.getQuery().toString().equals("")) {

        } else {
            mAdapter.filterList.get(itemPosition).getProductName();
            intent.putExtra("product_name", allProduct.getProductName());
        }
        intent.putExtra("product_location", itemPosition);
        intent.putExtra("product_id", allProduct.getProductID());
        intent.putExtra("defaultID", allProduct.getDefaultUOMID());
        intent.putExtra("available", inStock);
        intent.putExtra("product_name", allProduct.getProductName());


        if (backToCheckIn)
            intent.putExtra("backoncheckin", "true");
        else
            intent.putExtra("backoncheckin", "false");


//        Log.e("UOM_ID", "::on click product" + listAllProduct.get(itemPosition).getDefaultUOMID());
        if (searchSubcatSpn.getSelectedItem() != null) {
            if (searchSubcatSpn.getSelectedItem().equals("Select Brand")) {
                intent.putExtra("brand_name", "");
            } else {
                intent.putExtra("brand_name", "" + brandName);
            }
        } else {
            intent.putExtra("brand_name", "");
        }
        if (searchCatSpn.getSelectedItem() != null) {
            if (searchCatSpn.getSelectedItem().equals("Select Category")) {
                intent.putExtra("category_id", "0");
                mAdapter.filterList.get(itemPosition).getProductName();
                if (!searchSubcatSpn.getSelectedItem().equals("Select Brand")) {
                    intent.putExtra("product_name", allProduct.getProductName());
                }
            } else {
                intent.putExtra("category_id", categoryID + "");
            }
        } else {
            intent.putExtra("category_id", categoryID + "");
        }
        intent.putExtra("nameActivity", "ProductDetail");
        startActivity(intent);
    }

    @Override
    public void notAvailableDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_not_available);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        AppCompatButton dialogButton = (AppCompatButton) dialog.findViewById(R.id.btnOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.filter(newText);
        return true;
    }

    public void getMeasurementValues() {

        //showDialog();
//        Log.e("showDialog()","3");

        UOMArray.deleteAll(UOMArray.class);
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(baseActivity, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getAllMeasurement(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                try {
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        UOMArray uomArray = builder.fromJson(jsonArr.get(i).toString(), UOMArray.class);
                        uomArray.save();
                    }

                    // hideDialog();

                } catch (Exception ex) {
                    ex.printStackTrace();

                    //hideDialog();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();

                //hideDialog();
//                if (AllProduct.listAll(AllProduct.class).size() != 0) {
//                    listAllProduct = AllProduct.listAll(AllProduct.class);
//                    mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
//                    productRv.setAdapter(mAdapter);
//
//                    for (AllProduct allproduct : listAllProduct) {
//                        if (!categories.contains(allproduct.getProductCategoryName())) {
//                            categories.add(allproduct.getProductCategoryName());
//                            categoriesids.add(allproduct.getProductCategoryID()+"");
//                        }
//                    }
//
//
//                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.single_text_item, categories);
//                    dataAdapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                    searchCatSpn.setAdapter(dataAdapter1);
//                    dataAdapter1.notifyDataSetChanged();
//                    searchCatSpn.setSelection(0);
//
//
//                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.single_text_item, brandArrayList);
//                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//                    searchSubcatSpn.setAdapter(dataAdapter);
//                    dataAdapter.notifyDataSetChanged();
//                    searchSubcatSpn.setSelection(0);
//
//                } else {
//                    Toast.makeText(getContext(), "No data avilable", Toast.LENGTH_SHORT).show();
//                }
            }
        });


//
//        CommonUtils.dismissProgress();
    }

    public void getAllvariants() {
        isAllVariant = true;

        // showDialog();
//        Log.e("showDialog()","4");
        if (Connectivity.isNetworkAvailable(baseActivity)) {

            AllVariants.deleteAll(AllVariants.class);
            VariantProduct.deleteAll(VariantProduct.class);


            final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
            ServiceHandler serviceHandler = NetworkManager.createRetrofitService(baseActivity, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.getAllVariants(new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    // hideDialog();
                    String arr = CommonUtils.getServerResponse(response);


                    AllVariants.deleteAll(AllVariants.class);
                    VariantProduct.deleteAll(VariantProduct.class);

                    try {
                        JSONArray jsonArr = new JSONArray(arr);
                        for (int i = 0; i < jsonArr.length(); i++) {

                            AllVariants allVariants = builder.fromJson(jsonArr.get(i).toString(), AllVariants.class);
                            JSONObject jsonObject = (JSONObject) jsonArr.get(i);

                            if (jsonObject.has("product")) {
                                if (!jsonObject.isNull("product")) {


                                    VariantProduct variantProduct = builder.fromJson(jsonObject.getJSONObject("product").toString(), VariantProduct.class);
                                    variantProduct.save();
                                }

                            }
                            allVariants.save();

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();


                        // hideDialog();
                    }
                }


                @Override
                public void failure(RetrofitError error) {

                    if (error.getMessage().equals("timeout")) {
                        getAllvariants();
                    } else {


                        //hideDialog();
                    }


                    //hideDialog();
                }

            });

        } else {


            //hideDialog();

        }


    }


    boolean isAllProduct = false, isAllVariant = false;


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.e(TAG, " NetworkChangeReceiver ");


            if (Connectivity.isNetworkAvailable(context)) {
                if (!isConnected) {
                    isConnected = true;
                    sharedPreference.saveBooleanValue("IsProductSearchFrag", true);
                    // Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();

                    getMeasurementValues();
                    if (!isAllProduct) {
                        getAllProduct();

                    }
                    if (!isAllVariant) {
                        getAllvariants();
                    }
                    // if (ApplicationClass.getAppContext() != null)
                    //   LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);

                    //do your processing here ---
                    //if you need to post any data to the server or get status
                    //update from the server

                } else {
                    //if (ApplicationClass.getAppContext() != null)
                    //  LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);

                }
            } else {
                if (AllProduct.listAll(AllProduct.class).size() != 0) {
                    listAllProduct = AllProduct.listAll(AllProduct.class);
                    if (backToCheckIn) {
                        List<AllProduct> allProductList = new ArrayList<>();
                        allProductList.addAll(listAllProduct);
                        listAllProduct.clear();
                        for (AllProduct allProduct : allProductList) {
                            if (productIdList.contains(allProduct.getProductID())) {
                                listAllProduct.add(allProduct);
                            }
                        }
                    }
                    mAdapter.setListProduct(listAllProduct, listUomArray, listUomIdArray);
                    productRv.setAdapter(mAdapter);

                    for (AllProduct allproduct : listAllProduct) {
                        if (!categories.contains(allproduct.getProductCategoryName())) {
                            categories.add(allproduct.getProductCategoryName());
                            categoriesids.add(allproduct.getProductCategoryID() + "");
                        }
                    }


                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, categories);
                    dataAdapter1.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    searchCatSpn.setAdapter(dataAdapter1);
                    dataAdapter1.notifyDataSetChanged();
                    searchCatSpn.setSelection(0);


                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, brandArrayList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    searchSubcatSpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();
                    searchSubcatSpn.setSelection(0);

                } else {
                    Toast.makeText(baseActivity, "No data available", Toast.LENGTH_SHORT).show();
                }
                isConnected = false;

            }

        }
    }


    private void SecurityLogsApi() {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Products/GetAll");
                object.put("JsonObject", "null");
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    getAllProductRefresh();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            getAllProduct();
        }


    }
}
