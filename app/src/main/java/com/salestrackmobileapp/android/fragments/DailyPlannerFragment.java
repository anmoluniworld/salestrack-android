package com.salestrackmobileapp.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.MyGoalsAdapter;
import com.salestrackmobileapp.android.gson.GoalBusiness;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.salestrackmobileapp.android.activities.GoalsActivities.actionBarTitle;
import static com.salestrackmobileapp.android.activities.GoalsActivities.homeIconImg;

public class DailyPlannerFragment extends BaseFragment {
    @BindView(R.id.list_goals_rv)
    RecyclerView listGoalRv;

    @BindView(R.id.txtGoalBusiness)
    TextView txtGoalBusiness;
    private MyGoalsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    GoalBusiness goalBusiness;
    List<GoalBusiness> goalBusinessList;
    int itemPosition;
    String businessName, type;


    public DailyPlannerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daily_planner, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            if (getArguments().getSerializable("business_Name") == null) {

            } else {
                businessName = (String) getArguments().getSerializable("business_Name");
                type = (String) getArguments().getSerializable("type");
                txtGoalBusiness.setText(businessName);

                ApplicationClass.getInstance().setBusinessName(businessName);
                ApplicationClass.getInstance().setType(type);
            }
        } else {
            businessName = ApplicationClass.getInstance().getBusinessName();
            type = ApplicationClass.getInstance().getType();
            txtGoalBusiness.setText(businessName);
        }
        goalBusinessList = GoalBusiness.listAll(GoalBusiness.class);
        mLayoutManager = new LinearLayoutManager(getActivity());
        listGoalRv.setLayoutManager(mLayoutManager);
        listGoalRv.setNestedScrollingEnabled(false);
        mAdapter = new MyGoalsAdapter(baseActivity, this, businessName, type, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
        mAdapter.setListGoals();
        listGoalRv.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        return view;
    }

    public void productClick(GoalBusiness v, int position) {

        homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));
        actionBarTitle.setText("BUSINESS");

        goalBusiness = v;

        itemPosition = position;

        CheckInPriOrderFragment fragment = new CheckInPriOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("goal_position", itemPosition);
        bundle.putSerializable("goal_object", goalBusiness);
        bundle.putSerializable("business_id", goalBusiness.getBusinessID());

        sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, String.valueOf(goalBusiness.getBusinessID()));
        sharedPreference.saveStringData(PrefsHelper.GOAL_LOCATION, String.valueOf(itemPosition));
        fragment.setArguments(bundle);
        FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
        //ft.addToBackStack("checkinpriorder");
        ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void onBackPressed() {
        //homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.home_icon));
        //baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new MyGoalFragment()).commit();

        Intent intent = new Intent(baseActivity, GoalsActivities.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("nameActivity", "MyGoals");
        startActivity(intent);
        baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);

//        FragmentManager fm = getActivity()
//                .getSupportFragmentManager();
//        //actionBarTitle.setText("DAILY PLANNER");
//        fm.popBackStack ("dailypalnnerfragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}
