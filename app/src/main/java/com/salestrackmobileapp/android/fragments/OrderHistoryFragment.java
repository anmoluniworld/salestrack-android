package com.salestrackmobileapp.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.OrderHistoryAdapter;
import com.salestrackmobileapp.android.adapter.PendingOrderHistoryAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.Business;
import com.salestrackmobileapp.android.gson.OrderHistory;
import com.salestrackmobileapp.android.gson.OrderItem;
import com.salestrackmobileapp.android.gson.PendingOrderItem;
import com.salestrackmobileapp.android.gson.SaveOrder;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClickInterface;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.salestrackmobileapp.android.activities.GoalsActivities.actionBarTitle;
import static com.salestrackmobileapp.android.utils.PrefsHelper.ACCESS_TOKEN;

public class OrderHistoryFragment extends BaseFragment implements RecyclerClickInterface, SearchView.OnQueryTextListener {
    // TODO: Rename parameter arguments, choose names that match

    @BindView(R.id.order_history_items)
    RecyclerView orderHistoryProductRV;

    @BindView(R.id.order_pending_items)
    RecyclerView orderPendingProductRV;

    @BindView(R.id.pending_title)
    TextView pendingValuetxt;
    SearchView mSearchView;

    @BindView(R.id.null_value_txt)
    Custome_TextView nullValuetxt;
    List<SaveOrder> saveOrderProduct = new ArrayList<SaveOrder>();

    List<PendingOrderItem> pendingOrderItemList = new ArrayList<PendingOrderItem>();

//    @BindView(R.id.view)
//    View view;

    Context mContext;

    @BindView(R.id.ll_focus)
    RelativeLayout ll_focus;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayoutManager mLayoutManager, layoutManager;
    PendingOrderHistoryAdapter pendingOrderHistoryAdapter;
    OrderHistoryAdapter orderHistoryAdapter;
    CoordinatorLayout coordinateLayout;
    //private ProgressDialog mProgressDialog;
    Gson builder;
    ServiceHandler serviceHandler;

    List<OrderHistory> orderHistoriesList;
    private static final String LOG_TAG = "CheckNetworkStatus";
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    private int businessId;
    private Boolean isFromCart;



    String TAG = "OrderHistoryFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_place_order, container, false);
        ButterKnife.bind(this, view);
        coordinateLayout = (CoordinatorLayout) view.findViewById(R.id.coordinateLayout);
        mSearchView = (SearchView) view.findViewById(R.id.searchview);
        EditText searchEditText = (EditText) mSearchView.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.grey));
        searchEditText.clearFocus();
        Log.e(TAG, "onCreateView");

        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

        saveOrderProduct = Select.from(SaveOrder.class).where(Condition.prop("send_to_server").eq("0")).list();

        pendingOrderItemList = Select.from(PendingOrderItem.class).list();

        if (saveOrderProduct != null && pendingOrderItemList != null) {
            for (int i = 0; i < saveOrderProduct.size(); i++) {
                SaveOrder saveOrder = saveOrderProduct.get(i);
                List<PendingOrderItem> orderItemList = new ArrayList<PendingOrderItem>();

                for (int j = 0; j < pendingOrderItemList.size(); j++) {
                    if (pendingOrderItemList.get(j).getSaveOrderNumber().equalsIgnoreCase(saveOrder.getSaveOrderNumber())) {
                        orderItemList.add(pendingOrderItemList.get(j));
                    }
                }
                saveOrder.setPendingOrderItems(orderItemList);
            }
        }


        mLayoutManager = new LinearLayoutManager(baseActivity);
        layoutManager = new LinearLayoutManager(baseActivity);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        pendingOrderHistoryAdapter = new PendingOrderHistoryAdapter(baseActivity, this, orderPendingProductRV);
        orderPendingProductRV.setLayoutManager(mLayoutManager);
        orderHistoryAdapter = new OrderHistoryAdapter(baseActivity, this, orderHistoryProductRV);
        orderHistoryProductRV.setLayoutManager(layoutManager);

        pendingOrderHistoryAdapter.setSaveOrderList(saveOrderProduct);
        orderPendingProductRV.setAdapter(pendingOrderHistoryAdapter);


        if (saveOrderProduct.size() != 0) {
            nullValuetxt.setVisibility(View.GONE);
            orderPendingProductRV.setVisibility(View.VISIBLE);
            requestHttp(saveOrderProduct);

        } else {
            nullValuetxt.setVisibility(View.VISIBLE);
            orderPendingProductRV.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            orderHistoryProductRV.setLayoutParams(lp);
        }

        if (Connectivity.isNetworkAvailable(getActivity())) {
            //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_offline));
            pendingValuetxt.setVisibility(View.GONE);
            // isConnected = true;
        } else {
            //ivInternet.setImageDrawable(getResources().getDrawable(R.drawable.ic_online));
            pendingValuetxt.setVisibility(View.VISIBLE);
            //isConnected = false;
        }


        if (getArguments() != null) {
            if (getArguments().getInt("businessId", 0) != 0) {
                businessId = getArguments().getInt("businessId", 0);
            }
        }
        if (getArguments() != null) {
            isFromCart = getArguments().getBoolean("isFromCart");
        }


        if (orderHistoriesList != null) {
            if (orderHistoriesList.size() == 0 || saveOrderProduct.size() == 0) {
                nullValuetxt.setVisibility(View.VISIBLE);
            }
        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        try {
            if (getActivity() != null && isAdded())
                getActivity().registerReceiver(receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setupSearchView();


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //getOrderHistorAfterCartAddedyArray();
                SecurityLogsApi();
                swipeRefreshLayout.setRefreshing(false);
            }

        });
        showDialog(mContext);
        //SecurityLogsApi();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (isFromCart)
                    SecurityLogsApi();//getOrderHistorAfterCartAddedyArray();
                else
                    getOrderHistoryArray();
            }
        }, 1000);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {
            if (getActivity() != null && receiver != null)
                getActivity().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SecurityLogsApi() {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Order/GetAll");
                object.put("JsonObject", "null");
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER+" "+Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);
                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    getOrderHistorAfterCartAddedyArray();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            getOrderHistoryArray();
        }


    }

    private void getOrderHistorAfterCartAddedyArray() {
        Log.e(TAG, " getOrderHistorAfterCartAddedyArray ");
        if (mContext != null)
            showDialog(mContext);
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            if (OrderHistory.listAll(OrderHistory.class).size() != 0) {
                OrderHistory.deleteAll(OrderHistory.class);
            }
            if (Business.listAll(Business.class).size() != 0) {
                Business.deleteAll(Business.class);
            }


            serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            serviceHandler.getOrderHistory("NEW", new Callback<Response>() {
                @Override
                public void success(Response response, Response response2) {
                    String arr = CommonUtils.getServerResponse(response);
                    sharedPreference.saveStringData("orderList", arr);
                    sharedPreference.saveBooleanValue("IsOrderHistoryFrag", true);

                    try {
                        JSONArray json = new JSONArray(arr);
                        int count = json.length() - 1;
                        for (int i = 0; i < json.length(); i++) {
                            JSONObject jsonObject = json.getJSONObject(i);
                            System.out.println(jsonObject.getString("OrderItems"));
                            if (jsonObject.getJSONArray("OrderItems").length() > 0) {
                                if (businessId != 0) {
                                    if (businessId == jsonObject.getInt("BusinessID")) {
                                        OrderHistory orderHistory = builder.fromJson(json.get(i).toString(), OrderHistory.class);
                                        List<OrderItem> list = orderHistory.getPendingOrderItems();

                                        Business business = builder.fromJson(jsonObject.getJSONObject("Business").toString(), Business.class);
                                        orderHistory.setOrderPositions(count);
                                        orderHistory.save();
                                        business.save();
                                        count--;
                                    }
                                } else {
                                    OrderHistory orderHistory = builder.fromJson(json.get(i).toString(), OrderHistory.class);
                                    List<OrderItem> list = orderHistory.getPendingOrderItems();

                                    Business business = builder.fromJson(jsonObject.getJSONObject("Business").toString(), Business.class);
                                    orderHistory.setOrderPositions(count);
                                    orderHistory.save();
                                    business.save();
                                    count--;
                                }
                            }
                        }
                        orderHistoriesList = Select.from(OrderHistory.class).orderBy("id Desc").list();

                        if (orderHistoriesList.size() != 0) {
                            orderHistoryProductRV.setVisibility(View.VISIBLE);
                            orderHistoryAdapter.setSaveOrderList(orderHistoriesList);
                            orderHistoryProductRV.setAdapter(orderHistoryAdapter);
                            orderHistoryAdapter.notifyDataSetChanged();
                        } else {
                            // Toast.makeText(baseActivity, "No server order history available", Toast.LENGTH_SHORT).show();
                            hideDialog();
                        }

                        orderHistoryAdapter.notifyDataSetChanged();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideDialog();
                            }
                        }, 1500);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideDialog();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();

//                    Log.e("Exception","::::"+error.toString());
                    try {
                        // Toast.makeText(getActivity(), "History not found", Toast.LENGTH_SHORT).show();
//                        if (mProgressDialog != null && mProgressDialog.isShowing())
//                            mProgressDialog.dismiss();

                        orderHistoriesList = Select.from(OrderHistory.class).orderBy("id Desc").list();
                        if (orderHistoriesList.size() != 0) {
                            orderHistoryProductRV.setVisibility(View.VISIBLE);
                            orderHistoryAdapter.setSaveOrderList(orderHistoriesList);
                            orderHistoryProductRV.setAdapter(orderHistoryAdapter);
                            orderHistoryAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(baseActivity, "No server order history available", Toast.LENGTH_SHORT).show();
                            // hideDialog();
                        }
                        //hideDialog();

                        orderHistoryAdapter.notifyDataSetChanged();


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    hideDialog();
                }
            });
        } else {

//            Log.e("ELSE_api","::::::");
//            if (!sharedPreference.getBooleanValue("IsOrderHistoryFrag")){
//                Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//            }
            //  else{


            orderHistoriesList = Select.from(OrderHistory.class).orderBy("id Desc").list();
            if (orderHistoriesList.size() != 0) {
                orderHistoryProductRV.setVisibility(View.VISIBLE);
                orderHistoryAdapter.setSaveOrderList(orderHistoriesList);
                orderHistoryProductRV.setAdapter(orderHistoryAdapter);
                orderHistoryAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(baseActivity, "No server order history available", Toast.LENGTH_SHORT).show();
                //hideDialog();
            }
            hideDialog();

            orderHistoryAdapter.notifyDataSetChanged();


        }


    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(true);
//        mSearchView.setFocusableInTouchMode(true);
        mSearchView.setQueryHint("Search by business");
        mSearchView.clearFocus();
//        mSearchView.requestFocusFromTouch();
    }

    @Override
    public void productClick(View v, int position, RecyclerView recyclerView) {
        if (recyclerView.equals(orderPendingProductRV)) {

            int itemPosition = orderPendingProductRV.getChildPosition(v);
            SaveOrder saveOrder = saveOrderProduct.get(itemPosition);
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "orderPendingDetailFragment");
            intent.putExtra("lorderNumber", Integer.parseInt(saveOrder.getOrderNo()));
            intent.putExtra("orderPosition", itemPosition);
            intent.putExtra("saveorderObj", saveOrder);
            startActivity(intent);
        } else if (recyclerView.equals(orderHistoryProductRV)) {
            int itemPosition = orderHistoryProductRV.getChildPosition(v);
            OrderHistory orderHistory = orderHistoriesList.get(itemPosition);
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "orderDetailFragment");
            intent.putExtra("orderId", orderHistory.getOrderID());
            intent.putExtra("orderhistoryObject", orderHistory);
            intent.putExtra("orderPosition", itemPosition);
            intent.putExtra("subTotal", String.valueOf(new DecimalFormat("##.##").format(orderHistory.getSubTotal())));
            intent.putExtra("tax", String.valueOf(new DecimalFormat("##.##").format(orderHistory.getTaxAmount())));
            intent.putExtra("ordertotal", String.valueOf(new DecimalFormat("##.##").format(orderHistory.getTotalOrderValue())));
            startActivity(intent);
        }
    }


    public void onBackPressed() {
//        Log.e("OnBAckPressed",":::::True");
        if (CommonUtils.lastFragment.equalsIgnoreCase("goalFragment")) {
            CommonUtils.lastFragment = "";
            actionBarTitle.setText("BUSINESS");
            CheckInPriOrderFragment fragment = new CheckInPriOrderFragment();
            Bundle bundle = new Bundle();

            fragment.setArguments(bundle);
            FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
            ft.replace(R.id.container, fragment);
            ft.commit();
        } else {
            Intent intent = new Intent(baseActivity, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
        }
    }

    ArrayList<Business> businesslist = new ArrayList<>();

    public void getOrderHistoryArray() {

        Log.e(TAG, " getOrderHistoryArray ");


        //if (mContext != null)
        //  showDialog(mContext);
        try {
            if (OrderHistory.listAll(OrderHistory.class).size() != 0) {
                OrderHistory.deleteAll(OrderHistory.class);
            }
            if (Business.listAll(Business.class).size() != 0) {
                Business.deleteAll(Business.class);
            }


            builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

            String arr = sharedPreference.getStringValue("orderList");
            if (arr != null || !arr.isEmpty()) {
                sharedPreference.saveBooleanValue("IsOrderHistoryFrag", true);

                try {
                    JSONArray json = new JSONArray(arr);
                    int count = json.length() - 1;
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObject = json.getJSONObject(i);
                        System.out.println(jsonObject.getString("OrderItems"));
                        if (businessId != 0) {
                            if (businessId == jsonObject.getInt("BusinessID")) {
                                OrderHistory orderHistory = builder.fromJson(json.get(i).toString(), OrderHistory.class);
                                List<OrderItem> list = orderHistory.getPendingOrderItems();

                                Business business = builder.fromJson(jsonObject.getJSONObject("Business").toString(), Business.class);
                                orderHistory.setOrderPositions(count);
                                orderHistory.save();
                                business.save();
                                count--;
                            }
                        } else {
                            if (jsonObject.getJSONArray("OrderItems").length() > 0) {
                                OrderHistory orderHistory = builder.fromJson(json.get(i).toString(), OrderHistory.class);
                                List<OrderItem> list = orderHistory.getPendingOrderItems();

                                Business business = builder.fromJson(jsonObject.getJSONObject("Business").toString(), Business.class);
                                orderHistory.setOrderPositions(count);
                                orderHistory.save();
                                business.save();
                                count--;
                            }
                        }
                    }
                    orderHistoriesList = Select.from(OrderHistory.class).orderBy("id Desc").list();

                    if (orderHistoriesList.size() != 0) {
                        orderHistoryProductRV.setVisibility(View.VISIBLE);
                        orderHistoryAdapter.setSaveOrderList(orderHistoriesList);
                        orderHistoryProductRV.setAdapter(orderHistoryAdapter);
                        orderHistoryAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(baseActivity, "No server order history available", Toast.LENGTH_SHORT).show();
                    }

                    orderHistoryAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                orderHistoriesList = Select.from(OrderHistory.class).orderBy("id Desc").list();
                if (orderHistoriesList.size() != 0) {
                    orderHistoryProductRV.setVisibility(View.VISIBLE);
                    orderHistoryAdapter.setSaveOrderList(orderHistoriesList);
                    orderHistoryProductRV.setAdapter(orderHistoryAdapter);
                    orderHistoryAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(baseActivity, "No server order history available", Toast.LENGTH_SHORT).show();
                }
                orderHistoryAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        hideDialog();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        orderHistoryAdapter.filter(newText);
        pendingOrderHistoryAdapter.filter(newText);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            View currentFocusedView = activity.getCurrentFocus();
            if (currentFocusedView != null) {
                inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.e("OrderHistoryFragment", "Receieved notification about network status");
            if (Connectivity.isNetworkAvailable(context)) {
                pendingValuetxt.setVisibility(View.GONE);

                if (!isConnected) {
                    // Log.v(LOG_TAG, "Now you are connected to Internet!");
                    Log.e("OrderHistoryFragment", " Now you are connected to Internet!");

                    if (saveOrderProduct != null)
                        Log.e("OrderHistoryFragment", " saveOrderProduct " + saveOrderProduct.size());

                    //isConnected = true;
                    sharedPreference.saveBooleanValue("IsOrderHistoryFrag", true);
                    //  Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                    if (saveOrderProduct.size() != 0) {
                        requestHttp(saveOrderProduct);
                    }
                    if (ApplicationClass.getAppContext() != null)
                        LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);
                    //do your processing here ---
                    //if you need to post any data to the server or get status
                    //update from the server
                } else {
//                    Log.e("*******************","No Internet");

                }
            } else {
                //hideDialog();
//                Log.e("*******************","No Internet");
//                Log.e("*******************","#######"+sharedPreference.getBooleanValue("IsOrderHistoryFrag"));
//            if (!sharedPreference.getBooleanValue("IsOrderHistoryFrag")){
//                Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//            }
//            else{
                // getOrderHistorAfterCartAddedyArray();
                pendingValuetxt.setVisibility(View.VISIBLE);

                //  }

                Log.d("Internet not Connected", "??????");

                Log.v(LOG_TAG, "You are not connected to Internet!");
                //   Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
                //isConnected = false;
            }

//            view.clearFocus();

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v(LOG_TAG, "Now you are connected to Internet!");
                                isConnected = true;
                                sharedPreference.saveBooleanValue("IsOrderHistoryFrag", true);
                                //  Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                                if (saveOrderProduct.size() != 0) {
                                    requestHttp(saveOrderProduct);
                                }
                                if (ApplicationClass.getAppContext() != null)
                                    LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);
                                //do your processing here ---
                                //if you need to post any data to the server or get status
                                //update from the server
                            } else {
//                                Log.e("*******************","No Internet");
//                                Log.e("*******************","#######"+sharedPreference.getBooleanValue("IsOrderHistoryFrag"));
//                                if (!sharedPreference.getBooleanValue("IsOrderHistoryFrag")){
//                                    Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//                                }

                                Log.d("Internet not Connected", "??????");
                            }
                            return true;
                        }
                    }
                }
            }

            //hideDialog();
//            Log.e("*******************","No Internet");
//            Log.e("*******************","#######"+sharedPreference.getBooleanValue("IsOrderHistoryFrag"));
//            if (!sharedPreference.getBooleanValue("IsOrderHistoryFrag")){
//                Toast.makeText(getActivity(),"You must visit every screen once after first time login with internet connected.",Toast.LENGTH_SHORT).show();
//
//            }
//            else{
            getOrderHistoryArray();
            //  }

            Log.d("Internet not Connected", "??????");

            Log.v(LOG_TAG, "You are not connected to Internet!");
            //   Toast.makeText(context, "not Connected", Toast.LENGTH_SHORT).show();
            isConnected = false;
            return false;
        }

    }

    private void requestHttp(List<SaveOrder> listSaveorderArray) {
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(ACCESS_TOKEN), NetworkManager.BASE_URL);
        final List<SaveOrder> listSaveOrderListArray = listSaveorderArray;
        serviceHandler.placeOrder(listSaveorderArray, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                String serverResponse = CommonUtils.getServerResponse(response);
                Log.e("OrderHistoryFragment", " serverResponse " + serverResponse);

                try {
                    JSONObject jsonObject1 = new JSONObject(serverResponse);
                    if (jsonObject1.getString("Message").equals("Success")) {
                        for (SaveOrder saveOrder : listSaveOrderListArray) {
                            saveOrder.setSendToServer("1");
                            saveOrder.save();
                        }

                        PendingOrderItem.deleteAll(PendingOrderItem.class);
                        SaveOrder.deleteAll(SaveOrder.class);

                        nullValuetxt.setVisibility(View.VISIBLE);
                        orderPendingProductRV.setVisibility(View.GONE);
                        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                        orderHistoryProductRV.setLayoutParams(lp);
                        SecurityLogsApi();
                        //getOrderHistorAfterCartAddedyArray();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("OrderHistoryFragment", " failure error " + error.getMessage());

                error.printStackTrace();
                Toast.makeText(baseActivity, "Please check your internet connectivity", Toast.LENGTH_SHORT).show();
            }
        });
        //  CommonUtils.dismissProgress();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onDestroy() {
        hideDialog();
        super.onDestroy();

    }
}
