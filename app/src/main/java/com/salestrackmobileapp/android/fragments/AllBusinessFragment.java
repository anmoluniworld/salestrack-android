package com.salestrackmobileapp.android.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.AddBusinessActivity;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.AllBusiAdapter;
import com.salestrackmobileapp.android.custome_views.MovableFloatingActionButton;
import com.salestrackmobileapp.android.gson.AddBusiness;
import com.salestrackmobileapp.android.gson.AllBusiness;
import com.salestrackmobileapp.android.gson.CheckedDataHit;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.singleton.Singleton;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;
import com.salestrackmobileapp.android.utils.SwipeController;
import com.salestrackmobileapp.android.utils.SwipeControllerActions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AllBusinessFragment extends BaseFragment implements RecyclerClick, SearchView.OnQueryTextListener {

    String TAG = "AllBusinessFragment";

    @BindView(R.id.buisness_rv)
    RecyclerView buisnessRV;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.addBusiness)
    MovableFloatingActionButton addBusiness;

    private Context context;

    private AllBusiAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;

    @BindView(R.id.linearLayoutSpinner)
    LinearLayout linearLayoutSpinner;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    //private ProgressDialog mProgressDialog;

    String navigateToProductDetail;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    IntentFilter filter;

    static Boolean checkedInStatus = false;


    @BindView(R.id.searchview)
    SearchView searchview;

    @BindView(R.id.state_spn)
    Spinner stateSpn;

    @BindView(R.id.city_spn)
    Spinner citySpn;

    static List<String> statesList = new ArrayList<String>();
    static List<String> cityList = new ArrayList<String>();

    static String cityNameSt = "Select City", stateNameSt = "Select State";
    GoalsActivities activity;

    public List<AllBusiness> listAllBusiness = new ArrayList<>();

    SwipeController swipeController;

    boolean fromAddBusiness;


    public AllBusinessFragment() {
        // Required empty public constructor
    }


    @OnClick(R.id.addBusiness)
    public void clickedBusiness() {
       /* FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        GoalsActivities.homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));
        GoalsActivities.cartImg.setVisibility(View.GONE);
        GoalsActivities.total_items_bracket.setVisibility(View.GONE);
        AddBusinessActivity fragment = new AddBusinessActivity();
        ft.replace(R.id.container, fragment);
        ft.commit();*/
        Intent intent = new Intent(context, AddBusinessActivity.class);
        intent.putExtra("function", "addBusiness");
        startActivity(intent);
        /*context.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_business, container, false);
        ButterKnife.bind(this, view);

        if (sharedPreference.getBooleanValue("UserLogin")) {
            addBusiness.setVisibility(View.VISIBLE);
        } else addBusiness.setVisibility(View.VISIBLE);
        //addBusiness.setVisibility(View.VISIBLE);

        //CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) addBusiness.getLayoutParams();
        //addBusiness.setCoordinatorLayout(lp);
        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        mLayoutManager = new LinearLayoutManager(baseActivity);
        mAdapter = new AllBusiAdapter(baseActivity, this, sharedPreference.getBooleanValue("UserLogin"));
        buisnessRV.setLayoutManager(mLayoutManager);
        buisnessRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = buisnessRV.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    Log.i("Yaeye!", "end called");
                    loading = true;
                }
            }
        });

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                Intent i = new Intent(context, AddBusinessActivity.class);
                i.putExtra("BusinessID", mAdapter.getBusinessObject(position).getBusinessID());
                i.putExtra("function", "edit");
                startActivity(i);
            }

            @Override
            public void onLeftClicked(int position) {
                //super.onLeftClicked(position);

                Intent i = new Intent(context, AddBusinessActivity.class);
                i.putExtra("BusinessID", mAdapter.getBusinessObject(position).getBusinessID());
                i.putExtra("function", "edit");
                startActivity(i);
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(buisnessRV);

        buisnessRV.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });

        setupSearchView();


        EditText searchEditText = (EditText) searchview.findViewById(androidx.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.grey));

        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();

        try {
            if (isAdded() && getActivity() != null)
                getActivity().registerReceiver(receiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // getActivity().registerReceiver(receiver, filter);

        if (getArguments() != null) {
            navigateToProductDetail = getArguments().getString("navigateToProduct");
            fromAddBusiness = getArguments().getBoolean("addBusiness", false);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //if (ApplicationClass.getAppContext() != null)
                //  LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).registerReceiver(receiver, filter);
                SecurityLogsApi();

                swipeRefreshLayout.setRefreshing(false);
                //getAllBusinessAfterAdd();


            }
        });

        showDialog(context);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                init();
            }
        }, 1000);


        return view;

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, " onPause ");

        try {
            if (getActivity() != null)
                getActivity().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void init() {
        showDialog(context);
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            if (!isAsyncTask) {
                if (!cityList.contains("Select City")) {
                    cityList.add("Select City");
                }

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, cityList);
                dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                citySpn.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();
                citySpn.setSelection(0);

                if (!statesList.contains("Select State")) {
                    statesList.add("Select State");
                }

                ArrayAdapter<String> dataAdapters = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                dataAdapters.setDropDownViewResource(R.layout.spinner_dropdown_item);
                stateSpn.setAdapter(dataAdapters);
                dataAdapters.notifyDataSetChanged();
                stateSpn.setSelection(0);

                if (sharedPreference.getBooleanValue("refresh"))
                    SecurityLogsApi(); //getAllBusinessAfterAdd();
                else
                    getAllBusiness();
            }
        } else {

            getAllBusiness();

            if (!cityList.contains("Select City")) {
                cityList.add("Select City");
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, cityList);
            dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            citySpn.setAdapter(dataAdapter);
            dataAdapter.notifyDataSetChanged();
            citySpn.setSelection(0);
//            if (!cityList.contains("Select City")) {
//                cityList.add("Select City");
//            }
            if (!statesList.contains("Select State")) {
                statesList.add("Select State");
            }

            ArrayAdapter<String> dataAdapters = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
            dataAdapters.setDropDownViewResource(R.layout.spinner_dropdown_item);
            stateSpn.setAdapter(dataAdapters);
            dataAdapters.notifyDataSetChanged();
            stateSpn.setSelection(0);


            listAllBusiness = AllBusiness.listAll(AllBusiness.class);
            Log.e(TAG, "::::size::::" + listAllBusiness.size());


            if (listAllBusiness != null && listAllBusiness.size() > 0) {


                for (int k = 0; k < listAllBusiness.size(); k++) {
                    AllBusiness business = listAllBusiness.get(k);
                    if (business.getCity() != null) {
                        if (!cityList.contains(business.getCity())) {
                            cityList.add(business.getCity() + "");
                        }
                    }

                    if (business.getState() != null) {
                        if (!statesList.contains(business.getState())) {
                            statesList.add(business.getState() + "");
                        }
                    }
                }

                if (statesList.size() != 0) {
                    dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    stateSpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();
                    stateSpn.setSelection(0);

                }


            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                builder1.setMessage("Sorry..Detail not available.Please contact your Administrator");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //    Toast.makeText(getContext(), "Back is pressed", Toast.LENGTH_LONG).show();

                    }
                });
                builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //  getAllBusiness();
                        getAllBusinessAfterAdd();
                    }
                });
                builder1.show();
            }
            //getAllBusinessAfterAdd();
        }

        stateSpn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                return false;
            }
        });

        stateSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e(TAG, " position " + position);

                showDialog(context);

                cityList.clear();
                cityList.add("Select City");
                citySpn.setSelection(0);

                if (position >= 1) {
                    stateNameSt = statesList.get(position);
                    List<AllBusiness> listProduct = Select.from(AllBusiness.class).where(Condition.prop("state").eq(stateNameSt)).list();
                    for (int k = 0; k < listProduct.size(); k++) {
                        if (!cityList.contains(listProduct.get(k).getCity())) {
                            cityList.add(listProduct.get(k).getCity());
                        }
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, cityList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    citySpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();
                    citySpn.setSelection(0);

                    cityNameSt = "";
                    if (stateNameSt.equals("Select State")) {
                        listAllBusiness = AllBusiness.listAll(AllBusiness.class);
                        mAdapter.setAllBusinessList(listAllBusiness);
                        buisnessRV.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        viewAllBusiness();
                    }


                } else {

//                    listAllBusiness = Select.from(AllBusiness.class).list();
                    listAllBusiness = AllBusiness.listAll(AllBusiness.class);

                    mAdapter.setAllBusinessList(listAllBusiness);
                    buisnessRV.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                }
                hideDialog();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                hideDialog();

            }

        });

        citySpn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                return false;
            }
        });

        citySpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityNameSt = cityList.get(position);
//                Log.e("cityNameSt",":::"+cityNameSt);
//                Log.e("stateNameSt","::::"+stateNameSt);
                if (cityNameSt.equalsIgnoreCase("Select City")) {
                    if (stateNameSt.equalsIgnoreCase("Select State")) {
                        listAllBusiness = Select.from(AllBusiness.class).list();
//                        Log.e("listAllBusiness if","::::"+listAllBusiness.size());
                    } else {
                        listAllBusiness = Select.from(AllBusiness.class).where(Condition.prop("state").eq(stateNameSt)).list();
//                        Log.e("listAllBusiness else","::::"+listAllBusiness.size());
                    }
                    mAdapter.setAllBusinessList(listAllBusiness);
                    buisnessRV.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                } else {
                    hideDialog();
                    viewAllBusiness();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                hideDialog();
            }
        });
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.context = context;
    }


    public void viewAllBusiness() {
        hideDialog();
        if (stateNameSt.equals("") || cityNameSt.equals("")) {
            //listAllBusiness
        } else {
            if (cityNameSt.equals("") || cityNameSt.equals("Select City")) {
                listAllBusiness = Select.from(AllBusiness.class).where(Condition.prop("state").eq(stateNameSt)).list();
            } else {
                listAllBusiness = Select.from(AllBusiness.class).where(Condition.prop("state").eq(stateNameSt), Condition.prop("city").eq(cityNameSt)).list();
            }
            mAdapter.setAllBusinessList(listAllBusiness);
            buisnessRV.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        }

    }

    private void setupSearchView() {
        searchview.setIconifiedByDefault(false);
        searchview.setOnQueryTextListener(this);
        searchview.setSubmitButtonEnabled(true);
        searchview.setFocusableInTouchMode(true);
        searchview.setQueryHint("Search by business..");
    }


    private void getAllBusinessAfterAdd() {
        showDialog(context);

        isAsyncTask = true;
        AllBusiness.deleteAll(AllBusiness.class);
        if (!cityList.contains("Select City")) {
            cityList.add("Select City");
        }
        if (!statesList.contains("Select State")) {
            statesList.add("Select State");
        }

        showDialog(context);


        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();


        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(baseActivity, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);

        serviceHandler.getAllBusinessArray(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                hideDialog();
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("businessList", arr);
                try {
//                    Log.e("getAllBusiness", "getAllBusinessArray");
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        AllBusiness allBusiness = builder.fromJson(jsonArr.get(i).toString(), AllBusiness.class);

                        if (allBusiness.getCity() != null) {
                            if (!cityList.contains(allBusiness.getCity())) {
                                cityList.add(allBusiness.getCity() + "");
                            }
                        }

                        if (allBusiness.getState() != null) {
                            if (!statesList.contains(allBusiness.getState())) {
                                statesList.add(allBusiness.getState() + "");
                            }
                        }

                        allBusiness.save();
                    }
                    listAllBusiness = AllBusiness.listAll(AllBusiness.class);

//                    Log.e("getAllBusiness", "list true");

                    if (listAllBusiness.size() != 0) {
                        mAdapter.setAllBusinessList(listAllBusiness);
                        buisnessRV.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                        if (fromAddBusiness)
                            buisnessRV.smoothScrollToPosition(listAllBusiness.size() - 1);


                    }
                    hideDialog();


                    if (cityList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.single_text_item, cityList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        citySpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        citySpn.setSelection(0);
                    }

                    if (statesList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        stateSpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        stateSpn.setSelection(0);
                    }


                    if (listAllBusiness.size() == 0) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                        builder1.setMessage("No goals are available");
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                hideDialog();

                            }
                        });
                        builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                // getAllBusiness();
                                getAllBusinessAfterAdd();
                            }
                        });
                        builder1.show();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
//                    Log.e("getAllBusiness", "catch");
                    // mProgressAsynctAsk.dismiss();

                    hideDialog();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
//                Log.e("getAllBusiness", "failure");
                //mProgressAsynctAsk.dismiss();
                hideDialog();
                if (listAllBusiness != null && listAllBusiness.size() > 0) {

                    if (!cityList.contains("Select City")) {
                        cityList.add("Select City");
                    }
                    if (!statesList.contains("Select State")) {
                        statesList.add("Select State");
                    }

                    for (int k = 0; k < listAllBusiness.size(); k++) {
                        AllBusiness business = listAllBusiness.get(k);
                        if (business.getCity() != null) {
                            if (!cityList.contains(business.getCity())) {
                                cityList.add(business.getCity() + "");
                            }
                        }

                        if (business.getState() != null) {
                            if (!statesList.contains(business.getState())) {
                                statesList.add(business.getState() + "");
                            }
                        }
                    }

                    mAdapter.setAllBusinessList(listAllBusiness);
                    buisnessRV.setAdapter(mAdapter);

                    hideDialog();
                    //if (mProgressDialog != null && mProgressDialog.isShowing())
                    //  mProgressDialog.dismiss();
                    if (statesList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        stateSpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        stateSpn.setSelection(0);
                    }
                    hideDialog();
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                    builder1.setMessage("Sorry..Detail not available.Please contact your Administrator");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            hideDialog();

                        }
                    });
                    builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            // getAllBusiness();
                            getAllBusinessAfterAdd();
                        }
                    });
                    builder1.show();
                }
            }
        });
    }

    private void getAllBusiness() {
        //showDialog(context);

        isAsyncTask = true;
        AllBusiness.deleteAll(AllBusiness.class);
        if (!cityList.contains("Select City")) {
            cityList.add("Select City");
        }
        if (!statesList.contains("Select State")) {
            statesList.add("Select State");
        }


        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        String arr = sharedPreference.getStringValue("businessList");
        try {
//                    Log.e("getAllBusiness", "getAllBusinessArray");
            JSONArray jsonArr = new JSONArray(arr);
            for (int i = 0; i < jsonArr.length(); i++) {
                AllBusiness allBusiness = builder.fromJson(jsonArr.get(i).toString(), AllBusiness.class);

                if (allBusiness.getCity() != null) {
                    if (!cityList.contains(allBusiness.getCity())) {
                        cityList.add(allBusiness.getCity() + "");
                    }
                }

                if (allBusiness.getState() != null) {
                    if (!statesList.contains(allBusiness.getState())) {
                        statesList.add(allBusiness.getState() + "");
                    }
                }

                allBusiness.save();
            }
            listAllBusiness = AllBusiness.listAll(AllBusiness.class);
            if (listAllBusiness.size() != 0) {
                Log.e(TAG, " getAllBusiness isFromDeal " + ApplicationClass.getInstance().isFromDeal());
                Log.e(TAG, " getAllBusiness getBusinessId " + ApplicationClass.getInstance().getBusinessId());

                if (ApplicationClass.getInstance().isFromDeal()) {
                    if (ApplicationClass.getInstance().getBusinessId() != null && !ApplicationClass.getInstance().getBusinessId().isEmpty()) {
                        List<String> myList = new ArrayList<String>(Arrays.asList(ApplicationClass.getInstance().getBusinessId().split(",")));
                        if (myList != null) {
                            Log.e(TAG, " getAllBusiness myList " + myList.size());

                            Log.e(TAG, " getAllBusiness listAllBusiness " + listAllBusiness.size());
                            List<AllBusiness> allDealBusiness = new ArrayList<>();

                            for (int i = 0; i < myList.size(); i++) {
                                Log.e(TAG, " getAllBusiness myList.get(i) " + myList.get(i));

                                for (AllBusiness allBusiness : listAllBusiness) {
                                    Log.e(TAG, " getAllBusiness getBusinessID " + allBusiness.getBusinessID());

                                    if (allBusiness.getBusinessID() == Integer.parseInt(myList.get(i))) {
                                        allDealBusiness.add(allBusiness);
                                    }
                                }
                            }
                            listAllBusiness.clear();
                            listAllBusiness.addAll(allDealBusiness);

                            mAdapter = new AllBusiAdapter(baseActivity, this, sharedPreference.getBooleanValue("UserLogin"));
                            mAdapter.setAllBusinessList(listAllBusiness);
                            buisnessRV.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        }
                        Log.e(TAG, " after getAllBusiness listAllBusiness " + listAllBusiness.size());
                    } else {
                        GetBusinessListByAreaId(ApplicationClass.getInstance().getAreaId());
                    }
                } else {
                    mAdapter = new AllBusiAdapter(baseActivity, this, sharedPreference.getBooleanValue("UserLogin"));
                    mAdapter.setAllBusinessList(listAllBusiness);
                    buisnessRV.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }


            if (ApplicationClass.getInstance().isFromDeal())
                linearLayoutSpinner.setVisibility(View.GONE);
            else
                linearLayoutSpinner.setVisibility(View.VISIBLE);



                   /* if (cityList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.single_text_item, cityList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        citySpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        citySpn.setSelection(0);
                    }*/

            if (statesList.size() != 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                stateSpn.setAdapter(dataAdapter);
                dataAdapter.notifyDataSetChanged();
                stateSpn.setSelection(0);
            }


            if (listAllBusiness.size() == 0) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                builder1.setMessage("No business are available");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        hideDialog();

                    }
                });
                builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        // getAllBusiness();
                        getAllBusinessAfterAdd();
                    }
                });
                builder1.show();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        hideDialog();

    }

    AllBusiness businessObject;
    int businessLocation;

    @Override
    public void productClick(View v, int position, Boolean inStock) {
        int itemPosition = buisnessRV.getChildPosition(v);
        if (!sharedPreference.getBooleanValue("UserLogin")) {
            showDialog(context);

            sharedPreference.saveStringData(PrefsHelper.BUSINESS_LOCATION, itemPosition + "");
            businessLocation = itemPosition;
//        Log.e("Business_loc", "::::::" + itemPosition);
//        Log.e("STATE_click:", "--->" + listAllBusiness.get(itemPosition).getState());
            sharedPreference.saveStringData("STATE_PRODUCT", listAllBusiness.get(itemPosition).getState());

            if (stateNameSt.equals("Select State")) {
                sharedPreference.saveStringData(PrefsHelper.CITY, "");
            } else {
                sharedPreference.saveStringData(PrefsHelper.STATE, stateNameSt);
            }
            if (cityNameSt.equals("Select City")) {
                sharedPreference.saveStringData(PrefsHelper.CITY, "");
            } else {
                sharedPreference.saveStringData(PrefsHelper.CITY, cityNameSt);
            }

            if (searchview.getQuery().toString().equals("")) {
                businessObject = listAllBusiness.get(itemPosition);
                sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, String.valueOf(listAllBusiness.get(itemPosition).getBusinessID()));
                sharedPreference.saveStringData("BusinessName", listAllBusiness.get(itemPosition).getBusnessName());
            } else {
                businessObject = mAdapter.filterList.get(itemPosition);
                sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, String.valueOf(mAdapter.filterList.get(itemPosition).getBusinessID()));
                sharedPreference.saveStringData("BusinessName", listAllBusiness.get(itemPosition).getBusnessName());
            }


            try {
                if (stateNameSt == null) {
                    if (!sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID).equals("business_location")) {
                        List<AllBusiness> listGoal = AllBusiness.listAll(AllBusiness.class);
                        if (listGoal.size() != 0) {
                            int location = Integer.parseInt(sharedPreference.getStringValue(PrefsHelper.BUSINESS_LOCATION));
                            businessObject = listGoal.get(location);
                            checkedInStatus = businessObject.getCheckedIN();
                        }

                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                if (!sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID).equals("business_location")) {
                    List<AllBusiness> listGoal = AllBusiness.listAll(AllBusiness.class);

                    // List<AllBusiness> listGoal = Select.from(AllBusiness.class).where(Condition.prop("business_id").eq(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID))).list();
                    if (listGoal.size() != 0) {
                        int location = Integer.parseInt(sharedPreference.getStringValue(PrefsHelper.BUSINESS_LOCATION));
                        businessObject = listGoal.get(location);
                        checkedInStatus = businessObject.getCheckedIN();
                    }

                }
            }
            if (businessObject != null) {

                checkedInStatus = businessObject.getCheckedIN();
            } else {
                Toast.makeText(ApplicationClass.getAppContext(), "No detail available", Toast.LENGTH_SHORT).show();
            }
            //hideDialog();
            setCheckInBtn(itemPosition);
        } else {
            Intent i = new Intent(context, AddBusinessActivity.class);
//            Integer BusinessID = listAllBusiness.get(itemPosition).getBusinessID();
            Integer BusinessID = (Integer) v.getTag();
            i.putExtra("BusinessID", BusinessID);
            i.putExtra("function", "edit");
            startActivity(i);
        }

    }

    @Override
    public void notAvailableDialog() {

    }


    public void onBackPressed() {
//        Log.e("OnBAck", ":::+business true");

        if (navigateToProductDetail != null) {
            if (navigateToProductDetail.equals("productDetail")) {
//                Intent intent = new Intent(getContext(), GoalsActivities.class);
//                intent.putExtra("product_location", sharedPreference.getIntValue(PrefsHelper.PRODUCT_LOC));
//                intent.putExtra("product_id", sharedPreference.getIntValue(PrefsHelper.PRODUCT_ID_FOR_DETAIL));
//                intent.putExtra("brand_name", "" + sharedPreference.getStringValue(PrefsHelper.BRAND_NAME));
//                intent.putExtra("category_id", sharedPreference.getIntValue(PrefsHelper.CATEGORYID) + "");
//                intent.putExtra("nameActivity", "ProductDetail");
//                startActivity(intent);
                Intent intent = new Intent(baseActivity, GoalsActivities.class);
                intent.putExtra("nameActivity", "AllProduct");
                startActivity(intent);
            }
        } else if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("product")) {
//            Intent intent = new Intent(getContext(), GoalsActivities.class);
//            intent.putExtra("product_location", sharedPreference.getIntValue(PrefsHelper.PRODUCT_LOC));
//            intent.putExtra("product_id", sharedPreference.getIntValue(PrefsHelper.PRODUCT_ID_FOR_DETAIL));
//            intent.putExtra("brand_name", "" + sharedPreference.getStringValue(PrefsHelper.BRAND_NAME));
//            intent.putExtra("category_id", sharedPreference.getIntValue(PrefsHelper.CATEGORYID) + "");
//            intent.putExtra("nameActivity", "ProductDetail");
//            startActivity(intent);
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "AllProduct");
            startActivity(intent);
        } else if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("cartFragment")) {
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "cart_fragment");
            startActivity(intent);
        } else {
            Intent intent1 = new Intent(baseActivity, DashboardActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            baseActivity.overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
        }
    }


    public void setCheckInBtn(int position) {
        ProductInCart.businessID = String.valueOf(mAdapter.filterList.get(position).getBusinessID());

        if (checkedInStatus) {
            if (ProductInCart.listAll(ProductInCart.class).size() != 0) {

                FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                GoalsActivities.homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));

                Bundle data = new Bundle();
                data.putString("business_loc", String.valueOf(position));
                /*sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "business");*/
                CartFragment fragment = new CartFragment();
                fragment.setArguments(data);
                ft.replace(R.id.container, fragment);
                ft.commit();

            } else {
                Toast.makeText(ApplicationClass.getAppContext(), "You have already checked in ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(baseActivity, GoalsActivities.class);
                intent.putExtra("nameActivity", "AllProduct");
                intent.putExtra("businesscheckin", "true");
                Log.d("successfully checkin", "checkined");
                startActivity(intent);
            }
        } else {

            if (Connectivity.isNetworkAvailable(baseActivity))
            {
                checkInApiCall(position);


                ProductInCart.businessID = String.valueOf(businessObject.getBusinessID());

                if (ProductInCart.listAll(ProductInCart.class).size() != 0) {

                    if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("business")) {
                        Toast.makeText(baseActivity, "You have already checked in ", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(baseActivity, GoalsActivities.class);
                        intent.putExtra("nameActivity", "AllProduct");
                        intent.putExtra("businesscheckin", "true");
                        startActivity(intent);

                    } else {

                        FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                        Bundle data = new Bundle();
                        data.putString("moveto", "business");
                        data.putString("business_loc", String.valueOf(position));
                        GoalsActivities.homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));

                        // sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "business");
                        CartFragment fragment = new CartFragment();
                        fragment.setArguments(data);
                        ft.replace(R.id.container, fragment);
                        ft.commit();
                    }

                } else {

                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                    intent.putExtra("nameActivity", "AllProduct");
                    intent.putExtra("businesCheckIn", "true");
                    Log.d("successfully checkin", "checkined");
                    startActivity(intent);
                    if (baseActivity != null)
                        baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                }
                checkedInStatus = true;
                businessObject.setCheckedIN(true);
                businessObject.save();

            } else {

                if (ProductInCart.listAll(ProductInCart.class).size() != 0) {

                    FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                    GoalsActivities.homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));

                    Bundle data = new Bundle();
                    data.putString("business_loc", String.valueOf(position));
                    /*sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "business");*/
                    CartFragment fragment = new CartFragment();
                    fragment.setArguments(data);
                    ft.replace(R.id.container, fragment);
                    ft.commit();

                } else {
                    Toast.makeText(ApplicationClass.getAppContext(), "You have already checkin", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                    intent.putExtra("nameActivity", "AllProduct");
                    intent.putExtra("backoncheckin", "true");
                    Log.d("successfully checkin", "checkined");
                    startActivity(intent);
                    baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }


            }
        }
    }

    Gson builder;
    ServiceHandler serviceHandler;
    CheckedDataHit checkedDataHit;


    private void checkInApiCall(final int itemposition) {
        if (Connectivity.isNetworkAvailable(ApplicationClass.getAppContext())) {
            //showDialog(context);
            builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                    .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
            serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            try {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z
                JSONObject object = new JSONObject();
                object.put("UserCheckInId", 0);
                object.put("UserProfileID", sharedPreference.getStringValue(PrefsHelper.USER_ID));
                object.put("Longitude", (Singleton.instance.SLNG));
                object.put("Latitude", (Singleton.instance.SLAT));
                //object.put("CheckInDate", "2017-04-13T06:54:04.543Z");
                object.put("CheckInDate", date);
                object.put("BusinessID", businessObject.getBusinessID());

//                Log.e("OBJECT", "::::" + object.toString());

                checkedDataHit = builder.fromJson(object.toString(), CheckedDataHit.class);
                serviceHandler.checkedInSave(checkedDataHit, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        //hideDialog();
                        String arr = CommonUtils.getServerResponse(response);
                        sharedPreference.saveBooleanValue("IsBusinessDetailFrag", true);

                        try {
                            JSONObject jsonArr = new JSONObject(arr);
                            if (jsonArr.getString("Message").equals("Success")) {
                              /*  ProductInCart.businessID = String.valueOf(businessObject.getBusinessID());

                                if (ProductInCart.listAll(ProductInCart.class).size() != 0) {

                                    if (sharedPreference.getStringValue(PrefsHelper.NAVI_PRODUCT_BUSINESS).equals("business"))
                                    {
                                        Toast.makeText(baseActivity, "You have already checked in ", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(baseActivity, GoalsActivities.class);
                                        intent.putExtra("nameActivity", "AllProduct");
                                        intent.putExtra("businesscheckin", "true");
                                        startActivity(intent);

                                    } else {

                                        FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                                        ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                                        Bundle data = new Bundle();
                                        data.putString("moveto", "business");
                                        data.putString("business_loc", String.valueOf(itemposition));
                                        GoalsActivities.homeIconImg.setImageDrawable(getResources().getDrawable(R.drawable.back_arrow));

                                        // sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "business");
                                        CartFragment fragment = new CartFragment();
                                        fragment.setArguments(data);
                                        ft.replace(R.id.container, fragment);
                                        ft.commit();
                                    }

                                } else {

                                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                                    intent.putExtra("nameActivity", "AllProduct");
                                    intent.putExtra("businesCheckIn", "true");
                                    Log.d("successfully checkin", "checkined");
                                    startActivity(intent);
                                    if (baseActivity != null)
                                        baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                                }
                                checkedInStatus = true;
                                businessObject.setCheckedIN(true);
                                businessObject.save();*/
                            } else {
                                Log.d("unsuccessful", "data not found in response of api ");
                                Toast.makeText(baseActivity, "No data found", Toast.LENGTH_SHORT).show();
                            }

                            sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, String.valueOf(businessObject.getBusinessID()));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            // Toast.makeText(getContext(), "json data", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideDialog();

                        error.printStackTrace();
                        Toast.makeText(baseActivity, " error " + error, Toast.LENGTH_SHORT).show();
                        CommonUtils.dismissProgress();
                        //Log.d("checkInApiCall Retrofit", "checkin error");
                        //  Toast.makeText(getContext(), "checkInApiCall Retrofit", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(baseActivity, "Please do login again", Toast.LENGTH_SHORT).show();
            }
            CommonUtils.dismissProgress();
        } else {
            Toast.makeText(baseActivity, "You need Internet connection for check-in", Toast.LENGTH_SHORT).show();
            CommonUtils.dismissProgress();

        }
        hideDialog();

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.filter(newText);
        return true;
    }


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.e(TAG, "Receieved notification about network status");
            if (Connectivity.isNetworkAvailable(context)) {
                if (!isConnected) {
                    Log.e(TAG, "Now you are connected to Internet!");
                    isConnected = true;
                    sharedPreference.saveBooleanValue("IsAllBusinessfrag", true);
                    List<AddBusiness> addBusinessList = AddBusiness.listAll(AddBusiness.class);

                    if (addBusinessList != null && addBusinessList.size() > 0) {
                        serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);

                        if (addBusinessList != null) {
                            Log.e(TAG, " addBusinessList " + addBusinessList.size());

                            for (int i = 0; i < addBusinessList.size(); i++) {
                                Log.e(TAG, " addBusinessList.get(i) " + addBusinessList.get(i));
                                Log.e(TAG, " toString " + addBusinessList.get(i).toString());

                                SecurityLogsApiAddBusiness(addBusinessList.get(i));


                            }
                        }
                        AddBusiness.deleteAll(AddBusiness.class);
                        SecurityLogsApi(); //getAllBusinessAfterAdd();
                    }


                }
            } else {
                listAllBusiness = AllBusiness.listAll(AllBusiness.class);


                if (listAllBusiness != null && listAllBusiness.size() > 0) {
                    if (!cityList.contains("Select City")) {
                        cityList.add("Select City");
                    }
                    if (!statesList.contains("Select State")) {
                        statesList.add("Select State");
                    }
                    for (int k = 0; k < listAllBusiness.size(); k++) {
                        AllBusiness business = listAllBusiness.get(k);
                        if (business.getCity() != null) {
                            if (!cityList.contains(business.getCity())) {
                                cityList.add(business.getCity() + "");
                            }
                        }

                        if (business.getState() != null) {
                            if (!statesList.contains(business.getState())) {
                                statesList.add(business.getState() + "");
                            }
                        }
                    }

                    mAdapter.setAllBusinessList(listAllBusiness);
                    buisnessRV.setAdapter(mAdapter);


                    if (statesList.size() != 0) {
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        stateSpn.setAdapter(dataAdapter);
                        dataAdapter.notifyDataSetChanged();
                        stateSpn.setSelection(0);
                    }

                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                    builder1.setMessage("Sorry..Detail not available.Please contact your Administrator");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            //    Toast.makeText(getContext(), "Back is pressed", Toast.LENGTH_LONG).show();

                        }
                    });
                    builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            //  getAllBusiness();
                            getAllBusinessAfterAdd();
                        }
                    });
                    builder1.show();
                }
                isConnected = false;
            }

        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v("is network Available", "Now you are connected to Internet!");
                                isConnected = true;
                                sharedPreference.saveBooleanValue("IsAllBusinessfrag", true);
                                //Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();
                                if (isNetworkAvailable(ApplicationClass.getAppContext())) {
                                    if (!isAsyncTask) {
//                                        Log.e("###", "::::::Async calling...");
                                        getAllBusiness();


                                    }
                                }
                                if (ApplicationClass.getAppContext() != null)
                                    LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);


                            }
                            return true;
                        }
                    }
                }
            }
            listAllBusiness = AllBusiness.listAll(AllBusiness.class);


            if (listAllBusiness != null && listAllBusiness.size() > 0) {
                if (!cityList.contains("Select City")) {
                    cityList.add("Select City");
                }
                if (!statesList.contains("Select State")) {
                    statesList.add("Select State");
                }
                for (int k = 0; k < listAllBusiness.size(); k++) {
                    AllBusiness business = listAllBusiness.get(k);
                    if (business.getCity() != null) {
                        if (!cityList.contains(business.getCity())) {
                            cityList.add(business.getCity() + "");
                        }
                    }

                    if (business.getState() != null) {
                        if (!statesList.contains(business.getState())) {
                            statesList.add(business.getState() + "");
                        }
                    }
                }

                mAdapter.setAllBusinessList(listAllBusiness);
                buisnessRV.setAdapter(mAdapter);


                if (statesList.size() != 0) {
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(baseActivity, R.layout.single_text_item, statesList);
                    dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    stateSpn.setAdapter(dataAdapter);
                    dataAdapter.notifyDataSetChanged();
                    stateSpn.setSelection(0);
                }

            } else {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(baseActivity);
                builder1.setMessage("Sorry..Detail not available.Please contact your Administrator");
                builder1.setCancelable(false);
                builder1.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //    Toast.makeText(getContext(), "Back is pressed", Toast.LENGTH_LONG).show();

                    }
                });
                builder1.setNegativeButton("Reload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //  getAllBusiness();
                        getAllBusinessAfterAdd();
                    }
                });
                builder1.show();
            }
            isConnected = false;
            return false;
        }
    }


    boolean isAsyncTask = false;

    private void SecurityLogsApi() {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Business/GetAll");
                object.put("JsonObject", "null");
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    getAllBusinessAfterAdd();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            getAllBusiness();
        }
    }

    private void SecurityLogsApiAddBusiness(AddBusiness location) {
        if (Connectivity.isNetworkAvailable(getActivity())) {
            try {

                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", "/Business/SaveBusiness");
                object.put("JsonObject", location.toString());
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                        .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getActivity(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    AddBusiness(location);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void AddBusiness(AddBusiness business) {
        serviceHandler.addBusiness(business, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String serverResponse = CommonUtils.getServerResponse(response);
                Log.e(TAG, "serverResponse ::: " + serverResponse);
                try {
                    JSONObject jsonObject = new JSONObject(serverResponse);
                    if (jsonObject.has("Message")) {
                        String Message = jsonObject.getString("Message");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    try {
                        JSONObject jsonObject = new JSONObject(serverResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    String responseError = CommonUtils.getServerResponse(error.getResponse());
                    JSONObject jsonErrorObj = new JSONObject(responseError);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetBusinessListByAreaId(String areaId) {
        showDialog(context);
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getActivity(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.GetMapping(areaId, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                hideDialog();
                String arr = CommonUtils.getServerResponse(response);
                try {
                    Log.e(TAG, " GetBusinessListByAreaId arr " + arr);

                    if (arr != null && !arr.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(arr);
                        if (jsonArray != null) {
                            listAllBusiness.clear();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject != null && jsonObject.has("BuinessID")) {
                                    String id = jsonObject.getString("BuinessID");
                                    AllBusiness allBusiness = Select.from(AllBusiness.class).where(Condition.prop("business_id").eq(id)).first();
                                    if (allBusiness != null) {
                                        listAllBusiness.add(allBusiness);
                                    }
                                }
                            }
                        }

                        mAdapter = new AllBusiAdapter(baseActivity, AllBusinessFragment.this, sharedPreference.getBooleanValue("UserLogin"));
                        mAdapter.setAllBusinessList(listAllBusiness);
                        buisnessRV.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                hideDialog();
                Log.e(TAG, " GetBusinessListByAreaId failure " + error.getMessage());

                error.printStackTrace();
            }
        });
    }
}
