package com.salestrackmobileapp.android.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.CartAdapter;
import com.salestrackmobileapp.android.adapter.OffersDealAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AllBusiness;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.gson.GoalBusiness;
import com.salestrackmobileapp.android.gson.PendingOrderItem;
import com.salestrackmobileapp.android.gson.SaveOrder;
import com.salestrackmobileapp.android.gson.SecurityLog;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class CartFragment extends BaseFragment implements RecyclerClick {

    private static int value;
    @BindView(R.id.item_in_cart)
    RecyclerView itemCartRv;

    ServiceHandler serviceHandler;

    static
    Custome_TextView rowTotalAmount;
    public static
    Custome_TextView totalDiscountTv;
    public static
    Custome_BoldTextView totalAmountTv;

    @BindView(R.id.updated_ampunt_tv)
    Custome_BoldTextView updated_ampunt_tv;

    @BindView(R.id.business_name_txt)
    Custome_BoldTextView businessNameTxt;
    @BindView(R.id.business_contactperson_txt)
    Custome_TextView businessContactTxt;
    @BindView(R.id.business_contactnumber_txt)
    Custome_TextView businessContactNumberTxt;

    @BindView(R.id.linearDetails)
    LinearLayout linearDetails;

    @BindView(R.id.linearLayoutDeals)
    LinearLayout linearLayoutDeals;

    @BindView(R.id.linearLayoutApplyDeals)
    LinearLayout linearLayoutApplyDeals;

    @BindView(R.id.textViewDealDescription)
    TextView textViewDealDescription;

    @BindView(R.id.textViewDealDetails)
    TextView textViewDealDetails;

    @BindView(R.id.textViewDealRemove)
    TextView textViewDealRemove;

    @BindView(R.id.linearLayoutType)
    LinearLayout linearLayoutType;

    @BindView(R.id.textViewDealAppliedType)
    TextView textViewDealAppliedType;

    @BindView(R.id.textViewDealAppliedTypeDescription)
    TextView textViewDealAppliedTypeDescription;


    @BindView(R.id.relativeLayoutAppliedDeal)
    RelativeLayout relativeLayoutAppliedDeal;

    @BindView(R.id.ivMinusDetails)
    ImageView ivMinusDetails;

    @BindView(R.id.ivPlusDetails)
    ImageView ivPlusDetails;

    NestedScrollView nested_scroll_view;
    Custome_TextView business_type_tv;

    Dialog contacts_dialog;

    //business_contactnumber_txt

    private LinearLayoutManager mLayoutManager, dealLayoutManager;
    CartAdapter cardAdapter;
    List<ProductInCart> cartList;
    private int year;
    private int month;
    private int day;
    SaveOrder saveOrderObject;
    double subTotal = 0;
    public static double amount = 0;
    public static double discount = 0;
    int location = 0, productId = 0;
    Integer categoryID = 0;
    String brandName = "";
    public double rawAmount = -1;
    static float sumVat = -1;
    public double rowAmountTax = -1;

    GoalBusiness goalBusiness;
    //private NetworkChangeReceiver receiver;
    private boolean isConnected = false;

    static int orderIdInt = 0;
    String businessLoc = "";
    static String moveTo = "";
    List<SaveOrder> listSaveorderArray;


    //OffersDealAdapter offersAdapter;
    static Custome_TextView tvtax, tvAmountTax;
    public static String STATE = "";
    public static Custome_TextView total_discount_head;
    private ProgressDialog mProgressDialog;
    ImageView ivbusiness;
    LinearLayout llupdate_business;

    List<AllDeals> alldeals = new ArrayList<>();
    List<AllDeals> availableDeals = new ArrayList<>();

    List<ProductInCart> productInCartList;

    public static String TAG = "CartFragment";

    Gson builder;

    String businessId;


    @BindView(R.id.linearLayoutDiscount)
    LinearLayout linearLayoutDiscount;

    AllDeals selectedDeal;

    double subTotalAmountForOrderDeal = 0;

    String giftname = "", DealName = "";

    double couponAmount = 0.0;

    int giftquantity = 0, extraquantity = 0;
    int DealId = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, view);
        productInCartList = ProductInCart.listAll(ProductInCart.class);
        ivbusiness = (ImageView) view.findViewById(R.id.ivbusiness);
        llupdate_business = view.findViewById(R.id.llupdate_business);
        business_type_tv = view.findViewById(R.id.business_type_tv);
        // getActivity().setTitle("Cart");
        GoalsActivities.actionBarTitle.setText("CART");
        GoalsActivities.addProduct.setVisibility(View.VISIBLE);
        GoalsActivities.total_items_bracket.setVisibility(View.VISIBLE);
        GoalsActivities.totalAmt.setVisibility(View.GONE);
        GoalsActivities.cartImg.setVisibility(View.GONE);
        //  GoalsActivities.homeIconImg.setBackground(getResources().getDrawable(R.drawable.home_icon));
        rowTotalAmount = (Custome_TextView) view.findViewById(R.id.raw_amount_tv);
        totalDiscountTv = (Custome_TextView) view.findViewById(R.id.total_discount_txt);
        tvtax = (Custome_TextView) view.findViewById(R.id.tvtax);
        tvAmountTax = (Custome_TextView) view.findViewById(R.id.tvAmountTax);
        totalAmountTv = (Custome_BoldTextView) view.findViewById(R.id.total_amount_tv);
        nested_scroll_view = (NestedScrollView) view.findViewById(R.id.nested_scroll_view);
        total_discount_head = view.findViewById(R.id.total_discount_head);
        selectedDeal = null;
        businessId = sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID);

        Log.e(TAG, " businessId " + businessId);


        builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        STATE = sharedPreference.getStringValue("STATE_PRODUCT");
        Log.e("STATE_cart", ":::" + STATE);

        mProgressDialog = new ProgressDialog(baseActivity);
       /* mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.show();*/
        Log.e("ProgressDialog", ":::True");

        if (!sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID).equals("business_id")) {
            goalBusiness = Select.from(GoalBusiness.class).where(Condition.prop("business_id").eq(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID))).first();
        }
        if (goalBusiness == null) {
            AllBusiness allBusiness = Select.from(AllBusiness.class).where(Condition.prop("business_id").eq(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID))).first();
            if (allBusiness != null) {
                goalBusiness = new GoalBusiness();
                goalBusiness.setBusinessID(allBusiness.getBusinessID());
                goalBusiness.setBusnessName(allBusiness.getBusnessName());
                goalBusiness.setCheckedIN(true);
                goalBusiness.setAddress1(allBusiness.getAddress1());
                goalBusiness.setAddress2(allBusiness.getAddress2());
                goalBusiness.setCity(allBusiness.getCity());
                goalBusiness.setContactPersonEmail(allBusiness.getContactPersonEmail());
                goalBusiness.setContactPersonPhone(allBusiness.getContactPersonPhone());
                goalBusiness.setContactPersonName(allBusiness.getContactPersonName());
                goalBusiness.setCountry(allBusiness.getCountry());
                goalBusiness.setImageName(allBusiness.getImageName());
                goalBusiness.setState(allBusiness.getState());
                goalBusiness.setWebsiteName(allBusiness.getWebsiteName());
                goalBusiness.setZipCode(allBusiness.getZipCode());
                goalBusiness.setId(allBusiness.getId());
                goalBusiness.setBusinesstype(allBusiness.getBusinesstype());
            }
        } else {
            AllBusiness allBusiness = Select.from(AllBusiness.class).where(Condition.prop("business_id").eq(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID))).first();
            if (allBusiness != null) {
                goalBusiness.setImageName(allBusiness.getImageName());
            }
        }


        if (goalBusiness != null) {
            if (goalBusiness.getImageName() != null && !goalBusiness.getImageName().isEmpty()) {
                Log.e("getImageName", ":::not null");
                Picasso.with(baseActivity).load(goalBusiness.getImageName()).placeholder(getContext().getResources().getDrawable(R.drawable.calendar_icon)).error(R.drawable.calendar_icon).into(ivbusiness);
            } else {
                Log.e("getImageName", ":::null  null");

                ivbusiness.setImageDrawable(null);
            }

            Log.e("BusinessType", "::" + goalBusiness.getBusinesstype());
            businessContactTxt.setTypeface(businessContactTxt.getTypeface(), Typeface.ITALIC);
            business_type_tv.setTypeface(business_type_tv.getTypeface(), Typeface.ITALIC);


            businessNameTxt.setText(goalBusiness.getBusnessName());
            if (goalBusiness.getBusinesstype() != null && goalBusiness.getBusinesstype().length() > 0) {
                business_type_tv.setText("(" + goalBusiness.getBusinesstype() + ")");
            }

            if (goalBusiness.getContactPersonName() != null) {
                businessContactTxt.setText(goalBusiness.getContactPersonName() + " ");
            } else {
                businessContactTxt.setVisibility(View.GONE);
            }

            if (goalBusiness.getContactPersonPhone() != null) {
                businessContactNumberTxt.setText(goalBusiness.getContactPersonEmail());
            } else {
                businessContactNumberTxt.setVisibility(View.GONE);
            }
            String selectedBusinessID = String.valueOf(goalBusiness.getBusinessID());


        }


        //offersAdapter = new OffersDealAdapter(baseActivity, totalDiscountTv, totalAmountTv, sharedPreference, offersDeal, "");
        if (getArguments() != null) {

            if (getArguments().containsKey("product_location")) {
                location = getArguments().getInt("product_location");
                productId = getArguments().getInt("product_id");
                categoryID = getArguments().getInt("category_id");
                brandName = getArguments().getString("brand_name");
            } else if (getArguments().containsKey("business_loc")) {
                businessLoc = getArguments().getString("business_loc");
            }

            if (getArguments().containsKey("moveto")) {
                moveTo = getArguments().getString("moveto");
            }
            //Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID), Condition.prop("brand_name").eq(brandName)).list();
        } else {

        }

        if (ApplicationClass.getInstance().isFromDeal()) {
            selectedDeal = Select.from(AllDeals.class).where(Condition.prop("deal_id").eq(ApplicationClass.getInstance().getDealId())).first();
            Log.e(TAG, " selectedDeal " + selectedDeal);
        }

        mLayoutManager = new LinearLayoutManager(baseActivity);
        itemCartRv.setLayoutManager(mLayoutManager);


        hideDialog();


        cardAdapter = new CartAdapter(baseActivity, this, this);

        itemCartRv.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();

        linearLayoutDiscount.setVisibility(View.GONE);

        sumValues();

        llupdate_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateBusiness();
            }
        });

        linearLayoutDeals.setVisibility(View.GONE);
        //linearLayoutApplyDeals.setVisibility(View.VISIBLE);
        //linearLayoutDeals.setVisibility(View.VISIBLE);
        relativeLayoutAppliedDeal.setVisibility(View.GONE);
        alldeals = AllDeals.listAll(AllDeals.class);

        if (alldeals != null) {
            Log.e(TAG, " onCreateView alldeals " + alldeals.size());


            if (availableDeals != null)
                availableDeals.clear();
            else
                availableDeals = new ArrayList<>();
            for (AllDeals alldeal : alldeals) {
                Log.e(TAG, " onCreateView alldeal getBussinessID " + alldeal.getBussinessID());
                Log.e(TAG, " onCreateView alldeal businessId " + businessId);

                if (alldeal.getBussinessID().contains(businessId)) {
                    Log.e(TAG, " onCreateView match found alldeal ");
                    availableDeals.add(alldeal);

                }
            }
            Log.e(TAG, " availableDeals " + availableDeals.size());

            if (availableDeals.size() == 0)
                linearLayoutDeals.setVisibility(View.GONE);
            else {
                linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                linearLayoutDeals.setVisibility(View.VISIBLE);
                List<AllDeals> tempAvailableDeals = new ArrayList<>();
                for (AllDeals alldeal : availableDeals) {
                    if (alldeal.getDealType().equalsIgnoreCase("order")) {
                        tempAvailableDeals.add(alldeal);
                    } else {
                        boolean temp = false;
                        for (ProductInCart productInCart : productInCartList) {
                            Log.e(TAG, " onCreateView alldeal  product " + alldeal.getProductID());
                            Log.e(TAG, " onCreateView alldeal  productID " + productInCart.productID);
                            if (alldeal.getProductID().contains(String.valueOf(productInCart.productID))) {
                                Log.e(TAG, " onCreateView alldeal  productID match found  ");
                                temp = true;
                                break;
                            }
                        }
                        if (temp)
                            tempAvailableDeals.add(alldeal);
                    }
                }
                availableDeals.clear();
                availableDeals.addAll(tempAvailableDeals);
                if (availableDeals.size() == 0)
                    linearLayoutDeals.setVisibility(View.GONE);
            }
        } else
            linearLayoutDeals.setVisibility(View.GONE);

        linearLayoutApplyDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDealDialog(availableDeals);
            }
        });

        textViewDealRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutType.setVisibility(View.GONE);
                linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                relativeLayoutAppliedDeal.setVisibility(View.GONE);
                ResetDealApiValues();
            }
        });

        relativeLayoutAppliedDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDealDialog(availableDeals);
            }
        });

        linearLayoutType.setVisibility(View.GONE);
        return view;
    }


    public void sumValues() {

        sumVat = 0;
        //Field f = null;
        rawAmount = 0;
        discount = 0;
        subTotal = 0;
        amount = 0;
        rowAmountTax = 0;
        int sum = 0;
        subTotalAmountForOrderDeal = 0;
        try {
            //f = SugarContext.getSugarContext().getClass().getDeclaredField("sugarDb");
            //f.setAccessible(true);
            int proQty = 0;
            List<ProductInCart> cartItemList = Select.from(ProductInCart.class).list();
            try {
                String disAmount = "0";
                for (ProductInCart productsInCart : cartItemList) {
                    if (productsInCart.dealAmount == null) {
                        productsInCart.dealAmount = "0";
                    }

                    Double baseprice = productsInCart.price;
                    double cgstRate = 0.0;
                    double sgstRate = 0.0;

                    if (productsInCart.uomST.equalsIgnoreCase("Pcs")) {
                        value = 1;
                    } else if (productsInCart.uomST.equalsIgnoreCase("Dozen")) {
                        value = 12;
                    } else if (productsInCart.uomST.equalsIgnoreCase("STD PKG")) {
                        value = 120;
                    } else if (productsInCart.uomST.equalsIgnoreCase("Piece")) {
                        value = 1;
                    } else if (productsInCart.uomST.equalsIgnoreCase("Special Box")) {
                        value = 20;
                    }
                    Log.e(TAG, " baseprice " + baseprice);

                    Log.e(TAG, " qty " + productsInCart.qty);

                    baseprice = baseprice * productsInCart.qty;

                    if (productsInCart.cGSTPercentage != null) {

                        cgstRate = (baseprice * productsInCart.cGSTPercentage) / 100;
                    }
                    if (productsInCart.sGSTPercentage != null) {
                        sgstRate = (baseprice * productsInCart.sGSTPercentage) / 100;
                    }

                    cgstRate = format2Decimal(cgstRate);
                    sgstRate = format2Decimal(sgstRate);

                    Log.e(TAG, " cgstRate " + cgstRate);
                    Log.e(TAG, " sgstRate " + sgstRate);
                    Log.e(TAG, " rowAmountTax " + rowAmountTax);


                    rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                    rowAmountTax = format2Decimal(rowAmountTax);
                    //rowAmountTax = rowAmountTax * productsInCart.qty;

                    Log.e(TAG, " rowAmountTax " + rowAmountTax);

                    rawAmount = rawAmount + baseprice;
                    Log.e(TAG, " rawAmount " + rawAmount);

                    proQty += productsInCart.CartQty;
                }
                rowAmountTax = format2Decimal(rowAmountTax);
                tvtax.setText("₹" + " " + Double.parseDouble(new DecimalFormat("##.##").format(rowAmountTax)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

//30-4-18
            Log.e("ProQTy", ":::" + proQty);


            if (GoalsActivities.total_items_bracket != null) {
                GoalsActivities.total_items_bracket.setText("(" + "" + proQty + ")");
            }


            Double DrowTotalAmount = Double.parseDouble(new DecimalFormat("##.##").format(rawAmount));
            Double DTotalAmount = Double.parseDouble(new DecimalFormat("##.##").format(rawAmount + rowAmountTax));
            DrowTotalAmount = format2Decimal(DrowTotalAmount);
            DTotalAmount = format2Decimal(DTotalAmount);
            subTotalAmountForOrderDeal = DrowTotalAmount;
            //int DTotalAmount = (int)amount + (int)rowAmountTax;
            rowTotalAmount.setText(String.format("₹ %.2f", DrowTotalAmount));
            totalAmountTv.setText(String.format("₹ %.2f", DTotalAmount));

            ProductInCart productInCart = Select.from(ProductInCart.class).where(Condition.prop("deal_category").eq("Order")).first();
            if (productInCart != null) {
                if (productInCart.dealType.equals("Amount")) {

                    if (productInCart.dealAmount != null && productInCart.dealAmount.equals(0.0)) {
                        totalDiscountTv.setVisibility(View.GONE);
                        total_discount_head.setVisibility(View.GONE);
                    } else {
                        totalDiscountTv.setVisibility(View.VISIBLE);
                        total_discount_head.setVisibility(View.VISIBLE);
                    }
                    Log.e("%%%", ":::deal" + productInCart.dealAmount);

                    totalDiscountTv.setText("₹" + " " + productInCart.dealAmount);
                    double discount1 = getDoubleFromString(productInCart.dealAmount);
                    double amount1 = rawAmount - discount1;

                    amount = amount1;
                    discount = discount1;
                    Double dTotalAmount = Double.parseDouble(new DecimalFormat("##.##").format((amount + rowAmountTax)));

                    totalAmountTv.setText(String.format("₹ %.2f", DTotalAmount));
                } else {

//                    Log.e("DEAL_amnount",":::"+productInCart.dealAmount);
//                    Log.e("RAWAMOUNT",":::"+rawAmount);
                    Double disAmount = (getDoubleFromString(productInCart.dealAmount) / 100) * rawAmount;
                    double amount1 = rawAmount - disAmount;

                    discount = disAmount;
                    amount = amount1;
                    Double DTotalDiscount = Double.parseDouble(new DecimalFormat("##.##").format((disAmount)));
                    DTotalAmount = Double.parseDouble(new DecimalFormat("##.##").format(amount1 + rowAmountTax));


                    if (DTotalDiscount.equals(0.0)) {
                        totalDiscountTv.setVisibility(View.GONE);
                        total_discount_head.setVisibility(View.GONE);
                    } else {
                        totalDiscountTv.setVisibility(View.VISIBLE);
                        total_discount_head.setVisibility(View.VISIBLE);
                    }
                    Log.e("%%%", ":::total_disc0" + DTotalDiscount);

                    totalDiscountTv.setText("₹" + " " + DTotalDiscount);
                    totalAmountTv.setText(String.format("₹ %.2f", DTotalAmount));
                }
            } else {
//                Log.e("%%%",":::discount"+discount);
                Double DTotalDiscount = Double.parseDouble(new DecimalFormat("##.##").format((discount)));
                if (DTotalDiscount.equals(0.0)) {
                    totalDiscountTv.setVisibility(View.GONE);
                    total_discount_head.setVisibility(View.GONE);
                } else {
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                }
                Log.e("%%%", ":::total_discount_txt" + DTotalDiscount);
                totalDiscountTv.setText("₹" + " " + (DTotalDiscount));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        DealCalculation();
    }

    public static Double getDoubleFromString(String amount) {
        double totalPrice = Double.parseDouble(amount.replaceAll("[^0-9\\.]+", ""));
        return totalPrice;
    }

    public static Float getFloatFromString(String amount) {
        Float totalPrice = Float.parseFloat(amount.replaceAll("[^0-9\\.]+", ""));
        return totalPrice;
    }

 /*   @OnClick(R.id.btnAddProducts)
    public void addMoreProduct() {
        GoalsActivities.addProduct.setVisibility(View.GONE);
        GoalsActivities.actionBarTitle.setText("PRODUCTS");
        Intent intent = new Intent(baseActivity, GoalsActivities.class);
        sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, null);
        intent.putExtra("nameActivity", "AllProduct");
        startActivity(intent);
//        getContext().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }*/

    @OnClick({R.id.ivMinusDetails, R.id.ivPlusDetails})
    public void expandDetails(View view) {
        switch (view.getId()) {
            case R.id.ivMinusDetails:
                ivPlusDetails.setVisibility(View.VISIBLE);
                ivMinusDetails.setVisibility(View.GONE);
                linearDetails.setVisibility(View.GONE);
                break;
            case R.id.ivPlusDetails:
                ivMinusDetails.setVisibility(View.VISIBLE);
                ivPlusDetails.setVisibility(View.GONE);
                linearDetails.setVisibility(View.VISIBLE);
                break;
        }
    }


    @OnClick(R.id.confirm_order_btn)
    public void placeOrder() {
        SharedPreferences preferences = baseActivity.getSharedPreferences("MyPref", 0);
        boolean check = true;
        if (ProductInCart.listAll(ProductInCart.class).size() != 0) {
            Log.e("***ProductIncart", ":::::::Not empty");
            SharedPreferences.Editor editor = preferences.edit();


            for (ProductInCart products : ProductInCart.listAll(ProductInCart.class)) {
                Log.e("***ProductID", "::::::" + products.productID);
                if (products.priceOriginal == 0)
                    check = false;
                editor.putBoolean("" + products.productID, false);
                editor.commit();
            }


        }

        if (check)
            getPlaceOrder();
    }


    public void onBackPressed() {
        hideDialog();
        if (moveTo.equals("business")) {
            sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "product");


            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusiness");
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        } else if (moveTo.equals("product")) {
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "ProductDetail");
            intent.putExtra("product_location", location);
            intent.putExtra("product_id", productId);
            intent.putExtra("brand_name", brandName);
            intent.putExtra("category_id", categoryID);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            GoalsActivities.total_items_bracket.setVisibility(View.GONE);
            Intent intent = new Intent(baseActivity, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

//    @Override
//    public void productClick(View v, int position) {
//    }
//    }

    public void getPlaceOrder() {

        cartList = ProductInCart.listAll(ProductInCart.class);
        //if (SaveOrder.listAll(SaveOrder.class).size() != 0) {
        //    SaveOrder.listAll(SaveOrder.class).clear();
        //}
        SaveOrder.LOrderNumber++;
        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray array = new JSONArray();
            orderIdInt++;
            for (ProductInCart productsInCart : cartList) {
                JSONObject object = new JSONObject();
                String disAmount = "0";
                if (productsInCart.qty != 0) {
                    object.put("OrderItemID", productsInCart.productID);
                    object.put("Quantity", productsInCart.qty);
                    object.put("VariantID", productsInCart.variantID);
                    Double d = new Double(productsInCart.price * value);
                    int price = d.intValue();

                    if (productsInCart.dealCategory.equals("product")) {
                        if (productsInCart.dealId.equals("0")) {
                            object.put("deal", productsInCart.dealId);
                            object.put("Cost", price);
                            subTotal += 0;
                            amount += 0;
                            discount += 0;
                            Double d2 = new Double((productsInCart.price * value) * (productsInCart.qty));
                            int amount = d2.intValue();

                            object.put("Amount", amount);
                        } else {
                            String dealType = productsInCart.dealType;
                            String dealAmount = productsInCart.dealAmount;
                            if (dealType.equals("Amount")) {
                                object.put("deal", productsInCart.dealId);
                                object.put("Cost", price);
                                disAmount = productsInCart.dealAmount;
                                object.put("Discount", disAmount);
                                Double priceAfterDiscountAmount = (productsInCart.price * value) - Double.parseDouble(productsInCart.dealAmount);

                                Double d3 = new Double(priceAfterDiscountAmount * (productsInCart.qty));
                                int amount2 = d3.intValue();

                                object.put("Amount", amount2);

                            } else {
                                Double discountAmount = (productsInCart.price * value) * Double.parseDouble(dealAmount) / 100;
                                disAmount = String.valueOf(dealAmount);

                                Double discAmt = (productsInCart.price * value) - discountAmount;
                                object.put("Discount", Math.round(discountAmount));
                                object.put("deal", productsInCart.dealId);
                                object.put("Cost", price);
                                Double d4 = new Double(discAmt * (productsInCart.qty));
                                int amount3 = d4.intValue();

                                object.put("Amount", amount3);
                            }
                        }

                    } else {
                        object.put("deal", productsInCart.dealId);
                        object.put("Cost", price);
                        subTotal += 0;
                        amount += 0;
                        discount += 0;
                        Double d5 = new Double((productsInCart.price * value) * (productsInCart.qty));
                        int amount4 = d5.intValue();

                        object.put("Amount", amount4);
                    }

                    object.put("UOM", productsInCart.uomST + "");
                    object.put("orderId", 0);

                    Log.e("***Array", "::::_object:::" + object.toString());


                    array.put(object);
                }
            }


            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.DAY_OF_MONTH, day);
            cal.set(Calendar.MONTH, month);

            String format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS").format(cal.getTime());
//                    Long Totalvalue = (Math.round(subTotal) + Math.round(rowAmountTax)) - (Math.round(discount));
            double Totalvalue = Double.parseDouble(totalAmountTv.getText().toString().replaceAll("₹ ", ""));

            jsonObject.put("OrderItems", array);
            jsonObject.put("OrderID", 0);
            jsonObject.put("LOrderNumber", SaveOrder.LOrderNumber);
            jsonObject.put("OrderDate", format + "Z");
            jsonObject.put("BusinessID", sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID));
            jsonObject.put("OrderNo", SaveOrder.LOrderNumber);
            jsonObject.put("SubTotal", rawAmount);
            jsonObject.put("TaxPercentage", 0);
            jsonObject.put("TaxAmount", rowAmountTax);
            jsonObject.put("DeliveryAmount", 0);
            jsonObject.put("DiscountAmount", discount);//DiscountAmount
            jsonObject.put("TotalOrderValue", Totalvalue);
            //jsonObject.put("dealID", 0);
            jsonObject.put("BusinessName", sharedPreference.getStringValue("BusinessName"));
            jsonObject.put("saveOrderNumber", String.valueOf(System.currentTimeMillis()));

            jsonObject.put("ExtraQuantity", extraquantity);
            jsonObject.put("GiftName", giftname);
            jsonObject.put("GiftQuantity", giftquantity);
            jsonObject.put("DealId", DealId);
            jsonObject.put("DealName", DealName);
            jsonObject.put("CouponAmount", couponAmount);

            Log.e("***Array", "::::jsonObject::::" + jsonObject.toString());
            Log.d("***Array", jsonObject.toString());

            saveOrderObject = builder.fromJson(jsonObject.toString(), SaveOrder.class);

            List<PendingOrderItem> itemList = saveOrderObject.getPendingOrderItems();

            //   saveOrderObject.setOrderNo(SaveOrder.LOrderNumber + "");
            for (int i = 0; i < itemList.size(); i++) {
                PendingOrderItem pendingOrderItem = itemList.get(i);
                pendingOrderItem.setOrderNumberID(SaveOrder.LOrderNumber + "");
                pendingOrderItem.setSaveOrderNumber(saveOrderObject.getSaveOrderNumber());
                pendingOrderItem.save();
            }
            saveOrderObject.setSendToServer("0");

            saveOrderObject.save();
            listSaveorderArray = new ArrayList<SaveOrder>();
            listSaveorderArray.add(saveOrderObject);

            SecurityLogsApi(listSaveorderArray, jsonObject.toString(), "/Order/SaveOrder");


            /*if (saveOrderObject.getPendingOrderItems().size() != 0)
            {
                if (Select.from(SaveOrder.class).where(Condition.prop("send_to_server").eq("0")).list().size() != 0) {
                    //if (Connectivity.isNetworkAvailable(baseActivity)) {

                }
                if (!isOrderPlaced) {
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
                            receiver = new NetworkChangeReceiver();

                            LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).registerReceiver(receiver, filter);
                        }
                    }, 1000);
                }
            } else {
                Toast.makeText(baseActivity, "no value found to order", Toast.LENGTH_SHORT).show();
            }*/
            //hideDialog();


        } catch (Exception ex) {
            //hideDialog();
            ex.printStackTrace();
        }
        Log.d("orderplace", "successfully");


        /*} else {

            Toast.makeText(ApplicationClass.getAppContext(), "you must have internet connection for placing orders.", Toast.LENGTH_SHORT).show();
        }*/
    }

    boolean isOrderPlaced = false;

    private void requestHttp(List<SaveOrder> listSaveorderArray) {
        if (Connectivity.isNetworkAvailable(ApplicationClass.getAppContext())) {
            isOrderPlaced = true;
            serviceHandler = NetworkManager.createRetrofitService(baseActivity, ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
            final List<SaveOrder> listSaveOrderListArray = listSaveorderArray;
            //Log.e("listSAvrORder", "::::" + listSaveOrderListArray.size());
            if (cartList.size() != 0)
            {
                showDialog();
                serviceHandler.placeOrder(listSaveorderArray, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        hideDialog();
                        String serverResponse = CommonUtils.getServerResponse(response);
//                        Log.e("ServerRes", ":::" + serverResponse);
                        try {
                            JSONObject jsonObject1 = new JSONObject(serverResponse);
                            if (jsonObject1.getString("Message").equals("Success")) {
                                for (SaveOrder saveOrder : listSaveOrderListArray) {
                                    saveOrder.setSendToServer("1");
                                    saveOrder.save();
                                }
                                ProductInCart.deleteAll(ProductInCart.class);
                                //  sharedPreference.saveStringData(PrefsHelper.BUSINESS_ID, "");
                                Toast.makeText(baseActivity, "Your order has been successfully placed", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(baseActivity, GoalsActivities.class);
                                intent.putExtra("nameActivity", "orderhistory");
                                intent.putExtra("isfrom", "cartfrag");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                baseActivity.finish();
                            } else {
                                for (SaveOrder saveOrder : listSaveorderArray) {
                                    List<PendingOrderItem> pendingOrderItemList = Select.from(PendingOrderItem.class).where(Condition.prop("save_order_number").eq(saveOrder.getSaveOrderNumber())).list();
                                    if (pendingOrderItemList != null) {
                                        for (PendingOrderItem pendingOrderItem : pendingOrderItemList) {
                                            pendingOrderItem.delete();
                                        }
                                    }
                                    saveOrder.delete();

                                }
                                ProductInCart.deleteAll(ProductInCart.class);

                                Intent intent = new Intent(baseActivity, GoalsActivities.class);
                                intent.putExtra("nameActivity", "orderhistory");
                                intent.putExtra("isfrom", "cartfrag");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                baseActivity.finish();
                            }


                        } catch (Exception ex) {
                            Log.e("EXCEPTION", "::::" + ex.toString());

//                            Log.e("Response:::", response.toString());
//                            Log.e("responseExc", "::::" + ex.toString());
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideDialog();
                        for (SaveOrder saveOrder : listSaveorderArray) {
                            List<PendingOrderItem> pendingOrderItemList = Select.from(PendingOrderItem.class).where(Condition.prop("save_order_number").eq(saveOrder.getSaveOrderNumber())).list();
                            if (pendingOrderItemList != null) {
                                for (PendingOrderItem pendingOrderItem : pendingOrderItemList) {
                                    pendingOrderItem.delete();
                                }
                            }
                            saveOrder.delete();

                        }
                        ProductInCart.deleteAll(ProductInCart.class);

                        Intent intent = new Intent(baseActivity, GoalsActivities.class);
                        intent.putExtra("nameActivity", "orderhistory");
                        intent.putExtra("isfrom", "cartfrag");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        baseActivity.finish();
                    }
                });
                // CommonUtils.dismissProgress();
            }
        } else {
            Toast.makeText(baseActivity, "Your offline order has been saved successfully", Toast.LENGTH_LONG).show();

            /*final List<SaveOrder> listSaveOrderListArray = listSaveorderArray;
            for (SaveOrder saveOrder : listSaveOrderListArray)
            {
                saveOrder.setSendToServer("0");
                saveOrder.save();
            }*/
            //Toast.makeText(ApplicationClass.getAppContext(), "you must have internet connection for placing orders.", Toast.LENGTH_SHORT).show();
            //Intent i = new Intent(baseActivity, GoalsActivities.class);
            //i.putExtra("nameActivity", "DashboardActivity");
            //startActivity(i);
            ProductInCart.deleteAll(ProductInCart.class);

            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "orderhistory");
            intent.putExtra("isfrom", "cartfrag");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            baseActivity.finish();


        }

    }

    //    @OnClick(R.id.update_business)
    public void updateBusiness() {
        sharedPreference.saveStringData(PrefsHelper.NAVI_PRODUCT_BUSINESS, "cartFragment");
        Intent intent = new Intent(baseActivity, GoalsActivities.class);
        intent.putExtra("nameActivity", "AllBusiness");
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }


    @OnClick(R.id.update_amount)
    public void updateAmount() {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custome_edit_dialog);

        final EditText text = (EditText) dialog.findViewById(R.id.txt_dia);
        text.setText(amount + "");

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_yes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float updateAmt = Float.parseFloat(text.getText().toString());
               /* if (amount < updateAmt) {
                    Toast.makeText(getActivity(), "update amount ", Toast.LENGTH_SHORT).show();
                }
                {*/

                if (!totalAmountTv.getText().equals(text.getText())) {
                    updated_ampunt_tv.setVisibility(View.VISIBLE);
                    updated_ampunt_tv.setText("" + text.getText());
                    amount = Float.parseFloat(text.getText().toString());
//                    totalAmountTv.setPaintFlags(totalAmountTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                    totalAmountTv.setTextColor(getActivity().getResources().getColor(R.color.dark_dim_gray));
                } else {
                    updated_ampunt_tv.setVisibility(View.GONE);
                }
                /* }*/
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void productClick(View v, int position, Boolean inStock) {

    }

    @Override
    public void notAvailableDialog() {

    }


   /* public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.v("Goals getting", "Receieved notification about network status");
            if (Connectivity.isNetworkAvailable(context)) {
                if (!isConnected) {
                    Log.v("is network Available", "Now you are connected to Internet!");
                    isConnected = true;
                    //  Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show();

                    if (Select.from(SaveOrder.class).where(Condition.prop("send_to_server").eq("0")).list().size() != 0) {
                        requestHttp(listSaveorderArray);
                    }

                    sharedPreference.saveBooleanValue("IsCartFragment", true);
                    if (ApplicationClass.getAppContext() != null)
                        LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);
                    //do your processing here ---
                    //if you need to post any data to the server or get status
                    //update from the server

                } else {
                    if (ApplicationClass.getAppContext() != null)
                        LocalBroadcastManager.getInstance(ApplicationClass.getAppContext()).unregisterReceiver(receiver);
                }
            } else {
                isConnected = false;
            }

        }


    }*/

    public void showDialog() {

        try {
            if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
                //mProgressDialog.setMessage("Connecting with server");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideDialog() {


        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
    //            Log.e("hideDialog", "::::::true");
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Double format2Decimal(double d) {
        //return (Double.parseDouble(String.format("%.2f", new BigDecimal(d))));

        /*BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();*/

        double scale = Math.pow(10, 2);
        return Math.round(d * scale) / scale;
    }

    private void SecurityLogsApi(List<SaveOrder> listSaveorderArray, String jsonobject, String api) {
        if (Connectivity.isNetworkAvailable(baseActivity)) {
            try {
                JSONObject object = new JSONObject();
                object.put("TokenID", sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN));
                object.put("CreatedDate", CommonUtils.CurrentDateTime());
                object.put("ApiName", api);
                object.put("JsonObject", jsonobject);
                object.put("SessionUserid", sharedPreference.getStringValue(PrefsHelper.USER_EMAIL));
                object.put("IpAddress", CommonUtils.getDeviceID(getActivity()));
                object.put("AppVersion", BuildConfig.VERSION_NAME);
                object.put("DeviceName", Build.MANUFACTURER + " " + Build.MODEL);
                object.put("OS", "Android");
                object.put("OSversion", Build.VERSION.RELEASE);

                SecurityLog securityLog = builder.fromJson(object.toString(), SecurityLog.class);
                serviceHandler = NetworkManager.createRetrofitService(getContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
                serviceHandler.SecurityLogs(securityLog, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        String arr = CommonUtils.getServerResponse(response);
                        Log.e(TAG, " success SecurityLogsApi arr " + arr);
                        try {
                            JSONObject jsonObject = new JSONObject(arr);
                            if (jsonObject != null && jsonObject.has("Message")) {
                                if (jsonObject.getString("Message").equalsIgnoreCase("Success")) {
                                    requestHttp(listSaveorderArray);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Log.e(TAG, " failure retrofitError arr " + retrofitError.getMessage());

                        for (SaveOrder saveOrder : listSaveorderArray) {
                            List<PendingOrderItem> pendingOrderItemList = Select.from(PendingOrderItem.class).where(Condition.prop("saveOrderNumber").eq(saveOrder.getSaveOrderNumber())).list();
                            if (pendingOrderItemList != null) {
                                for (PendingOrderItem pendingOrderItem : pendingOrderItemList) {
                                    pendingOrderItem.delete();
                                }
                            }
                            saveOrder.delete();

                        }
                        ProductInCart.deleteAll(ProductInCart.class);

                        Intent intent = new Intent(baseActivity, GoalsActivities.class);
                        intent.putExtra("nameActivity", "orderhistory");
                        intent.putExtra("isfrom", "cartfrag");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        baseActivity.finish();

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(baseActivity, "Your offline order has been saved successfully", Toast.LENGTH_LONG).show();

            /*final List<SaveOrder> listSaveOrderListArray = listSaveorderArray;
            for (SaveOrder saveOrder : listSaveOrderListArray)
            {
                saveOrder.setSendToServer("0");
                saveOrder.save();
            }*/
            //Toast.makeText(ApplicationClass.getAppContext(), "you must have internet connection for placing orders.", Toast.LENGTH_SHORT).show();
            //Intent i = new Intent(baseActivity, GoalsActivities.class);
            //i.putExtra("nameActivity", "DashboardActivity");
            //startActivity(i);
            ProductInCart.deleteAll(ProductInCart.class);

            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "orderhistory");
            intent.putExtra("isfrom", "cartfrag");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            baseActivity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            baseActivity.finish();
        }

    }

    void ShowDealDialog(List<AllDeals> allDealsList) {
        contacts_dialog = new Dialog(getActivity());
        contacts_dialog.setContentView(R.layout.deal_dialog_layout);
        contacts_dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        TextView textViewDealBack = (TextView) contacts_dialog.findViewById(R.id.textViewDealBack);
        textViewDealBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacts_dialog.dismiss();
            }
        });

        EditText editTextPromoCode = (EditText) contacts_dialog.findViewById(R.id.editTextPromoCode);

        TextView textViewApply = (TextView) contacts_dialog.findViewById(R.id.textViewApply);


        textViewApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextPromoCode.getText().toString().trim();
                if (code.isEmpty())
                    CommonUtils.ShowDialog(getActivity(), "Please Enter Promo Code");
                else {
                    SelectedDealClick(0, code);
                    contacts_dialog.dismiss();
                }

            }
        });

        RecyclerView recyclerViewDeal = (RecyclerView) contacts_dialog.findViewById(R.id.recyclerViewDeal);
        mLayoutManager = new LinearLayoutManager(baseActivity);
        recyclerViewDeal.setLayoutManager(mLayoutManager);
        OffersDealAdapter cardAdapter = new OffersDealAdapter(getActivity(), CartFragment.this, allDealsList);
        recyclerViewDeal.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();

        contacts_dialog.show();
    }

    public void SelectedDealClick(int position, String pomoCode) {
        if (contacts_dialog != null && contacts_dialog.isShowing())
            contacts_dialog.dismiss();
        if (!pomoCode.isEmpty()) {
            Log.e(TAG, " SelectedDealClick pomoCode " + pomoCode);

            selectedDeal = Select.from(AllDeals.class).where(Condition.prop("coupon_code").eq(pomoCode)).first();
            if (selectedDeal != null)
                DealCalculation();
            else
                CommonUtils.ShowDialog(getActivity(), "Invalid Promo Code");
        } else {
            selectedDeal = availableDeals.get(position);
            DealCalculation();
        }
    }

    public void DealCalculation() {

        Log.e(TAG, " DealCalculation selectedDeal " + selectedDeal);
        linearLayoutType.setVisibility(View.GONE);
        ResetDealApiValues();
        if (selectedDeal != null) {


            Log.e(TAG, " DealCalculation BUSINESS_ID " + sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID));
            Log.e(TAG, " DealCalculation getBussinessID " + selectedDeal.getBussinessID());
            Log.e(TAG, " DealCalculation subTotalAmountForOrderDeal " + subTotalAmountForOrderDeal);
            Log.e(TAG, " DealCalculation getOrderAmount " + selectedDeal.getOrderAmount());
            Log.e(TAG, " DealCalculation getDealType " + selectedDeal.getDealType());

            if (!selectedDeal.getBussinessID().contains(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID))) {
                CommonUtils.ShowDialog(getActivity(), "Deal is not applicable selected busniess");
                selectedDeal = null;
                return;
            }

            if (selectedDeal.getDealType().equalsIgnoreCase("order")) {
                if (subTotalAmountForOrderDeal < Double.parseDouble(selectedDeal.getOrderAmount())) {
                    Log.e(TAG, " subTotalAmountForOrderDeal is less then order Amount  ");
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applied Min Order of ₹ " + selectedDeal.getOrderAmount() + " is required");
                    selectedDeal = null;
                    return;
                }
            }

            String dealType = selectedDeal.getDealType();
            String dealApplicableAs = selectedDeal.getDealApplicableAs();
            String dealAmount = selectedDeal.getAmount(); // Amount Deal
            String dealCouponAmount = selectedDeal.getCouponAmount(); // Coupon Deal
            String dealPercentage = selectedDeal.getPercentage(); // Percentage Deal

            ApplicationClass.getInstance().setDealId(String.valueOf(selectedDeal.getDealID()));
            ApplicationClass.getInstance().setFromDeal(true);

            String dealGiftName = selectedDeal.getGiftName(); // Percentage Deal

            String dealOrderAmount = selectedDeal.getOrderAmount(); // Percentage Deal
            String dealDiscountQuantity = selectedDeal.getDiscountQuantity(); // Percentage Deal
            String dealQuantity = selectedDeal.getQuantity(); // Percentage Deal
            linearLayoutDiscount.setVisibility(View.GONE);

            rowAmountTax = 0;
            rawAmount = 0;
            discount = 0;
            subTotal = 0;
            amount = 0;
            int tempQuantity = 0;
            double tempTax = 0;
            double rawAmounWithoutDiscount = 0;
            DealId = selectedDeal.getDealID();
            DealName = selectedDeal.getDealTitle();
            int productCount = 0;


            List<ProductInCart> cartItemList = Select.from(ProductInCart.class).list();
            if (cartItemList != null && cartItemList.size() == 0) {
                linearLayoutDeals.setVisibility(View.GONE);
                linearLayoutDiscount.setVisibility(View.GONE);
            }


            for (ProductInCart productInCart : cartItemList) {
                if (dealType.equalsIgnoreCase("product")) {
                    if (dealApplicableAs.equalsIgnoreCase("Percentage")) {

                        Double baseprice = 0.0, temp = 0.0, tempAmount = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        tempAmount = baseprice;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);


                        if (selectedDeal.getProductID().contains(String.valueOf(productInCart.productID))) {
                            Log.e(TAG, " DealCalculation Product Id Present ");
                            temp = (baseprice * Double.parseDouble(dealPercentage)) / 100;
                            temp = format2Decimal(temp);
                            baseprice = baseprice - temp;
                            Log.e(TAG, " DealCalculation Sub Total After Discount " + baseprice);
                            productCount = productCount + 1;
                        }
                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);

                        rawAmount = rawAmount + baseprice;
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + tempAmount;

                        discount = discount + temp;

                    } else if (dealApplicableAs.equalsIgnoreCase("Amount")) {

                        Double baseprice = 0.0, temp = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);
                        Double tempAmount = baseprice;


                        if (selectedDeal.getProductID().contains(String.valueOf(productInCart.productID))) {
                            Log.e(TAG, " DealCalculation Product Id Present ");
                            temp = productInCart.qty * Double.parseDouble(dealAmount);
                            temp = format2Decimal(temp);
                            baseprice = baseprice - temp;
                            Log.e(TAG, " DealCalculation Sub Total After Discount " + baseprice);
                            productCount = productCount + 1;
                        }
                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);

                        rawAmount = rawAmount + baseprice;
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + tempAmount;

                        discount = discount + temp;
                    } else if (dealApplicableAs.equalsIgnoreCase("Coupon")) {

                        Double baseprice = 0.0, temp = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);
                        Double tempAmount = baseprice;


                        if (selectedDeal.getProductID().contains(String.valueOf(productInCart.productID))) {
                            Log.e(TAG, " DealCalculation Product Id Present ");
                            temp = productInCart.qty * Double.parseDouble(dealCouponAmount);
                            temp = format2Decimal(temp);
                            //baseprice = baseprice - temp;
                            Log.e(TAG, " DealCalculation Sub Total After Discount " + baseprice);
                            productCount = productCount + 1;
                        }
                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + tempAmount;

                        rawAmount = rawAmount + baseprice;

                        discount = discount + temp;
                    } else if (dealApplicableAs.equalsIgnoreCase("Quantity")) {

                        Double baseprice = 0.0, temp = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);

                        Double tempAmount = baseprice;

                        if (selectedDeal.getProductID().contains(String.valueOf(productInCart.productID))) {
                            Log.e(TAG, " DealCalculation Product Id Present ");
                            tempQuantity = tempQuantity + productInCart.qty;
                            productCount = productCount + 1;
                        }
                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);

                        rawAmount = rawAmount + baseprice;
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + tempAmount;

                        discount = discount + temp;
                    }
                } else if (dealType.equalsIgnoreCase("order")) {
                    if (dealApplicableAs.equalsIgnoreCase("Gift") || dealApplicableAs.equalsIgnoreCase("Coupon") ||
                            dealApplicableAs.equalsIgnoreCase("Amount")) {
                        Double baseprice = 0.0, temp = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);

                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);

                        rawAmount = rawAmount + baseprice;
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + baseprice;

                        tempTax = tempTax + productInCart.cGSTPercentage + productInCart.sGSTPercentage;

                        discount = discount + temp;

                    } else if (dealApplicableAs.equalsIgnoreCase("Percentage")) {

                        Double baseprice = 0.0, temp = 0.0;

                        baseprice = productInCart.price;
                        Log.e(TAG, " DealCalculation baseprice " + baseprice);
                        Log.e(TAG, " DealCalculation qty " + productInCart.qty);

                        baseprice = baseprice * productInCart.qty;
                        Log.e(TAG, " DealCalculation Sub Total " + productInCart.qty);

                        temp = (baseprice * Double.parseDouble(dealPercentage)) / 100;
                        temp = format2Decimal(temp);
                        //baseprice = baseprice - temp;
                        Log.e(TAG, " DealCalculation Sub Total After Discount " + baseprice);

                        double cgstRate = 0.0;
                        double sgstRate = 0.0;

                        if (productInCart.cGSTPercentage != null) {

                            cgstRate = (baseprice * productInCart.cGSTPercentage) / 100;
                        }
                        if (productInCart.sGSTPercentage != null) {
                            sgstRate = (baseprice * productInCart.sGSTPercentage) / 100;
                        }

                        cgstRate = format2Decimal(cgstRate);
                        sgstRate = format2Decimal(sgstRate);

                        Log.e(TAG, " DealCalculation cgstRate " + cgstRate);
                        Log.e(TAG, " DealCalculation sgstRate " + sgstRate);
                        Log.e(TAG, " DealCalculation rowAmountTax " + rowAmountTax);


                        rowAmountTax = rowAmountTax + cgstRate + sgstRate;
                        rowAmountTax = format2Decimal(rowAmountTax);

                        rawAmount = rawAmount + baseprice;
                        rawAmounWithoutDiscount = rawAmounWithoutDiscount + baseprice;

                        discount = discount + temp;

                        tempTax = tempTax + productInCart.cGSTPercentage + productInCart.sGSTPercentage;

                    }
                }
            }
            tvtax.setText("₹" + " " + Double.parseDouble(new DecimalFormat("##.##").format(rowAmountTax)));

            Double subTotalAmount = Double.parseDouble(new DecimalFormat("##.##").format(rawAmounWithoutDiscount));

            Double totalAmount = Double.parseDouble(new DecimalFormat("##.##").format(rawAmounWithoutDiscount + rowAmountTax));

            subTotalAmount = format2Decimal(subTotalAmount);
            totalAmount = format2Decimal(totalAmount);
            discount = format2Decimal(discount);

            totalAmount = totalAmount - discount;

            Log.e(TAG, " DealCalculation discount " + discount);
            Log.e(TAG, " DealCalculation tempQuantity " + tempQuantity);

            //int DTotalAmount = (int)amount + (int)rowAmountTax;
            rowTotalAmount.setText(String.format("₹ %.2f", subTotalAmount));
            totalAmountTv.setText(String.format("₹ %.2f", totalAmount));

            rawAmount = rawAmounWithoutDiscount;

            linearLayoutApplyDeals.setVisibility(View.GONE);
            relativeLayoutAppliedDeal.setVisibility(View.VISIBLE);

            if (dealType.equalsIgnoreCase("product") && dealApplicableAs.equalsIgnoreCase("Percentage")) {
                if (discount > 0) {
                    linearLayoutDiscount.setVisibility(View.VISIBLE);
                    total_discount_head.setText("Deal Applied (Percentage)");
                    textViewDealAppliedType.setText("Deal Applied as Percentage");
                    textViewDealAppliedTypeDescription.setText(dealPercentage + "% Discount Applied on order selected product");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    totalDiscountTv.setText(String.valueOf(discount));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText(String.format("₹ %.2f", discount));
                    textViewDealDescription.setText("Discount of " + dealPercentage + "% Applied on selected products");
                    couponAmount = Double.parseDouble(dealPercentage);
                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applied selected products");
                    selectedDeal = null;
                    ResetDealApiValues();
                }

            } else if (dealType.equalsIgnoreCase("product") && dealApplicableAs.equalsIgnoreCase("Amount")) {
                if (discount > 0) {
                    linearLayoutDiscount.setVisibility(View.VISIBLE);
                    total_discount_head.setText("Deal Applied (Amount)");
                    textViewDealAppliedType.setText("Deal Applied as Amount");
                    textViewDealAppliedTypeDescription.setText(String.format("₹ %.2f", discount) + " Discount Applied on order " + productCount + " product");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    totalDiscountTv.setText(String.valueOf(discount));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText(String.format("₹ %.2f", discount));
                    textViewDealDescription.setText("Discount of ₹ " + dealAmount + " Applied on selected products");
                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applied selected products");
                    selectedDeal = null;
                    ResetDealApiValues();
                }


            } else if (dealType.equalsIgnoreCase("product") && dealApplicableAs.equalsIgnoreCase("Coupon")) {

                if (discount > 0) {
                    linearLayoutDiscount.setVisibility(View.GONE);
                    total_discount_head.setText("Deal Applied (Coupon)");
                    textViewDealAppliedType.setText("Deal Applied as Coupon");
                    textViewDealAppliedTypeDescription.setText(String.format("₹ %.2f", discount) + " amount of coupon added on order of " + productCount + " product");

                    linearLayoutType.setVisibility(View.VISIBLE);
                    totalDiscountTv.setText(String.valueOf(0.0));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText(String.format("₹ %.2f", discount));
                    textViewDealDescription.setText("Get Coupon of Amount ₹ " + discount);
                    couponAmount = discount;
                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applied selected products");
                    selectedDeal = null;
                    ResetDealApiValues();
                }

            } else if (dealType.equalsIgnoreCase("product") && dealApplicableAs.equalsIgnoreCase("Quantity")) {
                int temp = tempQuantity / Integer.parseInt(selectedDeal.getQuantity());
                Log.e(TAG, " DealCalculation Quantity temp " + temp);
                Log.e(TAG, " DealCalculation Quantity getDiscountQuantity " + selectedDeal.getDiscountQuantity());
                Log.e(TAG, " DealCalculation Quantity getGiftName " + selectedDeal.getGiftName());

                if (temp > 0) {
                    int a = Integer.parseInt(selectedDeal.getDiscountQuantity());
                    if (a != 0)
                        temp = temp * a;

                    Log.e(TAG, " DealCalculation Quantity final temp " + temp);
                    textViewDealAppliedType.setText("Deal Applied as Quantity");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    linearLayoutDiscount.setVisibility(View.GONE);
                    total_discount_head.setText("Deal Applied (Quantity)");

                    totalDiscountTv.setText(String.valueOf(0.0));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    if (selectedDeal.getDiscountQuantity() != null && !selectedDeal.getDiscountQuantity().isEmpty() &&
                            !selectedDeal.getDiscountQuantity().equalsIgnoreCase("0")) {
                        textViewDealDetails.setText(temp + " Device Free");
                        textViewDealDescription.setText("Get " + temp + " Device on purchase of " + tempQuantity + " products");
                        extraquantity = temp;
                        textViewDealAppliedTypeDescription.setText(temp + " Extra Device added on purchase of Min " + tempQuantity + " products.");

                    } else {
                        textViewDealDetails.setText(temp + " " + selectedDeal.getGiftName() + " Free");
                        textViewDealDescription.setText("Get " + temp + " " + selectedDeal.getGiftName() + " on purchase of " + tempQuantity + " products");
                        textViewDealAppliedTypeDescription.setText(temp + " " + selectedDeal.getGiftName() + " added on purchase of Min " + tempQuantity + " products.");

                        giftname = selectedDeal.getGiftName();
                        giftquantity = temp;
                    }

                } else if (tempQuantity == 0) {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applicable selected Products");
                    selectedDeal = null;
                    ResetDealApiValues();
                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    CommonUtils.ShowDialog(getActivity(), "Deal is not applied. Min " + selectedDeal.getQuantity() + " Quantity reqiured");
                    selectedDeal = null;
                    ResetDealApiValues();
                }

            } else if (dealType.equalsIgnoreCase("order") && dealApplicableAs.equalsIgnoreCase("Percentage")) {
                Log.e(TAG, " DealCalculation order Percentage rawAmount " + rawAmount);
                Log.e(TAG, " DealCalculation order Percentage discount " + discount);

                double temp1 = rawAmount - discount;

                Log.e(TAG, " DealCalculation order temp1 " + temp1);

                tempTax = tempTax / cartItemList.size();

                Log.e(TAG, " DealCalculation order tempTax " + tempTax);

                tempTax = (temp1 * tempTax) / 100;

                Log.e(TAG, " DealCalculation order Toal Tax " + tempTax);

                tvtax.setText("₹" + " " + Double.parseDouble(new DecimalFormat("##.##").format(tempTax)));

                textViewDealAppliedType.setText("Deal Applied as Percentage");
                linearLayoutType.setVisibility(View.VISIBLE);

                linearLayoutDiscount.setVisibility(View.VISIBLE);
                total_discount_head.setText("Deal Applied (Percentage)");
                totalDiscountTv.setText(String.valueOf(discount));
                totalDiscountTv.setVisibility(View.VISIBLE);
                total_discount_head.setVisibility(View.VISIBLE);
                textViewDealDetails.setText(String.format("₹ %.2f", discount));
                textViewDealDescription.setText("Discount of " + dealPercentage + "% Applied on your Order");
                textViewDealAppliedTypeDescription.setText(dealPercentage + "% Discount Applied on min order of ₹ " + selectedDeal.getOrderAmount());

                couponAmount = Double.parseDouble(dealPercentage);

            } else if (dealType.equalsIgnoreCase("order") && dealApplicableAs.equalsIgnoreCase("gift")) {
                double temp = rawAmount / Double.parseDouble(selectedDeal.getOrderAmount());
                int value = (int) temp;
                Log.e(TAG, " DealCalculation Quantity value " + value);
                Log.e(TAG, " DealCalculation Quantity temp " + temp);

                Log.e(TAG, " DealCalculation Quantity getDiscountQuantity " + selectedDeal.getDiscountQuantity());
                Log.e(TAG, " DealCalculation Quantity getGiftName " + selectedDeal.getGiftName());

                if (value > 0) {
                    linearLayoutDiscount.setVisibility(View.GONE);
                    total_discount_head.setText("Deal Applied (Gift)");
                    textViewDealAppliedType.setText("Deal Applied as Gift");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    totalDiscountTv.setText(String.valueOf(0.0));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText(value + " " + selectedDeal.getGiftName() + " Free");
                    textViewDealDescription.setText("Get " + value + " " + selectedDeal.getGiftName() + " on purchase of ₹ " + rawAmount);
                    giftname = selectedDeal.getGiftName();
                    giftquantity = value;
                    textViewDealAppliedTypeDescription.setText(temp + " " + selectedDeal.getGiftName() + " added on purchase Min order of ₹ " + selectedDeal.getOrderAmount());

                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    ResetDealApiValues();
                }

            } else if (dealType.equalsIgnoreCase("order") && dealApplicableAs.equalsIgnoreCase("Amount")) {
                double temp = rawAmount / Double.parseDouble(selectedDeal.getOrderAmount());
                int value = (int) temp;
                Log.e(TAG, " DealCalculation Amount value " + value);
                Log.e(TAG, " DealCalculation Amount temp " + temp);

                Log.e(TAG, " DealCalculation Quantity Amount " + selectedDeal.getDiscountQuantity());
                Log.e(TAG, " DealCalculation Quantity Amount " + selectedDeal.getGiftName());

                temp = value * Double.parseDouble(selectedDeal.getAmount());

                rawAmount = rawAmount - temp;

                if (value > 0) {

                    Log.e(TAG, " DealCalculation order Amount rawAmount " + rawAmount);
                    Log.e(TAG, " DealCalculation order Amount discount " + discount);

                    double temp1 = rawAmount;

                    Log.e(TAG, " DealCalculation order Amount temp1 " + temp1);

                    tempTax = tempTax / cartItemList.size();

                    Log.e(TAG, " DealCalculation order Amount tempTax " + tempTax);

                    tempTax = (temp1 * tempTax) / 100;

                    Log.e(TAG, " DealCalculation order Amount Toal Tax " + tempTax);

                    tvtax.setText("₹" + " " + Double.parseDouble(new DecimalFormat("##.##").format(tempTax)));


                    linearLayoutDiscount.setVisibility(View.VISIBLE);
                    total_discount_head.setText("Deal Applied (Amount)");
                    textViewDealAppliedType.setText("Deal Applied as Amount");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    totalDiscountTv.setText(String.valueOf(temp));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText("₹ " + temp);
                    textViewDealDescription.setText("Discount of ₹ " + temp + " Applied");
                    discount = temp;
                    textViewDealAppliedTypeDescription.setText("₹" + temp + " discount applied added on Min order of ₹ " + selectedDeal.getOrderAmount());

                    //rowTotalAmount.setText(String.format("₹ %.2f", rawAmount));
                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    ResetDealApiValues();
                }

            } else if (dealType.equalsIgnoreCase("order") && dealApplicableAs.equalsIgnoreCase("Coupon")) {
                double temp = rawAmount / Double.parseDouble(selectedDeal.getOrderAmount());
                int value = (int) temp;
                Log.e(TAG, " DealCalculation Quantity value " + value);
                Log.e(TAG, " DealCalculation Quantity temp " + temp);

                Log.e(TAG, " DealCalculation Quantity getDiscountQuantity " + selectedDeal.getDiscountQuantity());
                Log.e(TAG, " DealCalculation Quantity getGiftName " + selectedDeal.getGiftName());

                temp = value * Double.parseDouble(selectedDeal.getAmount());
                rawAmount = rawAmount - temp;
                if (value > 0) {
                    textViewDealAppliedType.setText("Deal Applied as Coupon");
                    linearLayoutType.setVisibility(View.VISIBLE);
                    linearLayoutDiscount.setVisibility(View.GONE);
                    total_discount_head.setText("Deal Applied (Coupon)");
                    totalDiscountTv.setText(String.valueOf(0.0));
                    totalDiscountTv.setVisibility(View.VISIBLE);
                    total_discount_head.setVisibility(View.VISIBLE);
                    textViewDealDetails.setText("₹ " + temp);
                    textViewDealDescription.setText("Get Coupon of Amount ₹ " + temp);
                    couponAmount = temp;
                    textViewDealAppliedTypeDescription.setText(String.format("₹ %.2f", discount) + " amount of coupon added on order of ₹ " + selectedDeal.getOrderAmount());

                } else {
                    linearLayoutApplyDeals.setVisibility(View.VISIBLE);
                    relativeLayoutAppliedDeal.setVisibility(View.GONE);
                    ResetDealApiValues();
                }

            }
        }
        textViewDealAppliedTypeDescription.setVisibility(View.GONE);
    }

    void ResetDealApiValues() {
        extraquantity = 0;
        giftname = "";
        giftquantity = 0;
        couponAmount = 0.0;
        DealId = 0;
        DealName = "";
        linearLayoutType.setVisibility(View.GONE);
        textViewDealAppliedType.setText("");
    }
}
