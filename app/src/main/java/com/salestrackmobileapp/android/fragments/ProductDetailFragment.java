package com.salestrackmobileapp.android.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.adapter.ProductPagerAdapter;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.gson.AllProduct;
import com.salestrackmobileapp.android.gson.UOMArray;
import com.salestrackmobileapp.android.interfaces.NotAvailableStock;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ProductDetailFragment extends BaseFragment implements RecyclerClick, NotAvailableStock {

    @BindView(R.id.product_view_pager)
    ViewPager productViewPager;
    @BindView(R.id.left_nav)
    ImageView leftNavImg;
    @BindView(R.id.right_nav)
    ImageView rightNavImg;

    public static Custome_BoldTextView continueTxt;

    ProductPagerAdapter pagerAdapter;
    List<AllProduct> allProductList;
    int location = 0, productId = 0, defaultId = 0;
    Integer categoryID = 0, dealId = 0;
    String brandName = "", dealType = "";
    String businessId;
    boolean available,backToCheckIn;
    RecyclerView listDealOnProductRv;
    private ProgressDialog mProgressDialog;

    IntentFilter filter;

    String dealFragment = "";


    private List<UOMArray> uomArray = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        ButterKnife.bind(this, view);
        GoalsActivities.totalAmt.setVisibility(View.VISIBLE);
        CommonUtils.variantID = 0;

        listDealOnProductRv = (RecyclerView) view.findViewById(R.id.list_deal_rv);
        allProductList = AllProduct.listAll(AllProduct.class);
        continueTxt = (Custome_BoldTextView) view.findViewById(R.id.continue_txt);


        if (getArguments() != null) {
            location = getArguments().getInt("ProductLocation");
            productId = getArguments().getInt("product_id");
            categoryID = getArguments().getInt("category_id");
            brandName = getArguments().getString("brandname");
            defaultId = getArguments().getInt("defaultID");
            available = getArguments().getBoolean("available");
            backToCheckIn = getArguments().getBoolean("backoncheckin",false);


            if (getArguments().containsKey("listofdeals")) {
                dealFragment = getArguments().getString("listofdeals");
            }
            if (getArguments().containsKey("dealID")) {

            }
            if (productId == 0) {
                location = sharedPreference.getIntValue(PrefsHelper.PRODUCT_LOC);
                categoryID = sharedPreference.getIntValue(PrefsHelper.CATEGORYID);
                brandName = sharedPreference.getStringValue(PrefsHelper.BRAND_NAME);
                productId = sharedPreference.getIntValue(PrefsHelper.PRODUCT_ID_FOR_DETAIL);
            } else {
                sharedPreference.saveIntData(PrefsHelper.CATEGORYID, categoryID);
                sharedPreference.saveStringData(PrefsHelper.BRAND_NAME, brandName);
                sharedPreference.saveIntData(PrefsHelper.PRODUCT_ID_FOR_DETAIL, productId);
                sharedPreference.saveIntData(PrefsHelper.PRODUCT_LOC, location);
            }

            if (categoryID != 0) {
                allProductList.clear();
                if (brandName == null || brandName.equals("null") || brandName.equals("")||brandName.equalsIgnoreCase("Select Brand")) {
                    brandName = "";
                    allProductList = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID)).list();
                } else {

                    allProductList = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(categoryID), Condition.prop("brand_name").eq(brandName)).list();
                }
            } else {
                if (getArguments().getString("product_name") != null) {
                    allProductList = Select.from(AllProduct.class).where(Condition.prop("product_name").eq(getArguments().getString("product_name"))).list();
                } else {
                    allProductList = Select.from(AllProduct.class).list();
                }
            }
        } else {

            allProductList = Select.from(AllProduct.class).where(Condition.prop("product_category_id").eq(sharedPreference.getIntValue(PrefsHelper.CATEGORYID)), Condition.prop("brand_name").eq(sharedPreference.getStringValue(PrefsHelper.BRAND_NAME))).list();
        }

        if (allProductList.size() == 0) {
            Toast.makeText(getContext(), "No Detail available", Toast.LENGTH_SHORT).show();
            hideDialog();
            Intent inten = new Intent(getContext(), DashboardActivity.class);
            startActivity(inten);
            baseActivity.finish();
        }

        getUmoList();

        setEvent();

        businessId = sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID);
        pagerAdapter = new ProductPagerAdapter(baseActivity, businessId, this);
        pagerAdapter.setListOfProduct(allProductList, uomArray, location);
        productViewPager.setAdapter(pagerAdapter);
        productViewPager.setOffscreenPageLimit(3);

        productViewPager.setCurrentItem(location);

//        if (ProductInCart.listAll(ProductInCart.class).size() != 0) {
//             continueTxt.setVisibility(View.VISIBLE);
//        } else {
//            continueTxt.setVisibility(View.GONE);
//        }


        return view;
    }

    private void getUmoList() {
        uomArray.clear();
        uomArray.addAll(UOMArray.listAll(UOMArray.class));
        if (uomArray.size() == 0)
            getMeasurementValues();
    }

    private void setEvent() {
        leftNavImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = productViewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    productViewPager.setCurrentItem(tab);
                    pagerAdapter.notifyDataSetChanged();
                } else if (tab == 0) {
                    productViewPager.setCurrentItem(tab);
                    pagerAdapter.notifyDataSetChanged();
                }
            }

        });

        // Images right navigation
        rightNavImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = productViewPager.getCurrentItem();
                tab++;
                productViewPager.setCurrentItem(tab);
                pagerAdapter.notifyDataSetChanged();
            }
        });
    }

    public void onBackPressed() {
        if (!dealFragment.equals("")) {
            baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, new ListOfDealsProductFragment()).commit();
        } else {
            hideDialog();
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "AllProduct");
            if (backToCheckIn)
                intent.putExtra("backoncheckin", "true");
            else
                intent.putExtra("backoncheckin", "false");
            startActivity(intent);
        }
    }

    @OnClick(R.id.continue_txt)
    public void continueShopping() {

//        if (!ProductPagerAdapter.uomSt.equals("")) {

        if (sharedPreference.getStringValue(PrefsHelper.BUSINESS_CHECKED).equals("true")) {

            if (businessId == null || businessId.equals("")) {


                if (!(sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID).equals("") || sharedPreference.getStringValue(PrefsHelper.BUSINESS_ID) == null)) {
                    Bundle data = new Bundle();
                    data.putString("product_location", String.valueOf(location));
                    data.putString("product_id", String.valueOf(productId));
                    data.putString("brand_name", brandName);
                    data.putString("category_id", String.valueOf(categoryID));
                    data.putString("nameActivity", "cart_fragment");
                    data.putString("moveto", "product");
                    FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                    CartFragment fragment = new CartFragment();
                    fragment.setArguments(data);
                    ft.replace(R.id.container, fragment);

                } else {
                    hideDialog();
                    Intent intent = new Intent(baseActivity, GoalsActivities.class);
                    intent.putExtra("nameActivity", "AllBusiness");
                    intent.putExtra("navigateToProduct", "productDetail");
                    startActivity(intent);
                }
            } else {


                Bundle data = new Bundle();
                data.putString("product_location", String.valueOf(location));
                data.putString("product_id", String.valueOf(productId));
                data.putString("brand_name", brandName);
                data.putString("category_id", String.valueOf(categoryID));
                data.putString("nameActivity", "cart_fragment");
                data.putString("moveto", "product");
                FragmentTransaction ft = ((GoalsActivities) baseActivity).getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
                CartFragment fragment = new CartFragment();
                fragment.setArguments(data);
                ft.replace(R.id.container, fragment);
                ft.commit();
            }


        } else {
            hideDialog();
            Intent intent = new Intent(baseActivity, GoalsActivities.class);
            intent.putExtra("nameActivity", "AllBusiness");
            intent.putExtra("navigateToProduct", "productDetail");
            startActivity(intent);
        }
//        }
//        else {
//            Toast.makeText(baseActivity, "Please select quantity", Toast.LENGTH_SHORT).show();
//        }
    }

    public void getMeasurementValues() {
        mProgressDialog = new ProgressDialog(baseActivity);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
        mProgressDialog.show();
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getActivity(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getAllMeasurement(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                String arr = CommonUtils.getServerResponse(response);
                try {
                    JSONArray jsonArr = new JSONArray(arr);
                    for (int i = 0; i < jsonArr.length(); i++) {
                        String stringJson = jsonArr.get(i).toString();
                        UOMArray uomArray = builder.fromJson(stringJson, UOMArray.class);
                        uomArray.save();
                    }

                    uomArray.addAll(UOMArray.listAll(UOMArray.class));
                    refreshAdapter();
                    hideDialog();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    hideDialog();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                uomArray.clear();
                refreshAdapter();
                hideDialog();
            }
        });
    }

    private void refreshAdapter() {
        pagerAdapter.notifyDataSetChanged();
        productViewPager.setCurrentItem(location);
    }

    @Override
    public void productClick(View v, int position, Boolean inStock) {
    }

    @Override
    public void notAvailableDialog() {

    }

    public void showDialog() {
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void notAvailablDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_not_available);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        AppCompatButton dialogButton = (AppCompatButton) dialog.findViewById(R.id.btnOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void notAvailableStock() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_not_available);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        AppCompatButton dialogButton = (AppCompatButton) dialog.findViewById(R.id.btnOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
