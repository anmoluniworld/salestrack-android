
package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SaveException implements Serializable {

    public String getExceptionMessage() {
        return ExceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        ExceptionMessage = exceptionMessage;
    }

    public String getControllerName() {
        return ControllerName;
    }

    public void setControllerName(String controllerName) {
        ControllerName = controllerName;
    }

    public String getExceptionStarckTrace() {
        return ExceptionStarckTrace;
    }

    public void setExceptionStarckTrace(String exceptionStarckTrace) {
        ExceptionStarckTrace = exceptionStarckTrace;
    }

    public String getLogtime() {
        return logtime;
    }

    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }

    public String getSalespersonId() {
        return SalespersonId;
    }

    public void setSalespersonId(String salespersonId) {
        SalespersonId = salespersonId;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    public String getIsAdmin() {
        return IsAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        IsAdmin = isAdmin;
    }

    public String getUserProfileId() {
        return UserProfileId;
    }

    public void setUserProfileId(String userProfileId) {
        UserProfileId = userProfileId;
    }

    @SerializedName("ExceptionMessage")
    @Expose
    private String ExceptionMessage;

    @SerializedName("ControllerName")
    @Expose
    private String ControllerName;

    //@SerializedName("ExceptionStarckTrace")
    //@Expose
    private String ExceptionStarckTrace;

    @SerializedName("logtime")
    @Expose
    private String logtime;

    @SerializedName("SalespersonId")
    @Expose
    private String SalespersonId;

    //@SerializedName("ModuleName")
    //@Expose
    private String ModuleName;

    //@SerializedName("IsAdmin")
    //@Expose
    private String IsAdmin;

    @SerializedName("UserProfileId")
    @Expose
    private String UserProfileId;

    @SerializedName("CompanyCode")
    @Expose
    private String companyCode;



}
