package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddChats implements Serializable {
    @SerializedName("MessageId")
    @Expose
    private Integer messageId;
    @SerializedName("SenderId")
    @Expose
    private Integer senderId;
    @SerializedName("ReceiverId")
    @Expose
    private Integer receiverId;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("IsRead")
    @Expose
    private Integer isRead;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Boolean getAttachment() {
        return isAttachment;
    }

    public void setAttachment(Boolean attachment) {
        isAttachment = attachment;
    }

    @SerializedName("IsAttachment")
    @Expose
    private Boolean isAttachment;

}
