package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class ProductMapping extends SugarRecord implements Serializable {

    @SerializedName("dailyGoals")
    @Expose
    private DailyGoals dailyGoals;
    @SerializedName("DailyGoalsMappingID")
    @Expose
    private Integer dailyGoalsMappingID;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("CreatedBy")
    @Expose
    private Object createdBy;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("ModifiedBy")
    @Expose
    private Object modifiedBy;
    @SerializedName("ModifiedDate")
    @Expose
    private Object modifiedDate;
    @SerializedName("OrgId")
    @Expose
    private Integer orgId;

    public DailyGoals getDailyGoals() {
        return dailyGoals;
    }

    public void setDailyGoals(DailyGoals dailyGoals) {
        this.dailyGoals = dailyGoals;
    }

    public Integer getDailyGoalsMappingID() {
        return dailyGoalsMappingID;
    }

    public void setDailyGoalsMappingID(Integer dailyGoalsMappingID) {
        this.dailyGoalsMappingID = dailyGoalsMappingID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Object getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Object modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Object getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Object modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

}
