package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class AllChats extends SugarRecord implements Serializable {
    @SerializedName("MessageId")
    @Expose
    private Integer messageId;
    @SerializedName("SenderId")
    @Expose
    private Integer senderId;
    @SerializedName("SalesPerson")
    @Expose
    private Object salesPerson;
    @SerializedName("ReceiverId")
    @Expose
    private Integer receiverId;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Subject")
    @Expose
    private Object subject;
    @SerializedName("IsRead")
    @Expose
    private Boolean isRead;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("IsAttachment")
    @Expose
    private Boolean isAttachment;
    @SerializedName("MsgTime")
    @Expose
    private String msgTime;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Object getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(Object salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getSubject() {
        return subject;
    }

    public void setSubject(Object subject) {
        this.subject = subject;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getIsAttachment() {
        return isAttachment;
    }

    public void setIsAttachment(Boolean isAttachment) {
        this.isAttachment = isAttachment;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }


    public int getDateLayout() {
        return dateLayout;
    }

    public void setDateLayout(int dateLayout) {
        this.dateLayout = dateLayout;
    }

    int dateLayout=0; // 0 for chat row layout , 1 For Date Row



}
