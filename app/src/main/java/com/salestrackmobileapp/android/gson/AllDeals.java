
package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class AllDeals extends SugarRecord implements Serializable {

    public Integer getDealID() {
        return dealID;
    }

    public void setDealID(Integer dealID) {
        this.dealID = dealID;
    }

    @SerializedName("DealID")
    @Expose
    private Integer dealID;

    public String getDealTitle() {
        return dealTitle;
    }

    public void setDealTitle(String dealTitle) {
        this.dealTitle = dealTitle;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getDealApplicableAs() {
        return dealApplicableAs;
    }

    public void setDealApplicableAs(String dealApplicableAs) {
        this.dealApplicableAs = dealApplicableAs;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBussinessID() {
        return bussinessID;
    }

    public void setBussinessID(String bussinessID) {
        this.bussinessID = bussinessID;
    }

    public String getSalesPersonID() {
        return salesPersonID;
    }

    public void setSalesPersonID(String salesPersonID) {
        this.salesPersonID = salesPersonID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public String getAreaID() {
        return areaID;
    }

    public void setAreaID(String areaID) {
        this.areaID = areaID;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealProductID() {
        return dealProductID;
    }

    public void setDealProductID(String dealProductID) {
        this.dealProductID = dealProductID;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDiscountQuantity() {
        return discountQuantity;
    }

    public void setDiscountQuantity(String discountQuantity) {
        this.discountQuantity = discountQuantity;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getDealImage() {
        return dealImage;
    }

    public void setDealImage(String dealImage) {
        this.dealImage = dealImage;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        this.couponAmount = couponAmount;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    @SerializedName("DealTitle")
    @Expose
    private String dealTitle;

    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("DealType")
    @Expose
    private String dealType;
    @SerializedName("DealApplicableAs")
    @Expose
    private String dealApplicableAs;
    @SerializedName("DealDescription")
    @Expose
    private String dealDescription;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("BussinessID")
    @Expose
    private String bussinessID;

    @SerializedName("SalesPersonID")
    @Expose
    private String salesPersonID;

    @SerializedName("ProductID")
    @Expose
    private String productID;

    @SerializedName("CouponCode")
    @Expose
    private String couponCode;

    @SerializedName("BusinessName")
    @Expose
    private String businessName;

    @SerializedName("ProductName")
    @Expose
    private String productName;

    @SerializedName("SalesPersonName")
    @Expose
    private String salesPersonName;

    @SerializedName("AreaID")
    @Expose
    private String areaID;

    @SerializedName("AreaName")
    @Expose
    private String areaName;

    @SerializedName("DealProductID")
    @Expose
    private String dealProductID;

    @SerializedName("Quantity")
    @Expose
    private String quantity;

    @SerializedName("DiscountQuantity")
    @Expose
    private String discountQuantity;

    @SerializedName("GiftName")
    @Expose
    private String giftName;

    @SerializedName("GiftId")
    @Expose
    private String giftId;

    @SerializedName("DealImage")
    @Expose
    private String dealImage;

    @SerializedName("OrderAmount")
    @Expose
    private String orderAmount;

    @SerializedName("CouponAmount")
    @Expose
    private String couponAmount;

    @SerializedName("Percentage")
    @Expose
    private String percentage;

}
