package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class DailyGoals extends SugarRecord implements Serializable {

    @SerializedName("business")
    @Expose
    private Business_ business;
    @SerializedName("products")
    @Expose
    private Object products;
    @SerializedName("salesPersons")
    @Expose
    private SalesPersons salesPersons;
    @SerializedName("DailyGoalID")
    @Expose
    private Integer dailyGoalID;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("GoalTitle")
    @Expose
    private String goalTitle;
    @SerializedName("GoalDetails")
    @Expose
    private String goalDetails;
    @SerializedName("GoalNote")
    @Expose
    private String goalNote;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("CompanyCode")
    @Expose
    private String companyCode;
    @SerializedName("OrgId")
    @Expose
    private Integer orgId;
    @SerializedName("GoalStatus")
    @Expose
    private String goalStatus;
    @SerializedName("GoalAdvancedAmount")
    @Expose
    private Double goalAdvancedAmount;
    @SerializedName("GoalAdvancedQuantity")
    @Expose
    private Integer goalAdvancedQuantity;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("CreatedBy")
    @Expose
    private Integer createdBy;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("ModifiedBy")
    @Expose
    private Object modifiedBy;
    @SerializedName("ModifiedDate")
    @Expose
    private Object modifiedDate;

    public Business_ getBusiness() {
        return business;
    }

    public void setBusiness(Business_ business) {
        this.business = business;
    }

    public Object getProducts() {
        return products;
    }

    public void setProducts(Object products) {
        this.products = products;
    }

    public SalesPersons getSalesPersons() {
        return salesPersons;
    }

    public void setSalesPersons(SalesPersons salesPersons) {
        this.salesPersons = salesPersons;
    }

    public Integer getDailyGoalID() {
        return dailyGoalID;
    }

    public void setDailyGoalID(Integer dailyGoalID) {
        this.dailyGoalID = dailyGoalID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGoalTitle() {
        return goalTitle;
    }

    public void setGoalTitle(String goalTitle) {
        this.goalTitle = goalTitle;
    }

    public String getGoalDetails() {
        return goalDetails;
    }

    public void setGoalDetails(String goalDetails) {
        this.goalDetails = goalDetails;
    }

    public String getGoalNote() {
        return goalNote;
    }

    public void setGoalNote(String goalNote) {
        this.goalNote = goalNote;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getGoalStatus() {
        return goalStatus;
    }

    public void setGoalStatus(String goalStatus) {
        this.goalStatus = goalStatus;
    }

    public Double getGoalAdvancedAmount() {
        return goalAdvancedAmount;
    }

    public void setGoalAdvancedAmount(Double goalAdvancedAmount) {
        this.goalAdvancedAmount = goalAdvancedAmount;
    }

    public Integer getGoalAdvancedQuantity() {
        return goalAdvancedQuantity;
    }

    public void setGoalAdvancedQuantity(Integer goalAdvancedQuantity) {
        this.goalAdvancedQuantity = goalAdvancedQuantity;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Object getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Object modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Object getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Object modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

}
