package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class GoalBusinessName extends SugarRecord implements Serializable {
    String name;
    Integer businessId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    @Override
    public String toString() {
        return "GoalBusinessName{" +
                "name='" + name + '\'' +
                ", businessId='" + businessId + '\'' +
                '}';
    }
}
