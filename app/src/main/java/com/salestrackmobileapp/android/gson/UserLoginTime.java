package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class UserLoginTime extends SugarRecord {
    @SerializedName("UserEmail")
    @Expose
    private String userEmail;
    @SerializedName("Time")
    @Expose
    private long time;

    @SerializedName("Date")
    @Expose
    private String date;

    /*public String getLongTime() {
        return longTime;
    }

    public void setLongTime(String longTime) {
        this.longTime = longTime;
    }*/

    //@SerializedName("longTime")
    //@Expose
    //private String longTime;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "UserLoginTime{" +
                "userEmail='" + userEmail + '\'' +
                ", time=" + time +
                ", date='" + date + '\'' +
                '}';
    }
}
