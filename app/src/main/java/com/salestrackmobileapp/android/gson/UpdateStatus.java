package com.salestrackmobileapp.android.gson;

import java.io.Serializable;

public class UpdateStatus implements Serializable {

    String GoalStatus;

    @Override
    public String toString() {
        return "UpdateStatus{" +
                "GoalStatus='" + GoalStatus + '\'' +
                ", DailyGoalID=" + DailyGoalID +
                '}';
    }

    Integer DailyGoalID;
    public String getGoalStatus() {
        return GoalStatus;
    }

    public void setGoalStatus(String goalStatus) {
        GoalStatus = goalStatus;
    }

    public Integer getDailyGoalID() {
        return DailyGoalID;
    }

    public void setDailyGoalID(Integer dailyGoalID) {
        DailyGoalID = dailyGoalID;
    }

}
