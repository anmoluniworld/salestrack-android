
package com.salestrackmobileapp.android.gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

public class OrderHistory extends SugarRecord implements Serializable {

    @SerializedName("OrderItems")
    @Expose
    private List<OrderItem> pendingOrderItems = null;
    @SerializedName("OrderID")
    @Expose
    private Integer orderID;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("BusinessID")
    @Expose
    private Integer businessID;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;


    @SerializedName("SubTotal")
    @Expose
    private Double subTotal;

    public Double getSubTotal() {
        DecimalFormat df = new DecimalFormat("#.##");
        Double d = Double.parseDouble(df.format(subTotal));
        return d;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTaxAmount() {
        DecimalFormat df = new DecimalFormat("#.##");
        Double d = Double.parseDouble(df.format(taxAmount));
        return d;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Double getTotalOrderValue() {
        DecimalFormat df = new DecimalFormat("#.##");
        Double d = Double.parseDouble(df.format(totalOrderValue));
        return d;
    }

    public void setTotalOrderValue(Double totalOrderValue) {
        this.totalOrderValue = totalOrderValue;
    }

    @SerializedName("TaxPercentage")
    @Expose
    private Double taxPercentage;
    @SerializedName("TaxAmount")
    @Expose
    private Double taxAmount;

//    public Double getSubTotal() {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        this.subTotal= Double.valueOf(dtime.format(subTotal));
//        return subTotal;
//    }
//
//    public void setSubTotal(Double subTotal) {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        this.subTotal= Double.valueOf(dtime.format(subTotal));
//    }

    @SerializedName("DeliveryAmount")
    @Expose
    private Double deliveryAmount;
    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;

//    public Double getTotalOrderValue() {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        this.totalOrderValue= Double.valueOf(dtime.format(totalOrderValue));
//        return totalOrderValue;
//    }
//
//    public void setTotalOrderValue(Double totalOrderValue) {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        this.totalOrderValue= Double.valueOf(dtime.format(totalOrderValue));
////        this.totalOrderValue = totalOrderValue;
//    }

    @SerializedName("TotalOrderValue")
    @Expose
    private Double totalOrderValue;
    @SerializedName("dealID")
    @Expose
    private Integer dealID;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("BusinessName")
    @Expose
    private String businessName;
    @SerializedName("BusinessAddress1")
    @Expose
    private String businessAddress1;
    @SerializedName("BusinessAddress2")
    @Expose
    private String businessAddress2;
    @SerializedName("BusinessCity")
    @Expose
    private String businessCity;
    @SerializedName("BusinessState")
    @Expose
    private String businessState;


    public Double getDiscountAmount() {
        DecimalFormat dtime = new DecimalFormat("#.##");
        discountAmount= Double.valueOf(dtime.format(discountAmount));
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    @SerializedName("SalesPersonName")

    @Expose
    private String salesPersonName;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("SalesPersonID")
    @Expose
    private Integer salesPersonID;
    @SerializedName("Business")
    @Expose
    private Business business;
    @SerializedName("slProductCategory")
    @Expose
    private String slProductCategory;
    @SerializedName("slProduct")
    @Expose
    private String slProduct = null;
    @SerializedName("ProductList")
    @Expose
    private String productList = null;
    private Integer orderPositions = 0;

    public Integer getOrderPositions() {
        return orderPositions;
    }

    public void setOrderPositions(Integer orderPositions) {
        this.orderPositions = orderPositions;
    }

    public List<OrderItem> getPendingOrderItems() {
        return pendingOrderItems;
    }

    public void setPendingOrderItems(List<OrderItem> pendingOrderItems) {
        this.pendingOrderItems = pendingOrderItems;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getBusinessID() {
        return businessID;
    }

    public void setBusinessID(Integer businessID) {
        this.businessID = businessID;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }


    public Double getTaxPercentage() {
        DecimalFormat dtime = new DecimalFormat("#.##");
        taxPercentage= Double.valueOf(dtime.format(taxPercentage));
        return taxPercentage;
    }

    public void setTaxPercentage(Double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

//    public Double getTaxAmount() {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        taxAmount= Double.valueOf(dtime.format(taxAmount));
//        return taxAmount;
//    }
//
//    public void setTaxAmount(Double taxAmount) {
//        DecimalFormat dtime = new DecimalFormat("#.##");
//        this.taxAmount= Double.valueOf(dtime.format(taxAmount));
////        this.taxAmount = taxAmount;
//    }

    public Double getDeliveryAmount() {
        DecimalFormat dtime = new DecimalFormat("#.##");
        deliveryAmount= Double.valueOf(dtime.format(deliveryAmount));
        return deliveryAmount;
    }

    public void setDeliveryAmount(Double deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }


    public Integer getDealID() {
        return dealID;
    }

    public void setDealID(Integer dealID) {
        this.dealID = dealID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress1() {
        return businessAddress1;
    }

    public void setBusinessAddress1(String businessAddress1) {
        this.businessAddress1 = businessAddress1;
    }

    public String getBusinessAddress2() {
        return businessAddress2;
    }

    public void setBusinessAddress2(String businessAddress2) {
        this.businessAddress2 = businessAddress2;
    }

    public String getBusinessCity() {
        return businessCity;
    }

    public void setBusinessCity(String businessCity) {
        this.businessCity = businessCity;
    }

    public String getBusinessState() {
        return businessState;
    }

    public void setBusinessState(String businessState) {
        this.businessState = businessState;
    }

    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSalesPersonID() {
        return salesPersonID;
    }

    public void setSalesPersonID(Integer salesPersonID) {
        this.salesPersonID = salesPersonID;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getSlProductCategory() {
        return slProductCategory;
    }

    public void setSlProductCategory(String slProductCategory) {
        this.slProductCategory = slProductCategory;
    }

    public String getSlProduct() {
        return slProduct;
    }

    public void setSlProduct(String slProduct) {
        this.slProduct = slProduct;
    }

    public String getProductList() {
        return productList;
    }

    public void setProductList(String productList) {
        this.productList = productList;
    }

    @SerializedName("ExtraQuantity")
    @Expose
    private int extraQuantity;

    @SerializedName("CouponAmount")
    @Expose
    private int couponAmount;

    @SerializedName("GiftQuantity")
    @Expose
    private int giftQuantity;


    @SerializedName("DealName")
    @Expose
    private String dealName;

    public int getExtraQuantity() {
        return extraQuantity;
    }

    public void setExtraQuantity(int extraQuantity) {
        this.extraQuantity = extraQuantity;
    }

    public int getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(int couponAmount) {
        this.couponAmount = couponAmount;
    }

    public int getGiftQuantity() {
        return giftQuantity;
    }

    public void setGiftQuantity(int giftQuantity) {
        this.giftQuantity = giftQuantity;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }


    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    @SerializedName("GiftName")
    @Expose
    private String giftName;

}
