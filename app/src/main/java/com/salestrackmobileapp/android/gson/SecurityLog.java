
package com.salestrackmobileapp.android.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SecurityLog implements Serializable {

    @SerializedName("TokenID")
    @Expose
    private String tokenID;

    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;

    @SerializedName("ApiName")
    @Expose
    private String apiName;

    @SerializedName("JsonObject")
    @Expose
    private String jsonObject;

    @SerializedName("SessionUserid")
    @Expose
    private String sessionUserid;

    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(String jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getSessionUserid() {
        return sessionUserid;
    }

    public void setSessionUserid(String sessionUserid) {
        this.sessionUserid = sessionUserid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @SerializedName("IpAddress")
    @Expose
    private String ipAddress;


    @SerializedName("AppVersion")
    @Expose
    private String appVersion;


    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getoSversion() {
        return oSversion;
    }

    public void setoSversion(String oSversion) {
        this.oSversion = oSversion;
    }

    @SerializedName("OS")
    @Expose
    private String os;


    @SerializedName("OSversion")
    @Expose
    private String oSversion;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @SerializedName("DeviceName")
    @Expose
    private String deviceName;


}
