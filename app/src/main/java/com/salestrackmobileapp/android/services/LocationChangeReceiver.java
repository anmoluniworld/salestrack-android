package com.salestrackmobileapp.android.services;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.activities.Utils;
import com.salestrackmobileapp.android.utils.PrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class LocationChangeReceiver extends BroadcastReceiver {

    String TAG = "LocationChangeReceiver";
    public PrefsHelper sharedPreference;

    @Override
    public void onReceive(final Context context, final Intent intent) {

        Log.e(TAG, " getAction "+intent.getAction());

        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED"))
        {

            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            Log.e(TAG, "  isGpsEnabled "+isGpsEnabled);
            Log.e(TAG, "  isNetworkEnabled "+isNetworkEnabled);

            sharedPreference = new PrefsHelper(context);
            Log.e(TAG, "  IS_LOGIN "+sharedPreference.getBooleanValue(PrefsHelper.IS_LOGIN));

            if (sharedPreference.getBooleanValue(PrefsHelper.IS_LOGIN))
            {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

                if (isGpsEnabled || isNetworkEnabled) {
                    //location is enabled
                    Utils.SaveExceptionApi(context, "Enable Location service from Settings", date, sharedPreference);

                } else {
                    //location is disabled
                    Utils.SaveExceptionApi(context, "Disabled Location service from Settings", date, sharedPreference);
                }
            }




           /* if (isAppRunning(context, BuildConfig.APPLICATION_ID))
            {
                Log.e(TAG, "  App Running");

                final ConnectivityManager connMgr = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                sharedPreference = new PrefsHelper(context);
                NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
                //should check null because in airplane mode it will be null

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

                if (netInfo != null && netInfo.isConnected()) {
                    Log.e(TAG, " Internet Connected");
                    Utils.SaveExceptionApi(context, "Internet disconnected", sharedPreference.getStringValue("Disconnected"), sharedPreference);
                    Utils.SaveExceptionApi(context, "Internet Connected", date, sharedPreference);
                    sharedPreference.saveStringData("Disconnected", "");
                    Log.e(TAG, " Internet Connected "+sharedPreference.getStringValue("Disconnected"));


                } else {
                    Log.e(TAG, " Internet Not Connected Disconnected "+sharedPreference.getStringValue("Disconnected"));

                    if (sharedPreference.getStringValue("Disconnected").isEmpty())
                    {
                        Log.e(TAG, "  Disconnected Save date "+date);
                        sharedPreference.saveStringData("Disconnected", date);
                    }
                }
            } else
                Log.e(TAG, "  App not Running");*/
        }
    }
}
