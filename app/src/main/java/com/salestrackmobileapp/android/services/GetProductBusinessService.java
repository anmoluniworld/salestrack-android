package com.salestrackmobileapp.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.AllNotification;
import com.salestrackmobileapp.android.gson.AllProduct;
import com.salestrackmobileapp.android.gson.UOMArray;
import com.salestrackmobileapp.android.gson.UserInfoProfile;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.crashlytics.android.Crashlytics.TAG;
import static com.salestrackmobileapp.android.activities.ForgetPasswordActivity.mypreference;

public class GetProductBusinessService extends Service {

    private Context context;
    public static String ACCESS_TOKEN = "Access_Token";
    public PrefsHelper sharedPreference;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreference = new PrefsHelper(this);
        this.context = this;
        try {

            if (intent != null) {
                if (intent.hasExtra("ACCESS_TOKEN")) {
                    ACCESS_TOKEN = intent.getStringExtra("ACCESS_TOKEN");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getAllProduct();
        getAllBusiness();
        getAllOrders();
        getAllDeals();
        getAllChats();
        getProfile();
        getNotification();
        getDailyPlanner();
        return START_STICKY;
    }

    private void getDailyPlanner() {
         int year;
         int month;
         int day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        String dateString = new SimpleDateFormat("MM-dd-yyyy").format(cal.getTime());
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);
        serviceHandler.getGoalsAccDate(dateString, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("dailyPlannerList", arr);
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "failure: " + error);
            }
        });
    }

    public void getAllProduct() {

        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);
        serviceHandler.getAllProduct(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("productList", arr);
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, "failure: " + error);
            }
        });
    }

    private void getAllBusiness() {

        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllBusinessArray(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("businessList", arr);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void getAllOrders() {
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getOrderHistory("NEW", new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("orderList", arr);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void getAllDeals() {
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllDealsArray(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("dealList", arr);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void getAllChats() {
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(this, ServiceHandler.class, ACCESS_TOKEN, NetworkManager.BASE_URL);

        serviceHandler.getAllChats(new Callback<Response>() {

            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("chatList", arr);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void getProfile() {
        UserInfoProfile.deleteAll(UserInfoProfile.class);
        final Gson builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getUserInfo(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                try {

                    UserInfoProfile userInfoProfile = builder.fromJson(arr, UserInfoProfile.class);
                    userInfoProfile.save();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }

    private void getNotification() {
        ServiceHandler serviceHandler = NetworkManager.createRetrofitService(getApplicationContext(), ServiceHandler.class, sharedPreference.getStringValue(PrefsHelper.ACCESS_TOKEN), NetworkManager.BASE_URL);
        serviceHandler.getAllNotification(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String arr = CommonUtils.getServerResponse(response);
                sharedPreference.saveStringData("notificationList", arr);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
            }
        });
    }
}
