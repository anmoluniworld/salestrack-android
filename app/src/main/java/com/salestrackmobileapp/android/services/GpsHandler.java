package com.salestrackmobileapp.android.services;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.content.ContextCompat;

import java.util.Timer;
import java.util.TimerTask;

// This class is used to find Current Location is user

public class GpsHandler extends BroadcastReceiver {

    LocationManager lm;
    private Context con;
    String TAG="GpsHandler";

    public void onReceive(Context context, Intent intent) {

        this.con = context;

        LocationManager manager = (LocationManager)con.getSystemService(Context.LOCATION_SERVICE );
        boolean flag=manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Log.e(TAG," onReceive  flag "+flag);

        if(!flag)
        {

        }

        LocationResult locationResult = new LocationResult() {
            @Override
            public void gotLocation(Location location) {
                try {
                    // if Location is not found
                    Log.e(TAG," gotLocation  location "+location);

                    if (location == null)
                    {

                    }
                    else
                    {
                        Log.e(TAG," gotLocation  location getLatitude "+location.getLatitude());
                        Log.e(TAG," gotLocation  location getLongitude "+location.getLongitude());
                    }

                } catch (Exception e) {
                    // any exception comes
                }
            }
        };
        MyLocation myLocation = new MyLocation();
        myLocation.getLocation(context, locationResult);
    }


    public static abstract class LocationResult {
        public abstract void gotLocation(Location location);
    }

    public class MyLocation {
        Timer timer1;
        LocationManager lm;
        LocationResult locationResult;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        LocationListener locationListenerNetwork = new LocationListener() {
            public void onLocationChanged(Location location) {
                timer1.cancel();
                locationResult.gotLocation(location);
                lm.removeUpdates(this);
                lm.removeUpdates(locationListenerGps);
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        };
        LocationListener locationListenerGps = new LocationListener() {
            public void onLocationChanged(Location location) {
                timer1.cancel();
                locationResult.gotLocation(location);
                lm.removeUpdates(this);
                lm.removeUpdates(locationListenerNetwork);
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        };

        public boolean getLocation(Context context, LocationResult result) {
            //I use LocationResult callback class to pass location value from MyLocation to user code.
            locationResult = result;
            if (lm == null)
                lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


            //exceptions will be thrown if provider is not permitted.
            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch (Exception ex) {
            }
            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
            }

            //don't start listeners if no provider is enabled
            if (!gps_enabled && !network_enabled)
                return false;

            if (gps_enabled)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
                    }
                    else
                        return false;
                }
                else
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
            }
            if (network_enabled)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
                    }
                    else
                        return false;
                }
                else
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
            }
            timer1 = new Timer();
            timer1.schedule(new GetLastLocation(), 20000);
            return true;
        }

        class GetLastLocation extends TimerTask {
            @Override
            public void run() {
                lm.removeUpdates(locationListenerGps);
                lm.removeUpdates(locationListenerNetwork);

                Location net_loc = null, gps_loc = null;

                if (gps_enabled)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        if (ContextCompat.checkSelfPermission(con, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        {
                            gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                    }
                    else
                        gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }


                if (network_enabled)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        if (ContextCompat.checkSelfPermission(con, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        {
                            net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        }
                    }
                    else
                        net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }

                //if there are both values use the latest one
                if (gps_loc != null && net_loc != null) {
                    if (gps_loc.getTime() > net_loc.getTime())
                        locationResult.gotLocation(gps_loc);
                    else
                        locationResult.gotLocation(net_loc);
                    return;
                }

                if (gps_loc != null) {
                    locationResult.gotLocation(gps_loc);
                    return;
                }
                if (net_loc != null) {
                    locationResult.gotLocation(net_loc);
                    return;
                }
                locationResult.gotLocation(null);
            }
        }


    }
}