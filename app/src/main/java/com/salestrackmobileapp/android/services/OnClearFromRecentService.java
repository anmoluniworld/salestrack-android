package com.salestrackmobileapp.android.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.salestrackmobileapp.android.activities.Utils;
import com.salestrackmobileapp.android.utils.PrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OnClearFromRecentService extends Service {

    String TAG = "OnClearFromRecentService";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "END");
        //Code here
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z
        PrefsHelper sharedPreference = new PrefsHelper(this);
        Utils.SaveExceptionApi(this, "Application Supspend or inactive", date, sharedPreference);
        stopSelf();
    }
}
