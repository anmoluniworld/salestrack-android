package com.salestrackmobileapp.android.services;

import android.app.NotificationManager;
import android.app.NotificationChannel;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.DashboardActivity;
import com.salestrackmobileapp.android.activities.MainActivity;
import com.salestrackmobileapp.android.utils.NotificationUtils;
import com.salestrackmobileapp.android.utils.PrefsHelper;

import java.util.Map;

/**
 * Created by kanchan on 7/11/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "AllNotification Body: " + remoteMessage.getNotification().getBody());

            try {
                //JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleNotification(remoteMessage.getNotification().getBody(), remoteMessage, remoteMessage.getFrom());
                //handleDataMessage(remoteMessage.getData());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }

        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                //JSONObject json = new JSONObject(remoteMessage.getData().toString());

                handleDataMessage(remoteMessage.getData());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
        PrefsHelper sharedPreference = new PrefsHelper(this);


        String count=sharedPreference.getStringValue(PrefsHelper.NOTIFICATION_COUNT);
        if(count.isEmpty())
            count="0";

        Log.e(TAG, " count "+count);


        int temp=Integer.parseInt(count)+1;
        Log.e(TAG, " temp "+temp);


        sharedPreference.saveStringData(PrefsHelper.NOTIFICATION_COUNT,String.valueOf(temp));

    }

    private void handleNotification(String message, RemoteMessage remoteMessage, String from) {

        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
           /* // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
          //  Toast.makeText(getApplicationContext(),"You got a notification",Toast.LENGTH_SHORT).show();
            Log.d("You got a notification","aaaa");
            //DashboardActivity.notificationNumberTV.setText("1");
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();*/


            Intent intent = new Intent(this, DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            intent.putExtra("fcm_notification", "Y");
            intent.putExtra("entity", remoteMessage.getData().get("Entity"));
            intent.putExtra("entity_id", remoteMessage.getData().get("EntityID"));
            intent.putExtra("message", remoteMessage.getData().get("message"));


            int uniqueInt = (int) (System.currentTimeMillis() & 0xff);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), uniqueInt, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setSmallIcon(R.drawable.logo2)
                    .setContentTitle("SalesTrack")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setLargeIcon(getBitmap())
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel notificationChannel = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                notificationChannel = new NotificationChannel("1001" , getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setShowBadge(false);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(0, notificationBuilder.build());


        } else {
            // If the app is in background, firebase itself handles the notification
            Intent intent = new Intent(this, DashboardActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("message", remoteMessage.getData().get("message"));
            intent.putExtra("fcm_notification", "Y");

            int uniqueInt = (int) (System.currentTimeMillis() & 0xff);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), uniqueInt, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setSmallIcon(R.drawable.logo2)
                    .setContentTitle("SalesTrack")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setLargeIcon(getBitmap())
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
// Create notification channel.

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationChannel notificationChannel = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                notificationChannel = new NotificationChannel("1001" , getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setShowBadge(false);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(0, notificationBuilder.build());


        }
    }

    private void handleDataMessage(Map<String, String> messageBody) {
        //Log.e(TAG, "push json: " + json.toString());
        try {
            //JSONObject data = json.getJSONObject("data");

            String title = messageBody.get("title");
            String message = messageBody.get("message");
            //boolean isBackground = messageBody.get("is_background");
            String imageUrl = messageBody.get("image");
            String timestamp = messageBody.get("timestamp");
            //JSONObject payload = messageBody.get("data");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            //Log.e(TAG, "isBackground: " + isBackground);
            //Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);

            //Log.e(TAG, "data: " + payload.getString("EntityID"));


            //DashboardActivity.relative_notification.setVisibility(View.GONE);
            // app is in background, show the notification in notification tray
            Intent intent = new Intent(this, DashboardActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            intent.putExtra("fcm_notification", "Y");
            intent.putExtra("message", message);

            int uniqueInt = (int) (System.currentTimeMillis() & 0xff);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), uniqueInt, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("SalesTrack")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setLargeIcon(getBitmap())
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());


           /* if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                //DashboardActivity.relative_notification.setVisibility(View.VISIBLE);
                //DashboardActivity.notificationNumberTV.setText("1");
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("fcm_notification", "Y");

                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            } else {


            }*/
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent1) {
        /*notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);*/
        Intent intent = new Intent(this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra("fcm_notification", "Y");
      /*  intent.putExtra("user_id", user_id);
        intent.putExtra("date", date);
        intent.putExtra("hal_id", hal_id);
        intent.putExtra("M_view", M_view);*/
        int uniqueInt = (int) (System.currentTimeMillis() & 0xff);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), uniqueInt, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("hello")
                .setAutoCancel(true)
                .setLargeIcon(getBitmap())
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private Bitmap getBitmap(){
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.logo2);
        return icon;
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
