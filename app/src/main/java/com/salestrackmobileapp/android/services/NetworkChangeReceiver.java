package com.salestrackmobileapp.android.services;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.salestrackmobileapp.android.BuildConfig;
import com.salestrackmobileapp.android.activities.Utils;
import com.salestrackmobileapp.android.utils.PrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class NetworkChangeReceiver extends BroadcastReceiver {

    String TAG = "NetworkChangeReceiver";
    public PrefsHelper sharedPreference;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (isAppRunning(context, BuildConfig.APPLICATION_ID)) {
            Log.e(TAG, "  App Running");

            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            sharedPreference = new PrefsHelper(context);
            NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            String date = df.format(Calendar.getInstance().getTime());//2017-05-02T20:30:26.933Z

            if (netInfo != null && netInfo.isConnected()) {
                Log.e(TAG, " Internet Connected");
                Utils.SaveExceptionApi(context, "Internet disconnected", sharedPreference.getStringValue("Disconnected"), sharedPreference);
                Utils.SaveExceptionApi(context, "Internet Connected", date, sharedPreference);
                sharedPreference.saveStringData("Disconnected", "");
                Log.e(TAG, " Internet Connected "+sharedPreference.getStringValue("Disconnected"));


            } else {
                Log.e(TAG, " Internet Not Connected Disconnected "+sharedPreference.getStringValue("Disconnected"));

                if (sharedPreference.getStringValue("Disconnected").isEmpty())
                {
                    Log.e(TAG, "  Disconnected Save date "+date);
                    sharedPreference.saveStringData("Disconnected", date);
                }
            }
        } else
            Log.e(TAG, "  App not Running");


    }

    public boolean isAppRunning(final Context context, final String packageName) {
        Log.e(TAG, "  packageName " + packageName);

        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        //final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        List<ActivityManager.RunningTaskInfo> procInfos = activityManager.getRunningTasks(Integer.MAX_VALUE);

        if (procInfos != null) {
            for (final ActivityManager.RunningTaskInfo processInfo : procInfos) {
                Log.e(TAG, "  processInfo.processName " + processInfo.baseActivity.getPackageName());

                if (processInfo.baseActivity.getPackageName().equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
