package com.salestrackmobileapp.android.networkManager;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.UrlConnectionClient;

/**
 * Created by kanchan on 3/23/2017.
 */

public class NetworkManager {

    public static final String BASE_URL = "https://salestrackapi.azurewebsites.net/api";
    //public static final String BASE_URL = "https://salestrackapiqa.azurewebsites.net/api";
    //public static final String BASE_URL = "https://salestrackapidev.azurewebsites.net/api";
    public static final int DRAW = 10;
    Context activity;

    public static <T> T createRetrofitService(Context activity, final Class<T> clazz, final String accessToken, final String endPoint) {

        T service = null;
        Log.e("TOKEN", ":::::" + accessToken);
        Log.e("endPoint", ":::::" + endPoint);
        if (!accessToken.equals("")) {
            RequestInterceptor requestInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Authorization", "bearer " + accessToken);
                    request.addHeader("content", "application/json");
                }
            };

            final RestAdapter restAdapter = new RestAdapter.Builder()
                    .setRequestInterceptor(requestInterceptor)
                    .setEndpoint(endPoint)
                    .setLogLevel(RestAdapter.LogLevel.FULL) // this is the important line
                    .setClient(new MyUrlConnectionClient())
                    .build();
            service = restAdapter.create(clazz);
        } else {
            final RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(endPoint)
                    .build();
            service = restAdapter.create(clazz);
        }

        return service;
    }

}
final class MyUrlConnectionClient extends UrlConnectionClient {
    @Override
    protected HttpURLConnection openConnection(Request request) throws IOException {
        HttpURLConnection connection = super.openConnection(request);
        connection.setConnectTimeout(60 * 1000);
        connection.setReadTimeout(60 * 1000);
        return connection;
    }
}

