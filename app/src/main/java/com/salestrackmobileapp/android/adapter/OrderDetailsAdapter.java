package com.salestrackmobileapp.android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.OrderItem;

import java.util.List;

public class OrderDetailsAdapter  extends RecyclerView.Adapter<OrderDetailsAdapter.MyViewHolder> {
    List<OrderItem> orderItemList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView cell_sn;
        public TextView cell_name;
        public TextView cell_qty;
        public TextView cell_price;
        public MyViewHolder(View v) {
            super(v);
            cell_sn = v.findViewById(R.id.cell_sn);
            cell_name = v.findViewById(R.id.cell_name);
            cell_qty = v.findViewById(R.id.cell_qty);
            cell_price = v.findViewById(R.id.cell_price);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderDetailsAdapter(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_column, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        if(position == 0){
            holder.cell_sn.setText("Sr.No.");
            holder.cell_name.setText("Item Name");
            holder.cell_qty.setText("Qty");
            holder.cell_price.setText("Price");
        }else {
            OrderItem orderItem = orderItemList.get(position-1);
            holder.cell_sn.setText("" + position);
            holder.cell_name.setText(orderItem.getProductName());
            holder.cell_qty.setText("" + Math.round(orderItem.getQuantity()));
            if (orderItem.getDiscount() != null) {
                double temp=orderItem.getCost() - orderItem.getDiscount();
                holder.cell_price.setText("₹ " + String.format("%.2f", temp) + "");
            } else {
                holder.cell_price.setText("₹ " + String.format("%.2f", orderItem.getCost()) + "");
            }
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return orderItemList.size()+1;
    }
}
