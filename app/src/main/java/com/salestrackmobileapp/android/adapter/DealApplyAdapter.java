package com.salestrackmobileapp.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.gson.ProductList;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.utils.RecyclerClick;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanchan on 5/31/2017.
 */

//deal_apply_for_prodescription
public class DealApplyAdapter extends RecyclerView.Adapter<DealApplyAdapter.ViewHolder> {

    RecyclerClick rvClick;
    Context context;
    List<AllDeals> allDeals = new ArrayList<AllDeals>();
    int proId;
    static int i = 0;
    Custome_TextView mrpTextPagerAdapter, mrpAfterDiscount;
    String dealPosition = null;

    public DealApplyAdapter(Context context, Integer productID, Custome_TextView mrpText, Custome_TextView mrpAfterDiscount) {
        this.context = context;
        proId = productID;
        mrpTextPagerAdapter = mrpText;
        this.mrpAfterDiscount = mrpAfterDiscount;

        if (proId != 0) {
            if (allDeals.size() != 0) {
                allDeals.clear();
            }
            List<ProductList> dealProduct = Select.from(ProductList.class).where(Condition.prop("product_id").eq(proId), Condition.prop("deal_type").eq("Product")).list();
            for (ProductList productObj : dealProduct) {

                allDeals.add(Select.from(AllDeals.class).where(Condition.prop("deal_id").eq(productObj.getDealID()), Condition.prop("deal_type").eq("Product")).first());


            }
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.deal_apply_for_prodescription, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        if (proId != 0) {
//            if (allDeals.size() != 0) {
//                allDeals.clear();
//            }
//            List<ProductList> dealProduct = Select.from(ProductList.class).where(Condition.prop("product_id").eq(proId), Condition.prop("deal_type").eq("Product")).list();
//            for (ProductList productObj : dealProduct) {
//                allDeals.add(Select.from(AllDeals.class).where(Condition.prop("deal_id").eq(productObj.getDealID()), Condition.prop("deal_type").eq("Product")).first());
//            }
//        }
        final AllDeals allDealsItem = allDeals.get(position);

        if (allDealsItem.getDealApplicableAs().equals("Percentage") && !allDealsItem.getDealApplicableAs().equalsIgnoreCase("Quantity")) {
            if (allDealsItem.getAmount() != null) {
                holder.mainTitle.setText(allDealsItem.getAmount() + "% off ");
            } else {
                holder.mainTitle.setText("");
            }
        }
        if (allDealsItem.getDealApplicableAs().equals("Amount") && !allDealsItem.getDealApplicableAs().equalsIgnoreCase("Quantity")) {
            if (allDealsItem.getAmount() != null) {
                holder.mainTitle.setText("Rs/-" + allDealsItem.getAmount() + " OFF");
            } else {
                holder.mainTitle.setText("");
            }
        }
        if (allDealsItem.getDealApplicableAs().equalsIgnoreCase("Quantity")) {
            holder.itemView.setVisibility(View.GONE);
        }

        String dateSt = allDealsItem.getStartDate();
        String dateArray[] = dateSt.split("T");

        String dateEndSt = allDealsItem.getStartDate();
        String endDateArray[] = dateEndSt.split("T");

        ProductInCart productInCart = Select.from(ProductInCart.class).where(Condition.prop("product_id").eq(proId)).first();
        if (productInCart != null) {
            if (productInCart.dealId.equals("0")) {
                mrpAfterDiscount.setVisibility(View.GONE);
            } else {
                if (dealPosition != null && !dealPosition.equalsIgnoreCase("null")) {
                    if (Integer.valueOf(dealPosition) == position) {
                        Double amount = 0.0;
                        if (productInCart.dealType.equals("Amount")) {
                            amount = Double.parseDouble(productInCart.dealAmount);
                        } else {
                            amount = (Integer.parseInt(productInCart.dealAmount) * productInCart.price) / 100;
                        }
                        Double mrp = Double.parseDouble(mrpTextPagerAdapter.getText().toString());
                        Double amountafterdicount = mrp - amount;
                        mrpAfterDiscount.setVisibility(View.VISIBLE);
                        mrpAfterDiscount.setText(amountafterdicount + "");
                        mrpTextPagerAdapter.setPaintFlags(mrpTextPagerAdapter.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.applyDealOnProduct.setBackgroundColor(context.getResources().getColor(R.color.fade_color));
                        holder.applyDealOnProduct.setText("Applied");
                    } else {
                        holder.applyDealOnProduct.setBackgroundResource(R.drawable.rounder_corner_shape);
                        holder.applyDealOnProduct.setText("APPLY");
                    }
                }
            }
        }

        if (dateSt.equals(dateEndSt)) {
            holder.startDateTv.setText("valid for today only ");
            holder.endDateTv.setVisibility(View.GONE);
        } else {
            holder.startDateTv.setText("start from" + dateArray[0] + "");
            holder.endDateTv.setText("till " + endDateArray[0] + "");
        }
        //  if (productInCart1.dealId.equals("") | productInCart1.dealId.equals(null)) {


        holder.applyDealOnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Toast.makeText(context, "apply", Toast.LENGTH_SHORT).show();
                final ProductInCart productInCart = Select.from(ProductInCart.class).where(Condition.prop("product_id").eq(proId)).first();
                if (productInCart != null) {
                    if (productInCart.dealId.equals(null) || productInCart.dealId.equals("0")) {
                        if (productInCart != null) {
                            dealPosition = String.valueOf(position);
                            holder.applyDealOnProduct.setBackgroundColor(context.getResources().getColor(R.color.fade_color));
                            holder.applyDealOnProduct.setText("Applied");
                            productInCart.dealId = String.valueOf(allDealsItem.getDealID() + "");
                            productInCart.dealCategory = "product";
                            productInCart.dealType = String.valueOf(allDealsItem.getDealApplicableAs() + "");
                            productInCart.dealAmount = String.valueOf(allDealsItem.getAmount() + "");
                            productInCart.save();
                            Double amount = 0.0;
                            if (productInCart.dealType.equals("Amount")) {
                                amount = Double.parseDouble(productInCart.dealAmount);
                            } else {
                                amount = (Integer.parseInt(productInCart.dealAmount) * productInCart.price) / 100;
                            }

                            Double mrp = Double.parseDouble(mrpTextPagerAdapter.getText().toString());
                            Double amountafterdicount = mrp - amount;
                            mrpAfterDiscount.setVisibility(View.VISIBLE);
                            mrpAfterDiscount.setText(amountafterdicount + "");
                            mrpTextPagerAdapter.setPaintFlags(mrpTextPagerAdapter.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                            notifyDataSetChanged();

                        } else {
                            Toast.makeText(context, "Please add item into cart..", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (productInCart.dealId.equals(String.valueOf(allDealsItem.getDealID()))) {
                            Toast.makeText(context, "you have already this deal on product", Toast.LENGTH_SHORT).show();
                        } else {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                            builder1.setTitle("You have already applied another deal.");
                            builder1.setMessage("Do you want to change deal value?");
                            builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override

                                public void onClick(DialogInterface dialog, int which) {
                                    dealPosition = String.valueOf(position);
                                    productInCart.dealId = String.valueOf(allDealsItem.getDealID() + "");
                                    productInCart.dealType = String.valueOf(allDealsItem.getDealApplicableAs() + "");
                                    productInCart.dealAmount = String.valueOf(allDealsItem.getAmount() + "");
                                    productInCart.dealCategory = "product";
                                    productInCart.save();
                                    notifyDataSetChanged();

                                    mrpTextPagerAdapter.setPaintFlags(mrpTextPagerAdapter.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                                    Toast.makeText(context, "you have successfully applied change deals", Toast.LENGTH_SHORT).show();

                                }
                            });
                            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            builder1.show();
                        }
                    }
                } else {
                    Toast.makeText(context, "Please add product in cart", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allDeals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Custome_BoldTextView mainTitle;
        public Custome_TextView startDateTv, endDateTv;
        Button applyDealOnProduct;

        public ViewHolder(View v) {
            super(v);

            mainTitle = (Custome_BoldTextView) v.findViewById(R.id.main_title);
            applyDealOnProduct = (Button) v.findViewById(R.id.apply_deal_btn);
            startDateTv = (Custome_TextView) v.findViewById(R.id.start_date);
            endDateTv = (Custome_TextView) v.findViewById(R.id.end_date);

        }

    }
}



