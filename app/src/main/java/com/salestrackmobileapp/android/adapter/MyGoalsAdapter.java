package com.salestrackmobileapp.android.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.ApplicationClass;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.fragments.DailyPlannerFragment;
import com.salestrackmobileapp.android.gson.GoalBusiness;
import com.salestrackmobileapp.android.gson.GoalsAccDate;
import com.salestrackmobileapp.android.gson.UpdateStatus;
import com.salestrackmobileapp.android.networkManager.NetworkManager;
import com.salestrackmobileapp.android.networkManager.ServiceHandler;
import com.salestrackmobileapp.android.utils.CommonUtils;
import com.salestrackmobileapp.android.utils.Connectivity;
import com.salestrackmobileapp.android.utils.RecyclerClick;
import com.salestrackmobileapp.android.utils.SuperclassExclusionStrategy;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by kanchan on 3/12/2017.
 */

public class MyGoalsAdapter extends RecyclerView.Adapter<MyGoalsAdapter.ViewHolder> {

    //RecyclerClick rvClick;
    Context context;
    List<GoalBusiness> allGoals, goalBusiness;
    List<GoalsAccDate> allGoalsDates, goalsDate;
    String type;
    String businessName, token;
    Gson builder;
    ServiceHandler serviceHandler;
    UpdateStatus updateStatus;
    private ProgressDialog mProgressDialog;

    Fragment fragment;


    public MyGoalsAdapter(Context context, Fragment fragment1, String businessName, String type, String token) {
        this.context = context;
        fragment = fragment1;
        this.businessName = businessName;
        this.type = type;
        this.allGoals = new ArrayList<>();
        this.allGoalsDates = new ArrayList<>();
        this.token = token;
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(true);
    }

    public void UpdateType(String t) {
        type = t;
    }

    public void setListGoals() {
        this.goalBusiness = GoalBusiness.listAll(GoalBusiness.class);
        this.goalsDate = GoalsAccDate.listAll(GoalsAccDate.class);
        if (type.equalsIgnoreCase("goal")) {
            for (int i = 0; i < goalsDate.size(); i++) {
                if (goalsDate.get(i).getGoalTitle().equalsIgnoreCase(businessName)) {
                    allGoals.add(goalBusiness.get(i));
                    allGoalsDates.add(goalsDate.get(i));
                }
            }
        } else {
            for (int i = 0; i < goalBusiness.size(); i++) {
                if (goalBusiness.get(i).getBusnessName().equalsIgnoreCase(businessName)) {
                    allGoals.add(goalBusiness.get(i));
                    allGoalsDates.add(goalsDate.get(i));
                }
            }
        }
    }


    @Override
    public MyGoalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_goals_item_layout, parent, false);
        MyGoalsAdapter.ViewHolder vh = new MyGoalsAdapter.ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        GoalBusiness goalsAccDate = allGoals.get(position);
        GoalsAccDate goalsAccDate1 = allGoalsDates.get(position);
        //if (type.equalsIgnoreCase("business")) {

        //holder.subGoalTitle.setText(goalsAccDate.getContactPersonName() + "");
        if (!(goalsAccDate.getAddress2().equals("") || goalsAccDate.getAddress2().equals(null))) {
            holder.addressTv.setText(goalsAccDate.getAddress1());
        } else {
            holder.addressTv.setText(goalsAccDate.getAddress1());
        }

        if (goalsAccDate.getCheckedIN()) {
            //  holder.visitedCheck.setVisibility(View.VISIBLE);
            holder.visitedCheck.setChecked(true);
        } else {
            // holder.visitedCheck.setVisibility(View.GONE);
            holder.visitedCheck.setChecked(false);
        }
        //}


        if (type.equalsIgnoreCase("business")) {
            holder.mainGoalTitle.setText(goalsAccDate.getBusnessName() + "");
        } else {
            holder.mainGoalTitle.setText(goalsAccDate1.getGoalTitle() + "");
        }

        holder.tvGoalEmail.setText(goalsAccDate.getContactPersonEmail());
        holder.tvGoalAmount.setText("₹ " + "" + goalsAccDate1.getAmountID());
        //holder.tvGoalQuantity.setText("Quantity " + goalsAccDate1.getQuantityID());

        if (goalsAccDate1.getGoalStatus().equalsIgnoreCase("Completed")) {
            holder.completed.setBackgroundResource(R.drawable.orange_background_square);
        } else {
            holder.completed.setBackgroundResource(R.drawable.grey_background_square);
        }
        if (goalsAccDate1.getGoalStatus().equalsIgnoreCase("PartiallyCompleted")) {
            holder.partialy_completed.setBackgroundResource(R.drawable.orange_background_square);
        } else {
            holder.partialy_completed.setBackgroundResource(R.drawable.grey_background_square);
        }
        if (goalsAccDate1.getGoalStatus().equalsIgnoreCase("Rejected")) {
            holder.rejected.setBackgroundResource(R.drawable.orange_background_square);
        } else {
            holder.rejected.setBackgroundResource(R.drawable.grey_background_square);
        }

        if (goalsAccDate1.getPriority() == 1) {
            holder.priority_color.setBackgroundResource(R.drawable.yellow_circle_background);
            holder.priority.setText("LOW");
        } else if (goalsAccDate1.getPriority() == 2) {
            holder.priority_color.setBackgroundResource(R.drawable.orange_circle_background);
            holder.priority.setText("MEDIUM");
        } else if (goalsAccDate1.getPriority() == 3) {
            holder.priority_color.setBackgroundResource(R.drawable.red_circle_background);
            holder.priority.setText("IMMEDIATE");
        }

        holder.completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isNetworkAvailable(ApplicationClass.getAppContext())) {
                    showDialog();
                    builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                            .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
                    serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, token, NetworkManager.BASE_URL);
                    try {
                        JSONObject object = new JSONObject();
                        object.put("GoalStatus", "Completed");
                        object.put("DailyGoalID", goalsAccDate1.getDailyGoalID());
                        updateStatus = builder.fromJson(object.toString(), UpdateStatus.class);
                        serviceHandler.updateStatus("Completed", goalsAccDate1.getDailyGoalID(), new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                hideDialog();
                                final String serverResponse = CommonUtils.getServerResponse(response);
                                try {
                                    final JSONObject jsonObject = new JSONObject(serverResponse);
                                    Log.e("REsponse::::", "::" + jsonObject.toString());
                                    holder.completed.setBackgroundResource(R.drawable.orange_background_square);
                                    holder.partialy_completed.setBackgroundResource(R.drawable.grey_background_square);
                                    holder.rejected.setBackgroundResource(R.drawable.grey_background_square);
                                    goalsAccDate1.setGoalStatus("Completed");
                                    notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                hideDialog();
                                try {
                                    if (error.getResponse() != null) {
                                        String responseError = CommonUtils.getServerResponse(error.getResponse());
                                        JSONObject jsonErrorObj = new JSONObject(responseError);
                                        Toast.makeText(context, "" + jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();

                                    } else {
                                        Toast.makeText(context, "Please check your Internet", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    } catch (Exception e) {
                        hideDialog();
                        e.printStackTrace();
                    }

                }
            }
        });

        holder.partialy_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isNetworkAvailable(ApplicationClass.getAppContext())) {
                    showDialog();
                    builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                            .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
                    serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, token, NetworkManager.BASE_URL);
                    try {
                        JSONObject object = new JSONObject();
                        object.put("GoalStatus", "PartiallyCompleted");
                        object.put("DailyGoalID", goalsAccDate1.getDailyGoalID());
                        updateStatus = builder.fromJson(object.toString(), UpdateStatus.class);
                        serviceHandler.updateStatus("PartiallyCompleted", goalsAccDate1.getDailyGoalID(), new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                hideDialog();
                                final String serverResponse = CommonUtils.getServerResponse(response);
                                try {
                                    final JSONObject jsonObject = new JSONObject(serverResponse);
                                    Log.e("REsponse::::", "::" + jsonObject.toString());
                                    holder.completed.setBackgroundResource(R.drawable.grey_background_square);
                                    holder.partialy_completed.setBackgroundResource(R.drawable.orange_background_square);
                                    holder.rejected.setBackgroundResource(R.drawable.grey_background_square);
                                    goalsAccDate1.setGoalStatus("PartiallyCompleted");
                                    notifyDataSetChanged();
                                    notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                hideDialog();
                                try {
                                    if (error.getResponse() != null) {
                                        String responseError = CommonUtils.getServerResponse(error.getResponse());
                                        JSONObject jsonErrorObj = new JSONObject(responseError);
                                        Toast.makeText(context, "" + jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();

                                    } else {
                                        Toast.makeText(context, "Please check your Internet", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


                    } catch (Exception e) {
                        hideDialog();
                        e.printStackTrace();
                    }

                }
            }
        });

        holder.rejected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Connectivity.isNetworkAvailable(ApplicationClass.getAppContext())) {
                    showDialog();
                    builder = new GsonBuilder().addDeserializationExclusionStrategy(new SuperclassExclusionStrategy())
                            .addSerializationExclusionStrategy(new SuperclassExclusionStrategy()).create();
                    serviceHandler = NetworkManager.createRetrofitService(context, ServiceHandler.class, token, NetworkManager.BASE_URL);
                    try {
                        JSONObject object = new JSONObject();
                        object.put("GoalStatus", "Rejected");
                        object.put("DailyGoalID", goalsAccDate1.getDailyGoalID());
                        updateStatus = builder.fromJson(object.toString(), UpdateStatus.class);
                        serviceHandler.updateStatus("Rejected", goalsAccDate1.getDailyGoalID(), new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                hideDialog();
                                final String serverResponse = CommonUtils.getServerResponse(response);
                                try {
                                    final JSONObject jsonObject = new JSONObject(serverResponse);
                                    Log.e("REsponse::::", "::" + jsonObject.toString());
                                    holder.completed.setBackgroundResource(R.drawable.grey_background_square);
                                    holder.partialy_completed.setBackgroundResource(R.drawable.grey_background_square);
                                    holder.rejected.setBackgroundResource(R.drawable.orange_background_square);
                                    goalsAccDate1.setGoalStatus("Rejected");
                                    notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                hideDialog();
                                try {
                                    if (error.getResponse() != null) {
                                        String responseError = CommonUtils.getServerResponse(error.getResponse());
                                        JSONObject jsonErrorObj = new JSONObject(responseError);
                                        Toast.makeText(context, "" + jsonErrorObj.getString("Message"), Toast.LENGTH_SHORT).show();

                                    } else {
                                        Toast.makeText(context, "Please check your Internet", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


                    } catch (Exception e) {
                        hideDialog();
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return allGoals.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Custome_TextView subGoalTitle, addressTv, tvGoalEmail, tvGoalAmount, tvGoalQuantity, priority, completed, partialy_completed, rejected;
        TextView mainGoalTitle;
        public ImageView proceedImg;
        //RecyclerClick recyclerClick;
        CheckBox visitedCheck;
        RelativeLayout priority_color;

        public ViewHolder(View v) {
            super(v);
            mainGoalTitle = (TextView) v.findViewById(R.id.main_goal_title);
            subGoalTitle = (Custome_TextView) v.findViewById(R.id.sub_goal_title);
            addressTv = (Custome_TextView) v.findViewById(R.id.start_date);
            visitedCheck = (CheckBox) v.findViewById(R.id.visited_ck);
            tvGoalEmail = (Custome_TextView) v.findViewById(R.id.tvEmail);
            tvGoalAmount = (Custome_TextView) v.findViewById(R.id.tvGoalAmount);
            tvGoalQuantity = (Custome_TextView) v.findViewById(R.id.tvGoalQuantity);

            priority = (Custome_TextView) v.findViewById(R.id.priority);
            completed = (Custome_TextView) v.findViewById(R.id.completed);
            partialy_completed = (Custome_TextView) v.findViewById(R.id.partialy_completed);
            rejected = (Custome_TextView) v.findViewById(R.id.rejected);
            priority_color = (RelativeLayout) v.findViewById(R.id.priority_color);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1) {
                        ((DailyPlannerFragment)fragment).productClick(allGoals.get(getAdapterPosition()),getAdapterPosition());
                    }
                }
            });

            //this.recyclerClick = recyclerClick;
        }
    }


    public void showDialog() {

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
//            Log.e("showDialog",":true");


        }
    }

    public void hideDialog() {


        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
//            Log.e("hideDialog",":true");


        }
    }
}
