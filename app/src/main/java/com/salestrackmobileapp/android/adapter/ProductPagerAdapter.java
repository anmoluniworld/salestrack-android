package com.salestrackmobileapp.android.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.gson.AllProduct;
import com.salestrackmobileapp.android.gson.AllVariants;
import com.salestrackmobileapp.android.gson.ProductList;
import com.salestrackmobileapp.android.gson.UOMArray;
import com.salestrackmobileapp.android.interfaces.NotAvailableStock;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanchan on 3/16/2017.
 */

public class ProductPagerAdapter extends PagerAdapter {

    Context context;
    List<AllProduct> allProductList;
    ProductInCart productInCart;
    Button addCartBtn;
    String businessId;
    DealApplyAdapter mAdapter;
    List<AllDeals> listOfDeals = new ArrayList<AllDeals>();
    List<String> listUomArray = new ArrayList<String>();
    List<Integer> listUomIdArray = new ArrayList<Integer>();
    RecyclerView listDealOnProductRv;
    private static Dialog mProgressDialog;
    static int locationUMO = 0;
    int location = 0;
    boolean check = false;
    String uomvalue;

    ImageView addToCartImg;
    int i = 0;
    Double sum = 1.0;
    Button btn_no;
    SwitchCompat switch_instock;
    AppCompatTextView instock;
    RecyclerView rv_variant;
    ProductVariantAdapter productVariantAdapter;
    List<AllVariants> allVariants = new ArrayList<>();
//    private Custome_TextView categoryTv,descProductTv,mrpAfterDiscount;
//    private Custome_BoldTextView nameProductTv;

    TextView alreadyInCart;
    ImageView productImg;
    AllProduct product;

    LinearLayout linearLayout;
    NotAvailableStock itemClicked;

    private List<UOMArray> uomArrays;


    public ProductPagerAdapter(Context context, String businessId, NotAvailableStock itemClicked) {
        this.context = context;
        this.businessId = businessId;
        this.itemClicked = itemClicked;
    }

    public void setListOfProduct(List<AllProduct> allProductList, List<UOMArray> uomArrays, int location) {
        i = 0;
        this.allProductList = allProductList;
        this.uomArrays = uomArrays;
        this.location = location;
    }


    @Override
    public Object instantiateItem(final ViewGroup container,
                                  int position) {

        final Custome_TextView mrpProductTv;
        final EditText qtyTv;
        Custome_TextView descProductTv;
        Custome_TextView mrpAfterDiscount;
        Custome_TextView mrp_product_tv_original;
        Custome_BoldTextView titleExcitingOffer;
        ImageView plusImg;
        ImageView minusImg;

        List<Integer> listUomId = new ArrayList<Integer>();
        List<String> listUOM = new ArrayList<String>();

        product = allProductList.get(position);
        productInCart = Select.from(ProductInCart.class).where(Condition.prop("product_id").eq(product.getProductID())).first();

        LayoutInflater inflater = LayoutInflater.from(context);

        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.product_detail_item, container, false);

        listDealOnProductRv = (RecyclerView) layout.findViewById(R.id.list_deal_rv);
        listDealOnProductRv.setNestedScrollingEnabled(true);

        //UMO spinner
        final Spinner spinner = (Spinner) layout.findViewById(R.id.measurmentSpn);

        productImg = (ImageView) layout.findViewById(R.id.product_img);
        productImg.setScaleType(ImageView.ScaleType.FIT_CENTER);

        Custome_BoldTextView nameProductTv = (Custome_BoldTextView) layout.findViewById(R.id.name_product_tv);
        mrpProductTv = (Custome_TextView) layout.findViewById(R.id.mrp_product_tv);
        Custome_TextView categoryTv = (Custome_TextView) layout.findViewById(R.id.category_tv);
        mrpProductTv.setTag(product);
        descProductTv = (Custome_TextView) layout.findViewById(R.id.desc_product_tv);
        mrpAfterDiscount = (Custome_TextView) layout.findViewById(R.id.mrp_after_dicount);
        mrp_product_tv_original = (Custome_TextView) layout.findViewById(R.id.mrp_product_tv_original);
        alreadyInCart = (TextView) layout.findViewById(R.id.already_in_cart);

        rv_variant = layout.findViewById(R.id.rv_variant);
        rv_variant.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        //addAllVeriants(position);

        plusImg = (ImageView) layout.findViewById(R.id.plus_img);
        minusImg = (ImageView) layout.findViewById(R.id.minus_img);
        addToCartImg = (ImageView) layout.findViewById(R.id.addtocart_img);

        //stock
        switch_instock = layout.findViewById(R.id.switch_instock);
        instock = layout.findViewById(R.id.instock);
        addCartBtn = layout.findViewById(R.id.add_cart_btn);

//        setStock();
        switch_instock.setChecked(true);
        switch_instock.setText("In stock");
        if (allProductList.get(position).getInStock()) {
            instock.setText("In stock");
            instock.setTextColor(context.getResources().getColor(R.color.green));
            addCartBtn.setText("In Stock");
        } else {
            instock.setText("Not Available");
            instock.setTextColor(context.getResources().getColor(R.color.red));
            addCartBtn.setText("Not Available");
        }
        switch_instock.setTextColor(context.getResources().getColor(R.color.green));

//        if (product.getInStock().equals(true)) {
//            switch_instock.setChecked(true);
//            switch_instock.setText("In stock");
//            switch_instock.setTextColor(context.getResources().getColor(R.color.green));
//        } else {
//            switch_instock.setChecked(false);
//            switch_instock.setText("Not Available");
//            switch_instock.setTextColor(context.getResources().getColor(R.color.red));
//        }
        switch_instock.setClickable(false);


        Custome_BoldTextView isInCart = (Custome_BoldTextView) layout.findViewById(R.id.is_add_tv);

        qtyTv = (EditText) layout.findViewById(R.id.qty_tv);
        qtyTv.setTag(product);

        //   mrpProductTv.setText(product.getDefaultPrice() + "");
        mrpProductTv.setText(((AllProduct) mrpProductTv.getTag()).getSalePrice() + "");
        mrp_product_tv_original.setText("₹" + product.getDefaultPrice() + "");
        mrp_product_tv_original.setPaintFlags(mrp_product_tv_original.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        titleExcitingOffer = (Custome_BoldTextView) layout.findViewById(R.id.title_exciting_offer);
        linearLayout = (LinearLayout) layout.findViewById(R.id.plus_minus_layout);
        final Button addCartBtn = (Button) layout.findViewById(R.id.add_cart_btn);
        addCartBtn.setTag(product);

        if (productInCart != null) {
            if ((productInCart.variantID).equals(product.getVariantID())) {
                //   alreadyInCart.setVisibility(View.VISIBLE);
                addCartBtn.setText("Update Order");
                addCartBtn.setAlpha(.5f);
                qtyTv.setText(productInCart.qty + "");
                linearLayout.setVisibility(View.VISIBLE);
                isInCart.setVisibility(View.VISIBLE);
                linearLayout.setEnabled(true);
                qtyTv.setText(productInCart.qty + "");
            } else {
                addCartBtn.setText("Add to Cart");
                qtyTv.setText("1");

            }
        } else {
            addCartBtn.setText("Add to Cart");
            alreadyInCart.setVisibility(View.GONE);
            qtyTv.setText("1");
        }
        addCartBtn.setVisibility(View.VISIBLE);


        if (!allProductList.get(position).getInStock())
            addCartBtn.setText("Not Available");

        String sourceString = "<b>" + "Description:" + "</b> " + product.getDescription();
//        descProductTv.setText(Html.fromHtml(sourceString));
        descProductTv.setText(product.getDescription() + "");

        nameProductTv.setText(product.getProductName() + "");
        categoryTv.setText(product.getProductCategoryName() + "");

        Picasso.with(context).load(product.getImageName()).placeholder(context.getResources().getDrawable(R.drawable.calendar_icon)).error(R.drawable.calendar_icon).into(productImg);

        linearLayout.setVisibility(View.VISIBLE);

//        setUmoSpinner(position);
        listUOM.clear();
        listUomId.clear();
        listUOM.add("Select");
        listUomId.add(0);

        if (!product.getUOMMapping().equals("")) {
            String[] separated = allProductList.get(position).getUOMMapping().split(",");
            if (separated.length > 0) {
                for (int i = 0; i < uomArrays.size(); i++) {
                    UOMArray uom = uomArrays.get(i);
                    for (int j = 0; j < separated.length; j++) {
                        try {
                            if (uom.getUOMID() == Integer.parseInt(separated[j].trim())) {
                                if (Integer.parseInt(separated[j].trim()) == product.getDefaultUOMID()) {
                                    listUOM.remove(0);
                                    listUomId.remove(0);
                                    listUOM.add(0, uom.getUOM());
                                    listUomId.add(0, uom.getUOMID());
                                } else {
                                    listUOM.add(uom.getUOM());
                                    listUomId.add(uom.getUOMID());
                                }

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            for (int i = 0; i < uomArrays.size(); i++) {
                try {
                    UOMArray uom = uomArrays.get(i);
                    int uomid = uom.getUOMID();
                    int def_uomid = product.getDefaultUOMID();
                    if (uomid == def_uomid) {
                        listUOM.remove(0);
                        listUomId.remove(0);
                        listUOM.add(0, uom.getUOM());
                        listUomId.add(0, uom.getUOMID());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.single_text_item, listUOM);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();

        for (int i = 0; i < listUOM.size(); i++) {
            if (productInCart != null) {
                if ((productInCart.variantID).equals(product.getVariantID())) {
                    if (listUOM.get(i).equals(productInCart.uomST)) {
                        spinner.setSelection(i);
                        break;
                    }
                }
            }
        }

        //spinner.setSelection(4);
//        spinner.setEnabled(true);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                type = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        addAllVeriants(position, listUOM, listUomId);

        Long longId = 0L;
        int seletedVariantID = 0;
        if (product.getProductCategoryID() != null) {
            try {
                longId = new Long("" + product.getProductID());
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (product.getVariantID() != null)
            try {
                seletedVariantID = new Integer("" + product.getVariantID());

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        allVariants = new ArrayList<>();
        allVariants = Select.from(AllVariants.class).where(Condition.prop("product_id").eq(longId)).list();

        if (allVariants.size() > 0 && allVariants != null) {

            productVariantAdapter = new ProductVariantAdapter(context, rv_variant, ProductPagerAdapter.this, allVariants, position, allProductList, uomArrays, location, product.getVariantID());
            rv_variant.setAdapter(productVariantAdapter);

//            for (int i = 0; i < allVariants.size(); i++) {
//                if (allVariants.get(i).getVariantID() == CommonUtils.variantID) {
//                    if (i > 2)
//                        rv_variant.scrollToPosition(i);
//                }
//            }
            productVariantAdapter.notifyDataSetChanged();
        }

//        setDeal();
        //Deal
        if (product.getProductID() != 0) {
            List<ProductList> dealProduct = Select.from(ProductList.class).where(Condition.prop("product_id").eq(product.getProductID()), Condition.prop("deal_type").eq("Product")).list();
            for (ProductList productObj : dealProduct) {
                listOfDeals.add(Select.from(AllDeals.class).where(Condition.prop("deal_id").eq(productObj.getDealID()), Condition.prop("deal_type").eq("Product")).first());
            }
            if (listOfDeals.size() != 0) {
                listOfDeals.clear();
                mAdapter = new DealApplyAdapter(context, product.getProductID(), mrpProductTv, mrpAfterDiscount);
                listDealOnProductRv.setAdapter(mAdapter);
                listDealOnProductRv.setLayoutManager(new LinearLayoutManager(context));
                mAdapter.notifyDataSetChanged();
                titleExcitingOffer.setVisibility(View.VISIBLE);
                /*make it visible*/
                listDealOnProductRv.setVisibility(View.GONE);

            } else {
                titleExcitingOffer.setVisibility(View.GONE);
                listDealOnProductRv.setVisibility(View.GONE);
            }
        }
//        titleExcitingOffer.setVisibility(View.GONE);
//        listDealOnProductRv.setVisibility(View.GONE);


//        setEvent();
        plusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("***", "plus click");
                product = ((AllProduct) mrpProductTv.getTag());
                addCartBtn.setAlpha(1f);

                String currentQtyString = qtyTv.getText().toString();
                if (currentQtyString != null && currentQtyString.length() > 0 && !currentQtyString.equals("")) {
                    try {
                        int currentQtyInt = Integer.parseInt(currentQtyString);
                        if (currentQtyInt >= 0) {
                            currentQtyInt = currentQtyInt + 1;
                        } else {
                            currentQtyInt = 1;
                        }
                        qtyTv.setText(currentQtyInt + "");
//                            Double d = Double.valueOf(((AllProduct) mrpProductTv.getTag()).getSalePrice());
//                            Double d2 = Double.valueOf(qtyTv.getText().toString());
//                            Double pri = d * d2;
//                            mrpProductTv.setText(pri + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        minusImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentQtyString = qtyTv.getText().toString();
                addCartBtn.setAlpha(1f);

                if (currentQtyString != null && currentQtyString.length() > 0 && !currentQtyString.equals("")) {
                    try {
                        int currentQtyInt = Integer.parseInt(currentQtyString);
                        if (currentQtyInt == 0 | currentQtyInt == 1) {
                            currentQtyInt = 1;
                        } else {
                            currentQtyInt = currentQtyInt - 1;
                        }

                        qtyTv.setText(currentQtyInt + "");


//                            qtyTv.setText(currentQtyInt + "");
//
//                            Double d = Double.valueOf(((AllProduct) mrpProductTv.getTag()).getSalePrice());
//                            Double d2 = Double.valueOf(qtyTv.getText().toString());
//                            Double pri = d * d2;
//                            mrpProductTv.setText(pri + "");

                    } catch (Exception e) {
                    }
                }

            }
        });

        setEventOnAddOrUpdateCart(addCartBtn, qtyTv, spinner, listUOM, listUomId, position);

        container.addView(layout);
        return layout;

    }


    private void setUmoSpinner(int position) {

    }

    private void setDeal() {

    }

    @Override
    public int getCount() {

        return allProductList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        // notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    private void setEvent() {

    }

    private void setEventOnAddOrUpdateCart(final Button addCartBtn, final EditText qtyTv, final Spinner spinner, final List<String> listUOM, final List<Integer> listUomId, final int position) {

        addCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (allProductList.get(position).getInStock()) {
                   /* if (spinner.getItemAtPosition(spinner.getSelectedItemPosition()).equals("Select")) {
                        Toast.makeText(context, "Please select quantity type", Toast.LENGTH_SHORT).show();
                    } else {*/
                    Log.e("###", ":::productID::" + (addCartBtn.getTag()));
                    product = (AllProduct) addCartBtn.getTag();
                    Log.e("###", ":::product variantID::" + (product.getVariantID()));

                    // product_id
                    linearLayout.setVisibility(View.VISIBLE);
                    productInCart = Select.from(ProductInCart.class).where(Condition.prop("variant_id").eq(((AllProduct) addCartBtn.getTag()).getVariantID())).first();
                    if (productInCart == null) {
                        product = (AllProduct) addCartBtn.getTag();
                        Log.e("$$$", "::::" + product.getProductID());


                        // UOMArray uomArray = Select.from(UOMArray.class).where(Condition.prop("u_omid").eq(uomId)).first();

                        long count = UOMArray.count(UOMArray.class);
                        if (count > 0) {
                            UOMArray.listAll(UOMArray.class);
                            List<UOMArray> userTable = UOMArray.listAll(UOMArray.class);
                            Log.e("$$$", "::::" + userTable.size());
                        }
                        //  if (uomArray.getBaseQty() != null && uomArray.getBaseQty() == 1) {

                        //   } else {
                        sum = 1.00;
                        Double baseqyt = 0.0;
                        String uomBaseName = "Product";
                        int i = 1;
                        while (baseqyt == 0.0) {//baseUOMID
                            UOMArray uomArray1 = Select.from(UOMArray.class).where(Condition.prop("base_uom_name").eq(uomBaseName)).first();
                            baseqyt = 1.00;
                            uomBaseName = uomBaseName;
                            if (i == 1) {

                            } else {
                                if (baseqyt != 0.0) {
                                    sum *= baseqyt;

                                }
                                i = 0;
                            }
                        }


                        String selected_uom = spinner.getSelectedItem().toString();
                        for (int j = 0; j < uomArrays.size(); j++) {

                            if (selected_uom.equals(uomArrays.get(j).getUOMID())) {
                                sum = uomArrays.get(j).getBaseQty();
                                break;
                            }


                        }


                        //qtyTv.setText("1");
                        // }
                        int qty = 1;
                        if (qtyTv.getText() != null && qtyTv.getText().length() > 0 && !qtyTv.getText().equals("")) {
                            try {
                                qty = Integer.parseInt(qtyTv.getText().toString());
                            } catch (Exception e) {
                            }
                        }


                        addToCartImg.setImageResource(R.drawable.bluecart);
                        productInCart = new ProductInCart();
                        productInCart.sGSTPercentage = product.getSGSTPercentage();
                        productInCart.cGSTPercentage = product.getCGSTPercentage();
                        productInCart.productID = product.getProductID();
                        productInCart.productCategoryId = String.valueOf(product.getProductCategoryID());
                        productInCart.brandId = String.valueOf(product.getBrandID());
                        product.getBrandName();

                        if (spinner.getItemAtPosition(spinner.getSelectedItemPosition()).equals("Select")) {
                            productInCart.uomID = 634;
                            productInCart.uomST = "Piece";
                        } else {
                            productInCart.uomID = listUomId.get(spinner.getSelectedItemPosition());
                            productInCart.uomST = listUOM.get(spinner.getSelectedItemPosition());
                        }


                        productInCart.productName = product.getProductName();
                        if (!product.getDefaultPrice().equals(null)) {
                            productInCart.priceOriginal = product.getDefaultPrice() * sum;
                            Log.e("$$$", "::getDefaultPrice::" + product.getDefaultPrice());
                            Log.e("$$$", "::sum::" + sum);

                        } else {
                            productInCart.priceOriginal = 0.0;
                        }

                        if (!product.getSalePrice().equals(null)) {
                            productInCart.price = product.getSalePrice() * sum;
                            Log.e("$$$", "::getDefaultPrice::" + product.getSalePrice());
                            Log.e("$$$", "::sum::" + sum);

                            Log.e("$$$", "::productInCart.price::" + productInCart.price);

//                            mrpProductTv.setText(productInCart.price + "");
                        } else {
                            productInCart.price = 0.0;
                        }
                        productInCart.imageUrl = String.valueOf(product.getImageName());

                        int value = 0;
                        if (productInCart.uomST.equalsIgnoreCase("Pcs")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Dozen")) {
                            value = 12;
                        } else if (productInCart.uomST.equalsIgnoreCase("STD PKG")) {
                            value = 120;
                        } else if (productInCart.uomST.equalsIgnoreCase("Piece")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Special Box")) {
                            value = 20;
                        }

                        productInCart.qty = qty*value;
                        productInCart.CartQty = 1;
                        productInCart.dealCategory = "";
                        productInCart.dealId = "0";
                        productInCart.dealAmount = "0";
                        productInCart.dealType = "";
                        productInCart.variantID = product.getVariantID();
                        productInCart.allowPriceEdit = product.getAllowPriceEdit();

                        productInCart.save();
                        addCartBtn.setText("Update Order");

                        // ProductDetailFragment.continueTxt.setVisibility(View.VISIBLE);

                    } else if ((productInCart != null) && ((productInCart.variantID).equals(product.getVariantID()))) {
                        product = (AllProduct) addCartBtn.getTag();
                        Log.e("###", ":::product cart variantID1::" + productInCart.variantID);

                        String selected_uom = spinner.getSelectedItem().toString();
//                        for(int j = 0 ; j < uomArrays.size(); j ++){
//
//                            if(selected_uom.equals(uomArrays.get(j).getUOMID())) {
//                                sum = uomArrays.get(j).getBaseQty();
//                                break;
//                            }
//
//
//                        }

                        if (spinner.getItemAtPosition(spinner.getSelectedItemPosition()).equals("Select")) {
                            productInCart.uomID = 634;
                            productInCart.uomST = "Piece";
                        } else {
                            productInCart.uomID = listUomId.get(spinner.getSelectedItemPosition());
                            productInCart.uomST = listUOM.get(spinner.getSelectedItemPosition());
                        }

                        //productInCart.uomID = listUomId.get(spinner.getSelectedItemPosition());
                        //productInCart.uomST = listUOM.get(spinner.getSelectedItemPosition());
                        productInCart.variantID = product.getVariantID();
                        productInCart.productName = product.getProductName();
                        productInCart.imageUrl = product.getImageName();
                        if (qtyTv.getText().length() > 0 && qtyTv.getText() != null && !qtyTv.getText().equals("")) {
                            try {
                                int value = 0;
                                if (productInCart.uomST.equalsIgnoreCase("Pcs")) {
                                    value = 1;
                                } else if (productInCart.uomST.equalsIgnoreCase("Dozen")) {
                                    value = 12;
                                } else if (productInCart.uomST.equalsIgnoreCase("STD PKG")) {
                                    value = 120;
                                } else if (productInCart.uomST.equalsIgnoreCase("Piece")) {
                                    value = 1;
                                } else if (productInCart.uomST.equalsIgnoreCase("Special Box")) {
                                    value = 20;
                                }
                                productInCart.qty = Integer.parseInt(qtyTv.getText().toString()) * value;
                            } catch (Exception e) {
                            }
                        }
                        productInCart.save();
                        //Toast.makeText(context, "Order is updated succesfully", Toast.LENGTH_SHORT).show();
                        addCartBtn.setText("Update Order");

                    } else if ((productInCart != null) && (!productInCart.variantID.equals(product.getVariantID()))) {
                        product = (AllProduct) addCartBtn.getTag();
                        Log.e("$$$", "::::" + product.getProductID());
                        Log.e("###", ":::product cart variantID2::" + productInCart.variantID);


                        UOMArray uomArray = Select.from(UOMArray.class).where(Condition.prop("u_omid").eq(product.getDefaultUOMID())).first();


                        if (uomArray.getBaseQty() != null && uomArray.getBaseQty() == 1) {

                        } else {
                            sum = uomArray.getBaseQty();
                            Double baseqyt = 0.0;
                            String uomBaseName = uomArray.getBaseUOMName();
                            int i = 1;
                            while (baseqyt == 0.0) {//baseUOMID
                                UOMArray uomArray1 = Select.from(UOMArray.class).where(Condition.prop("base_uom_name").eq(uomBaseName)).first();
                                baseqyt = uomArray1.getBaseQty();
                                uomBaseName = uomArray1.getBaseUOMName();
                                if (i == 1) {

                                } else {
                                    if (baseqyt != 0.0) {
                                        sum *= baseqyt;

                                    }
                                    i = 0;
                                }
                            }

                            //qtyTv.setText("1");
                        }
                        int qty = 1;
                        if (qtyTv.getText() != null && qtyTv.getText().length() > 0 && !qtyTv.getText().equals("")) {
                            try {
                                qty = Integer.parseInt(qtyTv.getText().toString());
                            } catch (Exception e) {
                            }
                        }


                        addToCartImg.setImageResource(R.drawable.bluecart);
                        productInCart = new ProductInCart();
                        productInCart.sGSTPercentage = product.getSGSTPercentage();
                        productInCart.cGSTPercentage = product.getCGSTPercentage();
                        productInCart.productID = product.getProductID();
                        productInCart.productCategoryId = String.valueOf(product.getProductCategoryID());
                        productInCart.brandId = String.valueOf(product.getBrandID());
                        product.getBrandName();

                        if (spinner.getItemAtPosition(spinner.getSelectedItemPosition()).equals("Select")) {
                            productInCart.uomID = 634;
                            productInCart.uomST = "Piece";
                        } else {
                            productInCart.uomID = listUomId.get(spinner.getSelectedItemPosition());
                            productInCart.uomST = listUOM.get(spinner.getSelectedItemPosition());
                        }
                        //productInCart.uomID = listUomId.get(spinner.getSelectedItemPosition());
                        //productInCart.uomST = listUOM.get(spinner.getSelectedItemPosition());
                        productInCart.productName = product.getProductName();

                        if (!product.getDefaultPrice().equals(null)) {
                            productInCart.priceOriginal = product.getDefaultPrice() * sum;
                            Log.e("$$$", "::getDefaultPrice::" + product.getDefaultPrice());
                            Log.e("$$$", "::sum::" + sum);

                        } else {
                            productInCart.priceOriginal = 0.0;
                        }

                        if (!product.getSalePrice().equals(null)) {
                            productInCart.price = product.getSalePrice() * sum;
                            Log.e("$$$", "::getDefaultPrice::" + product.getSalePrice());
                            Log.e("$$$", "::sum::" + sum);

                            Log.e("$$$", "::productInCart.price::" + productInCart.price);

//                            mrpProductTv.setText(productInCart.price + "");
                        } else {
                            productInCart.price = 0.0;
                        }
                        productInCart.imageUrl = String.valueOf(product.getImageName());

                        int value = 0;
                        if (productInCart.uomST.equalsIgnoreCase("Pcs")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Dozen")) {
                            value = 12;
                        } else if (productInCart.uomST.equalsIgnoreCase("STD PKG")) {
                            value = 120;
                        } else if (productInCart.uomST.equalsIgnoreCase("Piece")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Special Box")) {
                            value = 20;
                        }

                        productInCart.qty = qty * value;
                        productInCart.dealCategory = "";
                        productInCart.dealId = "0";
                        productInCart.dealAmount = "0";
                        productInCart.dealType = "";
                        productInCart.variantID = product.getVariantID();
                        productInCart.allowPriceEdit = product.getAllowPriceEdit();

                        productInCart.save();
                        //Toast.makeText(context, "Order is updated succesfully", Toast.LENGTH_SHORT).show();
                        addCartBtn.setText("Update Order");

                    }

                    //}

                    if (ProductInCart.listAll(ProductInCart.class).size() != 0) {
                        Log.e("cart_size", "::::" + ProductInCart.listAll(ProductInCart.class).size());

                        int sum = 0;
                        for (ProductInCart products : ProductInCart.listAll(ProductInCart.class)) {
                            sum += products.CartQty;
                        }
                        GoalsActivities.totalAmt.setText(sum + "");
                        GoalsActivities.total_items_bracket.setText("(" + sum + "" + ")");
                    }
                    addCartBtn.setAlpha(.5f);
                } else {
                    itemClicked.notAvailableStock();
                }
            }
        });
    }

}


