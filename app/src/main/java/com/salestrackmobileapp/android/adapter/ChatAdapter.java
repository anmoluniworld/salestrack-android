package com.salestrackmobileapp.android.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.AllChats;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private List<AllChats> chatsList;
    private String senderId;

    String TAG="ChatAdapter";
    public ChatAdapter(Context context, String senderId) {
        this.context = context;
        this.chatsList = new ArrayList<>();
        this.senderId = senderId;
    }

    @Override
    public int getItemViewType(int position)
    {
        return chatsList.get(position).getDateLayout();

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view =null;
        RecyclerView.ViewHolder viewHolder = null;

        Log.e(TAG, " onCreateViewHolder viewType "+viewType);
        if(viewType==1)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_date_layout,parent,false);
            viewHolder = new ViewHolderDate(view);
        }
        else
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_chat_layout,parent,false);
            viewHolder= new ViewHolderChat(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        Log.e(TAG, " onBindViewHolder viewType "+holder.getItemViewType());


        if(holder.getItemViewType()== 1)
        {
            ViewHolderDate viewHolderDate = (ViewHolderDate) holder;

            viewHolderDate.dateTextView.setText(formattedDate(chatsList.get(position).getCreatedDate()));
        }
        else
        {
            ViewHolderChat viewHolderChat = (ViewHolderChat) holder;
            if (chatsList.get(position).getSenderId() == Integer.parseInt(senderId)) {
                viewHolderChat.relative_in.setVisibility(View.GONE);
                viewHolderChat.relative_out.setVisibility(View.VISIBLE);
                viewHolderChat.tv_right.setText(chatsList.get(position).getMessage());
                //String dateArray[] = chatsList.get(i).getCreatedDate().split("T");
                viewHolderChat.tv_right_time.setText(chatsList.get(position).getMsgTime());
            } else {
                viewHolderChat.relative_in.setVisibility(View.VISIBLE);
                viewHolderChat.relative_out.setVisibility(View.GONE);
                viewHolderChat.tv_left.setText(chatsList.get(position).getMessage());
                //holder.tv_left_time.setText(chatsList.get(i).getMsgTime());
                //String dateArray[] = chatsList.get(i).getCreatedDate().split("T");
                viewHolderChat.tv_right_time.setText(chatsList.get(position).getMsgTime());
            }
        }

    }

    @Override
    public int getItemCount() {
        return chatsList.size();
    }

    public void setAllChatList(List<AllChats> allChatsList) {
        Log.e(TAG, " setAllChatList allChatsList "+allChatsList.size());
        chatsList.clear();
        chatsList.addAll(allChatsList);
        notifyDataSetChanged();
    }

    public class ViewHolderChat extends RecyclerView.ViewHolder {

        public ViewHolderChat(View itemView)
        {
            super(itemView);
            relative_in = itemView.findViewById(R.id.relative_in);
            relative_out = itemView.findViewById(R.id.relative_out);
            tv_left = itemView.findViewById(R.id.tv_left);
            tv_right = itemView.findViewById(R.id.tv_right);
            tv_left_time = itemView.findViewById(R.id.tv_left_time);
            tv_right_time = itemView.findViewById(R.id.tv_right_time);

        }

        LinearLayout relative_in;
        LinearLayout relative_out;
        TextView tv_left, tv_right, tv_left_time, tv_right_time;
    }

    //****************  VIEW HOLDER 1 ******************//

    public class ViewHolderDate extends RecyclerView.ViewHolder {

        public TextView dateTextView;

        public ViewHolderDate(View itemView) {
            super(itemView);
            dateTextView = (TextView)itemView.findViewById(R.id.textViewDate);
        }
    }

    private String formattedDate(String date) {

        Date inputDate = null;
        SimpleDateFormat output = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.ENGLISH);
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        if (date != null && !TextUtils.isEmpty(date)) {
            try {
                inputDate = input.parse(date); // parse input
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return output.format(inputDate);
        } else {
            return "";
        }

    }

}
