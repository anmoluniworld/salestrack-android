package com.salestrackmobileapp.android.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.custome_views.Custome_BoldTextView;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.fragments.CartFragment;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.utils.PrefsHelper;
import com.salestrackmobileapp.android.utils.RecyclerClick;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanchan on 8/6/2017.
 */

public class OffersDealAdapter extends RecyclerView.Adapter<OffersDealAdapter.ViewHolder> {
    Context context;
    List<AllDeals> allDeals = new ArrayList<AllDeals>();

    Fragment fragment;

    public OffersDealAdapter(Context context, Fragment fragment, List<AllDeals> tempDeals) {
        this.context = context;
        allDeals = tempDeals;
        this.fragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cart_deal_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AllDeals allDealsItem = allDeals.get(position);

        holder.dealTitle.setText(allDealsItem.getDealTitle());
        holder.dealDescriptionTv.setText(allDealsItem.getDealDescription());

        if (allDealsItem.getDealType().equalsIgnoreCase("product") && allDealsItem.getDealApplicableAs().equalsIgnoreCase("Coupon")) {
            holder.dealCoupon.setText(allDealsItem.getCouponCode());
            holder.dealCoupon.setVisibility(View.VISIBLE);
        } else
            holder.dealCoupon.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        Log.e("DEAL_COUNT", ":::" + allDeals.size());

        return allDeals.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView dealTitle, dealDescriptionTv, textViewDealApply, dealCoupon;

        public ViewHolder(View v) {
            super(v);
            dealTitle = (TextView) v.findViewById(R.id.dealTitle);
            dealDescriptionTv = (TextView) v.findViewById(R.id.dealDescriptionTv);
            textViewDealApply = (TextView) v.findViewById(R.id.textViewDealApply);
            dealCoupon = (TextView) v.findViewById(R.id.dealCoupon);


            textViewDealApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1)
                        ((CartFragment) fragment).SelectedDealClick(getAdapterPosition(), "");
                }
            });

        }

    }
}
