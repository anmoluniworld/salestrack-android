package com.salestrackmobileapp.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.utils.RecyclerClick;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoalBusinessNameAdapter extends RecyclerView.Adapter<GoalBusinessNameAdapter.ViewHolder> {

    RecyclerClick rvClick;
    Context context;
    ArrayList<String> list;

    public GoalBusinessNameAdapter(Context context, RecyclerClick rvClick) {
        this.context = context;
        this.rvClick = rvClick;
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public GoalBusinessNameAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_business_goal, parent, false);
        GoalBusinessNameAdapter.ViewHolder vh = new GoalBusinessNameAdapter.ViewHolder(view, rvClick);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull GoalBusinessNameAdapter.ViewHolder viewHolder, int i) {
        viewHolder.businessName.setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListGoals(List<String> noRepeat) {

        list.clear();
        list.addAll(noRepeat);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView, RecyclerClick rvClick) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getAdapterPosition() != -1)
                        rvClick.productClick(v, getAdapterPosition(), false);
                }
            });
        }

        @BindView(R.id.businessName)
        Custome_TextView businessName;
    }
}
