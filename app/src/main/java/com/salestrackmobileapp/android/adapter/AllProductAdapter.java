package com.salestrackmobileapp.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.orm.query.Condition;
import com.orm.query.Select;
import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.activities.GoalsActivities;
import com.salestrackmobileapp.android.custome_views.Custome_TextView;
import com.salestrackmobileapp.android.gson.AllProduct;
import com.salestrackmobileapp.android.my_cart.ProductInCart;
import com.salestrackmobileapp.android.utils.RecyclerNewClick;
import com.salestrackmobileapp.android.utils.RecyclerViewAnimator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanchan on 3/16/2017.
 */

public class AllProductAdapter extends RecyclerView.Adapter<AllProductAdapter.ViewHolder> {

    String TAG = "AllProductAdapter";
    RecyclerNewClick rvClick;
    List<AllProduct> allProductList = new ArrayList<>();
    public static List<AllProduct> filterList;
    RecyclerView recyclerView;
    Context context;
    private RecyclerViewAnimator mAnimator;
    ProductInCart productInCart;

    List<String> listUomArray = new ArrayList<String>();
    List<Integer> listUomIdArray = new ArrayList<Integer>();
    //boolean isAdded;
    SharedPreferences preferences;


    public AllProductAdapter(Context context, RecyclerNewClick rvClick, RecyclerView recyclerView) {
        this.context = context;
        this.rvClick = rvClick;
        this.recyclerView = recyclerView;
        mAnimator = new RecyclerViewAnimator(this.recyclerView);
        preferences = context.getSharedPreferences("MyPref", 0);
        filterList = new ArrayList<>();

    }

    public void setListProduct(List<AllProduct> allProductList, List<String> listUomArray, List<Integer> listUomIdArray) {
        if (allProductList != null)
            Log.e("ProductSearchFragment", " getAllProduct setListProduct " + allProductList.size());


        if (this.allProductList != null)
            this.allProductList.clear();
        else
            this.allProductList = new ArrayList<AllProduct>();

        this.allProductList.addAll(allProductList);

        this.listUomArray = listUomArray;
        this.listUomIdArray = listUomIdArray;

        if (this.filterList != null)
            this.filterList.clear();
        else
            this.filterList = new ArrayList<AllProduct>();

        this.filterList.addAll(this.allProductList);
        notifyDataSetChanged();
    }


    @Override
    public AllProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        final ViewHolder vh = new ViewHolder(v, rvClick);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vh.getAdapterPosition() != -1) {
                    rvClick.productClick(v, vh.getAdapterPosition(), filterList.get(vh.getAdapterPosition()).getInStock(), filterList.get(vh.getAdapterPosition()));
                }
            }
        });
        //   mAnimator.onCreateViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AllProduct product = filterList.get(position);
        Picasso.with(context).load(filterList.get(position).getImageName()).error(R.drawable.calendar_icon).into(holder.productImg);
        holder.productNameTv.setText(filterList.get(position).getProductName() + "");
        holder.mrpTv.setText("₹" + " " + filterList.get(position).getSalePrice());
        holder.mrp_product_tv_original.setText("₹" + " " + filterList.get(position).getDefaultPrice());
        holder.mrp_product_tv_original.setPaintFlags(holder.mrp_product_tv_original.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.categoryTv.setText(filterList.get(position).getDescription() + "");
        Log.e("InStock", ":::" + filterList.get(position).getInStock());
        if (filterList.get(position).getInStock().equals(true)) {
            holder.switch_instock.setChecked(true);
            holder.switch_instock.setClickable(false);
            holder.switch_instock.setText("In stock");
            holder.switch_instock.setTextColor(context.getResources().getColor(R.color.green));

        } else {
            holder.switch_instock.setChecked(false);
            holder.switch_instock.setClickable(false);
            holder.switch_instock.setText("N.A.");
            holder.switch_instock.setTextColor(context.getResources().getColor(R.color.red));
        }

        if (filterList.get(position).getInStock().equals(true)) {
            holder.instock.setText("In Stock");
            holder.instock.setTextColor(context.getResources().getColor(R.color.green));
            holder.addtocart.setText("Add to Cart");
        } else {
            holder.instock.setText("Not Available");
            holder.instock.setTextColor(context.getResources().getColor(R.color.red));
            holder.addtocart.setText("Add to Cart");
        }

//        if (position==0){
//            holder.switch_instock.setChecked(true);
//            holder.switch_instock.setClickable(false);
//        }
//        else if (position==1){
//            holder.switch_instock.setChecked(false);
//            holder.switch_instock.setClickable(false);
//        }
//        else{
//            holder.switch_instock.setChecked(true);
//            holder.switch_instock.setClickable(false);
//        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.single_text_item, listUomArray);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        holder.spinner.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();

        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int proQty = 0;

                if (filterList.get(position).getInStock()) {
                    Log.e(TAG, " getProductID " + product.getProductID());
                    Log.e(TAG, " preferences getProductID " + preferences.getBoolean("" + product.getProductID(), false));

                    productInCart = Select.from(ProductInCart.class).where(Condition.prop("product_id").eq(product.getProductID())).first();

                    Log.e(TAG, " productInCart " + productInCart);

                    if (productInCart == null) {
                        productInCart = new ProductInCart();
                        productInCart.productID = product.getProductID();
                        productInCart.productName = product.getProductName();
                        productInCart.productCategoryId = String.valueOf(product.getProductCategoryID());
                        productInCart.brandId = String.valueOf(product.getBrandID());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("" + product.getProductID(), true);
                        editor.commit();
                        productInCart.cGSTPercentage = product.getCGSTPercentage();
                        productInCart.sGSTPercentage = product.getSGSTPercentage();
                        productInCart.variantID = product.getVariantID();


                        productInCart.uomID = 634;
                        productInCart.uomST = "Piece";
                        productInCart.baseQty = 1.0;

                        if (!product.getDefaultPrice().equals(null)) {
                            productInCart.priceOriginal = product.getDefaultPrice();
                        } else {
                            productInCart.priceOriginal = 0.0;
                        }


                        if (!product.getSalePrice().equals(null)) {
                            productInCart.price = product.getSalePrice();
                        } else {
                            productInCart.price = 0.0;
                        }
                        productInCart.imageUrl = String.valueOf(product.getImageName());
                        int value = 0;
                        if (productInCart.uomST.equalsIgnoreCase("Pcs")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Dozen")) {
                            value = 12;
                        } else if (productInCart.uomST.equalsIgnoreCase("STD PKG")) {
                            value = 120;
                        } else if (productInCart.uomST.equalsIgnoreCase("Piece")) {
                            value = 1;
                        } else if (productInCart.uomST.equalsIgnoreCase("Special Box")) {
                            value = 20;
                        }
                        productInCart.qty = value;
                        productInCart.CartQty = 1;
                        productInCart.dealCategory = "";
                        productInCart.dealId = "0";
                        productInCart.dealAmount = "0";
                        productInCart.dealType = "";
                        Log.e("AllowPriceEdit", "::::" + product.getAllowPriceEdit());
                        productInCart.allowPriceEdit = product.getAllowPriceEdit();
                        productInCart.save();

                        Toast.makeText(context, "Product added in cart", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Already added in cart", Toast.LENGTH_SHORT).show();
                    }


                    Log.e(TAG, ":::productIncartSize:::" + ProductInCart.listAll(ProductInCart.class).size());

                    if (ProductInCart.listAll(ProductInCart.class).size() != 0) {
                        GoalsActivities.totalAmt.setVisibility(View.VISIBLE);
                        int sum = 0;
                        for (ProductInCart products : ProductInCart.listAll(ProductInCart.class)) {
                            sum += products.CartQty;
                        }
                        GoalsActivities.totalAmt.setText(sum + "");
                        GoalsActivities.total_items_bracket.setText("(" + sum + "" + ")");
                    }
                } else {
                    holder.recyclerClick.notAvailableDialog();
                }
            }

        });

        //mAnimator.onBindViewHolder(holder.itemView, position);
        // holder.description.setText(allProductList.get(position).getDefaultPrice() + "");
    }

    @Override
    public int getItemCount() {
        /* return allProductList.size();*/
        return (null != filterList ? filterList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public Custome_TextView productNameTv, mrpTv, categoryTv, mrp_product_tv_original;
        public ImageView productImg;
        Button addtocart;
        RecyclerNewClick recyclerClick;
        Spinner spinner;
        SwitchCompat switch_instock;
        TextView instock;

        public ViewHolder(View v, RecyclerNewClick recyclerClick) {
            super(v);
            productNameTv = (Custome_TextView) v.findViewById(R.id.product_title_tv);
            mrpTv = (Custome_TextView) v.findViewById(R.id.mrp_product_tv);
            categoryTv = (Custome_TextView) v.findViewById(R.id.category_tv);
            productImg = (ImageView) v.findViewById(R.id.product_item_img);
            addtocart = (Button) v.findViewById(R.id.addtocart);
            spinner = (Spinner) v.findViewById(R.id.measurmentSpn);
            switch_instock = v.findViewById(R.id.switch_instock);
            mrp_product_tv_original = v.findViewById(R.id.mrp_product_tv_original);
            instock = v.findViewById(R.id.instock);
            addtocart.setVisibility(View.VISIBLE);
            this.recyclerClick = recyclerClick;
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() != -1)
                recyclerClick.productClick(v, getAdapterPosition(), filterList.get(getAdapterPosition()).getInStock(), filterList.get(getAdapterPosition()));
        }
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {
                    if (filterList != null) {

                        filterList.addAll(allProductList);
                    }

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (AllProduct item : allProductList) {
                        if (item.getProductName().toLowerCase().contains(text.toLowerCase())) {
                            // Adding Matched items
                            filterList.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }
}
