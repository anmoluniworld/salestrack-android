package com.salestrackmobileapp.android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.salestrackmobileapp.android.R;
import com.salestrackmobileapp.android.gson.AllDeals;
import com.salestrackmobileapp.android.utils.RecyclerClick;

import java.util.List;

/**
 * Created by kanchan on 4/12/2017.
 */

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.ViewHolder> {

    //AllDeals
    RecyclerClick rvClick;
    Context context;
    List<AllDeals> allDeals;
    String TAG="DealsAdapter";

    public DealsAdapter(Context context, RecyclerClick rvClick) {
        this.context = context;
        this.rvClick = rvClick;
    }

    public void setListDeal(List<AllDeals> listAllDeals) {
        this.allDeals = listAllDeals;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.deal_item, parent, false);
        DealsAdapter.ViewHolder vh = new DealsAdapter.ViewHolder(view, rvClick);
        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvClick.productClick(v,viewType, false);
            }
        });*/
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AllDeals allDealsItem = allDeals.get(position);
        //Log.e(TAG, " allDeals " + allDeals.toString());

        holder.dealTitle.setText(allDealsItem.getDealTitle());
        String dateSt = allDealsItem.getStartDate();
        String dateArray[] = dateSt.split("T");
        String dateSt2 = allDealsItem.getEndDate();
        String dateArray1[] = dateSt2.split("T");
        holder.start_date.setText("Start Date : " + dateArray[0]);
        holder.end_date.setText("End Date : " + dateArray1[0]);
        holder.dealType.setText("Deal Type: " + allDealsItem.getDealType());
        holder.dealDescriptionTv.setText(allDealsItem.getDealDescription());

        if (allDealsItem.getDealDescription()!=null && !allDealsItem.getDealDescription().isEmpty())
        {
            holder.dealTypeQuantity.setVisibility(View.VISIBLE);

            if(allDealsItem.getDealApplicableAs().equalsIgnoreCase("Quantity"))
            {
                holder.dealTypeQuantity.setText("Deal On "+allDealsItem.getDealApplicableAs());
            }
            else if(allDealsItem.getDealApplicableAs().equalsIgnoreCase("Percentage"))
            {
                holder.dealTypeQuantity.setText(allDealsItem.getPercentage()+"% Off");
            }
            else if(allDealsItem.getDealApplicableAs().equalsIgnoreCase("Amount"))
            {
                holder.dealTypeQuantity.setText("₹ "+allDealsItem.getAmount()+" Off");
            }
            else if(allDealsItem.getDealApplicableAs().equalsIgnoreCase("Coupon"))
            {
                holder.dealTypeQuantity.setText("Coupon of ₹ "+allDealsItem.getCouponAmount());
            }
            else if(allDealsItem.getDealApplicableAs().equalsIgnoreCase("gift"))
            {
                holder.dealTypeQuantity.setText("Gift "+allDealsItem.getGiftName());
            }
        } else {
            holder.dealTypeQuantity.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return allDeals.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public TextView dealTitle, dealTypeQuantity, start_date, end_date, dealType, dealDescriptionTv;
        public TextView proceedImg;
        RecyclerClick recyclerClick;

        public ViewHolder(View v, RecyclerClick recyclerClick) {
            super(v);
            dealTitle = (TextView) v.findViewById(R.id.dealTitle);
            dealTypeQuantity = (TextView) v.findViewById(R.id.dealTypeQuantity);
            start_date = (TextView) v.findViewById(R.id.start_date);
            end_date = (TextView) v.findViewById(R.id.end_date);
            dealType = (TextView) v.findViewById(R.id.dealType);
            dealDescriptionTv = (TextView) v.findViewById(R.id.dealDescriptionTv);

            this.recyclerClick = recyclerClick;

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerClick.productClick(v, getAdapterPosition(), false);

                }
            });
        }
    }
}
