package com.salestrackmobileapp.android.utils;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created by kanchan on 3/15/2017.
 */

public interface RecyclerClickInterface {
    void productClick(View v, int position, RecyclerView recyclerView);
}
