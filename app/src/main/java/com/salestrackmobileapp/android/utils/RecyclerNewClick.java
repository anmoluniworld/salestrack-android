package com.salestrackmobileapp.android.utils;

import android.view.View;

import com.salestrackmobileapp.android.gson.AllProduct;

/**
 * Created by kanchan on 3/15/2017.
 */

public interface RecyclerNewClick
{
    void productClick(View v, int position, Boolean inStock, AllProduct allProduct);
    void notAvailableDialog();
}
