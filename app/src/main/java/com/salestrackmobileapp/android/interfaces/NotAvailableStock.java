package com.salestrackmobileapp.android.interfaces;

public interface NotAvailableStock {
    void notAvailableStock();
}
